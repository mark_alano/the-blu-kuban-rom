.class public Lcom/google/android/youtube/plus1/PlusOneButton;
.super Lcom/google/android/plus1/BasePlusOneButton;
.source "SourceFile"


# instance fields
.field private final a:Landroid/widget/FrameLayout;

.field private final b:Landroid/widget/ProgressBar;

.field private final c:Landroid/widget/TextView;

.field private d:Landroid/app/Activity;

.field private e:Lcom/google/android/youtube/core/utils/l;

.field private f:Lcom/google/android/youtube/plus1/a;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/plus1/PlusOneButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 13
    .parameter
    .parameter

    .prologue
    const/16 v9, 0x10

    const/4 v8, 0x1

    const/4 v7, -0x2

    const/4 v6, 0x0

    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/plus1/BasePlusOneButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    invoke-virtual {p0, v8}, Lcom/google/android/youtube/plus1/PlusOneButton;->setFocusable(Z)V

    .line 50
    invoke-virtual {p0, v6}, Lcom/google/android/youtube/plus1/PlusOneButton;->setOrientation(I)V

    .line 51
    invoke-virtual {p0, v9}, Lcom/google/android/youtube/plus1/PlusOneButton;->setGravity(I)V

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 54
    sget-object v0, Lcom/google/android/youtube/b;->h:[I

    const v2, 0x7f0c0018

    invoke-virtual {p1, p2, v0, v2, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 57
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->a:Landroid/widget/FrameLayout;

    .line 58
    const/4 v0, 0x3

    const v3, 0x7f0a0025

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    .line 61
    iget-object v3, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v6, v0, v6, v6}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 62
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    .line 63
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->a:Landroid/widget/FrameLayout;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v3}, Lcom/google/android/youtube/plus1/PlusOneButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    const/4 v0, 0x4

    const/high16 v3, 0x7f0f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 68
    new-instance v3, Landroid/widget/ProgressBar;

    const/4 v4, 0x0

    if-eqz v0, :cond_d0

    const v0, 0x1010288

    :goto_58
    invoke-direct {v3, p1, v4, v0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v3, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->b:Landroid/widget/ProgressBar;

    .line 70
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setFocusable(Z)V

    .line 71
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 72
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->a:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->b:Landroid/widget/ProgressBar;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v5, 0x11

    invoke-direct {v4, v7, v7, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v3, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 75
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->c:Landroid/widget/TextView;

    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 77
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setGravity(I)V

    .line 78
    const v0, 0x7f0a0027

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {v2, v6, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    .line 80
    iget-object v3, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v6, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 81
    const/4 v0, 0x2

    const v3, 0x7f0d0006

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    .line 83
    const v3, 0x7f090002

    invoke-virtual {v2, v8, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 85
    iget-object v4, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 86
    iget-object v3, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setLines(I)V

    .line 87
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v0, v3, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 89
    const/4 v3, 0x5

    const v4, 0x7f0a0026

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v2, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    .line 91
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 92
    iget-object v1, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/plus1/PlusOneButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 94
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 95
    return-void

    .line 68
    :cond_d0
    const v0, 0x1010079

    goto :goto_58
.end method


# virtual methods
.method protected final a(Ljava/lang/String;)Lcom/google/android/plus1/w;
    .registers 3
    .parameter

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->f:Lcom/google/android/youtube/plus1/a;

    invoke-interface {v0}, Lcom/google/android/youtube/plus1/a;->a()Lcom/google/android/plus1/w;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/plus1/u;)Ljava/lang/CharSequence;
    .registers 3
    .parameter

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/google/android/youtube/plus1/PlusOneButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/youtube/plus1/c;->a(Landroid/content/Context;Lcom/google/android/plus1/u;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected final a()V
    .registers 3

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->a:Landroid/widget/FrameLayout;

    const v1, 0x7f0200f4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 117
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->b:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 118
    return-void
.end method

.method public final a(Landroid/app/Activity;Lcom/google/android/youtube/plus1/a;Lcom/google/android/youtube/core/utils/l;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 145
    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->d:Landroid/app/Activity;

    .line 146
    const-string v0, "networkStatus cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/l;

    iput-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->e:Lcom/google/android/youtube/core/utils/l;

    .line 147
    const-string v0, "plusOneClient cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/plus1/a;

    iput-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->f:Lcom/google/android/youtube/plus1/a;

    .line 148
    iput-object p4, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->g:Ljava/lang/String;

    .line 149
    return-void
.end method

.method protected final a(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    return-void
.end method

.method protected final b()V
    .registers 3

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->a:Landroid/widget/FrameLayout;

    const v1, 0x7f0200f2

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 123
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->b:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 124
    return-void
.end method

.method protected final c()V
    .registers 3

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->a:Landroid/widget/FrameLayout;

    const v1, 0x7f0200f3

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 129
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->b:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 130
    return-void
.end method

.method protected final d()V
    .registers 3

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->a:Landroid/widget/FrameLayout;

    const v1, 0x7f0200f5

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 135
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->b:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 136
    return-void
.end method

.method protected final e()Ljava/lang/String;
    .registers 3

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->e:Lcom/google/android/youtube/core/utils/l;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/l;->a()Z

    move-result v0

    if-eqz v0, :cond_14

    const v0, 0x7f0b00c3

    .line 106
    :goto_b
    invoke-virtual {p0}, Lcom/google/android/youtube/plus1/PlusOneButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 104
    :cond_14
    const v0, 0x7f0b00c2

    goto :goto_b
.end method

.method protected final f()Ljava/lang/String;
    .registers 3

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/google/android/youtube/plus1/PlusOneButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00c4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final g()V
    .registers 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/plus1/PlusOneButton;->b(Ljava/lang/String;)V

    .line 163
    return-void
.end method

.method protected final h()V
    .registers 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/plus1/PlusOneButton;->b(Ljava/lang/String;)V

    .line 168
    return-void
.end method

.method protected final i()V
    .registers 1

    .prologue
    .line 178
    return-void
.end method

.method public setUri(Landroid/net/Uri;)V
    .registers 4
    .parameter

    .prologue
    .line 155
    const-string v0, "uri cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->d:Landroid/app/Activity;

    const-string v1, "setUri cannot be called before init is called"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    iget-object v0, p0, Lcom/google/android/youtube/plus1/PlusOneButton;->d:Landroid/app/Activity;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/youtube/plus1/PlusOneButton;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    .line 158
    return-void
.end method
