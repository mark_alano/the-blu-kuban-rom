.class public final Lcom/google/api/client/googleapis/auth/clientlogin/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public accountType:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/q;
    .end annotation
.end field

.field public applicationName:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/q;
        a = "source"
    .end annotation
.end field

.field public authTokenType:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/q;
        a = "service"
    .end annotation
.end field

.field public captchaAnswer:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/q;
        a = "logincaptcha"
    .end annotation
.end field

.field public captchaToken:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/q;
        a = "logintoken"
    .end annotation
.end field

.field public password:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/q;
        a = "Passwd"
    .end annotation
.end field

.field public username:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/q;
        a = "Email"
    .end annotation
.end field
