.class public Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
.super Lcom/google/api/client/http/HttpResponseException;
.source "SourceFile"


# static fields
.field private static final serialVersionUID:J = 0x5aff10c793dbb70L


# instance fields
.field private final transient details:Lcom/google/api/client/googleapis/json/a;

.field private final transient jsonFactory:Lcom/google/api/client/json/d;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/api/client/json/d;Lcom/google/api/client/http/r;Lcom/google/api/client/googleapis/json/a;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 74
    invoke-direct {p0, p2, p4}, Lcom/google/api/client/http/HttpResponseException;-><init>(Lcom/google/api/client/http/r;Ljava/lang/String;)V

    .line 75
    iput-object p1, p0, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->jsonFactory:Lcom/google/api/client/json/d;

    .line 76
    iput-object p3, p0, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->details:Lcom/google/api/client/googleapis/json/a;

    .line 77
    return-void
.end method

.method public static execute(Lcom/google/api/client/json/d;Lcom/google/api/client/http/n;)Lcom/google/api/client/http/r;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 193
    invoke-static {p0}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    invoke-virtual {p1}, Lcom/google/api/client/http/n;->j()Z

    move-result v0

    .line 195
    if-eqz v0, :cond_d

    .line 196
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/api/client/http/n;->b(Z)Lcom/google/api/client/http/n;

    .line 198
    :cond_d
    invoke-virtual {p1}, Lcom/google/api/client/http/n;->k()Lcom/google/api/client/http/r;

    move-result-object v1

    .line 199
    invoke-virtual {p1, v0}, Lcom/google/api/client/http/n;->b(Z)Lcom/google/api/client/http/n;

    .line 200
    if-eqz v0, :cond_1c

    invoke-virtual {v1}, Lcom/google/api/client/http/r;->c()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 201
    :cond_1c
    return-object v1

    .line 203
    :cond_1d
    invoke-static {p0, v1}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->from(Lcom/google/api/client/json/d;Lcom/google/api/client/http/r;)Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;

    move-result-object v0

    throw v0
.end method

.method public static from(Lcom/google/api/client/json/d;Lcom/google/api/client/http/r;)Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-static {p0}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    :try_start_4
    invoke-virtual {p1}, Lcom/google/api/client/http/r;->c()Z

    move-result v0

    if-nez v0, :cond_ac

    sget-object v0, Lcom/google/api/client/json/c;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/api/client/http/r;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/api/client/http/l;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ac

    invoke-virtual {p1}, Lcom/google/api/client/http/r;->f()Ljava/io/InputStream;
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_19} :catch_b1

    move-result-object v0

    if-eqz v0, :cond_ac

    .line 121
    :try_start_1c
    invoke-virtual {p1}, Lcom/google/api/client/http/r;->f()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/api/client/json/d;->a(Ljava/io/InputStream;)Lcom/google/api/client/json/g;
    :try_end_23
    .catchall {:try_start_1c .. :try_end_23} :catchall_97
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_23} :catch_7e

    move-result-object v3

    .line 122
    :try_start_24
    invoke-virtual {v3}, Lcom/google/api/client/json/g;->d()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    .line 124
    if-nez v0, :cond_2e

    .line 125
    invoke-virtual {v3}, Lcom/google/api/client/json/g;->c()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    .line 128
    :cond_2e
    if-eqz v0, :cond_cc

    .line 130
    const-string v0, "error"

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/api/client/json/g;->a(Ljava/util/Set;)Ljava/lang/String;

    .line 131
    invoke-virtual {v3}, Lcom/google/api/client/json/g;->d()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    sget-object v2, Lcom/google/api/client/json/JsonToken;->END_OBJECT:Lcom/google/api/client/json/JsonToken;

    if-eq v0, v2, :cond_cc

    .line 132
    const-class v0, Lcom/google/api/client/googleapis/json/a;

    const/4 v2, 0x0

    invoke-virtual {v3, v0, v2}, Lcom/google/api/client/json/g;->a(Ljava/lang/Class;Lcom/google/api/client/json/a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/googleapis/json/a;
    :try_end_4a
    .catchall {:try_start_24 .. :try_end_4a} :catchall_ba
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_4a} :catch_c2

    .line 133
    :try_start_4a
    invoke-virtual {v0}, Lcom/google/api/client/googleapis/json/a;->toPrettyString()Ljava/lang/String;
    :try_end_4d
    .catchall {:try_start_4a .. :try_end_4d} :catchall_bd
    .catch Ljava/io/IOException; {:try_start_4a .. :try_end_4d} :catch_c6

    move-result-object v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 140
    :goto_51
    if-nez v3, :cond_73

    .line 141
    :try_start_53
    invoke-virtual {p1}, Lcom/google/api/client/http/r;->g()V
    :try_end_56
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_56} :catch_79

    .line 154
    :cond_56
    :goto_56
    invoke-static {p1}, Lcom/google/api/client/http/HttpResponseException;->computeMessageBuffer(Lcom/google/api/client/http/r;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 155
    invoke-static {v0}, Lcom/google/common/base/al;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_69

    .line 156
    sget-object v3, Lcom/google/api/client/util/w;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    :cond_69
    new-instance v0, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;-><init>(Lcom/google/api/client/json/d;Lcom/google/api/client/http/r;Lcom/google/api/client/googleapis/json/a;Ljava/lang/String;)V

    return-object v0

    .line 142
    :cond_73
    if-nez v1, :cond_56

    .line 143
    :try_start_75
    invoke-virtual {v3}, Lcom/google/api/client/json/g;->b()V
    :try_end_78
    .catch Ljava/io/IOException; {:try_start_75 .. :try_end_78} :catch_79

    goto :goto_56

    .line 149
    :catch_79
    move-exception v2

    .line 151
    :goto_7a
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_56

    .line 136
    :catch_7e
    move-exception v0

    move-object v2, v0

    move-object v3, v1

    move-object v0, v1

    .line 138
    :goto_82
    :try_start_82
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_85
    .catchall {:try_start_82 .. :try_end_85} :catchall_bd

    .line 140
    if-nez v3, :cond_8e

    .line 141
    :try_start_87
    invoke-virtual {p1}, Lcom/google/api/client/http/r;->g()V

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_56

    .line 142
    :cond_8e
    if-nez v0, :cond_c8

    .line 143
    invoke-virtual {v3}, Lcom/google/api/client/json/g;->b()V
    :try_end_93
    .catch Ljava/io/IOException; {:try_start_87 .. :try_end_93} :catch_b5

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_56

    .line 140
    :catchall_97
    move-exception v0

    move-object v3, v1

    move-object v2, v1

    :goto_9a
    if-nez v3, :cond_a6

    .line 141
    :try_start_9c
    invoke-virtual {p1}, Lcom/google/api/client/http/r;->g()V

    .line 143
    :cond_9f
    :goto_9f
    throw v0

    .line 149
    :catch_a0
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v2

    move-object v2, v4

    goto :goto_7a

    .line 142
    :cond_a6
    if-nez v2, :cond_9f

    .line 143
    invoke-virtual {v3}, Lcom/google/api/client/json/g;->b()V
    :try_end_ab
    .catch Ljava/io/IOException; {:try_start_9c .. :try_end_ab} :catch_a0

    goto :goto_9f

    .line 147
    :cond_ac
    :try_start_ac
    invoke-virtual {p1}, Lcom/google/api/client/http/r;->i()Ljava/lang/String;
    :try_end_af
    .catch Ljava/io/IOException; {:try_start_ac .. :try_end_af} :catch_b1

    move-result-object v0

    goto :goto_56

    .line 149
    :catch_b1
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    goto :goto_7a

    :catch_b5
    move-exception v2

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_7a

    .line 140
    :catchall_ba
    move-exception v0

    move-object v2, v1

    goto :goto_9a

    :catchall_bd
    move-exception v2

    move-object v4, v2

    move-object v2, v0

    move-object v0, v4

    goto :goto_9a

    .line 136
    :catch_c2
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    goto :goto_82

    :catch_c6
    move-exception v2

    goto :goto_82

    :cond_c8
    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_56

    :cond_cc
    move-object v0, v1

    goto :goto_51
.end method


# virtual methods
.method public final getDetails()Lcom/google/api/client/googleapis/json/a;
    .registers 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->details:Lcom/google/api/client/googleapis/json/a;

    return-object v0
.end method

.method public final getJsonFactory()Lcom/google/api/client/json/d;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->jsonFactory:Lcom/google/api/client/json/d;

    return-object v0
.end method
