.class public final Lcom/google/api/client/http/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/api/client/http/i;

.field private b:Lcom/google/api/client/http/j;

.field private c:Lcom/google/api/client/http/j;

.field private d:Z

.field private e:I

.field private f:I

.field private g:Z

.field private h:Lcom/google/api/client/http/h;

.field private final i:Lcom/google/api/client/http/t;

.field private j:Lcom/google/api/client/http/HttpMethod;

.field private k:Lcom/google/api/client/http/g;

.field private l:I

.field private m:I

.field private n:Lcom/google/api/client/http/u;

.field private final o:Ljava/util/Map;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private p:Lcom/google/api/client/util/v;

.field private q:Z

.field private r:Lcom/google/api/client/http/c;

.field private s:Z

.field private t:Z

.field private u:Z


# direct methods
.method constructor <init>(Lcom/google/api/client/http/t;Lcom/google/api/client/http/HttpMethod;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/16 v2, 0x4e20

    const/4 v1, 0x1

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Lcom/google/api/client/http/j;

    invoke-direct {v0}, Lcom/google/api/client/http/j;-><init>()V

    iput-object v0, p0, Lcom/google/api/client/http/n;->b:Lcom/google/api/client/http/j;

    .line 84
    new-instance v0, Lcom/google/api/client/http/j;

    invoke-direct {v0}, Lcom/google/api/client/http/j;-><init>()V

    iput-object v0, p0, Lcom/google/api/client/http/n;->c:Lcom/google/api/client/http/j;

    .line 91
    iput-boolean v1, p0, Lcom/google/api/client/http/n;->d:Z

    .line 98
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/api/client/http/n;->e:I

    .line 125
    const/16 v0, 0x4000

    iput v0, p0, Lcom/google/api/client/http/n;->f:I

    .line 128
    iput-boolean v1, p0, Lcom/google/api/client/http/n;->g:Z

    .line 143
    iput v2, p0, Lcom/google/api/client/http/n;->l:I

    .line 149
    iput v2, p0, Lcom/google/api/client/http/n;->m:I

    .line 155
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/api/client/http/n;->o:Ljava/util/Map;

    .line 168
    iput-boolean v1, p0, Lcom/google/api/client/http/n;->s:Z

    .line 174
    iput-boolean v1, p0, Lcom/google/api/client/http/n;->t:Z

    .line 180
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/api/client/http/n;->u:Z

    .line 187
    iput-object p1, p0, Lcom/google/api/client/http/n;->i:Lcom/google/api/client/http/t;

    .line 188
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/api/client/http/n;->j:Lcom/google/api/client/http/HttpMethod;

    .line 189
    return-void
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1045
    if-nez p0, :cond_4

    .line 1046
    const/4 p0, 0x0

    .line 1049
    :cond_3
    :goto_3
    return-object p0

    .line 1048
    :cond_4
    const/16 v0, 0x3b

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1049
    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/api/client/http/m;
    .registers 4
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 655
    invoke-static {p1}, Lcom/google/api/client/http/n;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 656
    iget-object v1, p0, Lcom/google/api/client/http/n;->o:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/http/m;

    return-object v0
.end method

.method public final a(Lcom/google/api/client/http/HttpMethod;)Lcom/google/api/client/http/n;
    .registers 3
    .parameter

    .prologue
    .line 215
    invoke-static {p1}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/http/HttpMethod;

    iput-object v0, p0, Lcom/google/api/client/http/n;->j:Lcom/google/api/client/http/HttpMethod;

    .line 216
    return-object p0
.end method

.method public final a(Lcom/google/api/client/http/g;)Lcom/google/api/client/http/n;
    .registers 3
    .parameter

    .prologue
    .line 234
    invoke-static {p1}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/http/g;

    iput-object v0, p0, Lcom/google/api/client/http/n;->k:Lcom/google/api/client/http/g;

    .line 235
    return-object p0
.end method

.method public final a(Lcom/google/api/client/http/h;)Lcom/google/api/client/http/n;
    .registers 2
    .parameter

    .prologue
    .line 253
    iput-object p1, p0, Lcom/google/api/client/http/n;->h:Lcom/google/api/client/http/h;

    .line 254
    return-object p0
.end method

.method public final a(Lcom/google/api/client/http/i;)Lcom/google/api/client/http/n;
    .registers 2
    .parameter

    .prologue
    .line 535
    iput-object p1, p0, Lcom/google/api/client/http/n;->a:Lcom/google/api/client/http/i;

    .line 536
    return-object p0
.end method

.method public final a(Lcom/google/api/client/http/u;)Lcom/google/api/client/http/n;
    .registers 2
    .parameter

    .prologue
    .line 555
    iput-object p1, p0, Lcom/google/api/client/http/n;->n:Lcom/google/api/client/http/u;

    .line 556
    return-object p0
.end method

.method public final a(Z)Lcom/google/api/client/http/n;
    .registers 3
    .parameter

    .prologue
    .line 567
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/api/client/http/n;->d:Z

    .line 568
    return-object p0
.end method

.method public final a()Lcom/google/api/client/http/t;
    .registers 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/api/client/http/n;->i:Lcom/google/api/client/http/t;

    return-object v0
.end method

.method public final a(Lcom/google/api/client/http/m;)V
    .registers 4
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 626
    invoke-interface {p1}, Lcom/google/api/client/http/m;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/api/client/http/n;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 627
    iget-object v1, p0, Lcom/google/api/client/http/n;->o:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 628
    return-void
.end method

.method public final b()Lcom/google/api/client/http/HttpMethod;
    .registers 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/api/client/http/n;->j:Lcom/google/api/client/http/HttpMethod;

    return-object v0
.end method

.method public final b(Z)Lcom/google/api/client/http/n;
    .registers 2
    .parameter

    .prologue
    .line 712
    iput-boolean p1, p0, Lcom/google/api/client/http/n;->t:Z

    .line 713
    return-object p0
.end method

.method public final c()Lcom/google/api/client/http/g;
    .registers 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/api/client/http/n;->k:Lcom/google/api/client/http/g;

    return-object v0
.end method

.method public final d()Lcom/google/api/client/http/h;
    .registers 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/api/client/http/n;->h:Lcom/google/api/client/http/h;

    return-object v0
.end method

.method public final e()I
    .registers 2

    .prologue
    .line 342
    iget v0, p0, Lcom/google/api/client/http/n;->f:I

    return v0
.end method

.method public final f()Z
    .registers 2

    .prologue
    .line 389
    iget-boolean v0, p0, Lcom/google/api/client/http/n;->g:Z

    return v0
.end method

.method public final g()Lcom/google/api/client/http/j;
    .registers 2

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/api/client/http/n;->b:Lcom/google/api/client/http/j;

    return-object v0
.end method

.method public final h()Lcom/google/api/client/http/j;
    .registers 2

    .prologue
    .line 487
    iget-object v0, p0, Lcom/google/api/client/http/n;->c:Lcom/google/api/client/http/j;

    return-object v0
.end method

.method public final i()Lcom/google/api/client/util/v;
    .registers 2

    .prologue
    .line 665
    iget-object v0, p0, Lcom/google/api/client/http/n;->p:Lcom/google/api/client/util/v;

    return-object v0
.end method

.method public final j()Z
    .registers 2

    .prologue
    .line 698
    iget-boolean v0, p0, Lcom/google/api/client/http/n;->t:Z

    return v0
.end method

.method public final k()Lcom/google/api/client/http/r;
    .registers 15

    .prologue
    .line 773
    iget v0, p0, Lcom/google/api/client/http/n;->e:I

    if-ltz v0, :cond_1c6

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lcom/google/common/base/ag;->a(Z)V

    .line 774
    iget v1, p0, Lcom/google/api/client/http/n;->e:I

    .line 775
    iget-object v0, p0, Lcom/google/api/client/http/n;->r:Lcom/google/api/client/http/c;

    if-eqz v0, :cond_10

    .line 777
    iget-object v0, p0, Lcom/google/api/client/http/n;->r:Lcom/google/api/client/http/c;

    .line 779
    :cond_10
    const/4 v0, 0x0

    .line 782
    iget-object v2, p0, Lcom/google/api/client/http/n;->j:Lcom/google/api/client/http/HttpMethod;

    invoke-static {v2}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 783
    iget-object v2, p0, Lcom/google/api/client/http/n;->k:Lcom/google/api/client/http/g;

    invoke-static {v2}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move v9, v1

    .line 787
    :goto_1c
    if-eqz v0, :cond_21

    .line 788
    invoke-virtual {v0}, Lcom/google/api/client/http/r;->g()V

    .line 791
    :cond_21
    const/4 v11, 0x0

    .line 792
    const/4 v10, 0x0

    .line 795
    iget-object v0, p0, Lcom/google/api/client/http/n;->a:Lcom/google/api/client/http/i;

    if-eqz v0, :cond_2c

    .line 796
    iget-object v0, p0, Lcom/google/api/client/http/n;->a:Lcom/google/api/client/http/i;

    invoke-interface {v0, p0}, Lcom/google/api/client/http/i;->b(Lcom/google/api/client/http/n;)V

    .line 799
    :cond_2c
    iget-object v0, p0, Lcom/google/api/client/http/n;->k:Lcom/google/api/client/http/g;

    invoke-virtual {v0}, Lcom/google/api/client/http/g;->a()Ljava/lang/String;

    move-result-object v1

    .line 801
    sget-object v0, Lcom/google/api/client/http/o;->a:[I

    iget-object v2, p0, Lcom/google/api/client/http/n;->j:Lcom/google/api/client/http/HttpMethod;

    invoke-virtual {v2}, Lcom/google/api/client/http/HttpMethod;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_2b6

    .line 806
    iget-object v0, p0, Lcom/google/api/client/http/n;->i:Lcom/google/api/client/http/t;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/t;->b(Ljava/lang/String;)Lcom/google/api/client/http/w;

    move-result-object v0

    move-object v7, v0

    .line 825
    :goto_46
    sget-object v13, Lcom/google/api/client/http/t;->a:Ljava/util/logging/Logger;

    .line 826
    iget-boolean v0, p0, Lcom/google/api/client/http/n;->g:Z

    if-eqz v0, :cond_20c

    sget-object v0, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    invoke-virtual {v13, v0}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_20c

    const/4 v0, 0x1

    move v12, v0

    .line 827
    :goto_56
    const/4 v0, 0x0

    .line 829
    if-eqz v12, :cond_2b3

    .line 830
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 831
    const-string v2, "-------------- REQUEST  --------------"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/api/client/util/w;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 832
    iget-object v2, p0, Lcom/google/api/client/http/n;->j:Lcom/google/api/client/http/HttpMethod;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/api/client/util/w;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v8, v0

    .line 835
    :goto_7f
    iget-object v0, p0, Lcom/google/api/client/http/n;->b:Lcom/google/api/client/http/j;

    invoke-virtual {v0}, Lcom/google/api/client/http/j;->d()Ljava/lang/String;

    move-result-object v0

    .line 836
    if-nez v0, :cond_210

    .line 837
    iget-object v1, p0, Lcom/google/api/client/http/n;->b:Lcom/google/api/client/http/j;

    const-string v2, "Google-HTTP-Java-Client/1.10.2-beta (gzip)"

    invoke-virtual {v1, v2}, Lcom/google/api/client/http/j;->b(Ljava/lang/String;)V

    .line 842
    :goto_8e
    iget-object v1, p0, Lcom/google/api/client/http/n;->b:Lcom/google/api/client/http/j;

    invoke-static {v1, v8, v13, v7}, Lcom/google/api/client/http/j;->a(Lcom/google/api/client/http/j;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Lcom/google/api/client/http/w;)V

    .line 844
    iget-object v1, p0, Lcom/google/api/client/http/n;->b:Lcom/google/api/client/http/j;

    invoke-virtual {v1, v0}, Lcom/google/api/client/http/j;->b(Ljava/lang/String;)V

    .line 847
    iget-object v1, p0, Lcom/google/api/client/http/n;->h:Lcom/google/api/client/http/h;

    .line 848
    iget-boolean v0, p0, Lcom/google/api/client/http/n;->d:Z

    if-nez v0, :cond_c8

    iget-object v0, p0, Lcom/google/api/client/http/n;->j:Lcom/google/api/client/http/HttpMethod;

    sget-object v2, Lcom/google/api/client/http/HttpMethod;->PUT:Lcom/google/api/client/http/HttpMethod;

    if-eq v0, v2, :cond_b0

    iget-object v0, p0, Lcom/google/api/client/http/n;->j:Lcom/google/api/client/http/HttpMethod;

    sget-object v2, Lcom/google/api/client/http/HttpMethod;->POST:Lcom/google/api/client/http/HttpMethod;

    if-eq v0, v2, :cond_b0

    iget-object v0, p0, Lcom/google/api/client/http/n;->j:Lcom/google/api/client/http/HttpMethod;

    sget-object v2, Lcom/google/api/client/http/HttpMethod;->PATCH:Lcom/google/api/client/http/HttpMethod;

    if-ne v0, v2, :cond_c8

    :cond_b0
    if-eqz v1, :cond_bc

    invoke-interface {v1}, Lcom/google/api/client/http/h;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_c8

    .line 851
    :cond_bc
    const-string v0, " "

    new-instance v1, Lcom/google/api/client/http/d;

    const/4 v2, 0x0

    invoke-static {v0}, Lcom/google/api/client/util/w;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/api/client/http/d;-><init>(Ljava/lang/String;[B)V

    .line 853
    :cond_c8
    if-eqz v1, :cond_147

    .line 854
    invoke-interface {v1}, Lcom/google/api/client/http/h;->a()Ljava/lang/String;

    move-result-object v3

    .line 855
    invoke-interface {v1}, Lcom/google/api/client/http/h;->b()J

    move-result-wide v4

    .line 856
    invoke-interface {v1}, Lcom/google/api/client/http/h;->d()Ljava/lang/String;

    move-result-object v2

    .line 858
    if-eqz v12, :cond_2b0

    .line 859
    new-instance v0, Lcom/google/api/client/http/v;

    iget v6, p0, Lcom/google/api/client/http/n;->f:I

    invoke-direct/range {v0 .. v6}, Lcom/google/api/client/http/v;-><init>(Lcom/google/api/client/http/h;Ljava/lang/String;Ljava/lang/String;JI)V

    .line 863
    :goto_df
    iget-boolean v1, p0, Lcom/google/api/client/http/n;->q:Z

    if-eqz v1, :cond_2ad

    .line 864
    new-instance v1, Lcom/google/api/client/http/f;

    invoke-direct {v1, v0, v2}, Lcom/google/api/client/http/f;-><init>(Lcom/google/api/client/http/h;Ljava/lang/String;)V

    .line 865
    invoke-interface {v1}, Lcom/google/api/client/http/h;->a()Ljava/lang/String;

    move-result-object v3

    .line 866
    invoke-interface {v1}, Lcom/google/api/client/http/h;->b()J

    move-result-wide v4

    .line 869
    :goto_f0
    if-eqz v12, :cond_144

    .line 870
    if-eqz v2, :cond_10c

    .line 871
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "Content-Type: "

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/google/api/client/util/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 873
    :cond_10c
    if-eqz v3, :cond_126

    .line 874
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Content-Encoding: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/google/api/client/util/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 877
    :cond_126
    const-wide/16 v2, 0x0

    cmp-long v0, v4, v2

    if-ltz v0, :cond_144

    .line 878
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Content-Length: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/google/api/client/util/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 881
    :cond_144
    invoke-virtual {v7, v1}, Lcom/google/api/client/http/w;->a(Lcom/google/api/client/http/h;)V

    .line 884
    :cond_147
    if-eqz v12, :cond_150

    .line 885
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Ljava/util/logging/Logger;->config(Ljava/lang/String;)V

    .line 890
    :cond_150
    if-lez v9, :cond_22a

    if-eqz v1, :cond_15a

    invoke-interface {v1}, Lcom/google/api/client/http/h;->e()Z

    move-result v0

    if-eqz v0, :cond_22a

    :cond_15a
    const/4 v0, 0x1

    .line 893
    :goto_15b
    iget v1, p0, Lcom/google/api/client/http/n;->l:I

    iget v2, p0, Lcom/google/api/client/http/n;->m:I

    invoke-virtual {v7, v1, v2}, Lcom/google/api/client/http/w;->a(II)V

    .line 895
    :try_start_162
    invoke-virtual {v7}, Lcom/google/api/client/http/w;->a()Lcom/google/api/client/http/x;
    :try_end_165
    .catch Ljava/io/IOException; {:try_start_162 .. :try_end_165} :catch_236

    move-result-object v3

    .line 897
    :try_start_166
    new-instance v2, Lcom/google/api/client/http/r;

    invoke-direct {v2, p0, v3}, Lcom/google/api/client/http/r;-><init>(Lcom/google/api/client/http/n;Lcom/google/api/client/http/x;)V
    :try_end_16b
    .catchall {:try_start_166 .. :try_end_16b} :catchall_22d

    move-object v6, v10

    .line 917
    :goto_16c
    if-eqz v2, :cond_27f

    :try_start_16e
    invoke-virtual {v2}, Lcom/google/api/client/http/r;->c()Z

    move-result v1

    if-nez v1, :cond_27f

    .line 920
    const/4 v4, 0x0

    .line 921
    const/4 v3, 0x0

    .line 922
    const/4 v1, 0x0

    .line 923
    iget-object v5, p0, Lcom/google/api/client/http/n;->n:Lcom/google/api/client/http/u;

    if-eqz v5, :cond_2aa

    .line 927
    iget-object v4, p0, Lcom/google/api/client/http/n;->n:Lcom/google/api/client/http/u;

    invoke-interface {v4, p0, v2}, Lcom/google/api/client/http/u;->a(Lcom/google/api/client/http/n;Lcom/google/api/client/http/r;)Z

    move-result v4

    move v5, v4

    .line 930
    :goto_182
    if-nez v5, :cond_1b0

    .line 931
    iget-boolean v4, p0, Lcom/google/api/client/http/n;->s:Z

    if-eqz v4, :cond_259

    invoke-virtual {v2}, Lcom/google/api/client/http/r;->d()I

    move-result v4

    packed-switch v4, :pswitch_data_2c4

    :pswitch_18f
    const/4 v4, 0x0

    :goto_190
    if-eqz v4, :cond_259

    .line 933
    invoke-virtual {v2}, Lcom/google/api/client/http/r;->b()Lcom/google/api/client/http/j;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/api/client/http/j;->c()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/api/client/http/g;

    invoke-direct {v4, v3}, Lcom/google/api/client/http/g;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/google/api/client/http/n;->a(Lcom/google/api/client/http/g;)Lcom/google/api/client/http/n;

    invoke-virtual {v2}, Lcom/google/api/client/http/r;->d()I

    move-result v3

    const/16 v4, 0x12f

    if-ne v3, v4, :cond_1af

    sget-object v3, Lcom/google/api/client/http/HttpMethod;->GET:Lcom/google/api/client/http/HttpMethod;

    invoke-virtual {p0, v3}, Lcom/google/api/client/http/n;->a(Lcom/google/api/client/http/HttpMethod;)Lcom/google/api/client/http/n;

    .line 934
    :cond_1af
    const/4 v3, 0x1

    .line 950
    :cond_1b0
    :goto_1b0
    if-nez v5, :cond_1b6

    if-nez v3, :cond_1b6

    if-eqz v1, :cond_27c

    :cond_1b6
    const/4 v1, 0x1

    :goto_1b7
    and-int/2addr v0, v1

    .line 952
    if-eqz v0, :cond_1bd

    .line 953
    invoke-virtual {v2}, Lcom/google/api/client/http/r;->g()V
    :try_end_1bd
    .catchall {:try_start_16e .. :try_end_1bd} :catchall_287

    .line 961
    :cond_1bd
    :goto_1bd
    add-int/lit8 v1, v9, -0x1

    .line 963
    if-eqz v2, :cond_1c1

    .line 969
    :cond_1c1
    if-nez v0, :cond_2a6

    .line 971
    if-nez v2, :cond_28e

    .line 973
    throw v6

    .line 773
    :cond_1c6
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 803
    :pswitch_1c9
    iget-object v0, p0, Lcom/google/api/client/http/n;->i:Lcom/google/api/client/http/t;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/t;->a(Ljava/lang/String;)Lcom/google/api/client/http/w;

    move-result-object v0

    move-object v7, v0

    .line 804
    goto/16 :goto_46

    .line 809
    :pswitch_1d2
    iget-object v0, p0, Lcom/google/api/client/http/n;->i:Lcom/google/api/client/http/t;

    invoke-virtual {v0}, Lcom/google/api/client/http/t;->c()Z

    move-result v0

    const-string v2, "HTTP transport doesn\'t support HEAD"

    invoke-static {v0, v2}, Lcom/google/common/base/ag;->a(ZLjava/lang/Object;)V

    .line 811
    iget-object v0, p0, Lcom/google/api/client/http/n;->i:Lcom/google/api/client/http/t;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/t;->c(Ljava/lang/String;)Lcom/google/api/client/http/w;

    move-result-object v0

    move-object v7, v0

    .line 812
    goto/16 :goto_46

    .line 814
    :pswitch_1e6
    iget-object v0, p0, Lcom/google/api/client/http/n;->i:Lcom/google/api/client/http/t;

    invoke-virtual {v0}, Lcom/google/api/client/http/t;->d()Z

    move-result v0

    const-string v2, "HTTP transport doesn\'t support PATCH"

    invoke-static {v0, v2}, Lcom/google/common/base/ag;->a(ZLjava/lang/Object;)V

    .line 816
    iget-object v0, p0, Lcom/google/api/client/http/n;->i:Lcom/google/api/client/http/t;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/t;->d(Ljava/lang/String;)Lcom/google/api/client/http/w;

    move-result-object v0

    move-object v7, v0

    .line 817
    goto/16 :goto_46

    .line 819
    :pswitch_1fa
    iget-object v0, p0, Lcom/google/api/client/http/n;->i:Lcom/google/api/client/http/t;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/t;->e(Ljava/lang/String;)Lcom/google/api/client/http/w;

    move-result-object v0

    move-object v7, v0

    .line 820
    goto/16 :goto_46

    .line 822
    :pswitch_203
    iget-object v0, p0, Lcom/google/api/client/http/n;->i:Lcom/google/api/client/http/t;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/t;->f(Ljava/lang/String;)Lcom/google/api/client/http/w;

    move-result-object v0

    move-object v7, v0

    goto/16 :goto_46

    .line 826
    :cond_20c
    const/4 v0, 0x0

    move v12, v0

    goto/16 :goto_56

    .line 839
    :cond_210
    iget-object v1, p0, Lcom/google/api/client/http/n;->b:Lcom/google/api/client/http/j;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Google-HTTP-Java-Client/1.10.2-beta (gzip)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/api/client/http/j;->b(Ljava/lang/String;)V

    goto/16 :goto_8e

    .line 890
    :cond_22a
    const/4 v0, 0x0

    goto/16 :goto_15b

    .line 903
    :catchall_22d
    move-exception v1

    :try_start_22e
    invoke-virtual {v3}, Lcom/google/api/client/http/x;->a()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v1
    :try_end_236
    .catch Ljava/io/IOException; {:try_start_22e .. :try_end_236} :catch_236

    .line 913
    :catch_236
    move-exception v1

    .line 907
    iget-boolean v2, p0, Lcom/google/api/client/http/n;->u:Z

    if-nez v2, :cond_23c

    .line 908
    throw v1

    .line 912
    :cond_23c
    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13, v2, v3, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v6, v1

    move-object v2, v11

    goto/16 :goto_16c

    .line 931
    :pswitch_249
    :try_start_249
    invoke-virtual {v2}, Lcom/google/api/client/http/r;->b()Lcom/google/api/client/http/j;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/client/http/j;->c()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_256

    const/4 v4, 0x1

    goto/16 :goto_190

    :cond_256
    const/4 v4, 0x0

    goto/16 :goto_190

    .line 935
    :cond_259
    if-eqz v0, :cond_1b0

    iget-object v4, p0, Lcom/google/api/client/http/n;->r:Lcom/google/api/client/http/c;

    if-eqz v4, :cond_1b0

    iget-object v4, p0, Lcom/google/api/client/http/n;->r:Lcom/google/api/client/http/c;

    invoke-virtual {v2}, Lcom/google/api/client/http/r;->d()I

    invoke-interface {v4}, Lcom/google/api/client/http/c;->a()Z

    move-result v4

    if-eqz v4, :cond_1b0

    .line 940
    iget-object v4, p0, Lcom/google/api/client/http/n;->r:Lcom/google/api/client/http/c;

    invoke-interface {v4}, Lcom/google/api/client/http/c;->b()J
    :try_end_26f
    .catchall {:try_start_249 .. :try_end_26f} :catchall_287

    move-result-wide v7

    .line 941
    const-wide/16 v10, -0x1

    cmp-long v4, v7, v10

    if-eqz v4, :cond_1b0

    .line 942
    :try_start_276
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_279
    .catchall {:try_start_276 .. :try_end_279} :catchall_287
    .catch Ljava/lang/InterruptedException; {:try_start_276 .. :try_end_279} :catch_2a3

    .line 943
    :goto_279
    const/4 v1, 0x1

    goto/16 :goto_1b0

    .line 950
    :cond_27c
    const/4 v1, 0x0

    goto/16 :goto_1b7

    .line 957
    :cond_27f
    if-nez v2, :cond_285

    const/4 v1, 0x1

    :goto_282
    and-int/2addr v0, v1

    goto/16 :goto_1bd

    :cond_285
    const/4 v1, 0x0

    goto :goto_282

    .line 966
    :catchall_287
    move-exception v0

    if-eqz v2, :cond_28d

    invoke-virtual {v2}, Lcom/google/api/client/http/r;->h()V

    :cond_28d
    throw v0

    .line 976
    :cond_28e
    iget-boolean v0, p0, Lcom/google/api/client/http/n;->t:Z

    if-eqz v0, :cond_2a5

    invoke-virtual {v2}, Lcom/google/api/client/http/r;->c()Z

    move-result v0

    if-nez v0, :cond_2a5

    .line 978
    :try_start_298
    new-instance v0, Lcom/google/api/client/http/HttpResponseException;

    invoke-direct {v0, v2}, Lcom/google/api/client/http/HttpResponseException;-><init>(Lcom/google/api/client/http/r;)V

    throw v0
    :try_end_29e
    .catchall {:try_start_298 .. :try_end_29e} :catchall_29e

    .line 980
    :catchall_29e
    move-exception v0

    invoke-virtual {v2}, Lcom/google/api/client/http/r;->h()V

    throw v0

    :catch_2a3
    move-exception v1

    goto :goto_279

    .line 983
    :cond_2a5
    return-object v2

    :cond_2a6
    move-object v0, v2

    move v9, v1

    goto/16 :goto_1c

    :cond_2aa
    move v5, v4

    goto/16 :goto_182

    :cond_2ad
    move-object v1, v0

    goto/16 :goto_f0

    :cond_2b0
    move-object v0, v1

    goto/16 :goto_df

    :cond_2b3
    move-object v8, v0

    goto/16 :goto_7f

    .line 801
    :pswitch_data_2b6
    .packed-switch 0x1
        :pswitch_1c9
        :pswitch_1d2
        :pswitch_1e6
        :pswitch_1fa
        :pswitch_203
    .end packed-switch

    .line 931
    :pswitch_data_2c4
    .packed-switch 0x12d
        :pswitch_249
        :pswitch_249
        :pswitch_249
        :pswitch_18f
        :pswitch_18f
        :pswitch_18f
        :pswitch_249
    .end packed-switch
.end method
