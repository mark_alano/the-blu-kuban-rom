.class public final Lcom/google/api/client/util/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/Map;

.field private static final c:Ljava/util/Map;


# instance fields
.field final a:Ljava/util/List;

.field private final d:Ljava/lang/Class;

.field private final e:Z

.field private final f:Ljava/util/IdentityHashMap;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 43
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/google/api/client/util/g;->b:Ljava/util/Map;

    .line 46
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/google/api/client/util/g;->c:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Class;Z)V
    .registers 16
    .parameter
    .parameter

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/api/client/util/g;->f:Ljava/util/IdentityHashMap;

    .line 162
    iput-object p1, p0, Lcom/google/api/client/util/g;->d:Ljava/lang/Class;

    .line 163
    iput-boolean p2, p0, Lcom/google/api/client/util/g;->e:Z

    .line 164
    if-eqz p2, :cond_16

    invoke-virtual {p1}, Ljava/lang/Class;->isEnum()Z

    move-result v0

    if-nez v0, :cond_9b

    :cond_16
    const/4 v0, 0x1

    :goto_17
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot ignore case on an enum: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/ag;->a(ZLjava/lang/Object;)V

    .line 167
    new-instance v5, Ljava/util/TreeSet;

    new-instance v0, Lcom/google/api/client/util/h;

    invoke-direct {v0, p0}, Lcom/google/api/client/util/h;-><init>(Lcom/google/api/client/util/g;)V

    invoke-direct {v5, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 173
    invoke-virtual {p1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 174
    if-eqz v0, :cond_49

    .line 175
    invoke-static {v0, p2}, Lcom/google/api/client/util/g;->a(Ljava/lang/Class;Z)Lcom/google/api/client/util/g;

    move-result-object v0

    .line 176
    iget-object v1, p0, Lcom/google/api/client/util/g;->f:Ljava/util/IdentityHashMap;

    iget-object v2, v0, Lcom/google/api/client/util/g;->f:Ljava/util/IdentityHashMap;

    invoke-virtual {v1, v2}, Ljava/util/IdentityHashMap;->putAll(Ljava/util/Map;)V

    .line 177
    iget-object v0, v0, Lcom/google/api/client/util/g;->a:Ljava/util/List;

    invoke-virtual {v5, v0}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    .line 180
    :cond_49
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v6

    array-length v7, v6

    const/4 v0, 0x0

    move v4, v0

    :goto_50
    if-ge v4, v7, :cond_a8

    aget-object v8, v6, v4

    .line 181
    invoke-static {v8}, Lcom/google/api/client/util/n;->a(Ljava/lang/reflect/Field;)Lcom/google/api/client/util/n;

    move-result-object v9

    .line 182
    if-eqz v9, :cond_97

    .line 183
    invoke-virtual {v9}, Lcom/google/api/client/util/n;->b()Ljava/lang/String;

    move-result-object v0

    .line 186
    if-eqz p2, :cond_bf

    .line 187
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 189
    :goto_69
    iget-object v0, p0, Lcom/google/api/client/util/g;->f:Ljava/util/IdentityHashMap;

    invoke-virtual {v0, v1}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/util/n;

    .line 190
    if-nez v0, :cond_9e

    const/4 v2, 0x1

    :goto_74
    const-string v10, "two fields have the same %sname <%s>: %s and %s"

    const/4 v3, 0x4

    new-array v11, v3, [Ljava/lang/Object;

    const/4 v12, 0x0

    if-eqz p2, :cond_a0

    const-string v3, "case-insensitive "

    :goto_7e
    aput-object v3, v11, v12

    const/4 v3, 0x1

    aput-object v1, v11, v3

    const/4 v3, 0x2

    aput-object v8, v11, v3

    const/4 v3, 0x3

    if-nez v0, :cond_a3

    const/4 v0, 0x0

    :goto_8a
    aput-object v0, v11, v3

    invoke-static {v2, v10, v11}, Lcom/google/common/base/ag;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 196
    iget-object v0, p0, Lcom/google/api/client/util/g;->f:Ljava/util/IdentityHashMap;

    invoke-virtual {v0, v1, v9}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    invoke-virtual {v5, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 180
    :cond_97
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_50

    .line 164
    :cond_9b
    const/4 v0, 0x0

    goto/16 :goto_17

    .line 190
    :cond_9e
    const/4 v2, 0x0

    goto :goto_74

    :cond_a0
    const-string v3, ""

    goto :goto_7e

    :cond_a3
    invoke-virtual {v0}, Lcom/google/api/client/util/n;->a()Ljava/lang/reflect/Field;

    move-result-object v0

    goto :goto_8a

    .line 199
    :cond_a8
    invoke-virtual {v5}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b5

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_b2
    iput-object v0, p0, Lcom/google/api/client/util/g;->a:Ljava/util/List;

    .line 201
    return-void

    .line 199
    :cond_b5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_b2

    :cond_bf
    move-object v1, v0

    goto :goto_69
.end method

.method public static a(Ljava/lang/Class;)Lcom/google/api/client/util/g;
    .registers 2
    .parameter

    .prologue
    .line 72
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/api/client/util/g;->a(Ljava/lang/Class;Z)Lcom/google/api/client/util/g;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Z)Lcom/google/api/client/util/g;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 84
    if-nez p0, :cond_4

    .line 85
    const/4 v0, 0x0

    .line 96
    :goto_3
    return-object v0

    .line 87
    :cond_4
    if-eqz p1, :cond_1f

    sget-object v0, Lcom/google/api/client/util/g;->c:Ljava/util/Map;

    move-object v1, v0

    .line 89
    :goto_9
    monitor-enter v1

    .line 90
    :try_start_a
    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/util/g;

    .line 91
    if-nez v0, :cond_1a

    .line 92
    new-instance v0, Lcom/google/api/client/util/g;

    invoke-direct {v0, p0, p1}, Lcom/google/api/client/util/g;-><init>(Ljava/lang/Class;Z)V

    .line 93
    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    :cond_1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_a .. :try_end_1b} :catchall_1c

    goto :goto_3

    :catchall_1c
    move-exception v0

    monitor-exit v1

    throw v0

    .line 87
    :cond_1f
    sget-object v0, Lcom/google/api/client/util/g;->b:Ljava/util/Map;

    move-object v1, v0

    goto :goto_9
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/api/client/util/n;
    .registers 3
    .parameter

    .prologue
    .line 124
    if-eqz p1, :cond_e

    .line 125
    iget-boolean v0, p0, Lcom/google/api/client/util/g;->e:Z

    if-eqz v0, :cond_a

    .line 126
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 128
    :cond_a
    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object p1

    .line 130
    :cond_e
    iget-object v0, p0, Lcom/google/api/client/util/g;->f:Ljava/util/IdentityHashMap;

    invoke-virtual {v0, p1}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/util/n;

    return-object v0
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/google/api/client/util/g;->e:Z

    return v0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/reflect/Field;
    .registers 3
    .parameter

    .prologue
    .line 140
    invoke-virtual {p0, p1}, Lcom/google/api/client/util/g;->a(Ljava/lang/String;)Lcom/google/api/client/util/n;

    move-result-object v0

    .line 141
    if-nez v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    invoke-virtual {v0}, Lcom/google/api/client/util/n;->a()Ljava/lang/reflect/Field;

    move-result-object v0

    goto :goto_7
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/api/client/util/g;->d:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isEnum()Z

    move-result v0

    return v0
.end method
