.class public abstract Lcom/google/protobuf/GeneratedMessageLite;
.super Lcom/google/protobuf/a;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/protobuf/a;-><init>()V

    .line 25
    return-void
.end method

.method protected constructor <init>(Lcom/google/protobuf/o;)V
    .registers 2
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/protobuf/a;-><init>()V

    .line 28
    return-void
.end method

.method static synthetic access$300(Lcom/google/protobuf/k;Lcom/google/protobuf/ae;Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/protobuf/GeneratedMessageLite;->parseUnknownField(Lcom/google/protobuf/k;Lcom/google/protobuf/ae;Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z

    move-result v0

    return v0
.end method

.method public static newRepeatedGeneratedExtension(Lcom/google/protobuf/ae;Lcom/google/protobuf/ae;Lcom/google/protobuf/w;ILcom/google/protobuf/WireFormat$FieldType;Z)Lcom/google/protobuf/t;
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 590
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    .line 591
    new-instance v8, Lcom/google/protobuf/t;

    new-instance v0, Lcom/google/protobuf/s;

    const/4 v4, 0x1

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/protobuf/s;-><init>(Lcom/google/protobuf/w;ILcom/google/protobuf/WireFormat$FieldType;ZZB)V

    move-object v1, v8

    move-object v2, p0

    move-object v3, v7

    move-object v4, p1

    move-object v5, v0

    invoke-direct/range {v1 .. v6}, Lcom/google/protobuf/t;-><init>(Lcom/google/protobuf/ae;Ljava/lang/Object;Lcom/google/protobuf/ae;Lcom/google/protobuf/s;B)V

    return-object v8
.end method

.method public static newSingularGeneratedExtension(Lcom/google/protobuf/ae;Ljava/lang/Object;Lcom/google/protobuf/ae;Lcom/google/protobuf/w;ILcom/google/protobuf/WireFormat$FieldType;)Lcom/google/protobuf/t;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 570
    new-instance v7, Lcom/google/protobuf/t;

    new-instance v0, Lcom/google/protobuf/s;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p3

    move v2, p4

    move-object v3, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/protobuf/s;-><init>(Lcom/google/protobuf/w;ILcom/google/protobuf/WireFormat$FieldType;ZZB)V

    const/4 v6, 0x0

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v0

    invoke-direct/range {v1 .. v6}, Lcom/google/protobuf/t;-><init>(Lcom/google/protobuf/ae;Ljava/lang/Object;Lcom/google/protobuf/ae;Lcom/google/protobuf/s;B)V

    return-object v7
.end method

.method private static parseUnknownField(Lcom/google/protobuf/k;Lcom/google/protobuf/ae;Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 454
    invoke-static {p4}, Lcom/google/protobuf/WireFormat;->a(I)I

    move-result v2

    .line 455
    invoke-static {p4}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v3

    .line 457
    invoke-virtual {p3, p1, v3}, Lcom/google/protobuf/i;->a(Lcom/google/protobuf/ae;I)Lcom/google/protobuf/t;

    move-result-object v3

    .line 463
    if-eqz v3, :cond_4f

    .line 464
    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protobuf/s;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/WireFormat$FieldType;Z)I

    move-result v4

    if-ne v2, v4, :cond_26

    move v2, v0

    .line 479
    :goto_1f
    if-eqz v2, :cond_51

    .line 480
    invoke-virtual {p2, p4}, Lcom/google/protobuf/h;->b(I)Z

    move-result v0

    .line 555
    :goto_25
    return v0

    .line 469
    :cond_26
    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/s;->a(Lcom/google/protobuf/s;)Z

    move-result v4

    if-eqz v4, :cond_4f

    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/s;->b(Lcom/google/protobuf/s;)Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protobuf/WireFormat$FieldType;->isPackable()Z

    move-result v4

    if-eqz v4, :cond_4f

    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protobuf/s;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/WireFormat$FieldType;Z)I

    move-result v4

    if-ne v2, v4, :cond_4f

    move v2, v0

    move v0, v1

    .line 474
    goto :goto_1f

    :cond_4f
    move v2, v1

    .line 476
    goto :goto_1f

    .line 483
    :cond_51
    if-eqz v0, :cond_a9

    .line 484
    invoke-virtual {p2}, Lcom/google/protobuf/h;->g()I

    move-result v0

    .line 485
    invoke-virtual {p2, v0}, Lcom/google/protobuf/h;->c(I)I

    move-result v0

    .line 486
    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protobuf/s;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v2

    sget-object v4, Lcom/google/protobuf/WireFormat$FieldType;->ENUM:Lcom/google/protobuf/WireFormat$FieldType;

    if-ne v2, v4, :cond_89

    .line 487
    :goto_67
    invoke-virtual {p2}, Lcom/google/protobuf/h;->k()I

    move-result v2

    if-lez v2, :cond_a3

    .line 488
    invoke-virtual {p2}, Lcom/google/protobuf/h;->f()I

    move-result v2

    .line 489
    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protobuf/s;->f()Lcom/google/protobuf/w;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/google/protobuf/w;->a(I)Lcom/google/protobuf/v;

    move-result-object v2

    .line 491
    if-nez v2, :cond_81

    move v0, v1

    .line 494
    goto :goto_25

    .line 496
    :cond_81
    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v4

    invoke-virtual {p0, v4, v2}, Lcom/google/protobuf/k;->b(Lcom/google/protobuf/m;Ljava/lang/Object;)V

    goto :goto_67

    .line 499
    :cond_89
    :goto_89
    invoke-virtual {p2}, Lcom/google/protobuf/h;->k()I

    move-result v2

    if-lez v2, :cond_a3

    .line 500
    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protobuf/s;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/WireFormat$FieldType;)Ljava/lang/Object;

    move-result-object v2

    .line 503
    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v4

    invoke-virtual {p0, v4, v2}, Lcom/google/protobuf/k;->b(Lcom/google/protobuf/m;Ljava/lang/Object;)V

    goto :goto_89

    .line 506
    :cond_a3
    invoke-virtual {p2, v0}, Lcom/google/protobuf/h;->d(I)V

    :goto_a6
    move v0, v1

    .line 555
    goto/16 :goto_25

    .line 509
    :cond_a9
    sget-object v0, Lcom/google/protobuf/n;->a:[I

    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protobuf/s;->c()Lcom/google/protobuf/WireFormat$JavaType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protobuf/WireFormat$JavaType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_13c

    .line 543
    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/s;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/WireFormat$FieldType;)Ljava/lang/Object;

    move-result-object v0

    .line 548
    :cond_c8
    :goto_c8
    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protobuf/s;->d()Z

    move-result v2

    if-eqz v2, :cond_130

    .line 549
    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, Lcom/google/protobuf/k;->b(Lcom/google/protobuf/m;Ljava/lang/Object;)V

    goto :goto_a6

    .line 511
    :pswitch_da
    const/4 v2, 0x0

    .line 512
    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/s;->d()Z

    move-result v0

    if-nez v0, :cond_139

    .line 513
    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/k;->b(Lcom/google/protobuf/m;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ae;

    .line 515
    if-eqz v0, :cond_139

    .line 516
    invoke-interface {v0}, Lcom/google/protobuf/ae;->toBuilder()Lcom/google/protobuf/af;

    move-result-object v0

    .line 519
    :goto_f5
    if-nez v0, :cond_ff

    .line 520
    invoke-static {v3}, Lcom/google/protobuf/t;->c(Lcom/google/protobuf/t;)Lcom/google/protobuf/ae;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/ae;->newBuilderForType()Lcom/google/protobuf/af;

    move-result-object v0

    .line 522
    :cond_ff
    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protobuf/s;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v2

    sget-object v4, Lcom/google/protobuf/WireFormat$FieldType;->GROUP:Lcom/google/protobuf/WireFormat$FieldType;

    if-ne v2, v4, :cond_117

    .line 524
    invoke-virtual {v3}, Lcom/google/protobuf/t;->b()I

    move-result v2

    invoke-virtual {p2, v2, v0, p3}, Lcom/google/protobuf/h;->a(ILcom/google/protobuf/af;Lcom/google/protobuf/i;)V

    .line 529
    :goto_112
    invoke-interface {v0}, Lcom/google/protobuf/af;->f()Lcom/google/protobuf/ae;

    move-result-object v0

    goto :goto_c8

    .line 527
    :cond_117
    invoke-virtual {p2, v0, p3}, Lcom/google/protobuf/h;->a(Lcom/google/protobuf/af;Lcom/google/protobuf/i;)V

    goto :goto_112

    .line 533
    :pswitch_11b
    invoke-virtual {p2}, Lcom/google/protobuf/h;->f()I

    move-result v0

    .line 534
    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protobuf/s;->f()Lcom/google/protobuf/w;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/protobuf/w;->a(I)Lcom/google/protobuf/v;

    move-result-object v0

    .line 538
    if-nez v0, :cond_c8

    move v0, v1

    .line 539
    goto/16 :goto_25

    .line 551
    :cond_130
    invoke-static {v3}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/m;Ljava/lang/Object;)V

    goto/16 :goto_a6

    :cond_139
    move-object v0, v2

    goto :goto_f5

    .line 509
    nop

    :pswitch_data_13c
    .packed-switch 0x1
        :pswitch_da
        :pswitch_11b
    .end packed-switch
.end method


# virtual methods
.method public getParserForType()Lcom/google/protobuf/ah;
    .registers 3

    .prologue
    .line 31
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is supposed to be overridden by subclasses."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected makeExtensionsImmutable()V
    .registers 1

    .prologue
    .line 51
    return-void
.end method

.method protected parseUnknownField(Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-virtual {p1, p3}, Lcom/google/protobuf/h;->b(I)Z

    move-result v0

    return v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 767
    new-instance v0, Lcom/google/protobuf/GeneratedMessageLite$SerializedForm;

    invoke-direct {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite$SerializedForm;-><init>(Lcom/google/protobuf/ae;)V

    return-object v0
.end method
