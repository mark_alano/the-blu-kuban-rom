.class public abstract Lcom/google/protobuf/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/protobuf/ae;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    return-void
.end method


# virtual methods
.method newUninitializedMessageException()Lcom/google/protobuf/UninitializedMessageException;
    .registers 2

    .prologue
    .line 71
    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0, p0}, Lcom/google/protobuf/UninitializedMessageException;-><init>(Lcom/google/protobuf/ae;)V

    return-object v0
.end method

.method public toByteArray()[B
    .registers 4

    .prologue
    .line 34
    :try_start_0
    invoke-virtual {p0}, Lcom/google/protobuf/a;->getSerializedSize()I

    move-result v0

    new-array v0, v0, [B

    .line 35
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->a([B)Lcom/google/protobuf/CodedOutputStream;

    move-result-object v1

    .line 36
    invoke-virtual {p0, v1}, Lcom/google/protobuf/a;->writeTo(Lcom/google/protobuf/CodedOutputStream;)V

    .line 37
    invoke-virtual {v1}, Lcom/google/protobuf/CodedOutputStream;->b()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_10} :catch_11

    .line 38
    return-object v0

    .line 39
    :catch_11
    move-exception v0

    .line 40
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public toByteString()Lcom/google/protobuf/e;
    .registers 4

    .prologue
    .line 21
    :try_start_0
    invoke-virtual {p0}, Lcom/google/protobuf/a;->getSerializedSize()I

    move-result v0

    invoke-static {v0}, Lcom/google/protobuf/e;->a(I)Lcom/google/protobuf/g;

    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lcom/google/protobuf/g;->b()Lcom/google/protobuf/CodedOutputStream;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/a;->writeTo(Lcom/google/protobuf/CodedOutputStream;)V

    .line 24
    invoke-virtual {v0}, Lcom/google/protobuf/g;->a()Lcom/google/protobuf/e;
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_12} :catch_14

    move-result-object v0

    return-object v0

    .line 25
    :catch_14
    move-exception v0

    .line 26
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a ByteString threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public writeDelimitedTo(Ljava/io/OutputStream;)V
    .registers 4
    .parameter

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/protobuf/a;->getSerializedSize()I

    move-result v0

    .line 57
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->f(I)I

    move-result v1

    add-int/2addr v1, v0

    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->a(I)I

    move-result v1

    .line 59
    invoke-static {p1, v1}, Lcom/google/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;I)Lcom/google/protobuf/CodedOutputStream;

    move-result-object v1

    .line 61
    invoke-virtual {v1, v0}, Lcom/google/protobuf/CodedOutputStream;->e(I)V

    .line 62
    invoke-virtual {p0, v1}, Lcom/google/protobuf/a;->writeTo(Lcom/google/protobuf/CodedOutputStream;)V

    .line 63
    invoke-virtual {v1}, Lcom/google/protobuf/CodedOutputStream;->a()V

    .line 64
    return-void
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .registers 3
    .parameter

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/protobuf/a;->getSerializedSize()I

    move-result v0

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->a(I)I

    move-result v0

    .line 49
    invoke-static {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;I)Lcom/google/protobuf/CodedOutputStream;

    move-result-object v0

    .line 51
    invoke-virtual {p0, v0}, Lcom/google/protobuf/a;->writeTo(Lcom/google/protobuf/CodedOutputStream;)V

    .line 52
    invoke-virtual {v0}, Lcom/google/protobuf/CodedOutputStream;->a()V

    .line 53
    return-void
.end method
