.class final Lcom/google/protobuf/ai;
.super Lcom/google/protobuf/e;
.source "SourceFile"


# static fields
.field private static final c:[I


# instance fields
.field private final d:I

.field private final e:Lcom/google/protobuf/e;

.field private final f:Lcom/google/protobuf/e;

.field private final g:I

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 60
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    .line 68
    :goto_7
    if-lez v0, :cond_15

    .line 69
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    add-int/2addr v1, v0

    move v4, v1

    move v1, v0

    move v0, v4

    .line 73
    goto :goto_7

    .line 77
    :cond_15
    const v0, 0x7fffffff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/protobuf/ai;->c:[I

    .line 79
    const/4 v0, 0x0

    move v1, v0

    :goto_29
    sget-object v0, Lcom/google/protobuf/ai;->c:[I

    array-length v0, v0

    if-ge v1, v0, :cond_40

    .line 81
    sget-object v3, Lcom/google/protobuf/ai;->c:[I

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    .line 79
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_29

    .line 83
    :cond_40
    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/ai;)Lcom/google/protobuf/e;
    .registers 2
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/protobuf/ai;->e:Lcom/google/protobuf/e;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/ai;)Lcom/google/protobuf/e;
    .registers 2
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/protobuf/ai;->f:Lcom/google/protobuf/e;

    return-object v0
.end method


# virtual methods
.method protected final a(III)I
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 393
    add-int v0, p2, p3

    .line 394
    iget v1, p0, Lcom/google/protobuf/ai;->g:I

    if-gt v0, v1, :cond_d

    .line 395
    iget-object v0, p0, Lcom/google/protobuf/ai;->e:Lcom/google/protobuf/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/protobuf/e;->a(III)I

    move-result v0

    .line 401
    :goto_c
    return v0

    .line 396
    :cond_d
    iget v0, p0, Lcom/google/protobuf/ai;->g:I

    if-lt p2, v0, :cond_1c

    .line 397
    iget-object v0, p0, Lcom/google/protobuf/ai;->f:Lcom/google/protobuf/e;

    iget v1, p0, Lcom/google/protobuf/ai;->g:I

    sub-int v1, p2, v1

    invoke-virtual {v0, p1, v1, p3}, Lcom/google/protobuf/e;->a(III)I

    move-result v0

    goto :goto_c

    .line 399
    :cond_1c
    iget v0, p0, Lcom/google/protobuf/ai;->g:I

    sub-int/2addr v0, p2

    .line 400
    iget-object v1, p0, Lcom/google/protobuf/ai;->e:Lcom/google/protobuf/e;

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/protobuf/e;->a(III)I

    move-result v1

    .line 401
    iget-object v2, p0, Lcom/google/protobuf/ai;->f:Lcom/google/protobuf/e;

    const/4 v3, 0x0

    sub-int v0, p3, v0

    invoke-virtual {v2, v1, v3, v0}, Lcom/google/protobuf/e;->a(III)I

    move-result v0

    goto :goto_c
.end method

.method public final a()Lcom/google/protobuf/f;
    .registers 3

    .prologue
    .line 735
    new-instance v0, Lcom/google/protobuf/ak;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/protobuf/ak;-><init>(Lcom/google/protobuf/ai;B)V

    return-object v0
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 240
    iget v0, p0, Lcom/google/protobuf/ai;->d:I

    return v0
.end method

.method protected final b(III)I
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 524
    add-int v0, p2, p3

    .line 525
    iget v1, p0, Lcom/google/protobuf/ai;->g:I

    if-gt v0, v1, :cond_d

    .line 526
    iget-object v0, p0, Lcom/google/protobuf/ai;->e:Lcom/google/protobuf/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/protobuf/e;->b(III)I

    move-result v0

    .line 532
    :goto_c
    return v0

    .line 527
    :cond_d
    iget v0, p0, Lcom/google/protobuf/ai;->g:I

    if-lt p2, v0, :cond_1c

    .line 528
    iget-object v0, p0, Lcom/google/protobuf/ai;->f:Lcom/google/protobuf/e;

    iget v1, p0, Lcom/google/protobuf/ai;->g:I

    sub-int v1, p2, v1

    invoke-virtual {v0, p1, v1, p3}, Lcom/google/protobuf/e;->b(III)I

    move-result v0

    goto :goto_c

    .line 530
    :cond_1c
    iget v0, p0, Lcom/google/protobuf/ai;->g:I

    sub-int/2addr v0, p2

    .line 531
    iget-object v1, p0, Lcom/google/protobuf/ai;->e:Lcom/google/protobuf/e;

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/protobuf/e;->b(III)I

    move-result v1

    .line 532
    iget-object v2, p0, Lcom/google/protobuf/ai;->f:Lcom/google/protobuf/e;

    const/4 v3, 0x0

    sub-int v0, p3, v0

    invoke-virtual {v2, v1, v3, v0}, Lcom/google/protobuf/e;->b(III)I

    move-result v0

    goto :goto_c
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 378
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/protobuf/ai;->d()[B

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    return-object v0
.end method

.method protected final b([BIII)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 331
    add-int v0, p2, p4

    iget v1, p0, Lcom/google/protobuf/ai;->g:I

    if-gt v0, v1, :cond_c

    .line 332
    iget-object v0, p0, Lcom/google/protobuf/ai;->e:Lcom/google/protobuf/e;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/protobuf/e;->b([BIII)V

    .line 342
    :goto_b
    return-void

    .line 333
    :cond_c
    iget v0, p0, Lcom/google/protobuf/ai;->g:I

    if-lt p2, v0, :cond_1a

    .line 334
    iget-object v0, p0, Lcom/google/protobuf/ai;->f:Lcom/google/protobuf/e;

    iget v1, p0, Lcom/google/protobuf/ai;->g:I

    sub-int v1, p2, v1

    invoke-virtual {v0, p1, v1, p3, p4}, Lcom/google/protobuf/e;->b([BIII)V

    goto :goto_b

    .line 337
    :cond_1a
    iget v0, p0, Lcom/google/protobuf/ai;->g:I

    sub-int/2addr v0, p2

    .line 338
    iget-object v1, p0, Lcom/google/protobuf/ai;->e:Lcom/google/protobuf/e;

    invoke-virtual {v1, p1, p2, p3, v0}, Lcom/google/protobuf/e;->b([BIII)V

    .line 339
    iget-object v1, p0, Lcom/google/protobuf/ai;->f:Lcom/google/protobuf/e;

    const/4 v2, 0x0

    add-int v3, p3, v0

    sub-int v0, p4, v0

    invoke-virtual {v1, p1, v2, v3, v0}, Lcom/google/protobuf/e;->b([BIII)V

    goto :goto_b
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 15
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 410
    if-ne p1, p0, :cond_6

    move v2, v7

    .line 437
    :cond_5
    :goto_5
    return v2

    .line 413
    :cond_6
    instance-of v0, p1, Lcom/google/protobuf/e;

    if-eqz v0, :cond_5

    .line 417
    check-cast p1, Lcom/google/protobuf/e;

    .line 418
    iget v0, p0, Lcom/google/protobuf/ai;->d:I

    invoke-virtual {p1}, Lcom/google/protobuf/e;->b()I

    move-result v1

    if-ne v0, v1, :cond_5

    .line 421
    iget v0, p0, Lcom/google/protobuf/ai;->d:I

    if-nez v0, :cond_1a

    move v2, v7

    .line 422
    goto :goto_5

    .line 430
    :cond_1a
    iget v0, p0, Lcom/google/protobuf/ai;->h:I

    if-eqz v0, :cond_28

    .line 431
    invoke-virtual {p1}, Lcom/google/protobuf/e;->i()I

    move-result v0

    .line 432
    if-eqz v0, :cond_28

    iget v1, p0, Lcom/google/protobuf/ai;->h:I

    if-ne v1, v0, :cond_5

    .line 437
    :cond_28
    new-instance v8, Lcom/google/protobuf/aj;

    invoke-direct {v8, p0, v2}, Lcom/google/protobuf/aj;-><init>(Lcom/google/protobuf/e;B)V

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ac;

    new-instance v9, Lcom/google/protobuf/aj;

    invoke-direct {v9, p1, v2}, Lcom/google/protobuf/aj;-><init>(Lcom/google/protobuf/e;B)V

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/ac;

    move-object v3, v1

    move v4, v2

    move-object v5, v0

    move v6, v2

    move v0, v2

    :goto_43
    invoke-virtual {v5}, Lcom/google/protobuf/ac;->b()I

    move-result v1

    sub-int v10, v1, v6

    invoke-virtual {v3}, Lcom/google/protobuf/ac;->b()I

    move-result v1

    sub-int v11, v1, v4

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v12

    if-nez v6, :cond_67

    invoke-virtual {v5, v3, v4, v12}, Lcom/google/protobuf/ac;->a(Lcom/google/protobuf/ac;II)Z

    move-result v1

    :goto_59
    if-eqz v1, :cond_5

    add-int v1, v0, v12

    iget v0, p0, Lcom/google/protobuf/ai;->d:I

    if-lt v1, v0, :cond_72

    iget v0, p0, Lcom/google/protobuf/ai;->d:I

    if-ne v1, v0, :cond_6c

    move v2, v7

    goto :goto_5

    :cond_67
    invoke-virtual {v3, v5, v6, v12}, Lcom/google/protobuf/ac;->a(Lcom/google/protobuf/ac;II)Z

    move-result v1

    goto :goto_59

    :cond_6c
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_72
    if-ne v12, v10, :cond_88

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ac;

    move-object v5, v0

    move v6, v2

    :goto_7c
    if-ne v12, v11, :cond_8a

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ac;

    move-object v3, v0

    move v4, v2

    move v0, v1

    goto :goto_43

    :cond_88
    add-int/2addr v6, v12

    goto :goto_7c

    :cond_8a
    add-int v0, v4, v12

    move v4, v0

    move v0, v1

    goto :goto_43
.end method

.method public final f()Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 386
    iget-object v1, p0, Lcom/google/protobuf/ai;->e:Lcom/google/protobuf/e;

    iget v2, p0, Lcom/google/protobuf/ai;->g:I

    invoke-virtual {v1, v0, v0, v2}, Lcom/google/protobuf/e;->a(III)I

    move-result v1

    .line 387
    iget-object v2, p0, Lcom/google/protobuf/ai;->f:Lcom/google/protobuf/e;

    iget-object v3, p0, Lcom/google/protobuf/ai;->f:Lcom/google/protobuf/e;

    invoke-virtual {v3}, Lcom/google/protobuf/e;->b()I

    move-result v3

    invoke-virtual {v2, v1, v0, v3}, Lcom/google/protobuf/e;->a(III)I

    move-result v1

    .line 388
    if-nez v1, :cond_18

    const/4 v0, 0x1

    :cond_18
    return v0
.end method

.method public final g()Ljava/io/InputStream;
    .registers 2

    .prologue
    .line 546
    new-instance v0, Lcom/google/protobuf/al;

    invoke-direct {v0, p0}, Lcom/google/protobuf/al;-><init>(Lcom/google/protobuf/ai;)V

    return-object v0
.end method

.method public final h()Lcom/google/protobuf/h;
    .registers 2

    .prologue
    .line 541
    new-instance v0, Lcom/google/protobuf/al;

    invoke-direct {v0, p0}, Lcom/google/protobuf/al;-><init>(Lcom/google/protobuf/ai;)V

    invoke-static {v0}, Lcom/google/protobuf/h;->a(Ljava/io/InputStream;)Lcom/google/protobuf/h;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    .line 504
    iget v0, p0, Lcom/google/protobuf/ai;->h:I

    .line 506
    if-nez v0, :cond_12

    .line 507
    iget v0, p0, Lcom/google/protobuf/ai;->d:I

    .line 508
    const/4 v1, 0x0

    iget v2, p0, Lcom/google/protobuf/ai;->d:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/protobuf/ai;->b(III)I

    move-result v0

    .line 509
    if-nez v0, :cond_10

    .line 510
    const/4 v0, 0x1

    .line 512
    :cond_10
    iput v0, p0, Lcom/google/protobuf/ai;->h:I

    .line 514
    :cond_12
    return v0
.end method

.method protected final i()I
    .registers 2

    .prologue
    .line 519
    iget v0, p0, Lcom/google/protobuf/ai;->h:I

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/protobuf/ai;->a()Lcom/google/protobuf/f;

    move-result-object v0

    return-object v0
.end method
