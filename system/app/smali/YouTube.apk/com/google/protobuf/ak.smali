.class final Lcom/google/protobuf/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/protobuf/f;


# instance fields
.field a:I

.field final synthetic b:Lcom/google/protobuf/ai;

.field private final c:Lcom/google/protobuf/aj;

.field private d:Lcom/google/protobuf/f;


# direct methods
.method private constructor <init>(Lcom/google/protobuf/ai;)V
    .registers 4
    .parameter

    .prologue
    .line 744
    iput-object p1, p0, Lcom/google/protobuf/ak;->b:Lcom/google/protobuf/ai;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 745
    new-instance v0, Lcom/google/protobuf/aj;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/protobuf/aj;-><init>(Lcom/google/protobuf/e;B)V

    iput-object v0, p0, Lcom/google/protobuf/ak;->c:Lcom/google/protobuf/aj;

    .line 746
    iget-object v0, p0, Lcom/google/protobuf/ak;->c:Lcom/google/protobuf/aj;

    invoke-virtual {v0}, Lcom/google/protobuf/aj;->a()Lcom/google/protobuf/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ac;->a()Lcom/google/protobuf/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/ak;->d:Lcom/google/protobuf/f;

    .line 747
    invoke-virtual {p1}, Lcom/google/protobuf/ai;->b()I

    move-result v0

    iput v0, p0, Lcom/google/protobuf/ak;->a:I

    .line 748
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/ai;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 738
    invoke-direct {p0, p1}, Lcom/google/protobuf/ak;-><init>(Lcom/google/protobuf/ai;)V

    return-void
.end method


# virtual methods
.method public final a()B
    .registers 2

    .prologue
    .line 759
    iget-object v0, p0, Lcom/google/protobuf/ak;->d:Lcom/google/protobuf/f;

    invoke-interface {v0}, Lcom/google/protobuf/f;->hasNext()Z

    move-result v0

    if-nez v0, :cond_14

    .line 760
    iget-object v0, p0, Lcom/google/protobuf/ak;->c:Lcom/google/protobuf/aj;

    invoke-virtual {v0}, Lcom/google/protobuf/aj;->a()Lcom/google/protobuf/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ac;->a()Lcom/google/protobuf/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/ak;->d:Lcom/google/protobuf/f;

    .line 762
    :cond_14
    iget v0, p0, Lcom/google/protobuf/ak;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/protobuf/ak;->a:I

    .line 763
    iget-object v0, p0, Lcom/google/protobuf/ak;->d:Lcom/google/protobuf/f;

    invoke-interface {v0}, Lcom/google/protobuf/f;->a()B

    move-result v0

    return v0
.end method

.method public final hasNext()Z
    .registers 2

    .prologue
    .line 751
    iget v0, p0, Lcom/google/protobuf/ak;->a:I

    if-lez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final synthetic next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 738
    invoke-virtual {p0}, Lcom/google/protobuf/ak;->a()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .registers 2

    .prologue
    .line 767
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
