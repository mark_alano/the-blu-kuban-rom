.class public final Lcom/google/protobuf/au;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Lcom/google/protobuf/ab;
.implements Ljava/util/RandomAccess;


# instance fields
.field private final a:Lcom/google/protobuf/ab;


# direct methods
.method public constructor <init>(Lcom/google/protobuf/ab;)V
    .registers 2
    .parameter

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/protobuf/au;->a:Lcom/google/protobuf/ab;

    .line 24
    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/au;)Lcom/google/protobuf/ab;
    .registers 2
    .parameter

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/protobuf/au;->a:Lcom/google/protobuf/ab;

    return-object v0
.end method


# virtual methods
.method public final a(I)Lcom/google/protobuf/e;
    .registers 3
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/protobuf/au;->a:Lcom/google/protobuf/ab;

    invoke-interface {v0, p1}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/List;
    .registers 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/protobuf/au;->a:Lcom/google/protobuf/ab;

    invoke-interface {v0}, Lcom/google/protobuf/ab;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protobuf/e;)V
    .registers 3
    .parameter

    .prologue
    .line 43
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final bridge synthetic get(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/protobuf/au;->a:Lcom/google/protobuf/ab;

    invoke-interface {v0, p1}, Lcom/google/protobuf/ab;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 100
    new-instance v0, Lcom/google/protobuf/aw;

    invoke-direct {v0, p0}, Lcom/google/protobuf/aw;-><init>(Lcom/google/protobuf/au;)V

    return-object v0
.end method

.method public final listIterator(I)Ljava/util/ListIterator;
    .registers 3
    .parameter

    .prologue
    .line 48
    new-instance v0, Lcom/google/protobuf/av;

    invoke-direct {v0, p0, p1}, Lcom/google/protobuf/av;-><init>(Lcom/google/protobuf/au;I)V

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/protobuf/au;->a:Lcom/google/protobuf/ab;

    invoke-interface {v0}, Lcom/google/protobuf/ab;->size()I

    move-result v0

    return v0
.end method
