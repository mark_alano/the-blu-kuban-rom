.class final Lcom/google/protobuf/c;
.super Ljava/io/FilterInputStream;
.source "SourceFile"


# instance fields
.field private a:I


# direct methods
.method constructor <init>(Ljava/io/InputStream;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 207
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 208
    iput p2, p0, Lcom/google/protobuf/c;->a:I

    .line 209
    return-void
.end method


# virtual methods
.method public final available()I
    .registers 3

    .prologue
    .line 213
    invoke-super {p0}, Ljava/io/FilterInputStream;->available()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/c;->a:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public final read()I
    .registers 3

    .prologue
    .line 218
    iget v0, p0, Lcom/google/protobuf/c;->a:I

    if-gtz v0, :cond_6

    .line 219
    const/4 v0, -0x1

    .line 225
    :cond_5
    :goto_5
    return v0

    .line 221
    :cond_6
    invoke-super {p0}, Ljava/io/FilterInputStream;->read()I

    move-result v0

    .line 222
    if-ltz v0, :cond_5

    .line 223
    iget v1, p0, Lcom/google/protobuf/c;->a:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/protobuf/c;->a:I

    goto :goto_5
.end method

.method public final read([BII)I
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 231
    iget v0, p0, Lcom/google/protobuf/c;->a:I

    if-gtz v0, :cond_6

    .line 232
    const/4 v0, -0x1

    .line 239
    :cond_5
    :goto_5
    return v0

    .line 234
    :cond_6
    iget v0, p0, Lcom/google/protobuf/c;->a:I

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 235
    invoke-super {p0, p1, p2, v0}, Ljava/io/FilterInputStream;->read([BII)I

    move-result v0

    .line 236
    if-ltz v0, :cond_5

    .line 237
    iget v1, p0, Lcom/google/protobuf/c;->a:I

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/google/protobuf/c;->a:I

    goto :goto_5
.end method

.method public final skip(J)J
    .registers 7
    .parameter

    .prologue
    .line 244
    iget v0, p0, Lcom/google/protobuf/c;->a:I

    int-to-long v0, v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-super {p0, v0, v1}, Ljava/io/FilterInputStream;->skip(J)J

    move-result-wide v0

    .line 245
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_18

    .line 246
    iget v2, p0, Lcom/google/protobuf/c;->a:I

    int-to-long v2, v2

    sub-long/2addr v2, v0

    long-to-int v2, v2

    iput v2, p0, Lcom/google/protobuf/c;->a:I

    .line 248
    :cond_18
    return-wide v0
.end method
