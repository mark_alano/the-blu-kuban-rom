.class public abstract Lcom/google/protobuf/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/protobuf/ah;


# static fields
.field private static final a:Lcom/google/protobuf/i;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 50
    invoke-static {}, Lcom/google/protobuf/i;->a()Lcom/google/protobuf/i;

    move-result-object v0

    sput-object v0, Lcom/google/protobuf/d;->a:Lcom/google/protobuf/i;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/ae;
    .registers 3
    .parameter

    .prologue
    .line 42
    if-eqz p1, :cond_22

    invoke-interface {p1}, Lcom/google/protobuf/ae;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_22

    .line 43
    instance-of v0, p1, Lcom/google/protobuf/a;

    if-eqz v0, :cond_1c

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/a;

    invoke-virtual {v0}, Lcom/google/protobuf/a;->newUninitializedMessageException()Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    :goto_13
    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_1c
    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0, p1}, Lcom/google/protobuf/UninitializedMessageException;-><init>(Lcom/google/protobuf/ae;)V

    goto :goto_13

    .line 47
    :cond_22
    return-object p1
.end method

.method private a([BIILcom/google/protobuf/i;)Lcom/google/protobuf/ae;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 112
    :try_start_0
    invoke-static {p1, p2, p3}, Lcom/google/protobuf/h;->a([BII)Lcom/google/protobuf/h;

    move-result-object v1

    .line 113
    invoke-virtual {p0, v1, p4}, Lcom/google/protobuf/d;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ae;
    :try_end_a
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_a} :catch_15
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_a} :catch_17

    .line 115
    const/4 v2, 0x0

    :try_start_b
    invoke-virtual {v1, v2}, Lcom/google/protobuf/h;->a(I)V
    :try_end_e
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_b .. :try_end_e} :catch_f
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_e} :catch_17

    .line 119
    return-object v0

    .line 116
    :catch_f
    move-exception v1

    .line 117
    :try_start_10
    invoke-virtual {v1, v0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_15
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_10 .. :try_end_15} :catch_15
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_15} :catch_17

    .line 120
    :catch_15
    move-exception v0

    .line 121
    throw v0

    .line 122
    :catch_17
    move-exception v0

    .line 123
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 75
    :try_start_0
    invoke-virtual {p1}, Lcom/google/protobuf/e;->h()Lcom/google/protobuf/h;

    move-result-object v1

    .line 76
    invoke-virtual {p0, v1, p2}, Lcom/google/protobuf/d;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ae;
    :try_end_a
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_a} :catch_15
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_a} :catch_17

    .line 78
    const/4 v2, 0x0

    :try_start_b
    invoke-virtual {v1, v2}, Lcom/google/protobuf/h;->a(I)V
    :try_end_e
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_b .. :try_end_e} :catch_f
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_e} :catch_17

    .line 82
    return-object v0

    .line 79
    :catch_f
    move-exception v1

    .line 80
    :try_start_10
    invoke-virtual {v1, v0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_15
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_10 .. :try_end_15} :catch_15
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_15} :catch_17

    .line 83
    :catch_15
    move-exception v0

    .line 84
    throw v0

    .line 85
    :catch_17
    move-exception v0

    .line 86
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a ByteString threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b([BLcom/google/protobuf/i;)Lcom/google/protobuf/ae;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 160
    array-length v0, p1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0, p2}, Lcom/google/protobuf/d;->a([BIILcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/protobuf/d;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 100
    invoke-direct {p0, p1, p2}, Lcom/google/protobuf/d;->b(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/protobuf/d;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/d;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ae;

    invoke-direct {p0, v0}, Lcom/google/protobuf/d;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 171
    invoke-static {p1}, Lcom/google/protobuf/h;->a(Ljava/io/InputStream;)Lcom/google/protobuf/h;

    move-result-object v1

    .line 172
    invoke-virtual {p0, v1, p2}, Lcom/google/protobuf/d;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ae;

    .line 174
    const/4 v2, 0x0

    :try_start_b
    invoke-virtual {v1, v2}, Lcom/google/protobuf/h;->a(I)V
    :try_end_e
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_b .. :try_end_e} :catch_f

    .line 178
    return-object v0

    .line 175
    :catch_f
    move-exception v1

    .line 176
    invoke-virtual {v1, v0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
.end method

.method private d(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 189
    invoke-direct {p0, p1, p2}, Lcom/google/protobuf/d;->c(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/protobuf/d;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method

.method private e(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 204
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 205
    const/4 v1, -0x1

    if-ne v0, v1, :cond_9

    .line 206
    const/4 v0, 0x0

    .line 213
    :goto_8
    return-object v0

    .line 208
    :cond_9
    invoke-static {v0, p1}, Lcom/google/protobuf/h;->a(ILjava/io/InputStream;)I
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_c} :catch_17

    move-result v0

    .line 212
    new-instance v1, Lcom/google/protobuf/c;

    invoke-direct {v1, p1, v0}, Lcom/google/protobuf/c;-><init>(Ljava/io/InputStream;I)V

    .line 213
    invoke-direct {p0, v1, p2}, Lcom/google/protobuf/d;->c(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    goto :goto_8

    .line 209
    :catch_17
    move-exception v0

    .line 210
    new-instance v1, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private f(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 225
    invoke-direct {p0, p1, p2}, Lcom/google/protobuf/d;->e(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/protobuf/d;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/protobuf/e;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 21
    sget-object v0, Lcom/google/protobuf/d;->a:Lcom/google/protobuf/i;

    invoke-direct {p0, p1, v0}, Lcom/google/protobuf/d;->c(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/protobuf/d;->c(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/h;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 21
    sget-object v0, Lcom/google/protobuf/d;->a:Lcom/google/protobuf/i;

    invoke-direct {p0, p1, v0}, Lcom/google/protobuf/d;->c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/io/InputStream;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 21
    sget-object v0, Lcom/google/protobuf/d;->a:Lcom/google/protobuf/i;

    invoke-direct {p0, p1, v0}, Lcom/google/protobuf/d;->f(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/protobuf/d;->f(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a([B)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 21
    sget-object v0, Lcom/google/protobuf/d;->a:Lcom/google/protobuf/i;

    invoke-direct {p0, p1, v0}, Lcom/google/protobuf/d;->b([BLcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a([BLcom/google/protobuf/i;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/protobuf/d;->b([BLcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/protobuf/d;->c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/io/InputStream;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 21
    sget-object v0, Lcom/google/protobuf/d;->a:Lcom/google/protobuf/i;

    invoke-direct {p0, p1, v0}, Lcom/google/protobuf/d;->d(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/protobuf/d;->d(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method
