.class public final Lcom/google/protobuf/i;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static volatile a:Z

.field private static final c:Lcom/google/protobuf/i;


# instance fields
.field private final b:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x0

    .line 52
    sput-boolean v1, Lcom/google/protobuf/i;->a:Z

    .line 131
    new-instance v0, Lcom/google/protobuf/i;

    invoke-direct {v0, v1}, Lcom/google/protobuf/i;-><init>(B)V

    sput-object v0, Lcom/google/protobuf/i;->c:Lcom/google/protobuf/i;

    return-void
.end method

.method constructor <init>()V
    .registers 2

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/protobuf/i;->b:Ljava/util/Map;

    .line 113
    return-void
.end method

.method private constructor <init>(B)V
    .registers 3
    .parameter

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/i;->b:Ljava/util/Map;

    .line 130
    return-void
.end method

.method public static a()Lcom/google/protobuf/i;
    .registers 1

    .prologue
    .line 69
    sget-object v0, Lcom/google/protobuf/i;->c:Lcom/google/protobuf/i;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/ae;I)Lcom/google/protobuf/t;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/protobuf/i;->b:Ljava/util/Map;

    new-instance v1, Lcom/google/protobuf/j;

    invoke-direct {v1, p1, p2}, Lcom/google/protobuf/j;-><init>(Ljava/lang/Object;I)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/t;

    return-object v0
.end method
