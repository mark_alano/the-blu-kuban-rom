.class final Lcom/google/protobuf/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final d:Lcom/google/protobuf/k;


# instance fields
.field private final a:Lcom/google/protobuf/am;

.field private b:Z

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 74
    new-instance v0, Lcom/google/protobuf/k;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/protobuf/k;-><init>(B)V

    sput-object v0, Lcom/google/protobuf/k;->d:Lcom/google/protobuf/k;

    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/protobuf/k;->c:Z

    .line 49
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/protobuf/am;->a(I)Lcom/google/protobuf/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    .line 50
    return-void
.end method

.method private constructor <init>(B)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-boolean v0, p0, Lcom/google/protobuf/k;->c:Z

    .line 57
    invoke-static {v0}, Lcom/google/protobuf/am;->a(I)Lcom/google/protobuf/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    .line 58
    invoke-virtual {p0}, Lcom/google/protobuf/k;->b()V

    .line 59
    return-void
.end method

.method private static a(Lcom/google/protobuf/WireFormat$FieldType;ILjava/lang/Object;)I
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 751
    invoke-static {p1}, Lcom/google/protobuf/CodedOutputStream;->d(I)I

    move-result v0

    .line 752
    sget-object v1, Lcom/google/protobuf/WireFormat$FieldType;->GROUP:Lcom/google/protobuf/WireFormat$FieldType;

    if-ne p0, v1, :cond_a

    .line 753
    mul-int/lit8 v0, v0, 0x2

    .line 755
    :cond_a
    invoke-static {p0, p2}, Lcom/google/protobuf/k;->b(Lcom/google/protobuf/WireFormat$FieldType;Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method static a(Lcom/google/protobuf/WireFormat$FieldType;Z)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 437
    if-eqz p1, :cond_4

    .line 438
    const/4 v0, 0x2

    .line 440
    :goto_3
    return v0

    :cond_4
    invoke-virtual {p0}, Lcom/google/protobuf/WireFormat$FieldType;->getWireType()I

    move-result v0

    goto :goto_3
.end method

.method public static a()Lcom/google/protobuf/k;
    .registers 1

    .prologue
    .line 64
    new-instance v0, Lcom/google/protobuf/k;

    invoke-direct {v0}, Lcom/google/protobuf/k;-><init>()V

    return-object v0
.end method

.method public static a(Lcom/google/protobuf/h;Lcom/google/protobuf/WireFormat$FieldType;)Ljava/lang/Object;
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 512
    sget-object v0, Lcom/google/protobuf/l;->b:[I

    invoke-virtual {p1}, Lcom/google/protobuf/WireFormat$FieldType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_c2

    .line 542
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "There is no way to get here, but the compiler thinks otherwise."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 513
    :pswitch_13
    invoke-virtual {p0}, Lcom/google/protobuf/h;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 527
    :goto_1f
    return-object v0

    .line 514
    :pswitch_20
    invoke-virtual {p0}, Lcom/google/protobuf/h;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_1f

    .line 515
    :pswitch_2d
    invoke-virtual {p0}, Lcom/google/protobuf/h;->h()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1f

    .line 516
    :pswitch_36
    invoke-virtual {p0}, Lcom/google/protobuf/h;->h()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1f

    .line 517
    :pswitch_3f
    invoke-virtual {p0}, Lcom/google/protobuf/h;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1f

    .line 518
    :pswitch_48
    invoke-virtual {p0}, Lcom/google/protobuf/h;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1f

    .line 519
    :pswitch_51
    invoke-virtual {p0}, Lcom/google/protobuf/h;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1f

    .line 520
    :pswitch_5a
    invoke-virtual {p0}, Lcom/google/protobuf/h;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1f

    .line 521
    :pswitch_63
    invoke-virtual {p0}, Lcom/google/protobuf/h;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_1f

    .line 522
    :pswitch_68
    invoke-virtual {p0}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v0

    goto :goto_1f

    .line 523
    :pswitch_6d
    invoke-virtual {p0}, Lcom/google/protobuf/h;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1f

    .line 524
    :pswitch_76
    invoke-virtual {p0}, Lcom/google/protobuf/h;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1f

    .line 525
    :pswitch_7f
    invoke-virtual {p0}, Lcom/google/protobuf/h;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1f

    .line 526
    :pswitch_88
    invoke-virtual {p0}, Lcom/google/protobuf/h;->g()I

    move-result v0

    ushr-int/lit8 v1, v0, 0x1

    and-int/lit8 v0, v0, 0x1

    neg-int v0, v0

    xor-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1f

    .line 527
    :pswitch_97
    invoke-virtual {p0}, Lcom/google/protobuf/h;->h()J

    move-result-wide v0

    const/4 v2, 0x1

    ushr-long v2, v0, v2

    const-wide/16 v4, 0x1

    and-long/2addr v0, v4

    neg-long v0, v0

    xor-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_1f

    .line 530
    :pswitch_a9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "readPrimitiveField() cannot handle nested groups."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 533
    :pswitch_b1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "readPrimitiveField() cannot handle embedded messages."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 538
    :pswitch_b9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "readPrimitiveField() cannot handle enums."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 512
    nop

    :pswitch_data_c2
    .packed-switch 0x1
        :pswitch_13
        :pswitch_20
        :pswitch_2d
        :pswitch_36
        :pswitch_3f
        :pswitch_48
        :pswitch_51
        :pswitch_5a
        :pswitch_63
        :pswitch_68
        :pswitch_6d
        :pswitch_76
        :pswitch_7f
        :pswitch_88
        :pswitch_97
        :pswitch_a9
        :pswitch_b1
        :pswitch_b9
    .end packed-switch
.end method

.method private static a(Lcom/google/protobuf/WireFormat$FieldType;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 339
    if-nez p1, :cond_9

    .line 340
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 344
    :cond_9
    sget-object v1, Lcom/google/protobuf/l;->a:[I

    invoke-virtual {p0}, Lcom/google/protobuf/WireFormat$FieldType;->getJavaType()Lcom/google/protobuf/WireFormat$JavaType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protobuf/WireFormat$JavaType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_46

    .line 363
    :cond_18
    :goto_18
    if-nez v0, :cond_44

    .line 371
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong object type used with protocol message reflection."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :pswitch_22
    instance-of v0, p1, Ljava/lang/Integer;

    goto :goto_18

    .line 346
    :pswitch_25
    instance-of v0, p1, Ljava/lang/Long;

    goto :goto_18

    .line 347
    :pswitch_28
    instance-of v0, p1, Ljava/lang/Float;

    goto :goto_18

    .line 348
    :pswitch_2b
    instance-of v0, p1, Ljava/lang/Double;

    goto :goto_18

    .line 349
    :pswitch_2e
    instance-of v0, p1, Ljava/lang/Boolean;

    goto :goto_18

    .line 350
    :pswitch_31
    instance-of v0, p1, Ljava/lang/String;

    goto :goto_18

    .line 351
    :pswitch_34
    instance-of v0, p1, Lcom/google/protobuf/e;

    goto :goto_18

    .line 354
    :pswitch_37
    instance-of v0, p1, Lcom/google/protobuf/v;

    goto :goto_18

    .line 358
    :pswitch_3a
    instance-of v1, p1, Lcom/google/protobuf/ae;

    if-nez v1, :cond_42

    instance-of v1, p1, Lcom/google/protobuf/x;

    if-eqz v1, :cond_18

    :cond_42
    const/4 v0, 0x1

    goto :goto_18

    .line 374
    :cond_44
    return-void

    .line 344
    nop

    :pswitch_data_46
    .packed-switch 0x1
        :pswitch_22
        :pswitch_25
        :pswitch_28
        :pswitch_2b
        :pswitch_2e
        :pswitch_31
        :pswitch_34
        :pswitch_37
        :pswitch_3a
    .end packed-switch
.end method

.method private static a(Ljava/util/Map$Entry;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 403
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/m;

    .line 404
    invoke-interface {v0}, Lcom/google/protobuf/m;->c()Lcom/google/protobuf/WireFormat$JavaType;

    move-result-object v3

    sget-object v4, Lcom/google/protobuf/WireFormat$JavaType;->MESSAGE:Lcom/google/protobuf/WireFormat$JavaType;

    if-ne v3, v4, :cond_54

    .line 405
    invoke-interface {v0}, Lcom/google/protobuf/m;->d()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 407
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_20
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_54

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ae;

    .line 408
    invoke-interface {v0}, Lcom/google/protobuf/ae;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_20

    move v0, v1

    .line 426
    :goto_33
    return v0

    .line 413
    :cond_34
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 414
    instance-of v3, v0, Lcom/google/protobuf/ae;

    if-eqz v3, :cond_46

    .line 415
    check-cast v0, Lcom/google/protobuf/ae;

    invoke-interface {v0}, Lcom/google/protobuf/ae;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_54

    move v0, v1

    .line 416
    goto :goto_33

    .line 418
    :cond_46
    instance-of v0, v0, Lcom/google/protobuf/x;

    if-eqz v0, :cond_4c

    move v0, v2

    .line 419
    goto :goto_33

    .line 421
    :cond_4c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong object type used with protocol message reflection."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_54
    move v0, v2

    .line 426
    goto :goto_33
.end method

.method private static b(Lcom/google/protobuf/WireFormat$FieldType;Ljava/lang/Object;)I
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/16 v0, 0x8

    const/4 v1, 0x4

    .line 770
    sget-object v3, Lcom/google/protobuf/l;->b:[I

    invoke-virtual {p0}, Lcom/google/protobuf/WireFormat$FieldType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_c8

    .line 802
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "There is no way to get here, but the compiler thinks otherwise."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 773
    :pswitch_17
    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    .line 798
    :goto_1c
    return v0

    .line 774
    :pswitch_1d
    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move v0, v1

    goto :goto_1c

    .line 775
    :pswitch_24
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(J)I

    move-result v0

    goto :goto_1c

    .line 776
    :pswitch_2f
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(J)I

    move-result v0

    goto :goto_1c

    .line 777
    :pswitch_3a
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->c(I)I

    move-result v0

    goto :goto_1c

    .line 778
    :pswitch_45
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    goto :goto_1c

    .line 779
    :pswitch_4b
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move v0, v1

    goto :goto_1c

    .line 780
    :pswitch_52
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move v0, v2

    goto :goto_1c

    .line 781
    :pswitch_59
    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Lcom/google/protobuf/CodedOutputStream;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_1c

    .line 782
    :pswitch_60
    check-cast p1, Lcom/google/protobuf/ae;

    invoke-interface {p1}, Lcom/google/protobuf/ae;->getSerializedSize()I

    move-result v0

    goto :goto_1c

    .line 783
    :pswitch_67
    check-cast p1, Lcom/google/protobuf/e;

    invoke-static {p1}, Lcom/google/protobuf/CodedOutputStream;->a(Lcom/google/protobuf/e;)I

    move-result v0

    goto :goto_1c

    .line 784
    :pswitch_6e
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->f(I)I

    move-result v0

    goto :goto_1c

    .line 785
    :pswitch_79
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move v0, v1

    goto :goto_1c

    .line 786
    :pswitch_80
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    goto :goto_1c

    .line 787
    :pswitch_86
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    shl-int/lit8 v1, v0, 0x1

    shr-int/lit8 v0, v0, 0x1f

    xor-int/2addr v0, v1

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->f(I)I

    move-result v0

    goto :goto_1c

    .line 788
    :pswitch_96
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    shl-long v2, v0, v2

    const/16 v4, 0x3f

    shr-long/2addr v0, v4

    xor-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(J)I

    move-result v0

    goto/16 :goto_1c

    .line 791
    :pswitch_a8
    instance-of v0, p1, Lcom/google/protobuf/x;

    if-eqz v0, :cond_b4

    .line 792
    check-cast p1, Lcom/google/protobuf/x;

    invoke-static {p1}, Lcom/google/protobuf/CodedOutputStream;->a(Lcom/google/protobuf/x;)I

    move-result v0

    goto/16 :goto_1c

    .line 794
    :cond_b4
    check-cast p1, Lcom/google/protobuf/ae;

    invoke-static {p1}, Lcom/google/protobuf/CodedOutputStream;->a(Lcom/google/protobuf/ae;)I

    move-result v0

    goto/16 :goto_1c

    .line 798
    :pswitch_bc
    check-cast p1, Lcom/google/protobuf/v;

    invoke-interface {p1}, Lcom/google/protobuf/v;->getNumber()I

    move-result v0

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->c(I)I

    move-result v0

    goto/16 :goto_1c

    .line 770
    :pswitch_data_c8
    .packed-switch 0x1
        :pswitch_17
        :pswitch_1d
        :pswitch_24
        :pswitch_2f
        :pswitch_3a
        :pswitch_45
        :pswitch_4b
        :pswitch_52
        :pswitch_59
        :pswitch_67
        :pswitch_6e
        :pswitch_79
        :pswitch_80
        :pswitch_86
        :pswitch_96
        :pswitch_60
        :pswitch_a8
        :pswitch_bc
    .end packed-switch
.end method

.method private static b(Ljava/util/Map$Entry;)I
    .registers 8
    .parameter

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 721
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/m;

    .line 722
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 723
    invoke-interface {v0}, Lcom/google/protobuf/m;->c()Lcom/google/protobuf/WireFormat$JavaType;

    move-result-object v2

    sget-object v3, Lcom/google/protobuf/WireFormat$JavaType;->MESSAGE:Lcom/google/protobuf/WireFormat$JavaType;

    if-ne v2, v3, :cond_65

    invoke-interface {v0}, Lcom/google/protobuf/m;->d()Z

    move-result v2

    if-nez v2, :cond_65

    invoke-interface {v0}, Lcom/google/protobuf/m;->e()Z

    move-result v2

    if-nez v2, :cond_65

    .line 725
    instance-of v0, v1, Lcom/google/protobuf/x;

    if-eqz v0, :cond_48

    .line 726
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/m;

    invoke-interface {v0}, Lcom/google/protobuf/m;->a()I

    move-result v2

    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/x;

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->d(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v6}, Lcom/google/protobuf/CodedOutputStream;->d(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->a(Lcom/google/protobuf/x;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    .line 733
    :goto_47
    return v0

    .line 729
    :cond_48
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/m;

    invoke-interface {v0}, Lcom/google/protobuf/m;->a()I

    move-result v0

    check-cast v1, Lcom/google/protobuf/ae;

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->d(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->c(II)I

    move-result v0

    add-int/2addr v0, v2

    invoke-static {v6, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ae;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_47

    .line 733
    :cond_65
    invoke-static {v0, v1}, Lcom/google/protobuf/k;->c(Lcom/google/protobuf/m;Ljava/lang/Object;)I

    move-result v0

    goto :goto_47
.end method

.method private static c(Lcom/google/protobuf/m;Ljava/lang/Object;)I
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 811
    invoke-interface {p0}, Lcom/google/protobuf/m;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v1

    .line 812
    invoke-interface {p0}, Lcom/google/protobuf/m;->a()I

    move-result v2

    .line 813
    invoke-interface {p0}, Lcom/google/protobuf/m;->d()Z

    move-result v3

    if-eqz v3, :cond_4c

    .line 814
    invoke-interface {p0}, Lcom/google/protobuf/m;->e()Z

    move-result v3

    if-eqz v3, :cond_36

    .line 816
    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 817
    invoke-static {v1, v4}, Lcom/google/protobuf/k;->b(Lcom/google/protobuf/WireFormat$FieldType;Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1b

    .line 819
    :cond_2b
    invoke-static {v2}, Lcom/google/protobuf/CodedOutputStream;->d(I)I

    move-result v1

    add-int/2addr v1, v0

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->f(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 830
    :cond_35
    :goto_35
    return v0

    .line 824
    :cond_36
    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_35

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 825
    invoke-static {v1, v2, v4}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/WireFormat$FieldType;ILjava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_3c

    .line 830
    :cond_4c
    invoke-static {v1, v2, p1}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/WireFormat$FieldType;ILjava/lang/Object;)I

    move-result v0

    goto :goto_35
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/m;I)Ljava/lang/Object;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 268
    invoke-interface {p1}, Lcom/google/protobuf/m;->d()Z

    move-result v0

    if-nez v0, :cond_e

    .line 269
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "getRepeatedField() can only be called on repeated fields."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 273
    :cond_e
    invoke-virtual {p0, p1}, Lcom/google/protobuf/k;->b(Lcom/google/protobuf/m;)Ljava/lang/Object;

    move-result-object v0

    .line 275
    if-nez v0, :cond_1a

    .line 276
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 278
    :cond_1a
    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protobuf/m;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 209
    invoke-interface {p1}, Lcom/google/protobuf/m;->d()Z

    move-result v0

    if-eqz v0, :cond_40

    .line 210
    instance-of v0, p2, Ljava/util/List;

    if-nez v0, :cond_12

    .line 211
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong object type used with protocol message reflection."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 218
    check-cast p2, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 219
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_20
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_32

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 220
    invoke-interface {p1}, Lcom/google/protobuf/m;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/WireFormat$FieldType;Ljava/lang/Object;)V

    goto :goto_20

    :cond_32
    move-object p2, v0

    .line 227
    :goto_33
    instance-of v0, p2, Lcom/google/protobuf/x;

    if-eqz v0, :cond_3a

    .line 228
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/protobuf/k;->c:Z

    .line 230
    :cond_3a
    iget-object v0, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v0, p1, p2}, Lcom/google/protobuf/am;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    return-void

    .line 224
    :cond_40
    invoke-interface {p1}, Lcom/google/protobuf/m;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/WireFormat$FieldType;Ljava/lang/Object;)V

    goto :goto_33
.end method

.method public final a(Lcom/google/protobuf/m;)Z
    .registers 4
    .parameter

    .prologue
    .line 180
    invoke-interface {p1}, Lcom/google/protobuf/m;->d()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 181
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "hasField() can only be called on non-repeated fields."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_e
    iget-object v0, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v0, p1}, Lcom/google/protobuf/am;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_18

    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public final b(Lcom/google/protobuf/m;)Ljava/lang/Object;
    .registers 4
    .parameter

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v0, p1}, Lcom/google/protobuf/am;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 196
    instance-of v1, v0, Lcom/google/protobuf/x;

    if-eqz v1, :cond_10

    .line 197
    check-cast v0, Lcom/google/protobuf/x;

    invoke-virtual {v0}, Lcom/google/protobuf/x;->a()Lcom/google/protobuf/ae;

    move-result-object v0

    .line 199
    :cond_10
    return-object v0
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/google/protobuf/k;->b:Z

    if-eqz v0, :cond_5

    .line 84
    :goto_4
    return-void

    .line 82
    :cond_5
    iget-object v0, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v0}, Lcom/google/protobuf/am;->a()V

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/protobuf/k;->b:Z

    goto :goto_4
.end method

.method public final b(Lcom/google/protobuf/m;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 311
    invoke-interface {p1}, Lcom/google/protobuf/m;->d()Z

    move-result v0

    if-nez v0, :cond_e

    .line 312
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "addRepeatedField() can only be called on repeated fields."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 316
    :cond_e
    invoke-interface {p1}, Lcom/google/protobuf/m;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/WireFormat$FieldType;Ljava/lang/Object;)V

    .line 318
    invoke-virtual {p0, p1}, Lcom/google/protobuf/k;->b(Lcom/google/protobuf/m;)Ljava/lang/Object;

    move-result-object v0

    .line 320
    if-nez v0, :cond_29

    .line 321
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 322
    iget-object v1, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v1, p1, v0}, Lcom/google/protobuf/am;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    :goto_25
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 328
    return-void

    .line 324
    :cond_29
    check-cast v0, Ljava/util/List;

    goto :goto_25
.end method

.method public final c(Lcom/google/protobuf/m;)I
    .registers 4
    .parameter

    .prologue
    .line 249
    invoke-interface {p1}, Lcom/google/protobuf/m;->d()Z

    move-result v0

    if-nez v0, :cond_e

    .line 250
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "getRepeatedField() can only be called on repeated fields."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 254
    :cond_e
    invoke-virtual {p0, p1}, Lcom/google/protobuf/k;->b(Lcom/google/protobuf/m;)Ljava/lang/Object;

    move-result-object v0

    .line 255
    if-nez v0, :cond_16

    .line 256
    const/4 v0, 0x0

    .line 258
    :goto_15
    return v0

    :cond_16
    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_15
.end method

.method public final c()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/google/protobuf/k;->c:Z

    if-eqz v0, :cond_14

    .line 169
    new-instance v0, Lcom/google/protobuf/z;

    iget-object v1, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v1}, Lcom/google/protobuf/am;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/protobuf/z;-><init>(Ljava/util/Iterator;)V

    .line 172
    :goto_13
    return-object v0

    :cond_14
    iget-object v0, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v0}, Lcom/google/protobuf/am;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_13
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 5

    .prologue
    .line 22
    new-instance v2, Lcom/google/protobuf/k;

    invoke-direct {v2}, Lcom/google/protobuf/k;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_7
    iget-object v0, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v0}, Lcom/google/protobuf/am;->c()I

    move-result v0

    if-ge v1, v0, :cond_26

    iget-object v0, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v0, v1}, Lcom/google/protobuf/am;->b(I)Ljava/util/Map$Entry;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/m;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/m;Ljava/lang/Object;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    :cond_26
    iget-object v0, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v0}, Lcom/google/protobuf/am;->d()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_30
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/m;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/m;Ljava/lang/Object;)V

    goto :goto_30

    :cond_4a
    iget-boolean v0, p0, Lcom/google/protobuf/k;->c:Z

    iput-boolean v0, v2, Lcom/google/protobuf/k;->c:Z

    return-object v2
.end method

.method public final d()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    move v0, v1

    .line 386
    :goto_2
    iget-object v2, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v2}, Lcom/google/protobuf/am;->c()I

    move-result v2

    if-ge v0, v2, :cond_1a

    .line 387
    iget-object v2, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v2, v0}, Lcom/google/protobuf/am;->b(I)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-static {v2}, Lcom/google/protobuf/k;->a(Ljava/util/Map$Entry;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 397
    :goto_16
    return v1

    .line 386
    :cond_17
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 392
    :cond_1a
    iget-object v0, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v0}, Lcom/google/protobuf/am;->d()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_24
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 393
    invoke-static {v0}, Lcom/google/protobuf/k;->a(Ljava/util/Map$Entry;)Z

    move-result v0

    if-nez v0, :cond_24

    goto :goto_16

    .line 397
    :cond_37
    const/4 v1, 0x1

    goto :goto_16
.end method

.method public final e()I
    .registers 5

    .prologue
    const/4 v0, 0x0

    move v1, v0

    move v2, v0

    .line 692
    :goto_3
    iget-object v0, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v0}, Lcom/google/protobuf/am;->c()I

    move-result v0

    if-ge v1, v0, :cond_24

    .line 693
    iget-object v0, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v0, v1}, Lcom/google/protobuf/am;->b(I)Ljava/util/Map$Entry;

    move-result-object v3

    .line 695
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/m;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/protobuf/k;->c(Lcom/google/protobuf/m;Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v2, v0

    .line 692
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 698
    :cond_24
    iget-object v0, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v0}, Lcom/google/protobuf/am;->d()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 699
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/m;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/protobuf/k;->c(Lcom/google/protobuf/m;Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v2, v0

    goto :goto_2e

    .line 701
    :cond_4a
    return v2
.end method

.method public final f()I
    .registers 4

    .prologue
    const/4 v0, 0x0

    move v1, v0

    .line 709
    :goto_2
    iget-object v2, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v2}, Lcom/google/protobuf/am;->c()I

    move-result v2

    if-ge v0, v2, :cond_18

    .line 710
    iget-object v2, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v2, v0}, Lcom/google/protobuf/am;->b(I)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-static {v2}, Lcom/google/protobuf/k;->b(Ljava/util/Map$Entry;)I

    move-result v2

    add-int/2addr v1, v2

    .line 709
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 713
    :cond_18
    iget-object v0, p0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/am;

    invoke-virtual {v0}, Lcom/google/protobuf/am;->d()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_22
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 714
    invoke-static {v0}, Lcom/google/protobuf/k;->b(Ljava/util/Map$Entry;)I

    move-result v0

    add-int/2addr v1, v0

    goto :goto_22

    .line 716
    :cond_34
    return v1
.end method
