.class public final Lcom/google/zxing/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/zxing/e;


# static fields
.field private static final a:[Lcom/google/zxing/g;


# instance fields
.field private final b:Lcom/google/zxing/a/a/c;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 40
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/zxing/g;

    sput-object v0, Lcom/google/zxing/a/a;->a:[Lcom/google/zxing/g;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lcom/google/zxing/a/a/c;

    invoke-direct {v0}, Lcom/google/zxing/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/a/a;->b:Lcom/google/zxing/a/a/c;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/zxing/b;Ljava/util/Map;)Lcom/google/zxing/f;
    .registers 14
    .parameter
    .parameter

    .prologue
    .line 67
    if-eqz p2, :cond_83

    sget-object v0, Lcom/google/zxing/DecodeHintType;->PURE_BARCODE:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_83

    .line 68
    invoke-virtual {p1}, Lcom/google/zxing/b;->c()Lcom/google/zxing/common/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/zxing/common/b;->a()[I

    move-result-object v0

    if-nez v0, :cond_19

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_19
    const/4 v1, 0x0

    aget v3, v0, v1

    const/4 v1, 0x1

    aget v4, v0, v1

    const/4 v1, 0x2

    aget v5, v0, v1

    const/4 v1, 0x3

    aget v6, v0, v1

    new-instance v7, Lcom/google/zxing/common/b;

    const/16 v0, 0x1e

    const/16 v1, 0x21

    invoke-direct {v7, v0, v1}, Lcom/google/zxing/common/b;-><init>(II)V

    const/4 v0, 0x0

    move v1, v0

    :goto_30
    const/16 v0, 0x21

    if-ge v1, v0, :cond_60

    mul-int v0, v1, v6

    div-int/lit8 v8, v6, 0x2

    add-int/2addr v0, v8

    div-int/lit8 v0, v0, 0x21

    add-int v8, v4, v0

    const/4 v0, 0x0

    :goto_3e
    const/16 v9, 0x1e

    if-ge v0, v9, :cond_5c

    mul-int v9, v0, v5

    div-int/lit8 v10, v5, 0x2

    add-int/2addr v9, v10

    and-int/lit8 v10, v1, 0x1

    mul-int/2addr v10, v5

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x1e

    add-int/2addr v9, v3

    invoke-virtual {v2, v9, v8}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v9

    if-eqz v9, :cond_59

    invoke-virtual {v7, v0, v1}, Lcom/google/zxing/common/b;->b(II)V

    :cond_59
    add-int/lit8 v0, v0, 0x1

    goto :goto_3e

    :cond_5c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_30

    .line 69
    :cond_60
    iget-object v0, p0, Lcom/google/zxing/a/a;->b:Lcom/google/zxing/a/a/c;

    invoke-virtual {v0, v7}, Lcom/google/zxing/a/a/c;->a(Lcom/google/zxing/common/b;)Lcom/google/zxing/common/d;

    move-result-object v0

    .line 74
    sget-object v1, Lcom/google/zxing/a/a;->a:[Lcom/google/zxing/g;

    .line 75
    new-instance v2, Lcom/google/zxing/f;

    invoke-virtual {v0}, Lcom/google/zxing/common/d;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/zxing/common/d;->a()[B

    move-result-object v4

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->MAXICODE:Lcom/google/zxing/BarcodeFormat;

    invoke-direct {v2, v3, v4, v1, v5}, Lcom/google/zxing/f;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/g;Lcom/google/zxing/BarcodeFormat;)V

    .line 77
    invoke-virtual {v0}, Lcom/google/zxing/common/d;->d()Ljava/lang/String;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_82

    .line 79
    sget-object v1, Lcom/google/zxing/ResultMetadataType;->ERROR_CORRECTION_LEVEL:Lcom/google/zxing/ResultMetadataType;

    invoke-virtual {v2, v1, v0}, Lcom/google/zxing/f;->a(Lcom/google/zxing/ResultMetadataType;Ljava/lang/Object;)V

    .line 81
    :cond_82
    return-object v2

    .line 71
    :cond_83
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0
.end method

.method public final a()V
    .registers 1

    .prologue
    .line 87
    return-void
.end method
