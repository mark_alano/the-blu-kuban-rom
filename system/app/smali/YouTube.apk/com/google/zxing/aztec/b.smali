.class public final Lcom/google/zxing/aztec/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/zxing/e;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/zxing/b;Ljava/util/Map;)Lcom/google/zxing/f;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 60
    new-instance v0, Lcom/google/zxing/aztec/a/a;

    invoke-virtual {p1}, Lcom/google/zxing/b;->c()Lcom/google/zxing/common/b;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/zxing/aztec/a/a;-><init>(Lcom/google/zxing/common/b;)V

    invoke-virtual {v0}, Lcom/google/zxing/aztec/a/a;->a()Lcom/google/zxing/aztec/a;

    move-result-object v2

    .line 61
    invoke-virtual {v2}, Lcom/google/zxing/aztec/a;->e()[Lcom/google/zxing/g;

    move-result-object v3

    .line 63
    if-eqz p2, :cond_29

    .line 64
    sget-object v0, Lcom/google/zxing/DecodeHintType;->NEED_RESULT_POINT_CALLBACK:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/h;

    .line 65
    if-eqz v0, :cond_29

    .line 66
    array-length v4, v3

    const/4 v1, 0x0

    :goto_1f
    if-ge v1, v4, :cond_29

    aget-object v5, v3, v1

    .line 67
    invoke-interface {v0, v5}, Lcom/google/zxing/h;->a(Lcom/google/zxing/g;)V

    .line 66
    add-int/lit8 v1, v1, 0x1

    goto :goto_1f

    .line 72
    :cond_29
    new-instance v0, Lcom/google/zxing/aztec/decoder/Decoder;

    invoke-direct {v0}, Lcom/google/zxing/aztec/decoder/Decoder;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/zxing/aztec/decoder/Decoder;->a(Lcom/google/zxing/aztec/a;)Lcom/google/zxing/common/d;

    move-result-object v0

    .line 74
    new-instance v1, Lcom/google/zxing/f;

    invoke-virtual {v0}, Lcom/google/zxing/common/d;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/zxing/common/d;->a()[B

    move-result-object v4

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->AZTEC:Lcom/google/zxing/BarcodeFormat;

    invoke-direct {v1, v2, v4, v3, v5}, Lcom/google/zxing/f;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/g;Lcom/google/zxing/BarcodeFormat;)V

    .line 76
    invoke-virtual {v0}, Lcom/google/zxing/common/d;->c()Ljava/util/List;

    move-result-object v2

    .line 77
    if-eqz v2, :cond_4c

    .line 78
    sget-object v3, Lcom/google/zxing/ResultMetadataType;->BYTE_SEGMENTS:Lcom/google/zxing/ResultMetadataType;

    invoke-virtual {v1, v3, v2}, Lcom/google/zxing/f;->a(Lcom/google/zxing/ResultMetadataType;Ljava/lang/Object;)V

    .line 80
    :cond_4c
    invoke-virtual {v0}, Lcom/google/zxing/common/d;->d()Ljava/lang/String;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_57

    .line 82
    sget-object v2, Lcom/google/zxing/ResultMetadataType;->ERROR_CORRECTION_LEVEL:Lcom/google/zxing/ResultMetadataType;

    invoke-virtual {v1, v2, v0}, Lcom/google/zxing/f;->a(Lcom/google/zxing/ResultMetadataType;Ljava/lang/Object;)V

    .line 85
    :cond_57
    return-object v1
.end method

.method public final a()V
    .registers 1

    .prologue
    .line 91
    return-void
.end method
