.class public final Lcom/google/zxing/common/e;
.super Lcom/google/zxing/common/h;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/zxing/common/h;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/zxing/common/b;IIFFFFFFFFFFFFFFFF)Lcom/google/zxing/common/b;
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-static/range {p4 .. p19}, Lcom/google/zxing/common/j;->a(FFFFFFFFFFFFFFFF)Lcom/google/zxing/common/j;

    move-result-object v0

    .line 43
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/zxing/common/e;->a(Lcom/google/zxing/common/b;IILcom/google/zxing/common/j;)Lcom/google/zxing/common/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/zxing/common/b;IILcom/google/zxing/common/j;)Lcom/google/zxing/common/b;
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    if-lez p2, :cond_4

    if-gtz p3, :cond_9

    .line 52
    :cond_4
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1

    .line 54
    :cond_9
    new-instance v4, Lcom/google/zxing/common/b;

    move/from16 v0, p3

    invoke-direct {v4, p2, v0}, Lcom/google/zxing/common/b;-><init>(II)V

    .line 55
    shl-int/lit8 v1, p2, 0x1

    new-array v5, v1, [F

    .line 56
    const/4 v1, 0x0

    move v3, v1

    :goto_16
    move/from16 v0, p3

    if-ge v3, v0, :cond_f2

    .line 57
    array-length v6, v5

    .line 58
    int-to-float v1, v3

    const/high16 v2, 0x3f00

    add-float/2addr v2, v1

    .line 59
    const/4 v1, 0x0

    :goto_20
    if-ge v1, v6, :cond_31

    .line 60
    shr-int/lit8 v7, v1, 0x1

    int-to-float v7, v7

    const/high16 v8, 0x3f00

    add-float/2addr v7, v8

    aput v7, v5, v1

    .line 61
    add-int/lit8 v7, v1, 0x1

    aput v2, v5, v7

    .line 59
    add-int/lit8 v1, v1, 0x2

    goto :goto_20

    .line 63
    :cond_31
    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Lcom/google/zxing/common/j;->a([F)V

    .line 66
    invoke-virtual {p1}, Lcom/google/zxing/common/b;->d()I

    move-result v7

    invoke-virtual {p1}, Lcom/google/zxing/common/b;->e()I

    move-result v8

    const/4 v2, 0x1

    const/4 v1, 0x0

    :goto_40
    array-length v9, v5

    if-ge v1, v9, :cond_84

    if-eqz v2, :cond_84

    aget v2, v5, v1

    float-to-int v9, v2

    add-int/lit8 v2, v1, 0x1

    aget v2, v5, v2

    float-to-int v10, v2

    const/4 v2, -0x1

    if-lt v9, v2, :cond_57

    if-gt v9, v7, :cond_57

    const/4 v2, -0x1

    if-lt v10, v2, :cond_57

    if-le v10, v8, :cond_5c

    :cond_57
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1

    :cond_5c
    const/4 v2, 0x0

    const/4 v11, -0x1

    if-ne v9, v11, :cond_70

    const/4 v2, 0x0

    aput v2, v5, v1

    const/4 v2, 0x1

    :cond_64
    :goto_64
    const/4 v9, -0x1

    if-ne v10, v9, :cond_79

    add-int/lit8 v2, v1, 0x1

    const/4 v9, 0x0

    aput v9, v5, v2

    const/4 v2, 0x1

    :cond_6d
    :goto_6d
    add-int/lit8 v1, v1, 0x2

    goto :goto_40

    :cond_70
    if-ne v9, v7, :cond_64

    add-int/lit8 v2, v7, -0x1

    int-to-float v2, v2

    aput v2, v5, v1

    const/4 v2, 0x1

    goto :goto_64

    :cond_79
    if-ne v10, v8, :cond_6d

    add-int/lit8 v2, v1, 0x1

    add-int/lit8 v9, v8, -0x1

    int-to-float v9, v9

    aput v9, v5, v2

    const/4 v2, 0x1

    goto :goto_6d

    :cond_84
    const/4 v2, 0x1

    array-length v1, v5

    add-int/lit8 v1, v1, -0x2

    move v12, v1

    move v1, v2

    move v2, v12

    :goto_8b
    if-ltz v2, :cond_ce

    if-eqz v1, :cond_ce

    aget v1, v5, v2

    float-to-int v9, v1

    add-int/lit8 v1, v2, 0x1

    aget v1, v5, v1

    float-to-int v10, v1

    const/4 v1, -0x1

    if-lt v9, v1, :cond_a1

    if-gt v9, v7, :cond_a1

    const/4 v1, -0x1

    if-lt v10, v1, :cond_a1

    if-le v10, v8, :cond_a6

    :cond_a1
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1

    :cond_a6
    const/4 v1, 0x0

    const/4 v11, -0x1

    if-ne v9, v11, :cond_ba

    const/4 v1, 0x0

    aput v1, v5, v2

    const/4 v1, 0x1

    :cond_ae
    :goto_ae
    const/4 v9, -0x1

    if-ne v10, v9, :cond_c3

    add-int/lit8 v1, v2, 0x1

    const/4 v9, 0x0

    aput v9, v5, v1

    const/4 v1, 0x1

    :cond_b7
    :goto_b7
    add-int/lit8 v2, v2, -0x2

    goto :goto_8b

    :cond_ba
    if-ne v9, v7, :cond_ae

    add-int/lit8 v1, v7, -0x1

    int-to-float v1, v1

    aput v1, v5, v2

    const/4 v1, 0x1

    goto :goto_ae

    :cond_c3
    if-ne v10, v8, :cond_b7

    add-int/lit8 v1, v2, 0x1

    add-int/lit8 v9, v8, -0x1

    int-to-float v9, v9

    aput v9, v5, v1

    const/4 v1, 0x1

    goto :goto_b7

    .line 68
    :cond_ce
    const/4 v1, 0x0

    :goto_cf
    if-ge v1, v6, :cond_ed

    .line 69
    :try_start_d1
    aget v2, v5, v1

    float-to-int v2, v2

    add-int/lit8 v7, v1, 0x1

    aget v7, v5, v7

    float-to-int v7, v7

    invoke-virtual {p1, v2, v7}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v2

    if-eqz v2, :cond_e4

    .line 71
    shr-int/lit8 v2, v1, 0x1

    invoke-virtual {v4, v2, v3}, Lcom/google/zxing/common/b;->b(II)V
    :try_end_e4
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_d1 .. :try_end_e4} :catch_e7

    .line 68
    :cond_e4
    add-int/lit8 v1, v1, 0x2

    goto :goto_cf

    .line 82
    :catch_e7
    move-exception v1

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1

    .line 56
    :cond_ed
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_16

    .line 85
    :cond_f2
    return-object v4
.end method
