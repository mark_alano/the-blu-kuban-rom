.class public final Lcom/google/zxing/common/reedsolomon/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/zxing/common/reedsolomon/a;


# direct methods
.method public constructor <init>(Lcom/google/zxing/common/reedsolomon/a;)V
    .registers 2
    .parameter

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    .line 47
    return-void
.end method


# virtual methods
.method public final a([II)V
    .registers 15
    .parameter
    .parameter

    .prologue
    .line 59
    new-instance v3, Lcom/google/zxing/common/reedsolomon/b;

    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    invoke-direct {v3, v0, p1}, Lcom/google/zxing/common/reedsolomon/b;-><init>(Lcom/google/zxing/common/reedsolomon/a;[I)V

    .line 60
    new-array v4, p2, [I

    .line 61
    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    sget-object v1, Lcom/google/zxing/common/reedsolomon/a;->f:Lcom/google/zxing/common/reedsolomon/a;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 62
    const/4 v2, 0x1

    .line 63
    const/4 v1, 0x0

    :goto_13
    if-ge v1, p2, :cond_32

    .line 65
    iget-object v6, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    if-eqz v5, :cond_30

    add-int/lit8 v0, v1, 0x1

    :goto_1b
    invoke-virtual {v6, v0}, Lcom/google/zxing/common/reedsolomon/a;->a(I)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/google/zxing/common/reedsolomon/b;->b(I)I

    move-result v0

    .line 66
    array-length v6, v4

    add-int/lit8 v6, v6, -0x1

    sub-int/2addr v6, v1

    aput v0, v4, v6

    .line 67
    if-eqz v0, :cond_1b8

    .line 68
    const/4 v0, 0x0

    .line 63
    :goto_2c
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_13

    :cond_30
    move v0, v1

    .line 65
    goto :goto_1b

    .line 71
    :cond_32
    if-eqz v2, :cond_35

    .line 88
    :cond_34
    return-void

    .line 74
    :cond_35
    new-instance v1, Lcom/google/zxing/common/reedsolomon/b;

    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    invoke-direct {v1, v0, v4}, Lcom/google/zxing/common/reedsolomon/b;-><init>(Lcom/google/zxing/common/reedsolomon/a;[I)V

    .line 75
    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    const/4 v2, 0x1

    invoke-virtual {v0, p2, v2}, Lcom/google/zxing/common/reedsolomon/a;->a(II)Lcom/google/zxing/common/reedsolomon/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/common/reedsolomon/b;->a()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/zxing/common/reedsolomon/b;->a()I

    move-result v3

    if-ge v2, v3, :cond_1b3

    :goto_4d
    iget-object v2, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    invoke-virtual {v2}, Lcom/google/zxing/common/reedsolomon/a;->a()Lcom/google/zxing/common/reedsolomon/b;

    move-result-object v3

    iget-object v2, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    invoke-virtual {v2}, Lcom/google/zxing/common/reedsolomon/a;->b()Lcom/google/zxing/common/reedsolomon/b;

    move-result-object v2

    move-object v4, v3

    move-object v3, v0

    :goto_5b
    invoke-virtual {v3}, Lcom/google/zxing/common/reedsolomon/b;->a()I

    move-result v0

    div-int/lit8 v6, p2, 0x2

    if-lt v0, v6, :cond_cd

    invoke-virtual {v3}, Lcom/google/zxing/common/reedsolomon/b;->b()Z

    move-result v0

    if-eqz v0, :cond_71

    new-instance v0, Lcom/google/zxing/common/reedsolomon/ReedSolomonException;

    const-string v1, "r_{i-1} was zero"

    invoke-direct {v0, v1}, Lcom/google/zxing/common/reedsolomon/ReedSolomonException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_71
    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    invoke-virtual {v0}, Lcom/google/zxing/common/reedsolomon/a;->a()Lcom/google/zxing/common/reedsolomon/b;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/zxing/common/reedsolomon/b;->a()I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/google/zxing/common/reedsolomon/b;->a(I)I

    move-result v6

    iget-object v7, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    invoke-virtual {v7, v6}, Lcom/google/zxing/common/reedsolomon/a;->c(I)I

    move-result v6

    :goto_85
    invoke-virtual {v1}, Lcom/google/zxing/common/reedsolomon/b;->a()I

    move-result v7

    invoke-virtual {v3}, Lcom/google/zxing/common/reedsolomon/b;->a()I

    move-result v8

    if-lt v7, v8, :cond_bf

    invoke-virtual {v1}, Lcom/google/zxing/common/reedsolomon/b;->b()Z

    move-result v7

    if-nez v7, :cond_bf

    invoke-virtual {v1}, Lcom/google/zxing/common/reedsolomon/b;->a()I

    move-result v7

    invoke-virtual {v3}, Lcom/google/zxing/common/reedsolomon/b;->a()I

    move-result v8

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    invoke-virtual {v1}, Lcom/google/zxing/common/reedsolomon/b;->a()I

    move-result v9

    invoke-virtual {v1, v9}, Lcom/google/zxing/common/reedsolomon/b;->a(I)I

    move-result v9

    invoke-virtual {v8, v9, v6}, Lcom/google/zxing/common/reedsolomon/a;->c(II)I

    move-result v8

    iget-object v9, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    invoke-virtual {v9, v7, v8}, Lcom/google/zxing/common/reedsolomon/a;->a(II)Lcom/google/zxing/common/reedsolomon/b;

    move-result-object v9

    invoke-virtual {v0, v9}, Lcom/google/zxing/common/reedsolomon/b;->a(Lcom/google/zxing/common/reedsolomon/b;)Lcom/google/zxing/common/reedsolomon/b;

    move-result-object v0

    invoke-virtual {v3, v7, v8}, Lcom/google/zxing/common/reedsolomon/b;->a(II)Lcom/google/zxing/common/reedsolomon/b;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/google/zxing/common/reedsolomon/b;->a(Lcom/google/zxing/common/reedsolomon/b;)Lcom/google/zxing/common/reedsolomon/b;

    move-result-object v1

    goto :goto_85

    :cond_bf
    invoke-virtual {v0, v2}, Lcom/google/zxing/common/reedsolomon/b;->b(Lcom/google/zxing/common/reedsolomon/b;)Lcom/google/zxing/common/reedsolomon/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/zxing/common/reedsolomon/b;->a(Lcom/google/zxing/common/reedsolomon/b;)Lcom/google/zxing/common/reedsolomon/b;

    move-result-object v0

    move-object v4, v2

    move-object v2, v0

    move-object v11, v3

    move-object v3, v1

    move-object v1, v11

    goto :goto_5b

    :cond_cd
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/zxing/common/reedsolomon/b;->a(I)I

    move-result v0

    if-nez v0, :cond_dc

    new-instance v0, Lcom/google/zxing/common/reedsolomon/ReedSolomonException;

    const-string v1, "sigmaTilde(0) was zero"

    invoke-direct {v0, v1}, Lcom/google/zxing/common/reedsolomon/ReedSolomonException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_dc
    iget-object v1, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    invoke-virtual {v1, v0}, Lcom/google/zxing/common/reedsolomon/a;->c(I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/zxing/common/reedsolomon/b;->c(I)Lcom/google/zxing/common/reedsolomon/b;

    move-result-object v1

    invoke-virtual {v3, v0}, Lcom/google/zxing/common/reedsolomon/b;->c(I)Lcom/google/zxing/common/reedsolomon/b;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/zxing/common/reedsolomon/b;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v1, 0x1

    aput-object v0, v2, v1

    .line 77
    const/4 v0, 0x0

    aget-object v3, v2, v0

    .line 78
    const/4 v0, 0x1

    aget-object v6, v2, v0

    .line 79
    invoke-virtual {v3}, Lcom/google/zxing/common/reedsolomon/b;->a()I

    move-result v4

    const/4 v0, 0x1

    if-ne v4, v0, :cond_13a

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Lcom/google/zxing/common/reedsolomon/b;->a(I)I

    move-result v2

    aput v2, v0, v1

    .line 80
    :goto_10b
    array-length v7, v0

    new-array v8, v7, [I

    const/4 v1, 0x0

    move v3, v1

    :goto_110
    if-ge v3, v7, :cond_18c

    iget-object v1, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    aget v2, v0, v3

    invoke-virtual {v1, v2}, Lcom/google/zxing/common/reedsolomon/a;->c(I)I

    move-result v9

    const/4 v2, 0x1

    const/4 v1, 0x0

    move v4, v1

    move v1, v2

    :goto_11e
    if-ge v4, v7, :cond_16a

    if-eq v3, v4, :cond_136

    iget-object v2, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    aget v10, v0, v4

    invoke-virtual {v2, v10, v9}, Lcom/google/zxing/common/reedsolomon/a;->c(II)I

    move-result v2

    and-int/lit8 v10, v2, 0x1

    if-nez v10, :cond_167

    or-int/lit8 v2, v2, 0x1

    :goto_130
    iget-object v10, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    invoke-virtual {v10, v1, v2}, Lcom/google/zxing/common/reedsolomon/a;->c(II)I

    move-result v1

    :cond_136
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_11e

    .line 79
    :cond_13a
    new-array v2, v4, [I

    const/4 v1, 0x0

    const/4 v0, 0x1

    :goto_13e
    iget-object v7, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    invoke-virtual {v7}, Lcom/google/zxing/common/reedsolomon/a;->c()I

    move-result v7

    if-ge v0, v7, :cond_15b

    if-ge v1, v4, :cond_15b

    invoke-virtual {v3, v0}, Lcom/google/zxing/common/reedsolomon/b;->b(I)I

    move-result v7

    if-nez v7, :cond_158

    iget-object v7, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    invoke-virtual {v7, v0}, Lcom/google/zxing/common/reedsolomon/a;->c(I)I

    move-result v7

    aput v7, v2, v1

    add-int/lit8 v1, v1, 0x1

    :cond_158
    add-int/lit8 v0, v0, 0x1

    goto :goto_13e

    :cond_15b
    if-eq v1, v4, :cond_165

    new-instance v0, Lcom/google/zxing/common/reedsolomon/ReedSolomonException;

    const-string v1, "Error locator degree does not match number of roots"

    invoke-direct {v0, v1}, Lcom/google/zxing/common/reedsolomon/ReedSolomonException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_165
    move-object v0, v2

    goto :goto_10b

    .line 80
    :cond_167
    and-int/lit8 v2, v2, -0x2

    goto :goto_130

    :cond_16a
    iget-object v2, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    invoke-virtual {v6, v9}, Lcom/google/zxing/common/reedsolomon/b;->b(I)I

    move-result v4

    iget-object v10, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    invoke-virtual {v10, v1}, Lcom/google/zxing/common/reedsolomon/a;->c(I)I

    move-result v1

    invoke-virtual {v2, v4, v1}, Lcom/google/zxing/common/reedsolomon/a;->c(II)I

    move-result v1

    aput v1, v8, v3

    if-eqz v5, :cond_188

    iget-object v1, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    aget v2, v8, v3

    invoke-virtual {v1, v2, v9}, Lcom/google/zxing/common/reedsolomon/a;->c(II)I

    move-result v1

    aput v1, v8, v3

    :cond_188
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_110

    .line 81
    :cond_18c
    const/4 v1, 0x0

    :goto_18d
    array-length v2, v0

    if-ge v1, v2, :cond_34

    .line 82
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lcom/google/zxing/common/reedsolomon/c;->a:Lcom/google/zxing/common/reedsolomon/a;

    aget v4, v0, v1

    invoke-virtual {v3, v4}, Lcom/google/zxing/common/reedsolomon/a;->b(I)I

    move-result v3

    sub-int/2addr v2, v3

    .line 83
    if-gez v2, :cond_1a6

    .line 84
    new-instance v0, Lcom/google/zxing/common/reedsolomon/ReedSolomonException;

    const-string v1, "Bad error location"

    invoke-direct {v0, v1}, Lcom/google/zxing/common/reedsolomon/ReedSolomonException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_1a6
    aget v3, p1, v2

    aget v4, v8, v1

    invoke-static {v3, v4}, Lcom/google/zxing/common/reedsolomon/a;->b(II)I

    move-result v3

    aput v3, p1, v2

    .line 81
    add-int/lit8 v1, v1, 0x1

    goto :goto_18d

    :cond_1b3
    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    goto/16 :goto_4d

    :cond_1b8
    move v0, v2

    goto/16 :goto_2c
.end method
