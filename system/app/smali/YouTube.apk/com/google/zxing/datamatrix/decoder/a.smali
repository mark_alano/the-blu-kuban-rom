.class final Lcom/google/zxing/datamatrix/decoder/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/zxing/common/b;

.field private final b:Lcom/google/zxing/common/b;

.field private final c:Lcom/google/zxing/datamatrix/decoder/e;


# direct methods
.method constructor <init>(Lcom/google/zxing/common/b;)V
    .registers 5
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p1}, Lcom/google/zxing/common/b;->e()I

    move-result v0

    .line 37
    const/16 v1, 0x8

    if-lt v0, v1, :cond_13

    const/16 v1, 0x90

    if-gt v0, v1, :cond_13

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_18

    .line 38
    :cond_13
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 41
    :cond_18
    invoke-virtual {p1}, Lcom/google/zxing/common/b;->e()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/zxing/common/b;->d()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/zxing/datamatrix/decoder/e;->a(II)Lcom/google/zxing/datamatrix/decoder/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/zxing/datamatrix/decoder/a;->c:Lcom/google/zxing/datamatrix/decoder/e;

    .line 42
    invoke-direct {p0, p1}, Lcom/google/zxing/datamatrix/decoder/a;->a(Lcom/google/zxing/common/b;)Lcom/google/zxing/common/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/zxing/datamatrix/decoder/a;->a:Lcom/google/zxing/common/b;

    .line 43
    new-instance v0, Lcom/google/zxing/common/b;

    iget-object v1, p0, Lcom/google/zxing/datamatrix/decoder/a;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v1}, Lcom/google/zxing/common/b;->d()I

    move-result v1

    iget-object v2, p0, Lcom/google/zxing/datamatrix/decoder/a;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v2}, Lcom/google/zxing/common/b;->e()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/common/b;-><init>(II)V

    iput-object v0, p0, Lcom/google/zxing/datamatrix/decoder/a;->b:Lcom/google/zxing/common/b;

    .line 44
    return-void
.end method

.method private a(Lcom/google/zxing/common/b;)Lcom/google/zxing/common/b;
    .registers 16
    .parameter

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/zxing/datamatrix/decoder/a;->c:Lcom/google/zxing/datamatrix/decoder/e;

    invoke-virtual {v0}, Lcom/google/zxing/datamatrix/decoder/e;->b()I

    move-result v0

    .line 404
    iget-object v1, p0, Lcom/google/zxing/datamatrix/decoder/a;->c:Lcom/google/zxing/datamatrix/decoder/e;

    invoke-virtual {v1}, Lcom/google/zxing/datamatrix/decoder/e;->c()I

    move-result v1

    .line 406
    invoke-virtual {p1}, Lcom/google/zxing/common/b;->e()I

    move-result v2

    if-eq v2, v0, :cond_1a

    .line 407
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Dimension of bitMarix must match the version size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 410
    :cond_1a
    iget-object v2, p0, Lcom/google/zxing/datamatrix/decoder/a;->c:Lcom/google/zxing/datamatrix/decoder/e;

    invoke-virtual {v2}, Lcom/google/zxing/datamatrix/decoder/e;->d()I

    move-result v4

    .line 411
    iget-object v2, p0, Lcom/google/zxing/datamatrix/decoder/a;->c:Lcom/google/zxing/datamatrix/decoder/e;

    invoke-virtual {v2}, Lcom/google/zxing/datamatrix/decoder/e;->e()I

    move-result v5

    .line 413
    div-int v6, v0, v4

    .line 414
    div-int v7, v1, v5

    .line 416
    mul-int v0, v6, v4

    .line 417
    mul-int v1, v7, v5

    .line 419
    new-instance v8, Lcom/google/zxing/common/b;

    invoke-direct {v8, v1, v0}, Lcom/google/zxing/common/b;-><init>(II)V

    .line 420
    const/4 v0, 0x0

    move v3, v0

    :goto_35
    if-ge v3, v6, :cond_6f

    .line 421
    mul-int v9, v3, v4

    .line 422
    const/4 v0, 0x0

    move v2, v0

    :goto_3b
    if-ge v2, v7, :cond_6b

    .line 423
    mul-int v10, v2, v5

    .line 424
    const/4 v0, 0x0

    move v1, v0

    :goto_41
    if-ge v1, v4, :cond_67

    .line 425
    add-int/lit8 v0, v4, 0x2

    mul-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x1

    add-int v11, v0, v1

    .line 426
    add-int v12, v9, v1

    .line 427
    const/4 v0, 0x0

    :goto_4d
    if-ge v0, v5, :cond_63

    .line 428
    add-int/lit8 v13, v5, 0x2

    mul-int/2addr v13, v2

    add-int/lit8 v13, v13, 0x1

    add-int/2addr v13, v0

    .line 429
    invoke-virtual {p1, v13, v11}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v13

    if-eqz v13, :cond_60

    .line 430
    add-int v13, v10, v0

    .line 431
    invoke-virtual {v8, v13, v12}, Lcom/google/zxing/common/b;->b(II)V

    .line 427
    :cond_60
    add-int/lit8 v0, v0, 0x1

    goto :goto_4d

    .line 424
    :cond_63
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_41

    .line 422
    :cond_67
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3b

    .line 420
    :cond_6b
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_35

    .line 437
    :cond_6f
    return-object v8
.end method

.method private a(IIII)Z
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 156
    if-gez p1, :cond_21

    .line 157
    add-int v1, p1, p3

    .line 158
    add-int/lit8 v0, p3, 0x4

    and-int/lit8 v0, v0, 0x7

    rsub-int/lit8 v0, v0, 0x4

    add-int/2addr v0, p2

    .line 160
    :goto_b
    if-gez v0, :cond_15

    .line 161
    add-int/2addr v0, p4

    .line 162
    add-int/lit8 v2, p4, 0x4

    and-int/lit8 v2, v2, 0x7

    rsub-int/lit8 v2, v2, 0x4

    add-int/2addr v1, v2

    .line 164
    :cond_15
    iget-object v2, p0, Lcom/google/zxing/datamatrix/decoder/a;->b:Lcom/google/zxing/common/b;

    invoke-virtual {v2, v0, v1}, Lcom/google/zxing/common/b;->b(II)V

    .line 165
    iget-object v2, p0, Lcom/google/zxing/datamatrix/decoder/a;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v2, v0, v1}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v0

    return v0

    :cond_21
    move v0, p2

    move v1, p1

    goto :goto_b
.end method

.method private b(IIII)I
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 180
    const/4 v0, 0x0

    .line 181
    add-int/lit8 v1, p1, -0x2

    add-int/lit8 v2, p2, -0x2

    invoke-direct {p0, v1, v2, p3, p4}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 182
    const/4 v0, 0x1

    .line 184
    :cond_c
    shl-int/lit8 v0, v0, 0x1

    .line 185
    add-int/lit8 v1, p1, -0x2

    add-int/lit8 v2, p2, -0x1

    invoke-direct {p0, v1, v2, p3, p4}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 186
    or-int/lit8 v0, v0, 0x1

    .line 188
    :cond_1a
    shl-int/lit8 v0, v0, 0x1

    .line 189
    add-int/lit8 v1, p1, -0x1

    add-int/lit8 v2, p2, -0x2

    invoke-direct {p0, v1, v2, p3, p4}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 190
    or-int/lit8 v0, v0, 0x1

    .line 192
    :cond_28
    shl-int/lit8 v0, v0, 0x1

    .line 193
    add-int/lit8 v1, p1, -0x1

    add-int/lit8 v2, p2, -0x1

    invoke-direct {p0, v1, v2, p3, p4}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_36

    .line 194
    or-int/lit8 v0, v0, 0x1

    .line 196
    :cond_36
    shl-int/lit8 v0, v0, 0x1

    .line 197
    add-int/lit8 v1, p1, -0x1

    invoke-direct {p0, v1, p2, p3, p4}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_42

    .line 198
    or-int/lit8 v0, v0, 0x1

    .line 200
    :cond_42
    shl-int/lit8 v0, v0, 0x1

    .line 201
    add-int/lit8 v1, p2, -0x2

    invoke-direct {p0, p1, v1, p3, p4}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_4e

    .line 202
    or-int/lit8 v0, v0, 0x1

    .line 204
    :cond_4e
    shl-int/lit8 v0, v0, 0x1

    .line 205
    add-int/lit8 v1, p2, -0x1

    invoke-direct {p0, p1, v1, p3, p4}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_5a

    .line 206
    or-int/lit8 v0, v0, 0x1

    .line 208
    :cond_5a
    shl-int/lit8 v0, v0, 0x1

    .line 209
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_64

    .line 210
    or-int/lit8 v0, v0, 0x1

    .line 212
    :cond_64
    return v0
.end method


# virtual methods
.method final a()Lcom/google/zxing/datamatrix/decoder/e;
    .registers 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/zxing/datamatrix/decoder/a;->c:Lcom/google/zxing/datamatrix/decoder/e;

    return-object v0
.end method

.method final b()[B
    .registers 16

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/zxing/datamatrix/decoder/a;->c:Lcom/google/zxing/datamatrix/decoder/e;

    invoke-virtual {v0}, Lcom/google/zxing/datamatrix/decoder/e;->f()I

    move-result v0

    new-array v9, v0, [B

    .line 78
    const/4 v6, 0x0

    .line 80
    const/4 v5, 0x4

    .line 81
    const/4 v4, 0x0

    .line 83
    iget-object v0, p0, Lcom/google/zxing/datamatrix/decoder/a;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v0}, Lcom/google/zxing/common/b;->e()I

    move-result v10

    .line 84
    iget-object v0, p0, Lcom/google/zxing/datamatrix/decoder/a;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v0}, Lcom/google/zxing/common/b;->d()I

    move-result v11

    .line 86
    const/4 v3, 0x0

    .line 87
    const/4 v2, 0x0

    .line 88
    const/4 v1, 0x0

    .line 89
    const/4 v0, 0x0

    move v7, v4

    move v8, v6

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v0

    .line 94
    :goto_21
    if-ne v5, v10, :cond_ad

    if-nez v7, :cond_ad

    if-nez v4, :cond_ad

    .line 95
    add-int/lit8 v6, v8, 0x1

    const/4 v0, 0x0

    add-int/lit8 v4, v10, -0x1

    const/4 v12, 0x0

    invoke-direct {p0, v4, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v4

    if-eqz v4, :cond_34

    const/4 v0, 0x1

    :cond_34
    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v4, v10, -0x1

    const/4 v12, 0x1

    invoke-direct {p0, v4, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v4

    if-eqz v4, :cond_41

    or-int/lit8 v0, v0, 0x1

    :cond_41
    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v4, v10, -0x1

    const/4 v12, 0x2

    invoke-direct {p0, v4, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v4

    if-eqz v4, :cond_4e

    or-int/lit8 v0, v0, 0x1

    :cond_4e
    shl-int/lit8 v0, v0, 0x1

    const/4 v4, 0x0

    add-int/lit8 v12, v11, -0x2

    invoke-direct {p0, v4, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v4

    if-eqz v4, :cond_5b

    or-int/lit8 v0, v0, 0x1

    :cond_5b
    shl-int/lit8 v0, v0, 0x1

    const/4 v4, 0x0

    add-int/lit8 v12, v11, -0x1

    invoke-direct {p0, v4, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v4

    if-eqz v4, :cond_68

    or-int/lit8 v0, v0, 0x1

    :cond_68
    shl-int/lit8 v0, v0, 0x1

    const/4 v4, 0x1

    add-int/lit8 v12, v11, -0x1

    invoke-direct {p0, v4, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v4

    if-eqz v4, :cond_75

    or-int/lit8 v0, v0, 0x1

    :cond_75
    shl-int/lit8 v0, v0, 0x1

    const/4 v4, 0x2

    add-int/lit8 v12, v11, -0x1

    invoke-direct {p0, v4, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v4

    if-eqz v4, :cond_82

    or-int/lit8 v0, v0, 0x1

    :cond_82
    shl-int/lit8 v0, v0, 0x1

    const/4 v4, 0x3

    add-int/lit8 v12, v11, -0x1

    invoke-direct {p0, v4, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v4

    if-eqz v4, :cond_8f

    or-int/lit8 v0, v0, 0x1

    :cond_8f
    int-to-byte v0, v0

    aput-byte v0, v9, v8

    .line 96
    add-int/lit8 v5, v5, -0x2

    .line 97
    add-int/lit8 v4, v7, 0x2

    .line 98
    const/4 v0, 0x1

    move v13, v1

    move v1, v2

    move v2, v3

    move v3, v0

    move v0, v13

    .line 137
    :goto_9c
    if-lt v5, v10, :cond_28d

    if-lt v4, v11, :cond_28d

    .line 139
    iget-object v0, p0, Lcom/google/zxing/datamatrix/decoder/a;->c:Lcom/google/zxing/datamatrix/decoder/e;

    invoke-virtual {v0}, Lcom/google/zxing/datamatrix/decoder/e;->f()I

    move-result v0

    if-eq v6, v0, :cond_28c

    .line 140
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 99
    :cond_ad
    add-int/lit8 v0, v10, -0x2

    if-ne v5, v0, :cond_132

    if-nez v7, :cond_132

    and-int/lit8 v0, v11, 0x3

    if-eqz v0, :cond_132

    if-nez v3, :cond_132

    .line 100
    add-int/lit8 v6, v8, 0x1

    const/4 v0, 0x0

    add-int/lit8 v3, v10, -0x3

    const/4 v12, 0x0

    invoke-direct {p0, v3, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v3

    if-eqz v3, :cond_c6

    const/4 v0, 0x1

    :cond_c6
    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v3, v10, -0x2

    const/4 v12, 0x0

    invoke-direct {p0, v3, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v3

    if-eqz v3, :cond_d3

    or-int/lit8 v0, v0, 0x1

    :cond_d3
    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v3, v10, -0x1

    const/4 v12, 0x0

    invoke-direct {p0, v3, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v3

    if-eqz v3, :cond_e0

    or-int/lit8 v0, v0, 0x1

    :cond_e0
    shl-int/lit8 v0, v0, 0x1

    const/4 v3, 0x0

    add-int/lit8 v12, v11, -0x4

    invoke-direct {p0, v3, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v3

    if-eqz v3, :cond_ed

    or-int/lit8 v0, v0, 0x1

    :cond_ed
    shl-int/lit8 v0, v0, 0x1

    const/4 v3, 0x0

    add-int/lit8 v12, v11, -0x3

    invoke-direct {p0, v3, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v3

    if-eqz v3, :cond_fa

    or-int/lit8 v0, v0, 0x1

    :cond_fa
    shl-int/lit8 v0, v0, 0x1

    const/4 v3, 0x0

    add-int/lit8 v12, v11, -0x2

    invoke-direct {p0, v3, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v3

    if-eqz v3, :cond_107

    or-int/lit8 v0, v0, 0x1

    :cond_107
    shl-int/lit8 v0, v0, 0x1

    const/4 v3, 0x0

    add-int/lit8 v12, v11, -0x1

    invoke-direct {p0, v3, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v3

    if-eqz v3, :cond_114

    or-int/lit8 v0, v0, 0x1

    :cond_114
    shl-int/lit8 v0, v0, 0x1

    const/4 v3, 0x1

    add-int/lit8 v12, v11, -0x1

    invoke-direct {p0, v3, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v3

    if-eqz v3, :cond_121

    or-int/lit8 v0, v0, 0x1

    :cond_121
    int-to-byte v0, v0

    aput-byte v0, v9, v8

    .line 101
    add-int/lit8 v5, v5, -0x2

    .line 102
    add-int/lit8 v3, v7, 0x2

    .line 103
    const/4 v0, 0x1

    move v13, v1

    move v1, v2

    move v2, v0

    move v0, v13

    move v14, v4

    move v4, v3

    move v3, v14

    goto/16 :goto_9c

    .line 104
    :cond_132
    add-int/lit8 v0, v10, 0x4

    if-ne v5, v0, :cond_1b9

    const/4 v0, 0x2

    if-ne v7, v0, :cond_1b9

    and-int/lit8 v0, v11, 0x7

    if-nez v0, :cond_1b9

    if-nez v2, :cond_1b9

    .line 105
    add-int/lit8 v6, v8, 0x1

    const/4 v0, 0x0

    add-int/lit8 v2, v10, -0x1

    const/4 v12, 0x0

    invoke-direct {p0, v2, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v2

    if-eqz v2, :cond_14c

    const/4 v0, 0x1

    :cond_14c
    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v2, v10, -0x1

    add-int/lit8 v12, v11, -0x1

    invoke-direct {p0, v2, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v2

    if-eqz v2, :cond_15a

    or-int/lit8 v0, v0, 0x1

    :cond_15a
    shl-int/lit8 v0, v0, 0x1

    const/4 v2, 0x0

    add-int/lit8 v12, v11, -0x3

    invoke-direct {p0, v2, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v2

    if-eqz v2, :cond_167

    or-int/lit8 v0, v0, 0x1

    :cond_167
    shl-int/lit8 v0, v0, 0x1

    const/4 v2, 0x0

    add-int/lit8 v12, v11, -0x2

    invoke-direct {p0, v2, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v2

    if-eqz v2, :cond_174

    or-int/lit8 v0, v0, 0x1

    :cond_174
    shl-int/lit8 v0, v0, 0x1

    const/4 v2, 0x0

    add-int/lit8 v12, v11, -0x1

    invoke-direct {p0, v2, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v2

    if-eqz v2, :cond_181

    or-int/lit8 v0, v0, 0x1

    :cond_181
    shl-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    add-int/lit8 v12, v11, -0x3

    invoke-direct {p0, v2, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v2

    if-eqz v2, :cond_18e

    or-int/lit8 v0, v0, 0x1

    :cond_18e
    shl-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    add-int/lit8 v12, v11, -0x2

    invoke-direct {p0, v2, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v2

    if-eqz v2, :cond_19b

    or-int/lit8 v0, v0, 0x1

    :cond_19b
    shl-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    add-int/lit8 v12, v11, -0x1

    invoke-direct {p0, v2, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v2

    if-eqz v2, :cond_1a8

    or-int/lit8 v0, v0, 0x1

    :cond_1a8
    int-to-byte v0, v0

    aput-byte v0, v9, v8

    .line 106
    add-int/lit8 v5, v5, -0x2

    .line 107
    add-int/lit8 v2, v7, 0x2

    .line 108
    const/4 v0, 0x1

    move v13, v1

    move v1, v0

    move v0, v13

    move v14, v3

    move v3, v4

    move v4, v2

    move v2, v14

    goto/16 :goto_9c

    .line 109
    :cond_1b9
    add-int/lit8 v0, v10, -0x2

    if-ne v5, v0, :cond_29c

    if-nez v7, :cond_29c

    and-int/lit8 v0, v11, 0x7

    const/4 v6, 0x4

    if-ne v0, v6, :cond_29c

    if-nez v1, :cond_29c

    .line 110
    add-int/lit8 v6, v8, 0x1

    const/4 v0, 0x0

    add-int/lit8 v1, v10, -0x3

    const/4 v12, 0x0

    invoke-direct {p0, v1, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_1d3

    const/4 v0, 0x1

    :cond_1d3
    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v10, -0x2

    const/4 v12, 0x0

    invoke-direct {p0, v1, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_1e0

    or-int/lit8 v0, v0, 0x1

    :cond_1e0
    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v10, -0x1

    const/4 v12, 0x0

    invoke-direct {p0, v1, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_1ed

    or-int/lit8 v0, v0, 0x1

    :cond_1ed
    shl-int/lit8 v0, v0, 0x1

    const/4 v1, 0x0

    add-int/lit8 v12, v11, -0x2

    invoke-direct {p0, v1, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_1fa

    or-int/lit8 v0, v0, 0x1

    :cond_1fa
    shl-int/lit8 v0, v0, 0x1

    const/4 v1, 0x0

    add-int/lit8 v12, v11, -0x1

    invoke-direct {p0, v1, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_207

    or-int/lit8 v0, v0, 0x1

    :cond_207
    shl-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    add-int/lit8 v12, v11, -0x1

    invoke-direct {p0, v1, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_214

    or-int/lit8 v0, v0, 0x1

    :cond_214
    shl-int/lit8 v0, v0, 0x1

    const/4 v1, 0x2

    add-int/lit8 v12, v11, -0x1

    invoke-direct {p0, v1, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_221

    or-int/lit8 v0, v0, 0x1

    :cond_221
    shl-int/lit8 v0, v0, 0x1

    const/4 v1, 0x3

    add-int/lit8 v12, v11, -0x1

    invoke-direct {p0, v1, v12, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->a(IIII)Z

    move-result v1

    if-eqz v1, :cond_22e

    or-int/lit8 v0, v0, 0x1

    :cond_22e
    int-to-byte v0, v0

    aput-byte v0, v9, v8

    .line 111
    add-int/lit8 v5, v5, -0x2

    .line 112
    add-int/lit8 v1, v7, 0x2

    .line 113
    const/4 v0, 0x1

    move v13, v2

    move v2, v3

    move v3, v4

    move v4, v1

    move v1, v13

    goto/16 :goto_9c

    :cond_23d
    move v5, v0

    move v0, v6

    .line 117
    :goto_23f
    if-ge v7, v10, :cond_29a

    if-ltz v5, :cond_29a

    iget-object v6, p0, Lcom/google/zxing/datamatrix/decoder/a;->b:Lcom/google/zxing/common/b;

    invoke-virtual {v6, v5, v7}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v6

    if-nez v6, :cond_29a

    .line 118
    add-int/lit8 v6, v0, 0x1

    invoke-direct {p0, v7, v5, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->b(IIII)I

    move-result v8

    int-to-byte v8, v8

    aput-byte v8, v9, v0

    .line 120
    :goto_254
    add-int/lit8 v7, v7, -0x2

    .line 121
    add-int/lit8 v0, v5, 0x2

    .line 122
    if-ltz v7, :cond_25c

    if-lt v0, v11, :cond_23d

    .line 123
    :cond_25c
    add-int/lit8 v5, v7, 0x1

    .line 124
    add-int/lit8 v0, v0, 0x3

    move v7, v5

    move v5, v0

    move v0, v6

    .line 128
    :goto_263
    if-ltz v7, :cond_298

    if-ge v5, v11, :cond_298

    iget-object v6, p0, Lcom/google/zxing/datamatrix/decoder/a;->b:Lcom/google/zxing/common/b;

    invoke-virtual {v6, v5, v7}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v6

    if-nez v6, :cond_298

    .line 129
    add-int/lit8 v6, v0, 0x1

    invoke-direct {p0, v7, v5, v10, v11}, Lcom/google/zxing/datamatrix/decoder/a;->b(IIII)I

    move-result v8

    int-to-byte v8, v8

    aput-byte v8, v9, v0

    .line 131
    :goto_278
    add-int/lit8 v7, v7, 0x2

    .line 132
    add-int/lit8 v0, v5, -0x2

    .line 133
    if-ge v7, v10, :cond_280

    if-gez v0, :cond_295

    .line 134
    :cond_280
    add-int/lit8 v5, v7, 0x3

    .line 135
    add-int/lit8 v0, v0, 0x1

    move v13, v1

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v0

    move v0, v13

    goto/16 :goto_9c

    .line 142
    :cond_28c
    return-object v9

    :cond_28d
    move v7, v4

    move v8, v6

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v0

    goto/16 :goto_21

    :cond_295
    move v5, v0

    move v0, v6

    goto :goto_263

    :cond_298
    move v6, v0

    goto :goto_278

    :cond_29a
    move v6, v0

    goto :goto_254

    :cond_29c
    move v0, v8

    move v13, v5

    move v5, v7

    move v7, v13

    goto :goto_23f
.end method
