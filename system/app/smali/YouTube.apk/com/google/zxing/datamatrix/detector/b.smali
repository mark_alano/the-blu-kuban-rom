.class final Lcom/google/zxing/datamatrix/detector/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/zxing/g;

.field private final b:Lcom/google/zxing/g;

.field private final c:I


# direct methods
.method private constructor <init>(Lcom/google/zxing/g;Lcom/google/zxing/g;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406
    iput-object p1, p0, Lcom/google/zxing/datamatrix/detector/b;->a:Lcom/google/zxing/g;

    .line 407
    iput-object p2, p0, Lcom/google/zxing/datamatrix/detector/b;->b:Lcom/google/zxing/g;

    .line 408
    iput p3, p0, Lcom/google/zxing/datamatrix/detector/b;->c:I

    .line 409
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/zxing/g;Lcom/google/zxing/g;IB)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 399
    invoke-direct {p0, p1, p2, p3}, Lcom/google/zxing/datamatrix/detector/b;-><init>(Lcom/google/zxing/g;Lcom/google/zxing/g;I)V

    return-void
.end method


# virtual methods
.method final a()Lcom/google/zxing/g;
    .registers 2

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/zxing/datamatrix/detector/b;->a:Lcom/google/zxing/g;

    return-object v0
.end method

.method final b()Lcom/google/zxing/g;
    .registers 2

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/zxing/datamatrix/detector/b;->b:Lcom/google/zxing/g;

    return-object v0
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 420
    iget v0, p0, Lcom/google/zxing/datamatrix/detector/b;->c:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 425
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/zxing/datamatrix/detector/b;->a:Lcom/google/zxing/g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/zxing/datamatrix/detector/b;->b:Lcom/google/zxing/g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/zxing/datamatrix/detector/b;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
