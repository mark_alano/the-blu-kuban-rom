.class public Lcom/google/zxing/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:F

.field private final b:F


# direct methods
.method public constructor <init>(FF)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput p1, p0, Lcom/google/zxing/g;->a:F

    .line 34
    iput p2, p0, Lcom/google/zxing/g;->b:F

    .line 35
    return-void
.end method

.method public static a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 119
    iget v0, p0, Lcom/google/zxing/g;->a:F

    iget v1, p0, Lcom/google/zxing/g;->b:F

    iget v2, p1, Lcom/google/zxing/g;->a:F

    iget v3, p1, Lcom/google/zxing/g;->b:F

    invoke-static {v0, v1, v2, v3}, Lcom/google/zxing/common/a/a;->a(FFFF)F

    move-result v0

    return v0
.end method

.method public static a([Lcom/google/zxing/g;)V
    .registers 12
    .parameter

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 77
    aget-object v0, p0, v7

    aget-object v1, p0, v8

    invoke-static {v0, v1}, Lcom/google/zxing/g;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v0

    .line 78
    aget-object v1, p0, v8

    aget-object v2, p0, v9

    invoke-static {v1, v2}, Lcom/google/zxing/g;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v1

    .line 79
    aget-object v2, p0, v7

    aget-object v3, p0, v9

    invoke-static {v2, v3}, Lcom/google/zxing/g;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v2

    .line 85
    cmpl-float v3, v1, v0

    if-ltz v3, :cond_4b

    cmpl-float v3, v1, v2

    if-ltz v3, :cond_4b

    .line 86
    aget-object v1, p0, v7

    .line 87
    aget-object v2, p0, v8

    .line 88
    aget-object v0, p0, v9

    .line 103
    :goto_29
    iget v3, v1, Lcom/google/zxing/g;->a:F

    iget v4, v1, Lcom/google/zxing/g;->b:F

    iget v5, v0, Lcom/google/zxing/g;->a:F

    sub-float/2addr v5, v3

    iget v6, v2, Lcom/google/zxing/g;->b:F

    sub-float/2addr v6, v4

    mul-float/2addr v5, v6

    iget v6, v0, Lcom/google/zxing/g;->b:F

    sub-float v4, v6, v4

    iget v6, v2, Lcom/google/zxing/g;->a:F

    sub-float v3, v6, v3

    mul-float/2addr v3, v4

    sub-float v3, v5, v3

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_61

    .line 109
    :goto_44
    aput-object v0, p0, v7

    .line 110
    aput-object v1, p0, v8

    .line 111
    aput-object v2, p0, v9

    .line 112
    return-void

    .line 89
    :cond_4b
    cmpl-float v1, v2, v1

    if-ltz v1, :cond_5a

    cmpl-float v0, v2, v0

    if-ltz v0, :cond_5a

    .line 90
    aget-object v1, p0, v8

    .line 91
    aget-object v2, p0, v7

    .line 92
    aget-object v0, p0, v9

    goto :goto_29

    .line 94
    :cond_5a
    aget-object v1, p0, v9

    .line 95
    aget-object v2, p0, v7

    .line 96
    aget-object v0, p0, v8

    goto :goto_29

    :cond_61
    move-object v10, v0

    move-object v0, v2

    move-object v2, v10

    goto :goto_44
.end method


# virtual methods
.method public final a()F
    .registers 2

    .prologue
    .line 38
    iget v0, p0, Lcom/google/zxing/g;->a:F

    return v0
.end method

.method public final b()F
    .registers 2

    .prologue
    .line 42
    iget v0, p0, Lcom/google/zxing/g;->b:F

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 47
    instance-of v1, p1, Lcom/google/zxing/g;

    if-eqz v1, :cond_18

    .line 48
    check-cast p1, Lcom/google/zxing/g;

    .line 49
    iget v1, p0, Lcom/google/zxing/g;->a:F

    iget v2, p1, Lcom/google/zxing/g;->a:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_18

    iget v1, p0, Lcom/google/zxing/g;->b:F

    iget v2, p1, Lcom/google/zxing/g;->b:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_18

    const/4 v0, 0x1

    .line 51
    :cond_18
    return v0
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 56
    iget v0, p0, Lcom/google/zxing/g;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/zxing/g;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x19

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 62
    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 63
    iget v1, p0, Lcom/google/zxing/g;->a:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 64
    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 65
    iget v1, p0, Lcom/google/zxing/g;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 66
    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 67
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
