.class Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder$ModuleSizeComparator;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Comparator;


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/zxing/multi/qrcode/detector/a;)V
    .registers 2
    .parameter

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder$ModuleSizeComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/zxing/qrcode/detector/d;Lcom/google/zxing/qrcode/detector/d;)I
    .registers 8
    .parameter
    .parameter

    .prologue
    const-wide/16 v3, 0x0

    .line 82
    invoke-virtual {p2}, Lcom/google/zxing/qrcode/detector/d;->c()F

    move-result v0

    invoke-virtual {p1}, Lcom/google/zxing/qrcode/detector/d;->c()F

    move-result v1

    sub-float/2addr v0, v1

    .line 83
    float-to-double v1, v0

    cmpg-double v1, v1, v3

    if-gez v1, :cond_12

    const/4 v0, -0x1

    :goto_11
    return v0

    :cond_12
    float-to-double v0, v0

    cmpl-double v0, v0, v3

    if-lez v0, :cond_19

    const/4 v0, 0x1

    goto :goto_11

    :cond_19
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 79
    check-cast p1, Lcom/google/zxing/qrcode/detector/d;

    check-cast p2, Lcom/google/zxing/qrcode/detector/d;

    invoke-virtual {p0, p1, p2}, Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder$ModuleSizeComparator;->compare(Lcom/google/zxing/qrcode/detector/d;Lcom/google/zxing/qrcode/detector/d;)I

    move-result v0

    return v0
.end method
