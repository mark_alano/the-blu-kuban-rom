.class public final Lcom/google/zxing/oned/d;
.super Lcom/google/zxing/oned/k;
.source "SourceFile"


# static fields
.field private static final a:[C

.field private static final b:[I

.field private static final c:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 40
    const-string v0, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/zxing/oned/d;->a:[C

    .line 46
    const/16 v0, 0x30

    new-array v0, v0, [I

    fill-array-data v0, :array_18

    .line 54
    sput-object v0, Lcom/google/zxing/oned/d;->b:[I

    const/16 v1, 0x2f

    aget v0, v0, v1

    sput v0, Lcom/google/zxing/oned/d;->c:I

    return-void

    .line 46
    :array_18
    .array-data 0x4
        0x14t 0x1t 0x0t 0x0t
        0x48t 0x1t 0x0t 0x0t
        0x44t 0x1t 0x0t 0x0t
        0x42t 0x1t 0x0t 0x0t
        0x28t 0x1t 0x0t 0x0t
        0x24t 0x1t 0x0t 0x0t
        0x22t 0x1t 0x0t 0x0t
        0x50t 0x1t 0x0t 0x0t
        0x12t 0x1t 0x0t 0x0t
        0xat 0x1t 0x0t 0x0t
        0xa8t 0x1t 0x0t 0x0t
        0xa4t 0x1t 0x0t 0x0t
        0xa2t 0x1t 0x0t 0x0t
        0x94t 0x1t 0x0t 0x0t
        0x92t 0x1t 0x0t 0x0t
        0x8at 0x1t 0x0t 0x0t
        0x68t 0x1t 0x0t 0x0t
        0x64t 0x1t 0x0t 0x0t
        0x62t 0x1t 0x0t 0x0t
        0x34t 0x1t 0x0t 0x0t
        0x1at 0x1t 0x0t 0x0t
        0x58t 0x1t 0x0t 0x0t
        0x4ct 0x1t 0x0t 0x0t
        0x46t 0x1t 0x0t 0x0t
        0x2ct 0x1t 0x0t 0x0t
        0x16t 0x1t 0x0t 0x0t
        0xb4t 0x1t 0x0t 0x0t
        0xb2t 0x1t 0x0t 0x0t
        0xact 0x1t 0x0t 0x0t
        0xa6t 0x1t 0x0t 0x0t
        0x96t 0x1t 0x0t 0x0t
        0x9at 0x1t 0x0t 0x0t
        0x6ct 0x1t 0x0t 0x0t
        0x66t 0x1t 0x0t 0x0t
        0x36t 0x1t 0x0t 0x0t
        0x3at 0x1t 0x0t 0x0t
        0x2et 0x1t 0x0t 0x0t
        0xd4t 0x1t 0x0t 0x0t
        0xd2t 0x1t 0x0t 0x0t
        0xcat 0x1t 0x0t 0x0t
        0x6et 0x1t 0x0t 0x0t
        0x76t 0x1t 0x0t 0x0t
        0xaet 0x1t 0x0t 0x0t
        0x26t 0x1t 0x0t 0x0t
        0xdat 0x1t 0x0t 0x0t
        0xd6t 0x1t 0x0t 0x0t
        0x32t 0x1t 0x0t 0x0t
        0x5et 0x1t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/zxing/oned/k;-><init>()V

    return-void
.end method

.method private static a(I)C
    .registers 3
    .parameter

    .prologue
    .line 175
    const/4 v0, 0x0

    :goto_1
    sget-object v1, Lcom/google/zxing/oned/d;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_14

    .line 176
    sget-object v1, Lcom/google/zxing/oned/d;->b:[I

    aget v1, v1, v0

    if-ne v1, p0, :cond_11

    .line 177
    sget-object v1, Lcom/google/zxing/oned/d;->a:[C

    aget-char v0, v1, v0

    return v0

    .line 175
    :cond_11
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 180
    :cond_14
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0
.end method

.method private static a([I)I
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 148
    array-length v7, p0

    .line 150
    array-length v3, p0

    move v0, v2

    move v6, v2

    :goto_5
    if-ge v0, v3, :cond_e

    aget v1, p0, v0

    .line 151
    add-int/2addr v1, v6

    .line 150
    add-int/lit8 v0, v0, 0x1

    move v6, v1

    goto :goto_5

    :cond_e
    move v5, v2

    move v0, v2

    .line 154
    :goto_10
    if-ge v5, v7, :cond_2b

    .line 155
    aget v1, p0, v5

    shl-int/lit8 v1, v1, 0x8

    mul-int/lit8 v1, v1, 0x9

    div-int v3, v1, v6

    .line 156
    shr-int/lit8 v1, v3, 0x8

    .line 157
    and-int/lit16 v3, v3, 0xff

    const/16 v4, 0x7f

    if-le v3, v4, :cond_41

    .line 158
    add-int/lit8 v1, v1, 0x1

    move v4, v1

    .line 160
    :goto_25
    if-lez v4, :cond_2a

    const/4 v1, 0x4

    if-le v4, v1, :cond_2c

    .line 161
    :cond_2a
    const/4 v0, -0x1

    .line 171
    :cond_2b
    return v0

    .line 163
    :cond_2c
    and-int/lit8 v1, v5, 0x1

    if-nez v1, :cond_3c

    move v1, v2

    .line 164
    :goto_31
    if-ge v1, v4, :cond_3d

    .line 165
    shl-int/lit8 v0, v0, 0x1

    or-int/lit8 v3, v0, 0x1

    .line 164
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    goto :goto_31

    .line 168
    :cond_3c
    shl-int/2addr v0, v4

    .line 154
    :cond_3d
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_10

    :cond_41
    move v4, v1

    goto :goto_25
.end method

.method private static a(Ljava/lang/CharSequence;)Ljava/lang/String;
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x0

    const/16 v7, 0x5a

    const/16 v6, 0x41

    .line 184
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 185
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    move v2, v1

    .line 186
    :goto_f
    if-ge v2, v3, :cond_86

    .line 187
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 188
    const/16 v5, 0x61

    if-lt v0, v5, :cond_81

    const/16 v5, 0x64

    if-gt v0, v5, :cond_81

    .line 189
    add-int/lit8 v5, v3, -0x1

    if-lt v2, v5, :cond_26

    .line 190
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 192
    :cond_26
    add-int/lit8 v5, v2, 0x1

    invoke-interface {p0, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    .line 194
    packed-switch v0, :pswitch_data_8c

    move v0, v1

    .line 232
    :goto_30
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 234
    add-int/lit8 v0, v2, 0x1

    .line 186
    :goto_35
    add-int/lit8 v2, v0, 0x1

    goto :goto_f

    .line 197
    :pswitch_38
    if-lt v5, v6, :cond_40

    if-gt v5, v7, :cond_40

    .line 198
    add-int/lit8 v0, v5, 0x20

    int-to-char v0, v0

    goto :goto_30

    .line 200
    :cond_40
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 205
    :pswitch_45
    if-lt v5, v6, :cond_4d

    if-gt v5, v7, :cond_4d

    .line 206
    add-int/lit8 v0, v5, -0x40

    int-to-char v0, v0

    goto :goto_30

    .line 208
    :cond_4d
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 213
    :pswitch_52
    if-lt v5, v6, :cond_5c

    const/16 v0, 0x45

    if-gt v5, v0, :cond_5c

    .line 214
    add-int/lit8 v0, v5, -0x26

    int-to-char v0, v0

    goto :goto_30

    .line 215
    :cond_5c
    const/16 v0, 0x46

    if-lt v5, v0, :cond_68

    const/16 v0, 0x57

    if-gt v5, v0, :cond_68

    .line 216
    add-int/lit8 v0, v5, -0xb

    int-to-char v0, v0

    goto :goto_30

    .line 218
    :cond_68
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 223
    :pswitch_6d
    if-lt v5, v6, :cond_77

    const/16 v0, 0x4f

    if-gt v5, v0, :cond_77

    .line 224
    add-int/lit8 v0, v5, -0x20

    int-to-char v0, v0

    goto :goto_30

    .line 225
    :cond_77
    if-ne v5, v7, :cond_7c

    .line 226
    const/16 v0, 0x3a

    goto :goto_30

    .line 228
    :cond_7c
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 236
    :cond_81
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v2

    goto :goto_35

    .line 239
    :cond_86
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 194
    nop

    :pswitch_data_8c
    .packed-switch 0x61
        :pswitch_45
        :pswitch_52
        :pswitch_6d
        :pswitch_38
    .end packed-switch
.end method

.method private static a(Ljava/lang/CharSequence;II)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 251
    const/4 v2, 0x0

    .line 252
    add-int/lit8 v0, p1, -0x1

    move v4, v1

    move v6, v2

    move v2, v0

    move v0, v6

    :goto_8
    if-ltz v2, :cond_20

    .line 253
    const-string v3, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*"

    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    mul-int/2addr v3, v4

    add-int/2addr v3, v0

    .line 254
    add-int/lit8 v0, v4, 0x1

    if-le v0, p2, :cond_1b

    move v0, v1

    .line 252
    :cond_1b
    add-int/lit8 v2, v2, -0x1

    move v4, v0

    move v0, v3

    goto :goto_8

    .line 258
    :cond_20
    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    sget-object v2, Lcom/google/zxing/oned/d;->a:[C

    rem-int/lit8 v0, v0, 0x2f

    aget-char v0, v2, v0

    if-eq v1, v0, :cond_31

    .line 259
    invoke-static {}, Lcom/google/zxing/ChecksumException;->getChecksumInstance()Lcom/google/zxing/ChecksumException;

    move-result-object v0

    throw v0

    .line 261
    :cond_31
    return-void
.end method


# virtual methods
.method public final a(ILcom/google/zxing/common/a;Ljava/util/Map;)Lcom/google/zxing/f;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    .line 60
    invoke-virtual {p2}, Lcom/google/zxing/common/a;->a()I

    move-result v4

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/google/zxing/common/a;->c(I)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x6

    new-array v5, v2, [I

    const/4 v2, 0x0

    array-length v6, v5

    move v3, v0

    :goto_10
    if-ge v3, v4, :cond_81

    invoke-virtual {p2, v3}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v7

    xor-int/2addr v7, v2

    if-eqz v7, :cond_22

    aget v7, v5, v1

    add-int/lit8 v7, v7, 0x1

    aput v7, v5, v1

    :goto_1f
    add-int/lit8 v3, v3, 0x1

    goto :goto_10

    :cond_22
    add-int/lit8 v7, v6, -0x1

    if-ne v1, v7, :cond_7c

    invoke-static {v5}, Lcom/google/zxing/oned/d;->a([I)I

    move-result v7

    sget v8, Lcom/google/zxing/oned/d;->c:I

    if-ne v7, v8, :cond_5a

    const/4 v1, 0x2

    new-array v4, v1, [I

    const/4 v1, 0x0

    aput v0, v4, v1

    const/4 v0, 0x1

    aput v3, v4, v0

    .line 62
    const/4 v0, 0x1

    aget v0, v4, v0

    invoke-virtual {p2, v0}, Lcom/google/zxing/common/a;->c(I)I

    move-result v0

    .line 63
    invoke-virtual {p2}, Lcom/google/zxing/common/a;->a()I

    move-result v3

    .line 65
    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v1, 0x14

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 66
    const/4 v1, 0x6

    new-array v6, v1, [I

    .line 70
    :goto_4c
    invoke-static {p2, v0, v6}, Lcom/google/zxing/oned/d;->a(Lcom/google/zxing/common/a;I[I)V

    .line 71
    invoke-static {v6}, Lcom/google/zxing/oned/d;->a([I)I

    move-result v1

    .line 72
    if-gez v1, :cond_86

    .line 73
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 60
    :cond_5a
    const/4 v7, 0x0

    aget v7, v5, v7

    const/4 v8, 0x1

    aget v8, v5, v8

    add-int/2addr v7, v8

    add-int/2addr v0, v7

    const/4 v7, 0x2

    const/4 v8, 0x0

    add-int/lit8 v9, v6, -0x2

    invoke-static {v5, v7, v5, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v7, v6, -0x2

    const/4 v8, 0x0

    aput v8, v5, v7

    add-int/lit8 v7, v6, -0x1

    const/4 v8, 0x0

    aput v8, v5, v7

    add-int/lit8 v1, v1, -0x1

    :goto_75
    const/4 v7, 0x1

    aput v7, v5, v1

    if-nez v2, :cond_7f

    const/4 v2, 0x1

    goto :goto_1f

    :cond_7c
    add-int/lit8 v1, v1, 0x1

    goto :goto_75

    :cond_7f
    const/4 v2, 0x0

    goto :goto_1f

    :cond_81
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 75
    :cond_86
    invoke-static {v1}, Lcom/google/zxing/oned/d;->a(I)C

    move-result v7

    .line 76
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 78
    array-length v8, v6

    const/4 v1, 0x0

    move v2, v0

    :goto_90
    if-ge v1, v8, :cond_98

    aget v9, v6, v1

    .line 79
    add-int/2addr v2, v9

    .line 78
    add-int/lit8 v1, v1, 0x1

    goto :goto_90

    .line 82
    :cond_98
    invoke-virtual {p2, v2}, Lcom/google/zxing/common/a;->c(I)I

    move-result v1

    .line 83
    const/16 v2, 0x2a

    if-ne v7, v2, :cond_10f

    .line 84
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 87
    if-eq v1, v3, :cond_b1

    invoke-virtual {p2, v1}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v2

    if-nez v2, :cond_b6

    .line 88
    :cond_b1
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 91
    :cond_b6
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_c2

    .line 93
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 96
    :cond_c2
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/lit8 v3, v2, -0x2

    const/16 v6, 0x14

    invoke-static {v5, v3, v6}, Lcom/google/zxing/oned/d;->a(Ljava/lang/CharSequence;II)V

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0xf

    invoke-static {v5, v2, v3}, Lcom/google/zxing/oned/d;->a(Ljava/lang/CharSequence;II)V

    .line 98
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 100
    invoke-static {v5}, Lcom/google/zxing/oned/d;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 102
    const/4 v3, 0x1

    aget v3, v4, v3

    const/4 v5, 0x0

    aget v4, v4, v5

    add-int/2addr v3, v4

    int-to-float v3, v3

    const/high16 v4, 0x4000

    div-float/2addr v3, v4

    .line 103
    add-int/2addr v0, v1

    int-to-float v0, v0

    const/high16 v1, 0x4000

    div-float/2addr v0, v1

    .line 104
    new-instance v1, Lcom/google/zxing/f;

    const/4 v4, 0x0

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/zxing/g;

    const/4 v6, 0x0

    new-instance v7, Lcom/google/zxing/g;

    int-to-float v8, p1

    invoke-direct {v7, v3, v8}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v7, v5, v6

    const/4 v3, 0x1

    new-instance v6, Lcom/google/zxing/g;

    int-to-float v7, p1

    invoke-direct {v6, v0, v7}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, v5, v3

    sget-object v0, Lcom/google/zxing/BarcodeFormat;->CODE_93:Lcom/google/zxing/BarcodeFormat;

    invoke-direct {v1, v2, v4, v5, v0}, Lcom/google/zxing/f;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/g;Lcom/google/zxing/BarcodeFormat;)V

    return-object v1

    :cond_10f
    move v0, v1

    goto/16 :goto_4c
.end method
