.class public abstract Lcom/google/zxing/oned/p;
.super Lcom/google/zxing/oned/k;
.source "SourceFile"


# static fields
.field static final b:[I

.field static final c:[I

.field static final d:[[I

.field static final e:[[I


# instance fields
.field private final a:Ljava/lang/StringBuilder;

.field private final f:Lcom/google/zxing/oned/o;

.field private final g:Lcom/google/zxing/oned/g;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x3

    const/16 v0, 0xa

    const/4 v1, 0x0

    const/4 v5, 0x4

    .line 53
    new-array v2, v6, [I

    fill-array-data v2, :array_9a

    sput-object v2, Lcom/google/zxing/oned/p;->b:[I

    .line 58
    new-array v2, v7, [I

    fill-array-data v2, :array_a4

    sput-object v2, Lcom/google/zxing/oned/p;->c:[I

    .line 63
    new-array v2, v0, [[I

    new-array v3, v5, [I

    fill-array-data v3, :array_b2

    aput-object v3, v2, v1

    const/4 v3, 0x1

    new-array v4, v5, [I

    fill-array-data v4, :array_be

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-array v4, v5, [I

    fill-array-data v4, :array_ca

    aput-object v4, v2, v3

    new-array v3, v5, [I

    fill-array-data v3, :array_d6

    aput-object v3, v2, v6

    new-array v3, v5, [I

    fill-array-data v3, :array_e2

    aput-object v3, v2, v5

    new-array v3, v5, [I

    fill-array-data v3, :array_ee

    aput-object v3, v2, v7

    const/4 v3, 0x6

    new-array v4, v5, [I

    fill-array-data v4, :array_fa

    aput-object v4, v2, v3

    const/4 v3, 0x7

    new-array v4, v5, [I

    fill-array-data v4, :array_106

    aput-object v4, v2, v3

    const/16 v3, 0x8

    new-array v4, v5, [I

    fill-array-data v4, :array_112

    aput-object v4, v2, v3

    const/16 v3, 0x9

    new-array v4, v5, [I

    fill-array-data v4, :array_11e

    aput-object v4, v2, v3

    sput-object v2, Lcom/google/zxing/oned/p;->d:[[I

    .line 82
    const/16 v2, 0x14

    new-array v2, v2, [[I

    sput-object v2, Lcom/google/zxing/oned/p;->e:[[I

    .line 83
    sget-object v2, Lcom/google/zxing/oned/p;->d:[[I

    sget-object v3, Lcom/google/zxing/oned/p;->e:[[I

    invoke-static {v2, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v2, v0

    .line 84
    :goto_74
    const/16 v0, 0x14

    if-ge v2, v0, :cond_98

    .line 85
    sget-object v0, Lcom/google/zxing/oned/p;->d:[[I

    add-int/lit8 v3, v2, -0xa

    aget-object v3, v0, v3

    .line 86
    array-length v0, v3

    new-array v4, v0, [I

    move v0, v1

    .line 87
    :goto_82
    array-length v5, v3

    if-ge v0, v5, :cond_90

    .line 88
    array-length v5, v3

    sub-int/2addr v5, v0

    add-int/lit8 v5, v5, -0x1

    aget v5, v3, v5

    aput v5, v4, v0

    .line 87
    add-int/lit8 v0, v0, 0x1

    goto :goto_82

    .line 90
    :cond_90
    sget-object v0, Lcom/google/zxing/oned/p;->e:[[I

    aput-object v4, v0, v2

    .line 84
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_74

    .line 92
    :cond_98
    return-void

    .line 53
    nop

    :array_9a
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    .line 58
    :array_a4
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    .line 63
    :array_b2
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_be
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_ca
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    :array_d6
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_e2
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    :array_ee
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_fa
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    :array_106
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    :array_112
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    :array_11e
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method protected constructor <init>()V
    .registers 3

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/google/zxing/oned/k;-><init>()V

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/google/zxing/oned/p;->a:Ljava/lang/StringBuilder;

    .line 100
    new-instance v0, Lcom/google/zxing/oned/o;

    invoke-direct {v0}, Lcom/google/zxing/oned/o;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/oned/p;->f:Lcom/google/zxing/oned/o;

    .line 101
    new-instance v0, Lcom/google/zxing/oned/g;

    invoke-direct {v0}, Lcom/google/zxing/oned/g;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/oned/p;->g:Lcom/google/zxing/oned/g;

    .line 102
    return-void
.end method

.method static a(Lcom/google/zxing/common/a;[II[[I)I
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 322
    invoke-static {p0, p2, p1}, Lcom/google/zxing/oned/p;->a(Lcom/google/zxing/common/a;I[I)V

    .line 323
    const/16 v3, 0x7a

    .line 324
    const/4 v0, -0x1

    .line 325
    array-length v4, p3

    .line 326
    const/4 v1, 0x0

    :goto_8
    if-ge v1, v4, :cond_19

    .line 327
    aget-object v2, p3, v1

    .line 328
    const/16 v5, 0xb3

    invoke-static {p1, v2, v5}, Lcom/google/zxing/oned/p;->a([I[II)I

    move-result v2

    .line 329
    if-ge v2, v3, :cond_21

    move v0, v1

    .line 326
    :goto_15
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    goto :goto_8

    .line 334
    :cond_19
    if-ltz v0, :cond_1c

    .line 335
    return v0

    .line 337
    :cond_1c
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_21
    move v2, v3

    goto :goto_15
.end method

.method static a(Lcom/google/zxing/common/a;)[I
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 106
    const/4 v0, 0x0

    .line 108
    sget-object v1, Lcom/google/zxing/oned/p;->b:[I

    array-length v1, v1

    new-array v4, v1, [I

    move v1, v2

    move-object v3, v0

    move v0, v2

    .line 109
    :cond_a
    :goto_a
    if-nez v0, :cond_28

    .line 110
    sget-object v3, Lcom/google/zxing/oned/p;->b:[I

    array-length v3, v3

    invoke-static {v4, v2, v3, v2}, Ljava/util/Arrays;->fill([IIII)V

    .line 111
    sget-object v3, Lcom/google/zxing/oned/p;->b:[I

    invoke-static {p0, v1, v2, v3, v4}, Lcom/google/zxing/oned/p;->a(Lcom/google/zxing/common/a;IZ[I[I)[I

    move-result-object v3

    .line 112
    aget v5, v3, v2

    .line 113
    const/4 v1, 0x1

    aget v1, v3, v1

    .line 117
    sub-int v6, v1, v5

    sub-int v6, v5, v6

    .line 118
    if-ltz v6, :cond_a

    .line 119
    invoke-virtual {p0, v6, v5, v2}, Lcom/google/zxing/common/a;->a(IIZ)Z

    move-result v0

    goto :goto_a

    .line 122
    :cond_28
    return-object v3
.end method

.method static a(Lcom/google/zxing/common/a;IZ[I)[I
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 260
    array-length v0, p3

    new-array v0, v0, [I

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/zxing/oned/p;->a(Lcom/google/zxing/common/a;IZ[I[I)[I

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/zxing/common/a;IZ[I[I)[I
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 279
    array-length v6, p3

    .line 280
    invoke-virtual {p0}, Lcom/google/zxing/common/a;->a()I

    move-result v7

    .line 282
    if-eqz p2, :cond_23

    invoke-virtual {p0, p1}, Lcom/google/zxing/common/a;->d(I)I

    move-result v0

    :goto_e
    move v2, p2

    move v1, v4

    move v5, v0

    .line 285
    :goto_11
    if-ge v5, v7, :cond_5d

    .line 286
    invoke-virtual {p0, v5}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v8

    xor-int/2addr v8, v2

    if-eqz v8, :cond_28

    .line 287
    aget v8, p4, v1

    add-int/lit8 v8, v8, 0x1

    aput v8, p4, v1

    .line 285
    :goto_20
    add-int/lit8 v5, v5, 0x1

    goto :goto_11

    .line 282
    :cond_23
    invoke-virtual {p0, p1}, Lcom/google/zxing/common/a;->c(I)I

    move-result v0

    goto :goto_e

    .line 289
    :cond_28
    add-int/lit8 v8, v6, -0x1

    if-ne v1, v8, :cond_58

    .line 290
    const/16 v8, 0xb3

    invoke-static {p4, p3, v8}, Lcom/google/zxing/oned/p;->a([I[II)I

    move-result v8

    const/16 v9, 0x7a

    if-ge v8, v9, :cond_3d

    .line 291
    new-array v1, v10, [I

    aput v0, v1, v4

    aput v5, v1, v3

    return-object v1

    .line 293
    :cond_3d
    aget v8, p4, v4

    aget v9, p4, v3

    add-int/2addr v8, v9

    add-int/2addr v0, v8

    .line 294
    add-int/lit8 v8, v6, -0x2

    invoke-static {p4, v10, p4, v4, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 295
    add-int/lit8 v8, v6, -0x2

    aput v4, p4, v8

    .line 296
    add-int/lit8 v8, v6, -0x1

    aput v4, p4, v8

    .line 297
    add-int/lit8 v1, v1, -0x1

    .line 301
    :goto_52
    aput v3, p4, v1

    .line 302
    if-nez v2, :cond_5b

    move v2, v3

    goto :goto_20

    .line 299
    :cond_58
    add-int/lit8 v1, v1, 0x1

    goto :goto_52

    :cond_5b
    move v2, v4

    .line 302
    goto :goto_20

    .line 305
    :cond_5d
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method protected abstract a(Lcom/google/zxing/common/a;[ILjava/lang/StringBuilder;)I
.end method

.method public a(ILcom/google/zxing/common/a;Ljava/util/Map;)Lcom/google/zxing/f;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 128
    invoke-static {p2}, Lcom/google/zxing/oned/p;->a(Lcom/google/zxing/common/a;)[I

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/google/zxing/oned/p;->a(ILcom/google/zxing/common/a;[ILjava/util/Map;)Lcom/google/zxing/f;

    move-result-object v0

    return-object v0
.end method

.method public a(ILcom/google/zxing/common/a;[ILjava/util/Map;)Lcom/google/zxing/f;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 142
    if-nez p4, :cond_64

    const/4 v0, 0x0

    .line 145
    :goto_3
    if-eqz v0, :cond_19

    .line 146
    new-instance v1, Lcom/google/zxing/g;

    const/4 v2, 0x0

    aget v2, p3, v2

    const/4 v3, 0x1

    aget v3, p3, v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    const/high16 v3, 0x4000

    div-float/2addr v2, v3

    int-to-float v3, p1

    invoke-direct {v1, v2, v3}, Lcom/google/zxing/g;-><init>(FF)V

    invoke-interface {v0, v1}, Lcom/google/zxing/h;->a(Lcom/google/zxing/g;)V

    .line 151
    :cond_19
    iget-object v1, p0, Lcom/google/zxing/oned/p;->a:Ljava/lang/StringBuilder;

    .line 152
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 153
    invoke-virtual {p0, p2, p3, v1}, Lcom/google/zxing/oned/p;->a(Lcom/google/zxing/common/a;[ILjava/lang/StringBuilder;)I

    move-result v2

    .line 155
    if-eqz v0, :cond_2f

    .line 156
    new-instance v3, Lcom/google/zxing/g;

    int-to-float v4, v2

    int-to-float v5, p1

    invoke-direct {v3, v4, v5}, Lcom/google/zxing/g;-><init>(FF)V

    invoke-interface {v0, v3}, Lcom/google/zxing/h;->a(Lcom/google/zxing/g;)V

    .line 161
    :cond_2f
    invoke-virtual {p0, p2, v2}, Lcom/google/zxing/oned/p;->a(Lcom/google/zxing/common/a;I)[I

    move-result-object v2

    .line 163
    if-eqz v0, :cond_49

    .line 164
    new-instance v3, Lcom/google/zxing/g;

    const/4 v4, 0x0

    aget v4, v2, v4

    const/4 v5, 0x1

    aget v5, v2, v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    const/high16 v5, 0x4000

    div-float/2addr v4, v5

    int-to-float v5, p1

    invoke-direct {v3, v4, v5}, Lcom/google/zxing/g;-><init>(FF)V

    invoke-interface {v0, v3}, Lcom/google/zxing/h;->a(Lcom/google/zxing/g;)V

    .line 172
    :cond_49
    const/4 v0, 0x1

    aget v0, v2, v0

    .line 173
    const/4 v3, 0x0

    aget v3, v2, v3

    sub-int v3, v0, v3

    add-int/2addr v3, v0

    .line 174
    invoke-virtual {p2}, Lcom/google/zxing/common/a;->a()I

    move-result v4

    if-ge v3, v4, :cond_5f

    const/4 v4, 0x0

    invoke-virtual {p2, v0, v3, v4}, Lcom/google/zxing/common/a;->a(IIZ)Z

    move-result v0

    if-nez v0, :cond_6d

    .line 175
    :cond_5f
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 142
    :cond_64
    sget-object v0, Lcom/google/zxing/DecodeHintType;->NEED_RESULT_POINT_CALLBACK:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/h;

    goto :goto_3

    .line 178
    :cond_6d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 179
    invoke-virtual {p0, v0}, Lcom/google/zxing/oned/p;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7c

    .line 180
    invoke-static {}, Lcom/google/zxing/ChecksumException;->getChecksumInstance()Lcom/google/zxing/ChecksumException;

    move-result-object v0

    throw v0

    .line 183
    :cond_7c
    const/4 v1, 0x1

    aget v1, p3, v1

    const/4 v3, 0x0

    aget v3, p3, v3

    add-int/2addr v1, v3

    int-to-float v1, v1

    const/high16 v3, 0x4000

    div-float/2addr v1, v3

    .line 184
    const/4 v3, 0x1

    aget v3, v2, v3

    const/4 v4, 0x0

    aget v4, v2, v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    const/high16 v4, 0x4000

    div-float/2addr v3, v4

    .line 185
    invoke-virtual {p0}, Lcom/google/zxing/oned/p;->b()Lcom/google/zxing/BarcodeFormat;

    move-result-object v4

    .line 186
    new-instance v5, Lcom/google/zxing/f;

    const/4 v6, 0x0

    const/4 v7, 0x2

    new-array v7, v7, [Lcom/google/zxing/g;

    const/4 v8, 0x0

    new-instance v9, Lcom/google/zxing/g;

    int-to-float v10, p1

    invoke-direct {v9, v1, v10}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v9, v7, v8

    const/4 v1, 0x1

    new-instance v8, Lcom/google/zxing/g;

    int-to-float v9, p1

    invoke-direct {v8, v3, v9}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v8, v7, v1

    invoke-direct {v5, v0, v6, v7, v4}, Lcom/google/zxing/f;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/g;Lcom/google/zxing/BarcodeFormat;)V

    .line 194
    :try_start_b1
    iget-object v1, p0, Lcom/google/zxing/oned/p;->f:Lcom/google/zxing/oned/o;

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-virtual {v1, p1, p2, v2}, Lcom/google/zxing/oned/o;->a(ILcom/google/zxing/common/a;I)Lcom/google/zxing/f;

    move-result-object v1

    .line 195
    sget-object v2, Lcom/google/zxing/ResultMetadataType;->UPC_EAN_EXTENSION:Lcom/google/zxing/ResultMetadataType;

    invoke-virtual {v1}, Lcom/google/zxing/f;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Lcom/google/zxing/f;->a(Lcom/google/zxing/ResultMetadataType;Ljava/lang/Object;)V

    .line 196
    invoke-virtual {v1}, Lcom/google/zxing/f;->e()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/google/zxing/f;->a(Ljava/util/Map;)V

    .line 197
    invoke-virtual {v1}, Lcom/google/zxing/f;->c()[Lcom/google/zxing/g;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/google/zxing/f;->a([Lcom/google/zxing/g;)V
    :try_end_d1
    .catch Lcom/google/zxing/ReaderException; {:try_start_b1 .. :try_end_d1} :catch_e7

    .line 202
    :goto_d1
    sget-object v1, Lcom/google/zxing/BarcodeFormat;->EAN_13:Lcom/google/zxing/BarcodeFormat;

    if-eq v4, v1, :cond_d9

    sget-object v1, Lcom/google/zxing/BarcodeFormat;->UPC_A:Lcom/google/zxing/BarcodeFormat;

    if-ne v4, v1, :cond_e6

    .line 203
    :cond_d9
    iget-object v1, p0, Lcom/google/zxing/oned/p;->g:Lcom/google/zxing/oned/g;

    invoke-virtual {v1, v0}, Lcom/google/zxing/oned/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_e6

    .line 205
    sget-object v1, Lcom/google/zxing/ResultMetadataType;->POSSIBLE_COUNTRY:Lcom/google/zxing/ResultMetadataType;

    invoke-virtual {v5, v1, v0}, Lcom/google/zxing/f;->a(Lcom/google/zxing/ResultMetadataType;Ljava/lang/Object;)V

    .line 209
    :cond_e6
    return-object v5

    .line 198
    :catch_e7
    move-exception v1

    goto :goto_d1
.end method

.method a(Ljava/lang/String;)Z
    .registers 8
    .parameter

    .prologue
    const/16 v5, 0x9

    const/4 v0, 0x0

    .line 216
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-eqz v3, :cond_3f

    add-int/lit8 v1, v3, -0x2

    move v2, v0

    :goto_c
    if-ltz v1, :cond_21

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    add-int/lit8 v4, v4, -0x30

    if-ltz v4, :cond_18

    if-le v4, v5, :cond_1d

    :cond_18
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_1d
    add-int/2addr v2, v4

    add-int/lit8 v1, v1, -0x2

    goto :goto_c

    :cond_21
    mul-int/lit8 v2, v2, 0x3

    add-int/lit8 v1, v3, -0x1

    :goto_25
    if-ltz v1, :cond_3a

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    add-int/lit8 v3, v3, -0x30

    if-ltz v3, :cond_31

    if-le v3, v5, :cond_36

    :cond_31
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_36
    add-int/2addr v2, v3

    add-int/lit8 v1, v1, -0x2

    goto :goto_25

    :cond_3a
    rem-int/lit8 v1, v2, 0xa

    if-nez v1, :cond_3f

    const/4 v0, 0x1

    :cond_3f
    return v0
.end method

.method a(Lcom/google/zxing/common/a;I)[I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 253
    const/4 v0, 0x0

    sget-object v1, Lcom/google/zxing/oned/p;->b:[I

    invoke-static {p1, p2, v0, v1}, Lcom/google/zxing/oned/p;->a(Lcom/google/zxing/common/a;IZ[I)[I

    move-result-object v0

    return-object v0
.end method

.method abstract b()Lcom/google/zxing/BarcodeFormat;
.end method
