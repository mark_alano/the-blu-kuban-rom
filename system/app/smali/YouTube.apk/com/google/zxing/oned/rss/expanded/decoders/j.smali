.class public abstract Lcom/google/zxing/oned/rss/expanded/decoders/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/zxing/common/a;

.field private final b:Lcom/google/zxing/oned/rss/expanded/decoders/r;


# direct methods
.method constructor <init>(Lcom/google/zxing/common/a;)V
    .registers 3
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/zxing/oned/rss/expanded/decoders/j;->a:Lcom/google/zxing/common/a;

    .line 43
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/r;

    invoke-direct {v0, p1}, Lcom/google/zxing/oned/rss/expanded/decoders/r;-><init>(Lcom/google/zxing/common/a;)V

    iput-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/j;->b:Lcom/google/zxing/oned/rss/expanded/decoders/r;

    .line 44
    return-void
.end method

.method public static a(Lcom/google/zxing/common/a;)Lcom/google/zxing/oned/rss/expanded/decoders/j;
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 57
    invoke-virtual {p0, v1}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 58
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/g;

    invoke-direct {v0, p0}, Lcom/google/zxing/oned/rss/expanded/decoders/g;-><init>(Lcom/google/zxing/common/a;)V

    .line 86
    :goto_c
    return-object v0

    .line 60
    :cond_d
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 61
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/k;

    invoke-direct {v0, p0}, Lcom/google/zxing/oned/rss/expanded/decoders/k;-><init>(Lcom/google/zxing/common/a;)V

    goto :goto_c

    .line 64
    :cond_1a
    const/4 v0, 0x4

    invoke-static {p0, v1, v0}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(Lcom/google/zxing/common/a;II)I

    move-result v0

    .line 66
    packed-switch v0, :pswitch_data_b4

    .line 71
    const/4 v0, 0x5

    invoke-static {p0, v1, v0}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(Lcom/google/zxing/common/a;II)I

    move-result v0

    .line 72
    packed-switch v0, :pswitch_data_bc

    .line 77
    const/4 v0, 0x7

    invoke-static {p0, v1, v0}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(Lcom/google/zxing/common/a;II)I

    move-result v0

    .line 78
    packed-switch v0, :pswitch_data_c4

    .line 89
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown decoder: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :pswitch_47
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/a;

    invoke-direct {v0, p0}, Lcom/google/zxing/oned/rss/expanded/decoders/a;-><init>(Lcom/google/zxing/common/a;)V

    goto :goto_c

    .line 68
    :pswitch_4d
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/b;

    invoke-direct {v0, p0}, Lcom/google/zxing/oned/rss/expanded/decoders/b;-><init>(Lcom/google/zxing/common/a;)V

    goto :goto_c

    .line 73
    :pswitch_53
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/c;

    invoke-direct {v0, p0}, Lcom/google/zxing/oned/rss/expanded/decoders/c;-><init>(Lcom/google/zxing/common/a;)V

    goto :goto_c

    .line 74
    :pswitch_59
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/d;

    invoke-direct {v0, p0}, Lcom/google/zxing/oned/rss/expanded/decoders/d;-><init>(Lcom/google/zxing/common/a;)V

    goto :goto_c

    .line 79
    :pswitch_5f
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/e;

    const-string v1, "310"

    const-string v2, "11"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/zxing/oned/rss/expanded/decoders/e;-><init>(Lcom/google/zxing/common/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    .line 80
    :pswitch_69
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/e;

    const-string v1, "320"

    const-string v2, "11"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/zxing/oned/rss/expanded/decoders/e;-><init>(Lcom/google/zxing/common/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    .line 81
    :pswitch_73
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/e;

    const-string v1, "310"

    const-string v2, "13"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/zxing/oned/rss/expanded/decoders/e;-><init>(Lcom/google/zxing/common/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    .line 82
    :pswitch_7d
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/e;

    const-string v1, "320"

    const-string v2, "13"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/zxing/oned/rss/expanded/decoders/e;-><init>(Lcom/google/zxing/common/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    .line 83
    :pswitch_87
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/e;

    const-string v1, "310"

    const-string v2, "15"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/zxing/oned/rss/expanded/decoders/e;-><init>(Lcom/google/zxing/common/a;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 84
    :pswitch_92
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/e;

    const-string v1, "320"

    const-string v2, "15"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/zxing/oned/rss/expanded/decoders/e;-><init>(Lcom/google/zxing/common/a;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 85
    :pswitch_9d
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/e;

    const-string v1, "310"

    const-string v2, "17"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/zxing/oned/rss/expanded/decoders/e;-><init>(Lcom/google/zxing/common/a;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 86
    :pswitch_a8
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/e;

    const-string v1, "320"

    const-string v2, "17"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/zxing/oned/rss/expanded/decoders/e;-><init>(Lcom/google/zxing/common/a;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 66
    nop

    :pswitch_data_b4
    .packed-switch 0x4
        :pswitch_47
        :pswitch_4d
    .end packed-switch

    .line 72
    :pswitch_data_bc
    .packed-switch 0xc
        :pswitch_53
        :pswitch_59
    .end packed-switch

    .line 78
    :pswitch_data_c4
    .packed-switch 0x38
        :pswitch_5f
        :pswitch_69
        :pswitch_73
        :pswitch_7d
        :pswitch_87
        :pswitch_92
        :pswitch_9d
        :pswitch_a8
    .end packed-switch
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method protected final b()Lcom/google/zxing/common/a;
    .registers 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/j;->a:Lcom/google/zxing/common/a;

    return-object v0
.end method

.method protected final c()Lcom/google/zxing/oned/rss/expanded/decoders/r;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/j;->b:Lcom/google/zxing/oned/rss/expanded/decoders/r;

    return-object v0
.end method
