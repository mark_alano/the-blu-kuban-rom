.class public final Lcom/google/zxing/pdf417/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/zxing/e;


# static fields
.field private static final a:[Lcom/google/zxing/g;


# instance fields
.field private final b:Lcom/google/zxing/pdf417/decoder/c;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 43
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/zxing/g;

    sput-object v0, Lcom/google/zxing/pdf417/a;->a:[Lcom/google/zxing/g;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Lcom/google/zxing/pdf417/decoder/c;

    invoke-direct {v0}, Lcom/google/zxing/pdf417/decoder/c;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/pdf417/a;->b:Lcom/google/zxing/pdf417/decoder/c;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/zxing/b;Ljava/util/Map;)Lcom/google/zxing/f;
    .registers 15
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 64
    if-eqz p2, :cond_f3

    sget-object v0, Lcom/google/zxing/DecodeHintType;->PURE_BARCODE:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f3

    .line 65
    invoke-virtual {p1}, Lcom/google/zxing/b;->c()Lcom/google/zxing/common/b;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/zxing/common/b;->b()[I

    move-result-object v7

    invoke-virtual {v6}, Lcom/google/zxing/common/b;->c()[I

    move-result-object v3

    if-eqz v7, :cond_1c

    if-nez v3, :cond_21

    :cond_1c
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_21
    aget v0, v7, v1

    aget v4, v7, v2

    invoke-virtual {v6}, Lcom/google/zxing/common/b;->d()I

    move-result v5

    :goto_29
    if-ge v0, v5, :cond_34

    invoke-virtual {v6, v0, v4}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v8

    if-eqz v8, :cond_34

    add-int/lit8 v0, v0, 0x1

    goto :goto_29

    :cond_34
    if-ne v0, v5, :cond_3b

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_3b
    aget v4, v7, v1

    sub-int/2addr v0, v4

    ushr-int/lit8 v8, v0, 0x3

    if-nez v8, :cond_47

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_47
    aget v9, v7, v2

    aget v10, v3, v2

    aget v0, v7, v1

    invoke-virtual {v6}, Lcom/google/zxing/common/b;->d()I

    move-result v11

    move v3, v2

    move v5, v0

    move v0, v1

    :goto_54
    add-int/lit8 v4, v11, -0x1

    if-ge v5, v4, :cond_68

    const/16 v4, 0x8

    if-ge v0, v4, :cond_68

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v6, v5, v9}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v4

    if-eq v3, v4, :cond_66

    add-int/lit8 v0, v0, 0x1

    :cond_66
    move v3, v4

    goto :goto_54

    :cond_68
    add-int/lit8 v0, v11, -0x1

    if-ne v5, v0, :cond_71

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_71
    aget v7, v7, v1

    invoke-virtual {v6}, Lcom/google/zxing/common/b;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_79
    if-le v0, v7, :cond_84

    invoke-virtual {v6, v0, v9}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v3

    if-nez v3, :cond_84

    add-int/lit8 v0, v0, -0x1

    goto :goto_79

    :cond_84
    move v3, v0

    move v0, v1

    :goto_86
    if-le v3, v7, :cond_99

    const/16 v4, 0x9

    if-ge v0, v4, :cond_99

    add-int/lit8 v4, v3, -0x1

    invoke-virtual {v6, v4, v9}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v3

    if-eq v2, v3, :cond_96

    add-int/lit8 v0, v0, 0x1

    :cond_96
    move v2, v3

    move v3, v4

    goto :goto_86

    :cond_99
    if-ne v3, v7, :cond_a0

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_a0
    sub-int v0, v3, v5

    add-int/lit8 v0, v0, 0x1

    div-int v3, v0, v8

    sub-int v0, v10, v9

    add-int/lit8 v0, v0, 0x1

    div-int v4, v0, v8

    if-lez v3, :cond_b0

    if-gtz v4, :cond_b5

    :cond_b0
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_b5
    shr-int/lit8 v0, v8, 0x1

    add-int v7, v9, v0

    add-int/2addr v5, v0

    new-instance v9, Lcom/google/zxing/common/b;

    invoke-direct {v9, v3, v4}, Lcom/google/zxing/common/b;-><init>(II)V

    move v0, v1

    :goto_c0
    if-ge v0, v4, :cond_db

    mul-int v2, v0, v8

    add-int v10, v7, v2

    move v2, v1

    :goto_c7
    if-ge v2, v3, :cond_d8

    mul-int v11, v2, v8

    add-int/2addr v11, v5

    invoke-virtual {v6, v11, v10}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v11

    if-eqz v11, :cond_d5

    invoke-virtual {v9, v2, v0}, Lcom/google/zxing/common/b;->b(II)V

    :cond_d5
    add-int/lit8 v2, v2, 0x1

    goto :goto_c7

    :cond_d8
    add-int/lit8 v0, v0, 0x1

    goto :goto_c0

    .line 66
    :cond_db
    iget-object v0, p0, Lcom/google/zxing/pdf417/a;->b:Lcom/google/zxing/pdf417/decoder/c;

    invoke-virtual {v0, v9}, Lcom/google/zxing/pdf417/decoder/c;->a(Lcom/google/zxing/common/b;)Lcom/google/zxing/common/d;

    move-result-object v1

    .line 67
    sget-object v0, Lcom/google/zxing/pdf417/a;->a:[Lcom/google/zxing/g;

    .line 73
    :goto_e3
    new-instance v2, Lcom/google/zxing/f;

    invoke-virtual {v1}, Lcom/google/zxing/common/d;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/zxing/common/d;->a()[B

    move-result-object v1

    sget-object v4, Lcom/google/zxing/BarcodeFormat;->PDF_417:Lcom/google/zxing/BarcodeFormat;

    invoke-direct {v2, v3, v1, v0, v4}, Lcom/google/zxing/f;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/g;Lcom/google/zxing/BarcodeFormat;)V

    return-object v2

    .line 69
    :cond_f3
    new-instance v0, Lcom/google/zxing/pdf417/a/a;

    invoke-direct {v0, p1}, Lcom/google/zxing/pdf417/a/a;-><init>(Lcom/google/zxing/b;)V

    invoke-virtual {v0}, Lcom/google/zxing/pdf417/a/a;->a()Lcom/google/zxing/common/f;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/google/zxing/pdf417/a;->b:Lcom/google/zxing/pdf417/decoder/c;

    invoke-virtual {v0}, Lcom/google/zxing/common/f;->d()Lcom/google/zxing/common/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/zxing/pdf417/decoder/c;->a(Lcom/google/zxing/common/b;)Lcom/google/zxing/common/d;

    move-result-object v1

    .line 71
    invoke-virtual {v0}, Lcom/google/zxing/common/f;->e()[Lcom/google/zxing/g;

    move-result-object v0

    goto :goto_e3
.end method

.method public final a()V
    .registers 1

    .prologue
    .line 80
    return-void
.end method
