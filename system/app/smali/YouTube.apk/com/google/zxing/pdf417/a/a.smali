.class public final Lcom/google/zxing/pdf417/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I

.field private static final d:[I


# instance fields
.field private final e:Lcom/google/zxing/b;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/16 v2, 0x9

    const/16 v1, 0x8

    .line 48
    new-array v0, v1, [I

    fill-array-data v0, :array_22

    sput-object v0, Lcom/google/zxing/pdf417/a/a;->a:[I

    .line 51
    new-array v0, v1, [I

    fill-array-data v0, :array_36

    sput-object v0, Lcom/google/zxing/pdf417/a/a;->b:[I

    .line 54
    new-array v0, v2, [I

    fill-array-data v0, :array_4a

    sput-object v0, Lcom/google/zxing/pdf417/a/a;->c:[I

    .line 58
    new-array v0, v2, [I

    fill-array-data v0, :array_60

    sput-object v0, Lcom/google/zxing/pdf417/a/a;->d:[I

    return-void

    .line 48
    nop

    :array_22
    .array-data 0x4
        0x8t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    .line 51
    :array_36
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data

    .line 54
    :array_4a
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    .line 58
    :array_60
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Lcom/google/zxing/b;)V
    .registers 2
    .parameter

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/google/zxing/pdf417/a/a;->e:Lcom/google/zxing/b;

    .line 64
    return-void
.end method

.method private static a([Lcom/google/zxing/g;Z)V
    .registers 12
    .parameter
    .parameter

    .prologue
    .line 300
    const/4 v0, 0x0

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v1

    .line 301
    const/4 v0, 0x0

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v2

    .line 302
    const/4 v0, 0x2

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v3

    .line 303
    const/4 v0, 0x2

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v4

    .line 304
    const/4 v0, 0x4

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v5

    .line 305
    const/4 v0, 0x4

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v6

    .line 306
    const/4 v0, 0x6

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v7

    .line 307
    const/4 v0, 0x6

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v8

    .line 309
    sub-float v0, v6, v8

    .line 310
    if-eqz p1, :cond_3d

    .line 311
    neg-float v0, v0

    .line 313
    :cond_3d
    const/high16 v9, 0x4040

    cmpl-float v9, v0, v9

    if-lez v9, :cond_bb

    .line 315
    sub-float v0, v7, v1

    .line 316
    sub-float v3, v8, v2

    .line 317
    mul-float v4, v0, v0

    mul-float v6, v3, v3

    add-float/2addr v4, v6

    .line 318
    sub-float/2addr v5, v1

    mul-float/2addr v5, v0

    div-float v4, v5, v4

    .line 319
    const/4 v5, 0x4

    new-instance v6, Lcom/google/zxing/g;

    mul-float/2addr v0, v4

    add-float/2addr v0, v1

    mul-float v1, v4, v3

    add-float/2addr v1, v2

    invoke-direct {v6, v0, v1}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, p0, v5

    .line 329
    :cond_5d
    :goto_5d
    const/4 v0, 0x1

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v1

    .line 330
    const/4 v0, 0x1

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v2

    .line 331
    const/4 v0, 0x3

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v3

    .line 332
    const/4 v0, 0x3

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v4

    .line 333
    const/4 v0, 0x5

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v5

    .line 334
    const/4 v0, 0x5

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v6

    .line 335
    const/4 v0, 0x7

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v7

    .line 336
    const/4 v0, 0x7

    aget-object v0, p0, v0

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v8

    .line 338
    sub-float v0, v8, v6

    .line 339
    if-eqz p1, :cond_9a

    .line 340
    neg-float v0, v0

    .line 342
    :cond_9a
    const/high16 v9, 0x4040

    cmpl-float v9, v0, v9

    if-lez v9, :cond_e0

    .line 344
    sub-float v0, v7, v1

    .line 345
    sub-float v3, v8, v2

    .line 346
    mul-float v4, v0, v0

    mul-float v6, v3, v3

    add-float/2addr v4, v6

    .line 347
    sub-float/2addr v5, v1

    mul-float/2addr v5, v0

    div-float v4, v5, v4

    .line 348
    const/4 v5, 0x5

    new-instance v6, Lcom/google/zxing/g;

    mul-float/2addr v0, v4

    add-float/2addr v0, v1

    mul-float v1, v4, v3

    add-float/2addr v1, v2

    invoke-direct {v6, v0, v1}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, p0, v5

    .line 357
    :cond_ba
    :goto_ba
    return-void

    .line 320
    :cond_bb
    neg-float v0, v0

    const/high16 v1, 0x4040

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5d

    .line 322
    sub-float v0, v3, v5

    .line 323
    sub-float v1, v4, v6

    .line 324
    mul-float v2, v0, v0

    mul-float v5, v1, v1

    add-float/2addr v2, v5

    .line 325
    sub-float v5, v3, v7

    mul-float/2addr v5, v0

    div-float v2, v5, v2

    .line 326
    const/4 v5, 0x6

    new-instance v6, Lcom/google/zxing/g;

    mul-float/2addr v0, v2

    sub-float v0, v3, v0

    mul-float/2addr v1, v2

    sub-float v1, v4, v1

    invoke-direct {v6, v0, v1}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, p0, v5

    goto/16 :goto_5d

    .line 349
    :cond_e0
    neg-float v0, v0

    const/high16 v1, 0x4040

    cmpl-float v0, v0, v1

    if-lez v0, :cond_ba

    .line 351
    sub-float v0, v3, v5

    .line 352
    sub-float v1, v4, v6

    .line 353
    mul-float v2, v0, v0

    mul-float v5, v1, v1

    add-float/2addr v2, v5

    .line 354
    sub-float v5, v3, v7

    mul-float/2addr v5, v0

    div-float v2, v5, v2

    .line 355
    const/4 v5, 0x7

    new-instance v6, Lcom/google/zxing/g;

    mul-float/2addr v0, v2

    sub-float v0, v3, v0

    mul-float/2addr v1, v2

    sub-float v1, v4, v1

    invoke-direct {v6, v0, v1}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, p0, v5

    goto :goto_ba
.end method

.method private static a(Lcom/google/zxing/common/b;IIIZ[I[I)[I
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 476
    const/4 v2, 0x0

    move-object/from16 v0, p6

    array-length v3, v0

    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-static {v0, v2, v3, v4}, Ljava/util/Arrays;->fill([IIII)V

    .line 477
    move-object/from16 v0, p5

    array-length v10, v0

    .line 480
    const/4 v3, 0x0

    move/from16 v9, p1

    move/from16 v2, p1

    move/from16 v4, p4

    .line 482
    :goto_14
    add-int v5, p1, p3

    if-ge v9, v5, :cond_ab

    .line 483
    move/from16 v0, p2

    invoke-virtual {p0, v9, v0}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v5

    .line 484
    xor-int/2addr v5, v4

    if-eqz v5, :cond_2b

    .line 485
    aget v5, p6, v3

    add-int/lit8 v5, v5, 0x1

    aput v5, p6, v3

    .line 482
    :goto_27
    add-int/lit8 v5, v9, 0x1

    move v9, v5

    goto :goto_14

    .line 487
    :cond_2b
    add-int/lit8 v5, v10, -0x1

    if-ne v3, v5, :cond_a5

    .line 488
    move-object/from16 v0, p6

    array-length v11, v0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v5, 0x0

    move v8, v6

    :goto_36
    if-ge v5, v11, :cond_42

    aget v6, p6, v5

    add-int/2addr v6, v8

    aget v8, p5, v5

    add-int/2addr v7, v8

    add-int/lit8 v5, v5, 0x1

    move v8, v6

    goto :goto_36

    :cond_42
    if-ge v8, v7, :cond_56

    const v5, 0x7fffffff

    :goto_47
    const/16 v6, 0x6b

    if-ge v5, v6, :cond_7f

    .line 489
    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    aput v2, v3, v4

    const/4 v2, 0x1

    aput v9, v3, v2

    move-object v2, v3

    .line 503
    :goto_55
    return-object v2

    .line 488
    :cond_56
    shl-int/lit8 v5, v8, 0x8

    div-int v12, v5, v7

    mul-int/lit16 v5, v12, 0xcc

    shr-int/lit8 v13, v5, 0x8

    const/4 v6, 0x0

    const/4 v5, 0x0

    move v7, v6

    move v6, v5

    :goto_62
    if-ge v6, v11, :cond_7c

    aget v5, p6, v6

    shl-int/lit8 v5, v5, 0x8

    aget v14, p5, v6

    mul-int/2addr v14, v12

    if-le v5, v14, :cond_74

    sub-int/2addr v5, v14

    :goto_6e
    if-le v5, v13, :cond_77

    const v5, 0x7fffffff

    goto :goto_47

    :cond_74
    sub-int v5, v14, v5

    goto :goto_6e

    :cond_77
    add-int/2addr v7, v5

    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_62

    :cond_7c
    div-int v5, v7, v8

    goto :goto_47

    .line 491
    :cond_7f
    const/4 v5, 0x0

    aget v5, p6, v5

    const/4 v6, 0x1

    aget v6, p6, v6

    add-int/2addr v5, v6

    add-int/2addr v2, v5

    .line 492
    const/4 v5, 0x2

    const/4 v6, 0x0

    add-int/lit8 v7, v10, -0x2

    move-object/from16 v0, p6

    move-object/from16 v1, p6

    invoke-static {v0, v5, v1, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 493
    add-int/lit8 v5, v10, -0x2

    const/4 v6, 0x0

    aput v6, p6, v5

    .line 494
    add-int/lit8 v5, v10, -0x1

    const/4 v6, 0x0

    aput v6, p6, v5

    .line 495
    add-int/lit8 v3, v3, -0x1

    .line 499
    :goto_9e
    const/4 v5, 0x1

    aput v5, p6, v3

    .line 500
    if-nez v4, :cond_a8

    const/4 v4, 0x1

    goto :goto_27

    .line 497
    :cond_a5
    add-int/lit8 v3, v3, 0x1

    goto :goto_9e

    .line 500
    :cond_a8
    const/4 v4, 0x0

    goto/16 :goto_27

    .line 503
    :cond_ab
    const/4 v2, 0x0

    goto :goto_55
.end method


# virtual methods
.method public final a()Lcom/google/zxing/common/f;
    .registers 24

    .prologue
    .line 73
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/zxing/pdf417/a/a;->e:Lcom/google/zxing/b;

    invoke-virtual {v1}, Lcom/google/zxing/b;->c()Lcom/google/zxing/common/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/zxing/common/b;->e()I

    move-result v10

    invoke-virtual {v1}, Lcom/google/zxing/common/b;->d()I

    move-result v4

    const/16 v2, 0x8

    new-array v9, v2, [Lcom/google/zxing/g;

    const/4 v8, 0x0

    sget-object v2, Lcom/google/zxing/pdf417/a/a;->a:[I

    array-length v2, v2

    new-array v7, v2, [I

    const/4 v2, 0x1

    shr-int/lit8 v3, v10, 0x7

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v11

    const/4 v3, 0x0

    :goto_22
    if-ge v3, v10, :cond_2e2

    const/4 v2, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/google/zxing/pdf417/a/a;->a:[I

    invoke-static/range {v1 .. v7}, Lcom/google/zxing/pdf417/a/a;->a(Lcom/google/zxing/common/b;IIIZ[I[I)[I

    move-result-object v2

    if-eqz v2, :cond_1b7

    const/4 v5, 0x0

    new-instance v6, Lcom/google/zxing/g;

    const/4 v8, 0x0

    aget v8, v2, v8

    int-to-float v8, v8

    int-to-float v12, v3

    invoke-direct {v6, v8, v12}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, v9, v5

    const/4 v5, 0x4

    new-instance v6, Lcom/google/zxing/g;

    const/4 v8, 0x1

    aget v2, v2, v8

    int-to-float v2, v2

    int-to-float v3, v3

    invoke-direct {v6, v2, v3}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, v9, v5

    const/4 v2, 0x1

    :goto_49
    if-eqz v2, :cond_75

    const/4 v8, 0x0

    add-int/lit8 v3, v10, -0x1

    :goto_4e
    if-lez v3, :cond_2df

    const/4 v2, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/google/zxing/pdf417/a/a;->a:[I

    invoke-static/range {v1 .. v7}, Lcom/google/zxing/pdf417/a/a;->a(Lcom/google/zxing/common/b;IIIZ[I[I)[I

    move-result-object v2

    if-eqz v2, :cond_1ba

    const/4 v5, 0x1

    new-instance v6, Lcom/google/zxing/g;

    const/4 v7, 0x0

    aget v7, v2, v7

    int-to-float v7, v7

    int-to-float v8, v3

    invoke-direct {v6, v7, v8}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, v9, v5

    const/4 v5, 0x5

    new-instance v6, Lcom/google/zxing/g;

    const/4 v7, 0x1

    aget v2, v2, v7

    int-to-float v2, v2

    int-to-float v3, v3

    invoke-direct {v6, v2, v3}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, v9, v5

    const/4 v2, 0x1

    :cond_75
    :goto_75
    sget-object v3, Lcom/google/zxing/pdf417/a/a;->c:[I

    array-length v3, v3

    new-array v7, v3, [I

    if-eqz v2, :cond_a5

    const/4 v8, 0x0

    const/4 v3, 0x0

    :goto_7e
    if-ge v3, v10, :cond_2dc

    const/4 v2, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/google/zxing/pdf417/a/a;->c:[I

    invoke-static/range {v1 .. v7}, Lcom/google/zxing/pdf417/a/a;->a(Lcom/google/zxing/common/b;IIIZ[I[I)[I

    move-result-object v2

    if-eqz v2, :cond_1bd

    const/4 v5, 0x2

    new-instance v6, Lcom/google/zxing/g;

    const/4 v8, 0x1

    aget v8, v2, v8

    int-to-float v8, v8

    int-to-float v12, v3

    invoke-direct {v6, v8, v12}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, v9, v5

    const/4 v5, 0x6

    new-instance v6, Lcom/google/zxing/g;

    const/4 v8, 0x0

    aget v2, v2, v8

    int-to-float v2, v2

    int-to-float v3, v3

    invoke-direct {v6, v2, v3}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, v9, v5

    const/4 v2, 0x1

    :cond_a5
    :goto_a5
    if-eqz v2, :cond_d1

    const/4 v8, 0x0

    add-int/lit8 v3, v10, -0x1

    :goto_aa
    if-lez v3, :cond_2d9

    const/4 v2, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/google/zxing/pdf417/a/a;->c:[I

    invoke-static/range {v1 .. v7}, Lcom/google/zxing/pdf417/a/a;->a(Lcom/google/zxing/common/b;IIIZ[I[I)[I

    move-result-object v2

    if-eqz v2, :cond_1c0

    const/4 v4, 0x3

    new-instance v5, Lcom/google/zxing/g;

    const/4 v6, 0x1

    aget v6, v2, v6

    int-to-float v6, v6

    int-to-float v7, v3

    invoke-direct {v5, v6, v7}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v5, v9, v4

    const/4 v4, 0x7

    new-instance v5, Lcom/google/zxing/g;

    const/4 v6, 0x0

    aget v2, v2, v6

    int-to-float v2, v2

    int-to-float v3, v3

    invoke-direct {v5, v2, v3}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v5, v9, v4

    const/4 v2, 0x1

    :cond_d1
    :goto_d1
    if-eqz v2, :cond_1c3

    move-object v2, v9

    :goto_d4
    if-nez v2, :cond_1d2

    invoke-virtual {v1}, Lcom/google/zxing/common/b;->e()I

    move-result v12

    invoke-virtual {v1}, Lcom/google/zxing/common/b;->d()I

    move-result v2

    shr-int/lit8 v2, v2, 0x1

    const/16 v3, 0x8

    new-array v10, v3, [Lcom/google/zxing/g;

    const/4 v8, 0x0

    sget-object v3, Lcom/google/zxing/pdf417/a/a;->b:[I

    array-length v3, v3

    new-array v7, v3, [I

    const/4 v3, 0x1

    shr-int/lit8 v4, v12, 0x7

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v13

    add-int/lit8 v3, v12, -0x1

    :goto_f3
    if-lez v3, :cond_2d6

    const/4 v5, 0x1

    sget-object v6, Lcom/google/zxing/pdf417/a/a;->b:[I

    move v4, v2

    invoke-static/range {v1 .. v7}, Lcom/google/zxing/pdf417/a/a;->a(Lcom/google/zxing/common/b;IIIZ[I[I)[I

    move-result-object v4

    if-eqz v4, :cond_1c6

    const/4 v5, 0x0

    new-instance v6, Lcom/google/zxing/g;

    const/4 v8, 0x1

    aget v8, v4, v8

    int-to-float v8, v8

    int-to-float v9, v3

    invoke-direct {v6, v8, v9}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, v10, v5

    const/4 v5, 0x4

    new-instance v6, Lcom/google/zxing/g;

    const/4 v8, 0x0

    aget v4, v4, v8

    int-to-float v4, v4

    int-to-float v3, v3

    invoke-direct {v6, v4, v3}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, v10, v5

    const/4 v3, 0x1

    :goto_11a
    if-eqz v3, :cond_145

    const/4 v8, 0x0

    const/4 v3, 0x0

    :goto_11e
    if-ge v3, v12, :cond_2d3

    const/4 v5, 0x1

    sget-object v6, Lcom/google/zxing/pdf417/a/a;->b:[I

    move v4, v2

    invoke-static/range {v1 .. v7}, Lcom/google/zxing/pdf417/a/a;->a(Lcom/google/zxing/common/b;IIIZ[I[I)[I

    move-result-object v4

    if-eqz v4, :cond_1c9

    const/4 v5, 0x1

    new-instance v6, Lcom/google/zxing/g;

    const/4 v7, 0x1

    aget v7, v4, v7

    int-to-float v7, v7

    int-to-float v8, v3

    invoke-direct {v6, v7, v8}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, v10, v5

    const/4 v5, 0x5

    new-instance v6, Lcom/google/zxing/g;

    const/4 v7, 0x0

    aget v4, v4, v7

    int-to-float v4, v4

    int-to-float v3, v3

    invoke-direct {v6, v4, v3}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, v10, v5

    const/4 v3, 0x1

    :cond_145
    :goto_145
    sget-object v4, Lcom/google/zxing/pdf417/a/a;->d:[I

    array-length v4, v4

    new-array v9, v4, [I

    if-eqz v3, :cond_178

    const/4 v11, 0x0

    add-int/lit8 v5, v12, -0x1

    :goto_14f
    if-lez v5, :cond_2d0

    const/4 v4, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/zxing/pdf417/a/a;->d:[I

    move-object v3, v1

    move v6, v2

    invoke-static/range {v3 .. v9}, Lcom/google/zxing/pdf417/a/a;->a(Lcom/google/zxing/common/b;IIIZ[I[I)[I

    move-result-object v3

    if-eqz v3, :cond_1cc

    const/4 v4, 0x2

    new-instance v6, Lcom/google/zxing/g;

    const/4 v7, 0x0

    aget v7, v3, v7

    int-to-float v7, v7

    int-to-float v8, v5

    invoke-direct {v6, v7, v8}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, v10, v4

    const/4 v4, 0x6

    new-instance v6, Lcom/google/zxing/g;

    const/4 v7, 0x1

    aget v3, v3, v7

    int-to-float v3, v3

    int-to-float v5, v5

    invoke-direct {v6, v3, v5}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, v10, v4

    const/4 v3, 0x1

    :cond_178
    :goto_178
    if-eqz v3, :cond_2cd

    const/4 v11, 0x0

    const/4 v5, 0x0

    :goto_17c
    if-ge v5, v12, :cond_2ca

    const/4 v4, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/zxing/pdf417/a/a;->d:[I

    move-object v3, v1

    move v6, v2

    invoke-static/range {v3 .. v9}, Lcom/google/zxing/pdf417/a/a;->a(Lcom/google/zxing/common/b;IIIZ[I[I)[I

    move-result-object v3

    if-eqz v3, :cond_1ce

    const/4 v2, 0x3

    new-instance v4, Lcom/google/zxing/g;

    const/4 v6, 0x0

    aget v6, v3, v6

    int-to-float v6, v6

    int-to-float v7, v5

    invoke-direct {v4, v6, v7}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v4, v10, v2

    const/4 v2, 0x7

    new-instance v4, Lcom/google/zxing/g;

    const/4 v6, 0x1

    aget v3, v3, v6

    int-to-float v3, v3

    int-to-float v5, v5

    invoke-direct {v4, v3, v5}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v4, v10, v2

    const/4 v2, 0x1

    :goto_1a5
    if-eqz v2, :cond_1d0

    move-object v2, v10

    :goto_1a8
    if-eqz v2, :cond_1d6

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/google/zxing/pdf417/a/a;->a([Lcom/google/zxing/g;Z)V

    move-object/from16 v22, v2

    :goto_1b0
    if-nez v22, :cond_1d9

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1

    :cond_1b7
    add-int/2addr v3, v11

    goto/16 :goto_22

    :cond_1ba
    sub-int/2addr v3, v11

    goto/16 :goto_4e

    :cond_1bd
    add-int/2addr v3, v11

    goto/16 :goto_7e

    :cond_1c0
    sub-int/2addr v3, v11

    goto/16 :goto_aa

    :cond_1c3
    const/4 v2, 0x0

    goto/16 :goto_d4

    :cond_1c6
    sub-int/2addr v3, v13

    goto/16 :goto_f3

    :cond_1c9
    add-int/2addr v3, v13

    goto/16 :goto_11e

    :cond_1cc
    sub-int/2addr v5, v13

    goto :goto_14f

    :cond_1ce
    add-int/2addr v5, v13

    goto :goto_17c

    :cond_1d0
    const/4 v2, 0x0

    goto :goto_1a8

    :cond_1d2
    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/zxing/pdf417/a/a;->a([Lcom/google/zxing/g;Z)V

    :cond_1d6
    move-object/from16 v22, v2

    goto :goto_1b0

    :cond_1d9
    const/4 v2, 0x0

    aget-object v2, v22, v2

    const/4 v3, 0x4

    aget-object v3, v22, v3

    invoke-static {v2, v3}, Lcom/google/zxing/g;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v2

    const/4 v3, 0x1

    aget-object v3, v22, v3

    const/4 v4, 0x5

    aget-object v4, v22, v4

    invoke-static {v3, v4}, Lcom/google/zxing/g;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x4208

    div-float/2addr v2, v3

    const/4 v3, 0x6

    aget-object v3, v22, v3

    const/4 v4, 0x2

    aget-object v4, v22, v4

    invoke-static {v3, v4}, Lcom/google/zxing/g;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v3

    const/4 v4, 0x7

    aget-object v4, v22, v4

    const/4 v5, 0x3

    aget-object v5, v22, v5

    invoke-static {v4, v5}, Lcom/google/zxing/g;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v4

    add-float/2addr v3, v4

    const/high16 v4, 0x4210

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    const/high16 v3, 0x4000

    div-float/2addr v2, v3

    const/high16 v3, 0x3f80

    cmpg-float v3, v2, v3

    if-gez v3, :cond_218

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1

    :cond_218
    const/4 v3, 0x4

    aget-object v3, v22, v3

    const/4 v4, 0x6

    aget-object v4, v22, v4

    const/4 v5, 0x5

    aget-object v5, v22, v5

    const/4 v6, 0x7

    aget-object v6, v22, v6

    invoke-static {v3, v4}, Lcom/google/zxing/g;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v3

    div-float/2addr v3, v2

    const/high16 v4, 0x3f00

    add-float/2addr v3, v4

    float-to-int v3, v3

    invoke-static {v5, v6}, Lcom/google/zxing/g;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v4

    div-float/2addr v4, v2

    const/high16 v5, 0x3f00

    add-float/2addr v4, v5

    float-to-int v4, v4

    add-int/2addr v3, v4

    shr-int/lit8 v3, v3, 0x1

    add-int/lit8 v3, v3, 0x8

    div-int/lit8 v3, v3, 0x11

    mul-int/lit8 v4, v3, 0x11

    if-gtz v4, :cond_246

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1

    :cond_246
    const/4 v3, 0x4

    aget-object v3, v22, v3

    const/4 v5, 0x6

    aget-object v5, v22, v5

    const/4 v6, 0x5

    aget-object v6, v22, v6

    const/4 v7, 0x7

    aget-object v7, v22, v7

    invoke-static {v3, v6}, Lcom/google/zxing/g;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v3

    div-float/2addr v3, v2

    const/high16 v6, 0x3f00

    add-float/2addr v3, v6

    float-to-int v3, v3

    invoke-static {v5, v7}, Lcom/google/zxing/g;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v5

    div-float v2, v5, v2

    const/high16 v5, 0x3f00

    add-float/2addr v2, v5

    float-to-int v2, v2

    add-int/2addr v2, v3

    shr-int/lit8 v5, v2, 0x1

    if-le v5, v4, :cond_2c8

    :goto_26a
    const/4 v2, 0x4

    aget-object v3, v22, v2

    const/4 v2, 0x5

    aget-object v21, v22, v2

    const/4 v2, 0x6

    aget-object v17, v22, v2

    const/4 v2, 0x7

    aget-object v19, v22, v2

    invoke-static {}, Lcom/google/zxing/common/h;->a()Lcom/google/zxing/common/h;

    move-result-object v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    int-to-float v8, v4

    const/4 v9, 0x0

    int-to-float v10, v4

    int-to-float v11, v5

    const/4 v12, 0x0

    int-to-float v13, v5

    invoke-virtual {v3}, Lcom/google/zxing/g;->a()F

    move-result v14

    invoke-virtual {v3}, Lcom/google/zxing/g;->b()F

    move-result v15

    invoke-virtual/range {v17 .. v17}, Lcom/google/zxing/g;->a()F

    move-result v16

    invoke-virtual/range {v17 .. v17}, Lcom/google/zxing/g;->b()F

    move-result v17

    invoke-virtual/range {v19 .. v19}, Lcom/google/zxing/g;->a()F

    move-result v18

    invoke-virtual/range {v19 .. v19}, Lcom/google/zxing/g;->b()F

    move-result v19

    invoke-virtual/range {v21 .. v21}, Lcom/google/zxing/g;->a()F

    move-result v20

    invoke-virtual/range {v21 .. v21}, Lcom/google/zxing/g;->b()F

    move-result v21

    move-object v3, v1

    invoke-virtual/range {v2 .. v21}, Lcom/google/zxing/common/h;->a(Lcom/google/zxing/common/b;IIFFFFFFFFFFFFFFFF)Lcom/google/zxing/common/b;

    move-result-object v1

    new-instance v2, Lcom/google/zxing/common/f;

    const/4 v3, 0x4

    new-array v3, v3, [Lcom/google/zxing/g;

    const/4 v4, 0x0

    const/4 v5, 0x5

    aget-object v5, v22, v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x4

    aget-object v5, v22, v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const/4 v5, 0x6

    aget-object v5, v22, v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const/4 v5, 0x7

    aget-object v5, v22, v5

    aput-object v5, v3, v4

    invoke-direct {v2, v1, v3}, Lcom/google/zxing/common/f;-><init>(Lcom/google/zxing/common/b;[Lcom/google/zxing/g;)V

    return-object v2

    :cond_2c8
    move v5, v4

    goto :goto_26a

    :cond_2ca
    move v2, v11

    goto/16 :goto_1a5

    :cond_2cd
    move v2, v3

    goto/16 :goto_1a5

    :cond_2d0
    move v3, v11

    goto/16 :goto_178

    :cond_2d3
    move v3, v8

    goto/16 :goto_145

    :cond_2d6
    move v3, v8

    goto/16 :goto_11a

    :cond_2d9
    move v2, v8

    goto/16 :goto_d1

    :cond_2dc
    move v2, v8

    goto/16 :goto_a5

    :cond_2df
    move v2, v8

    goto/16 :goto_75

    :cond_2e2
    move v2, v8

    goto/16 :goto_49
.end method
