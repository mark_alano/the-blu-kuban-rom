.class public final Lcom/google/zxing/qrcode/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/zxing/e;


# static fields
.field private static final a:[Lcom/google/zxing/g;


# instance fields
.field private final b:Lcom/google/zxing/qrcode/decoder/m;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 45
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/zxing/g;

    sput-object v0, Lcom/google/zxing/qrcode/a;->a:[Lcom/google/zxing/g;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Lcom/google/zxing/qrcode/decoder/m;

    invoke-direct {v0}, Lcom/google/zxing/qrcode/decoder/m;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/qrcode/a;->b:Lcom/google/zxing/qrcode/decoder/m;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/zxing/b;Ljava/util/Map;)Lcom/google/zxing/f;
    .registers 16
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 71
    if-eqz p2, :cond_ee

    sget-object v0, Lcom/google/zxing/DecodeHintType;->PURE_BARCODE:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ee

    .line 72
    invoke-virtual {p1}, Lcom/google/zxing/b;->c()Lcom/google/zxing/common/b;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/zxing/common/b;->b()[I

    move-result-object v8

    invoke-virtual {v7}, Lcom/google/zxing/common/b;->c()[I

    move-result-object v9

    if-eqz v8, :cond_1c

    if-nez v9, :cond_21

    :cond_1c
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_21
    invoke-virtual {v7}, Lcom/google/zxing/common/b;->e()I

    move-result v10

    invoke-virtual {v7}, Lcom/google/zxing/common/b;->d()I

    move-result v11

    aget v3, v8, v2

    aget v0, v8, v1

    move v4, v1

    move v5, v0

    move v6, v3

    move v0, v2

    :goto_31
    if-ge v6, v11, :cond_4f

    if-ge v5, v10, :cond_4f

    invoke-virtual {v7, v6, v5}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v3

    if-eq v4, v3, :cond_10a

    add-int/lit8 v3, v0, 0x1

    const/4 v0, 0x5

    if-eq v3, v0, :cond_4f

    if-nez v4, :cond_4d

    move v0, v1

    :goto_43
    move v12, v3

    move v3, v0

    move v0, v12

    :goto_46
    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    goto :goto_31

    :cond_4d
    move v0, v2

    goto :goto_43

    :cond_4f
    if-eq v6, v11, :cond_53

    if-ne v5, v10, :cond_58

    :cond_53
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_58
    aget v0, v8, v2

    sub-int v0, v6, v0

    int-to-float v0, v0

    const/high16 v3, 0x40e0

    div-float v3, v0, v3

    aget v4, v8, v1

    aget v1, v9, v1

    aget v5, v8, v2

    aget v0, v9, v2

    sub-int v6, v1, v4

    sub-int v8, v0, v5

    if-eq v6, v8, :cond_72

    sub-int v0, v1, v4

    add-int/2addr v0, v5

    :cond_72
    sub-int/2addr v0, v5

    add-int/lit8 v0, v0, 0x1

    int-to-float v0, v0

    div-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v6

    sub-int v0, v1, v4

    add-int/lit8 v0, v0, 0x1

    int-to-float v0, v0

    div-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v8

    if-lez v6, :cond_89

    if-gtz v8, :cond_8e

    :cond_89
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_8e
    if-eq v8, v6, :cond_95

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_95
    const/high16 v0, 0x4000

    div-float v0, v3, v0

    float-to-int v0, v0

    add-int/2addr v4, v0

    add-int/2addr v5, v0

    new-instance v9, Lcom/google/zxing/common/b;

    invoke-direct {v9, v6, v8}, Lcom/google/zxing/common/b;-><init>(II)V

    move v1, v2

    :goto_a2
    if-ge v1, v8, :cond_c0

    int-to-float v0, v1

    mul-float/2addr v0, v3

    float-to-int v0, v0

    add-int v10, v4, v0

    move v0, v2

    :goto_aa
    if-ge v0, v6, :cond_bc

    int-to-float v11, v0

    mul-float/2addr v11, v3

    float-to-int v11, v11

    add-int/2addr v11, v5

    invoke-virtual {v7, v11, v10}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v11

    if-eqz v11, :cond_b9

    invoke-virtual {v9, v0, v1}, Lcom/google/zxing/common/b;->b(II)V

    :cond_b9
    add-int/lit8 v0, v0, 0x1

    goto :goto_aa

    :cond_bc
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a2

    .line 73
    :cond_c0
    iget-object v0, p0, Lcom/google/zxing/qrcode/a;->b:Lcom/google/zxing/qrcode/decoder/m;

    invoke-virtual {v0, v9, p2}, Lcom/google/zxing/qrcode/decoder/m;->a(Lcom/google/zxing/common/b;Ljava/util/Map;)Lcom/google/zxing/common/d;

    move-result-object v1

    .line 74
    sget-object v0, Lcom/google/zxing/qrcode/a;->a:[Lcom/google/zxing/g;

    .line 81
    :goto_c8
    new-instance v2, Lcom/google/zxing/f;

    invoke-virtual {v1}, Lcom/google/zxing/common/d;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/zxing/common/d;->a()[B

    move-result-object v4

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->QR_CODE:Lcom/google/zxing/BarcodeFormat;

    invoke-direct {v2, v3, v4, v0, v5}, Lcom/google/zxing/f;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/g;Lcom/google/zxing/BarcodeFormat;)V

    .line 82
    invoke-virtual {v1}, Lcom/google/zxing/common/d;->c()Ljava/util/List;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_e2

    .line 84
    sget-object v3, Lcom/google/zxing/ResultMetadataType;->BYTE_SEGMENTS:Lcom/google/zxing/ResultMetadataType;

    invoke-virtual {v2, v3, v0}, Lcom/google/zxing/f;->a(Lcom/google/zxing/ResultMetadataType;Ljava/lang/Object;)V

    .line 86
    :cond_e2
    invoke-virtual {v1}, Lcom/google/zxing/common/d;->d()Ljava/lang/String;

    move-result-object v0

    .line 87
    if-eqz v0, :cond_ed

    .line 88
    sget-object v1, Lcom/google/zxing/ResultMetadataType;->ERROR_CORRECTION_LEVEL:Lcom/google/zxing/ResultMetadataType;

    invoke-virtual {v2, v1, v0}, Lcom/google/zxing/f;->a(Lcom/google/zxing/ResultMetadataType;Ljava/lang/Object;)V

    .line 90
    :cond_ed
    return-object v2

    .line 76
    :cond_ee
    new-instance v0, Lcom/google/zxing/qrcode/detector/c;

    invoke-virtual {p1}, Lcom/google/zxing/b;->c()Lcom/google/zxing/common/b;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/zxing/qrcode/detector/c;-><init>(Lcom/google/zxing/common/b;)V

    invoke-virtual {v0, p2}, Lcom/google/zxing/qrcode/detector/c;->a(Ljava/util/Map;)Lcom/google/zxing/common/f;

    move-result-object v0

    .line 77
    iget-object v1, p0, Lcom/google/zxing/qrcode/a;->b:Lcom/google/zxing/qrcode/decoder/m;

    invoke-virtual {v0}, Lcom/google/zxing/common/f;->d()Lcom/google/zxing/common/b;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/google/zxing/qrcode/decoder/m;->a(Lcom/google/zxing/common/b;Ljava/util/Map;)Lcom/google/zxing/common/d;

    move-result-object v1

    .line 78
    invoke-virtual {v0}, Lcom/google/zxing/common/f;->e()[Lcom/google/zxing/g;

    move-result-object v0

    goto :goto_c8

    :cond_10a
    move v3, v4

    goto/16 :goto_46
.end method

.method public final a()V
    .registers 1

    .prologue
    .line 96
    return-void
.end method
