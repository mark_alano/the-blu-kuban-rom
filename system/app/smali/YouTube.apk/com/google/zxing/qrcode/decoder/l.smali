.class final Lcom/google/zxing/qrcode/decoder/l;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[C


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 45
    const/16 v0, 0x2d

    new-array v0, v0, [C

    fill-array-data v0, :array_a

    sput-object v0, Lcom/google/zxing/qrcode/decoder/l;->a:[C

    return-void

    :array_a
    .array-data 0x2
        0x30t 0x0t
        0x31t 0x0t
        0x32t 0x0t
        0x33t 0x0t
        0x34t 0x0t
        0x35t 0x0t
        0x36t 0x0t
        0x37t 0x0t
        0x38t 0x0t
        0x39t 0x0t
        0x41t 0x0t
        0x42t 0x0t
        0x43t 0x0t
        0x44t 0x0t
        0x45t 0x0t
        0x46t 0x0t
        0x47t 0x0t
        0x48t 0x0t
        0x49t 0x0t
        0x4at 0x0t
        0x4bt 0x0t
        0x4ct 0x0t
        0x4dt 0x0t
        0x4et 0x0t
        0x4ft 0x0t
        0x50t 0x0t
        0x51t 0x0t
        0x52t 0x0t
        0x53t 0x0t
        0x54t 0x0t
        0x55t 0x0t
        0x56t 0x0t
        0x57t 0x0t
        0x58t 0x0t
        0x59t 0x0t
        0x5at 0x0t
        0x20t 0x0t
        0x24t 0x0t
        0x25t 0x0t
        0x2at 0x0t
        0x2bt 0x0t
        0x2dt 0x0t
        0x2et 0x0t
        0x2ft 0x0t
        0x3at 0x0t
    .end array-data
.end method

.method private static a(I)C
    .registers 2
    .parameter

    .prologue
    .line 238
    sget-object v0, Lcom/google/zxing/qrcode/decoder/l;->a:[C

    array-length v0, v0

    if-lt p0, v0, :cond_a

    .line 239
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 241
    :cond_a
    sget-object v0, Lcom/google/zxing/qrcode/decoder/l;->a:[C

    aget-char v0, v0, p0

    return v0
.end method

.method static a([BLcom/google/zxing/qrcode/decoder/o;Lcom/google/zxing/qrcode/decoder/ErrorCorrectionLevel;Ljava/util/Map;)Lcom/google/zxing/common/d;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 60
    new-instance v0, Lcom/google/zxing/common/c;

    invoke-direct {v0, p0}, Lcom/google/zxing/common/c;-><init>([B)V

    .line 61
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x32

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 62
    const/4 v3, 0x0

    .line 63
    const/4 v6, 0x0

    .line 64
    new-instance v4, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 68
    :goto_14
    invoke-virtual {v0}, Lcom/google/zxing/common/c;->b()I

    move-result v2

    const/4 v5, 0x4

    if-ge v2, v5, :cond_43

    .line 70
    sget-object v2, Lcom/google/zxing/qrcode/decoder/Mode;->TERMINATOR:Lcom/google/zxing/qrcode/decoder/Mode;

    move-object v7, v2

    .line 78
    :goto_1e
    sget-object v2, Lcom/google/zxing/qrcode/decoder/Mode;->TERMINATOR:Lcom/google/zxing/qrcode/decoder/Mode;

    if-eq v7, v2, :cond_fb

    .line 79
    sget-object v2, Lcom/google/zxing/qrcode/decoder/Mode;->FNC1_FIRST_POSITION:Lcom/google/zxing/qrcode/decoder/Mode;

    if-eq v7, v2, :cond_2a

    sget-object v2, Lcom/google/zxing/qrcode/decoder/Mode;->FNC1_SECOND_POSITION:Lcom/google/zxing/qrcode/decoder/Mode;

    if-ne v7, v2, :cond_54

    .line 81
    :cond_2a
    const/4 v2, 0x1

    .line 120
    :goto_2b
    sget-object v5, Lcom/google/zxing/qrcode/decoder/Mode;->TERMINATOR:Lcom/google/zxing/qrcode/decoder/Mode;

    if-ne v7, v5, :cond_fe

    .line 122
    new-instance v2, Lcom/google/zxing/common/d;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3c

    const/4 v4, 0x0

    :cond_3c
    if-nez p2, :cond_f5

    const/4 v0, 0x0

    :goto_3f
    invoke-direct {v2, p0, v1, v4, v0}, Lcom/google/zxing/common/d;-><init>([BLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    return-object v2

    .line 73
    :cond_43
    const/4 v2, 0x4

    :try_start_44
    invoke-virtual {v0, v2}, Lcom/google/zxing/common/c;->a(I)I

    move-result v2

    invoke-static {v2}, Lcom/google/zxing/qrcode/decoder/Mode;->forBits(I)Lcom/google/zxing/qrcode/decoder/Mode;
    :try_end_4b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_44 .. :try_end_4b} :catch_4e

    move-result-object v2

    move-object v7, v2

    .line 76
    goto :goto_1e

    .line 75
    :catch_4e
    move-exception v0

    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 82
    :cond_54
    sget-object v2, Lcom/google/zxing/qrcode/decoder/Mode;->STRUCTURED_APPEND:Lcom/google/zxing/qrcode/decoder/Mode;

    if-ne v7, v2, :cond_5f

    .line 85
    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Lcom/google/zxing/common/c;->a(I)I

    move v2, v6

    goto :goto_2b

    .line 86
    :cond_5f
    sget-object v2, Lcom/google/zxing/qrcode/decoder/Mode;->ECI:Lcom/google/zxing/qrcode/decoder/Mode;

    if-ne v7, v2, :cond_a5

    .line 88
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/zxing/common/c;->a(I)I

    move-result v2

    and-int/lit16 v3, v2, 0x80

    if-nez v3, :cond_7a

    and-int/lit8 v2, v2, 0x7f

    .line 89
    :goto_6f
    invoke-static {v2}, Lcom/google/zxing/common/CharacterSetECI;->getCharacterSetECIByValue(I)Lcom/google/zxing/common/CharacterSetECI;

    move-result-object v3

    .line 90
    if-nez v3, :cond_a3

    .line 91
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 88
    :cond_7a
    and-int/lit16 v3, v2, 0xc0

    const/16 v5, 0x80

    if-ne v3, v5, :cond_8c

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/google/zxing/common/c;->a(I)I

    move-result v3

    and-int/lit8 v2, v2, 0x3f

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v2, v3

    goto :goto_6f

    :cond_8c
    and-int/lit16 v3, v2, 0xe0

    const/16 v5, 0xc0

    if-ne v3, v5, :cond_9e

    const/16 v3, 0x10

    invoke-virtual {v0, v3}, Lcom/google/zxing/common/c;->a(I)I

    move-result v3

    and-int/lit8 v2, v2, 0x1f

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v2, v3

    goto :goto_6f

    :cond_9e
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_a3
    move v2, v6

    .line 93
    goto :goto_2b

    .line 95
    :cond_a5
    sget-object v2, Lcom/google/zxing/qrcode/decoder/Mode;->HANZI:Lcom/google/zxing/qrcode/decoder/Mode;

    if-ne v7, v2, :cond_bf

    .line 97
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/zxing/common/c;->a(I)I

    move-result v2

    .line 98
    invoke-virtual {v7, p1}, Lcom/google/zxing/qrcode/decoder/Mode;->getCharacterCountBits(Lcom/google/zxing/qrcode/decoder/o;)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/google/zxing/common/c;->a(I)I

    move-result v5

    .line 99
    const/4 v8, 0x1

    if-ne v2, v8, :cond_bc

    .line 100
    invoke-static {v0, v1, v5}, Lcom/google/zxing/qrcode/decoder/l;->a(Lcom/google/zxing/common/c;Ljava/lang/StringBuilder;I)V

    :cond_bc
    move v2, v6

    .line 102
    goto/16 :goto_2b

    .line 105
    :cond_bf
    invoke-virtual {v7, p1}, Lcom/google/zxing/qrcode/decoder/Mode;->getCharacterCountBits(Lcom/google/zxing/qrcode/decoder/o;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/zxing/common/c;->a(I)I

    move-result v2

    .line 106
    sget-object v5, Lcom/google/zxing/qrcode/decoder/Mode;->NUMERIC:Lcom/google/zxing/qrcode/decoder/Mode;

    if-ne v7, v5, :cond_d1

    .line 107
    invoke-static {v0, v1, v2}, Lcom/google/zxing/qrcode/decoder/l;->c(Lcom/google/zxing/common/c;Ljava/lang/StringBuilder;I)V

    move v2, v6

    goto/16 :goto_2b

    .line 108
    :cond_d1
    sget-object v5, Lcom/google/zxing/qrcode/decoder/Mode;->ALPHANUMERIC:Lcom/google/zxing/qrcode/decoder/Mode;

    if-ne v7, v5, :cond_db

    .line 109
    invoke-static {v0, v1, v2, v6}, Lcom/google/zxing/qrcode/decoder/l;->a(Lcom/google/zxing/common/c;Ljava/lang/StringBuilder;IZ)V

    move v2, v6

    goto/16 :goto_2b

    .line 110
    :cond_db
    sget-object v5, Lcom/google/zxing/qrcode/decoder/Mode;->BYTE:Lcom/google/zxing/qrcode/decoder/Mode;

    if-ne v7, v5, :cond_e6

    move-object v5, p3

    .line 111
    invoke-static/range {v0 .. v5}, Lcom/google/zxing/qrcode/decoder/l;->a(Lcom/google/zxing/common/c;Ljava/lang/StringBuilder;ILcom/google/zxing/common/CharacterSetECI;Ljava/util/Collection;Ljava/util/Map;)V

    move v2, v6

    goto/16 :goto_2b

    .line 112
    :cond_e6
    sget-object v5, Lcom/google/zxing/qrcode/decoder/Mode;->KANJI:Lcom/google/zxing/qrcode/decoder/Mode;

    if-ne v7, v5, :cond_f0

    .line 113
    invoke-static {v0, v1, v2}, Lcom/google/zxing/qrcode/decoder/l;->b(Lcom/google/zxing/common/c;Ljava/lang/StringBuilder;I)V

    move v2, v6

    goto/16 :goto_2b

    .line 115
    :cond_f0
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 122
    :cond_f5
    invoke-virtual {p2}, Lcom/google/zxing/qrcode/decoder/ErrorCorrectionLevel;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3f

    :cond_fb
    move v2, v6

    goto/16 :goto_2b

    :cond_fe
    move v6, v2

    goto/16 :goto_14
.end method

.method private static a(Lcom/google/zxing/common/c;Ljava/lang/StringBuilder;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 135
    mul-int/lit8 v0, p2, 0xd

    invoke-virtual {p0}, Lcom/google/zxing/common/c;->b()I

    move-result v1

    if-le v0, v1, :cond_d

    .line 136
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 141
    :cond_d
    mul-int/lit8 v0, p2, 0x2

    new-array v2, v0, [B

    .line 142
    const/4 v0, 0x0

    move v1, v0

    .line 143
    :goto_13
    if-lez p2, :cond_43

    .line 145
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/google/zxing/common/c;->a(I)I

    move-result v0

    .line 146
    div-int/lit8 v3, v0, 0x60

    shl-int/lit8 v3, v3, 0x8

    rem-int/lit8 v0, v0, 0x60

    or-int/2addr v0, v3

    .line 147
    const/16 v3, 0x3bf

    if-ge v0, v3, :cond_3e

    .line 149
    const v3, 0xa1a1

    add-int/2addr v0, v3

    .line 154
    :goto_2a
    shr-int/lit8 v3, v0, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    .line 155
    add-int/lit8 v3, v1, 0x1

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    aput-byte v0, v2, v3

    .line 156
    add-int/lit8 v0, v1, 0x2

    .line 157
    add-int/lit8 p2, p2, -0x1

    move v1, v0

    .line 158
    goto :goto_13

    .line 152
    :cond_3e
    const v3, 0xa6a1

    add-int/2addr v0, v3

    goto :goto_2a

    .line 161
    :cond_43
    :try_start_43
    new-instance v0, Ljava/lang/String;

    const-string v1, "GB2312"

    invoke-direct {v0, v2, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4d
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_43 .. :try_end_4d} :catch_4e

    .line 164
    return-void

    .line 163
    :catch_4e
    move-exception v0

    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0
.end method

.method private static a(Lcom/google/zxing/common/c;Ljava/lang/StringBuilder;ILcom/google/zxing/common/CharacterSetECI;Ljava/util/Collection;Ljava/util/Map;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 210
    shl-int/lit8 v0, p2, 0x3

    invoke-virtual {p0}, Lcom/google/zxing/common/c;->b()I

    move-result v1

    if-le v0, v1, :cond_d

    .line 211
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 214
    :cond_d
    new-array v1, p2, [B

    .line 215
    const/4 v0, 0x0

    :goto_10
    if-ge v0, p2, :cond_1e

    .line 216
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/google/zxing/common/c;->a(I)I

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 215
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 219
    :cond_1e
    if-nez p3, :cond_30

    .line 225
    invoke-static {v1, p5}, Lcom/google/zxing/common/k;->a([BLjava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 230
    :goto_24
    :try_start_24
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2c
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_24 .. :try_end_2c} :catch_35

    .line 234
    invoke-interface {p4, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 235
    return-void

    .line 227
    :cond_30
    invoke-virtual {p3}, Lcom/google/zxing/common/CharacterSetECI;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_24

    .line 232
    :catch_35
    move-exception v0

    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0
.end method

.method private static a(Lcom/google/zxing/common/c;Ljava/lang/StringBuilder;IZ)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v6, 0x25

    const/16 v5, 0xb

    const/4 v4, 0x6

    const/4 v3, 0x1

    .line 249
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 250
    :goto_a
    if-le p2, v3, :cond_30

    .line 251
    invoke-virtual {p0}, Lcom/google/zxing/common/c;->b()I

    move-result v1

    if-ge v1, v5, :cond_17

    .line 252
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 254
    :cond_17
    invoke-virtual {p0, v5}, Lcom/google/zxing/common/c;->a(I)I

    move-result v1

    .line 255
    div-int/lit8 v2, v1, 0x2d

    invoke-static {v2}, Lcom/google/zxing/qrcode/decoder/l;->a(I)C

    move-result v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 256
    rem-int/lit8 v1, v1, 0x2d

    invoke-static {v1}, Lcom/google/zxing/qrcode/decoder/l;->a(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 257
    add-int/lit8 p2, p2, -0x2

    .line 258
    goto :goto_a

    .line 259
    :cond_30
    if-ne p2, v3, :cond_48

    .line 261
    invoke-virtual {p0}, Lcom/google/zxing/common/c;->b()I

    move-result v1

    if-ge v1, v4, :cond_3d

    .line 262
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 264
    :cond_3d
    invoke-virtual {p0, v4}, Lcom/google/zxing/common/c;->a(I)I

    move-result v1

    invoke-static {v1}, Lcom/google/zxing/qrcode/decoder/l;->a(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 267
    :cond_48
    if-eqz p3, :cond_74

    .line 269
    :goto_4a
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-ge v0, v1, :cond_74

    .line 270
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v6, :cond_6b

    .line 271
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_6e

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v6, :cond_6e

    .line 273
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 269
    :cond_6b
    :goto_6b
    add-int/lit8 v0, v0, 0x1

    goto :goto_4a

    .line 276
    :cond_6e
    const/16 v1, 0x1d

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_6b

    .line 281
    :cond_74
    return-void
.end method

.method private static b(Lcom/google/zxing/common/c;Ljava/lang/StringBuilder;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 171
    mul-int/lit8 v0, p2, 0xd

    invoke-virtual {p0}, Lcom/google/zxing/common/c;->b()I

    move-result v1

    if-le v0, v1, :cond_d

    .line 172
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 177
    :cond_d
    mul-int/lit8 v0, p2, 0x2

    new-array v2, v0, [B

    .line 178
    const/4 v0, 0x0

    move v1, v0

    .line 179
    :goto_13
    if-lez p2, :cond_3f

    .line 181
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/google/zxing/common/c;->a(I)I

    move-result v0

    .line 182
    div-int/lit16 v3, v0, 0xc0

    shl-int/lit8 v3, v3, 0x8

    rem-int/lit16 v0, v0, 0xc0

    or-int/2addr v0, v3

    .line 183
    const/16 v3, 0x1f00

    if-ge v0, v3, :cond_3a

    .line 185
    const v3, 0x8140

    add-int/2addr v0, v3

    .line 190
    :goto_2a
    shr-int/lit8 v3, v0, 0x8

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    .line 191
    add-int/lit8 v3, v1, 0x1

    int-to-byte v0, v0

    aput-byte v0, v2, v3

    .line 192
    add-int/lit8 v0, v1, 0x2

    .line 193
    add-int/lit8 p2, p2, -0x1

    move v1, v0

    .line 194
    goto :goto_13

    .line 188
    :cond_3a
    const v3, 0xc140

    add-int/2addr v0, v3

    goto :goto_2a

    .line 197
    :cond_3f
    :try_start_3f
    new-instance v0, Ljava/lang/String;

    const-string v1, "SJIS"

    invoke-direct {v0, v2, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_49
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3f .. :try_end_49} :catch_4a

    .line 200
    return-void

    .line 199
    :catch_4a
    move-exception v0

    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0
.end method

.method private static c(Lcom/google/zxing/common/c;Ljava/lang/StringBuilder;I)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x7

    const/4 v3, 0x4

    const/16 v2, 0xa

    .line 287
    :goto_4
    const/4 v0, 0x3

    if-lt p2, v0, :cond_3f

    .line 289
    invoke-virtual {p0}, Lcom/google/zxing/common/c;->b()I

    move-result v0

    if-ge v0, v2, :cond_12

    .line 290
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 292
    :cond_12
    invoke-virtual {p0, v2}, Lcom/google/zxing/common/c;->a(I)I

    move-result v0

    .line 293
    const/16 v1, 0x3e8

    if-lt v0, v1, :cond_1f

    .line 294
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 296
    :cond_1f
    div-int/lit8 v1, v0, 0x64

    invoke-static {v1}, Lcom/google/zxing/qrcode/decoder/l;->a(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 297
    div-int/lit8 v1, v0, 0xa

    rem-int/lit8 v1, v1, 0xa

    invoke-static {v1}, Lcom/google/zxing/qrcode/decoder/l;->a(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 298
    rem-int/lit8 v0, v0, 0xa

    invoke-static {v0}, Lcom/google/zxing/qrcode/decoder/l;->a(I)C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 299
    add-int/lit8 p2, p2, -0x3

    .line 300
    goto :goto_4

    .line 301
    :cond_3f
    const/4 v0, 0x2

    if-ne p2, v0, :cond_6d

    .line 303
    invoke-virtual {p0}, Lcom/google/zxing/common/c;->b()I

    move-result v0

    if-ge v0, v4, :cond_4d

    .line 304
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 306
    :cond_4d
    invoke-virtual {p0, v4}, Lcom/google/zxing/common/c;->a(I)I

    move-result v0

    .line 307
    const/16 v1, 0x64

    if-lt v0, v1, :cond_5a

    .line 308
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 310
    :cond_5a
    div-int/lit8 v1, v0, 0xa

    invoke-static {v1}, Lcom/google/zxing/qrcode/decoder/l;->a(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 311
    rem-int/lit8 v0, v0, 0xa

    invoke-static {v0}, Lcom/google/zxing/qrcode/decoder/l;->a(I)C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 323
    :cond_6c
    :goto_6c
    return-void

    .line 312
    :cond_6d
    const/4 v0, 0x1

    if-ne p2, v0, :cond_6c

    .line 314
    invoke-virtual {p0}, Lcom/google/zxing/common/c;->b()I

    move-result v0

    if-ge v0, v3, :cond_7b

    .line 315
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 317
    :cond_7b
    invoke-virtual {p0, v3}, Lcom/google/zxing/common/c;->a(I)I

    move-result v0

    .line 318
    if-lt v0, v2, :cond_86

    .line 319
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 321
    :cond_86
    invoke-static {v0}, Lcom/google/zxing/qrcode/decoder/l;->a(I)C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_6c
.end method
