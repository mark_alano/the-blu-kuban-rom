.class public final Lcom/google/zxing/qrcode/decoder/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/zxing/common/reedsolomon/c;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lcom/google/zxing/common/reedsolomon/c;

    sget-object v1, Lcom/google/zxing/common/reedsolomon/a;->e:Lcom/google/zxing/common/reedsolomon/a;

    invoke-direct {v0, v1}, Lcom/google/zxing/common/reedsolomon/c;-><init>(Lcom/google/zxing/common/reedsolomon/a;)V

    iput-object v0, p0, Lcom/google/zxing/qrcode/decoder/m;->a:Lcom/google/zxing/common/reedsolomon/c;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/zxing/common/b;Ljava/util/Map;)Lcom/google/zxing/common/d;
    .registers 16
    .parameter
    .parameter

    .prologue
    .line 87
    new-instance v0, Lcom/google/zxing/qrcode/decoder/a;

    invoke-direct {v0, p1}, Lcom/google/zxing/qrcode/decoder/a;-><init>(Lcom/google/zxing/common/b;)V

    .line 88
    invoke-virtual {v0}, Lcom/google/zxing/qrcode/decoder/a;->b()Lcom/google/zxing/qrcode/decoder/o;

    move-result-object v4

    .line 89
    invoke-virtual {v0}, Lcom/google/zxing/qrcode/decoder/a;->a()Lcom/google/zxing/qrcode/decoder/n;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/zxing/qrcode/decoder/n;->a()Lcom/google/zxing/qrcode/decoder/ErrorCorrectionLevel;

    move-result-object v5

    .line 92
    invoke-virtual {v0}, Lcom/google/zxing/qrcode/decoder/a;->c()[B

    move-result-object v0

    .line 94
    invoke-static {v0, v4, v5}, Lcom/google/zxing/qrcode/decoder/b;->a([BLcom/google/zxing/qrcode/decoder/o;Lcom/google/zxing/qrcode/decoder/ErrorCorrectionLevel;)[Lcom/google/zxing/qrcode/decoder/b;

    move-result-object v6

    .line 97
    const/4 v1, 0x0

    .line 98
    array-length v2, v6

    const/4 v0, 0x0

    :goto_1c
    if-ge v0, v2, :cond_28

    aget-object v3, v6, v0

    .line 99
    invoke-virtual {v3}, Lcom/google/zxing/qrcode/decoder/b;->a()I

    move-result v3

    add-int/2addr v1, v3

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    .line 101
    :cond_28
    new-array v7, v1, [B

    .line 102
    const/4 v1, 0x0

    .line 105
    array-length v8, v6

    const/4 v0, 0x0

    move v3, v0

    :goto_2e
    if-ge v3, v8, :cond_72

    aget-object v0, v6, v3

    .line 106
    invoke-virtual {v0}, Lcom/google/zxing/qrcode/decoder/b;->b()[B

    move-result-object v9

    .line 107
    invoke-virtual {v0}, Lcom/google/zxing/qrcode/decoder/b;->a()I

    move-result v10

    .line 108
    array-length v2, v9

    new-array v11, v2, [I

    const/4 v0, 0x0

    :goto_3e
    if-ge v0, v2, :cond_49

    aget-byte v12, v9, v0

    and-int/lit16 v12, v12, 0xff

    aput v12, v11, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3e

    :cond_49
    array-length v0, v9

    sub-int/2addr v0, v10

    :try_start_4b
    iget-object v2, p0, Lcom/google/zxing/qrcode/decoder/m;->a:Lcom/google/zxing/common/reedsolomon/c;

    invoke-virtual {v2, v11, v0}, Lcom/google/zxing/common/reedsolomon/c;->a([II)V
    :try_end_50
    .catch Lcom/google/zxing/common/reedsolomon/ReedSolomonException; {:try_start_4b .. :try_end_50} :catch_5b

    const/4 v0, 0x0

    :goto_51
    if-ge v0, v10, :cond_61

    aget v2, v11, v0

    int-to-byte v2, v2

    aput-byte v2, v9, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_51

    :catch_5b
    move-exception v0

    invoke-static {}, Lcom/google/zxing/ChecksumException;->getChecksumInstance()Lcom/google/zxing/ChecksumException;

    move-result-object v0

    throw v0

    .line 109
    :cond_61
    const/4 v0, 0x0

    :goto_62
    if-ge v0, v10, :cond_6e

    .line 110
    add-int/lit8 v2, v1, 0x1

    aget-byte v11, v9, v0

    aput-byte v11, v7, v1

    .line 109
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_62

    .line 105
    :cond_6e
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2e

    .line 115
    :cond_72
    invoke-static {v7, v4, v5, p2}, Lcom/google/zxing/qrcode/decoder/l;->a([BLcom/google/zxing/qrcode/decoder/o;Lcom/google/zxing/qrcode/decoder/ErrorCorrectionLevel;Ljava/util/Map;)Lcom/google/zxing/common/d;

    move-result-object v0

    return-object v0
.end method
