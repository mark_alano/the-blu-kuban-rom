.class public final Lcom/google/zxing/qrcode/detector/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/zxing/common/b;

.field private b:Lcom/google/zxing/h;


# direct methods
.method public constructor <init>(Lcom/google/zxing/common/b;)V
    .registers 2
    .parameter

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/zxing/qrcode/detector/c;->a:Lcom/google/zxing/common/b;

    .line 46
    return-void
.end method

.method private a(IIII)F
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x3f80

    .line 262
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/zxing/qrcode/detector/c;->b(IIII)F

    move-result v4

    .line 266
    sub-int v0, p3, p1

    sub-int v0, p1, v0

    .line 267
    if-gez v0, :cond_31

    .line 268
    int-to-float v3, p1

    sub-int v0, p1, v0

    int-to-float v0, v0

    div-float v0, v3, v0

    move v3, v2

    .line 274
    :goto_14
    int-to-float v5, p2

    sub-int v6, p4, p2

    int-to-float v6, v6

    mul-float/2addr v0, v6

    sub-float v0, v5, v0

    float-to-int v0, v0

    .line 277
    if-gez v0, :cond_50

    .line 278
    int-to-float v5, p2

    sub-int v0, p2, v0

    int-to-float v0, v0

    div-float v0, v5, v0

    .line 284
    :goto_24
    int-to-float v5, p1

    sub-int/2addr v3, p1

    int-to-float v3, v3

    mul-float/2addr v0, v3

    add-float/2addr v0, v5

    float-to-int v0, v0

    .line 286
    invoke-direct {p0, p1, p2, v0, v2}, Lcom/google/zxing/qrcode/detector/c;->b(IIII)F

    move-result v0

    add-float/2addr v0, v4

    .line 289
    sub-float/2addr v0, v1

    return v0

    .line 270
    :cond_31
    iget-object v3, p0, Lcom/google/zxing/qrcode/detector/c;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v3}, Lcom/google/zxing/common/b;->d()I

    move-result v3

    if-lt v0, v3, :cond_72

    .line 271
    iget-object v3, p0, Lcom/google/zxing/qrcode/detector/c;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v3}, Lcom/google/zxing/common/b;->d()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v3, p1

    int-to-float v3, v3

    sub-int/2addr v0, p1

    int-to-float v0, v0

    div-float v0, v3, v0

    .line 272
    iget-object v3, p0, Lcom/google/zxing/qrcode/detector/c;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v3}, Lcom/google/zxing/common/b;->d()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    goto :goto_14

    .line 280
    :cond_50
    iget-object v2, p0, Lcom/google/zxing/qrcode/detector/c;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v2}, Lcom/google/zxing/common/b;->e()I

    move-result v2

    if-lt v0, v2, :cond_6f

    .line 281
    iget-object v2, p0, Lcom/google/zxing/qrcode/detector/c;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v2}, Lcom/google/zxing/common/b;->e()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, p2

    int-to-float v2, v2

    sub-int/2addr v0, p2

    int-to-float v0, v0

    div-float v0, v2, v0

    .line 282
    iget-object v2, p0, Lcom/google/zxing/qrcode/detector/c;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v2}, Lcom/google/zxing/common/b;->e()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    goto :goto_24

    :cond_6f
    move v2, v0

    move v0, v1

    goto :goto_24

    :cond_72
    move v3, v0

    move v0, v1

    goto :goto_14
.end method

.method private a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F
    .registers 9
    .parameter
    .parameter

    .prologue
    const/high16 v5, 0x40e0

    .line 236
    invoke-virtual {p1}, Lcom/google/zxing/g;->a()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Lcom/google/zxing/g;->b()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Lcom/google/zxing/g;->a()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Lcom/google/zxing/g;->b()F

    move-result v3

    float-to-int v3, v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/zxing/qrcode/detector/c;->a(IIII)F

    move-result v0

    .line 240
    invoke-virtual {p2}, Lcom/google/zxing/g;->a()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Lcom/google/zxing/g;->b()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Lcom/google/zxing/g;->a()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Lcom/google/zxing/g;->b()F

    move-result v4

    float-to-int v4, v4

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/zxing/qrcode/detector/c;->a(IIII)F

    move-result v1

    .line 244
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_3b

    .line 245
    div-float v0, v1, v5

    .line 252
    :goto_3a
    return v0

    .line 247
    :cond_3b
    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_43

    .line 248
    div-float/2addr v0, v5

    goto :goto_3a

    .line 252
    :cond_43
    add-float/2addr v0, v1

    const/high16 v1, 0x4160

    div-float/2addr v0, v1

    goto :goto_3a
.end method

.method private a(Lcom/google/zxing/qrcode/detector/f;)Lcom/google/zxing/common/f;
    .registers 26
    .parameter

    .prologue
    .line 88
    invoke-virtual/range {p1 .. p1}, Lcom/google/zxing/qrcode/detector/f;->b()Lcom/google/zxing/qrcode/detector/d;

    move-result-object v21

    .line 89
    invoke-virtual/range {p1 .. p1}, Lcom/google/zxing/qrcode/detector/f;->c()Lcom/google/zxing/qrcode/detector/d;

    move-result-object v22

    .line 90
    invoke-virtual/range {p1 .. p1}, Lcom/google/zxing/qrcode/detector/f;->a()Lcom/google/zxing/qrcode/detector/d;

    move-result-object v23

    .line 92
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/qrcode/detector/c;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v3

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/qrcode/detector/c;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v4

    add-float/2addr v3, v4

    const/high16 v4, 0x4000

    div-float v9, v3, v4

    .line 93
    const/high16 v3, 0x3f80

    cmpg-float v3, v9, v3

    if-gez v3, :cond_30

    .line 94
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v3

    throw v3

    .line 96
    :cond_30
    invoke-static/range {v21 .. v22}, Lcom/google/zxing/g;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v3

    div-float/2addr v3, v9

    const/high16 v4, 0x3f00

    add-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/google/zxing/g;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v4

    div-float/2addr v4, v9

    const/high16 v5, 0x3f00

    add-float/2addr v4, v5

    float-to-int v4, v4

    add-int/2addr v3, v4

    shr-int/lit8 v3, v3, 0x1

    add-int/lit8 v3, v3, 0x7

    and-int/lit8 v4, v3, 0x3

    packed-switch v4, :pswitch_data_1b4

    :pswitch_50
    move/from16 v19, v3

    .line 97
    :goto_52
    invoke-static/range {v19 .. v19}, Lcom/google/zxing/qrcode/decoder/o;->a(I)Lcom/google/zxing/qrcode/decoder/o;

    move-result-object v3

    .line 98
    invoke-virtual {v3}, Lcom/google/zxing/qrcode/decoder/o;->d()I

    move-result v4

    add-int/lit8 v4, v4, -0x7

    .line 100
    const/4 v11, 0x0

    .line 102
    invoke-virtual {v3}, Lcom/google/zxing/qrcode/decoder/o;->b()[I

    move-result-object v3

    array-length v3, v3

    if-lez v3, :cond_1b0

    .line 105
    invoke-virtual/range {v22 .. v22}, Lcom/google/zxing/qrcode/detector/d;->a()F

    move-result v3

    invoke-virtual/range {v21 .. v21}, Lcom/google/zxing/qrcode/detector/d;->a()F

    move-result v5

    sub-float/2addr v3, v5

    invoke-virtual/range {v23 .. v23}, Lcom/google/zxing/qrcode/detector/d;->a()F

    move-result v5

    add-float/2addr v3, v5

    .line 106
    invoke-virtual/range {v22 .. v22}, Lcom/google/zxing/qrcode/detector/d;->b()F

    move-result v5

    invoke-virtual/range {v21 .. v21}, Lcom/google/zxing/qrcode/detector/d;->b()F

    move-result v6

    sub-float/2addr v5, v6

    invoke-virtual/range {v23 .. v23}, Lcom/google/zxing/qrcode/detector/d;->b()F

    move-result v6

    add-float/2addr v5, v6

    .line 110
    const/high16 v6, 0x3f80

    const/high16 v7, 0x4040

    int-to-float v4, v4

    div-float v4, v7, v4

    sub-float v4, v6, v4

    .line 111
    invoke-virtual/range {v21 .. v21}, Lcom/google/zxing/qrcode/detector/d;->a()F

    move-result v6

    invoke-virtual/range {v21 .. v21}, Lcom/google/zxing/qrcode/detector/d;->a()F

    move-result v7

    sub-float/2addr v3, v7

    mul-float/2addr v3, v4

    add-float/2addr v3, v6

    float-to-int v13, v3

    .line 112
    invoke-virtual/range {v21 .. v21}, Lcom/google/zxing/qrcode/detector/d;->b()F

    move-result v3

    invoke-virtual/range {v21 .. v21}, Lcom/google/zxing/qrcode/detector/d;->b()F

    move-result v6

    sub-float/2addr v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v14, v3

    .line 115
    const/4 v3, 0x4

    move v12, v3

    :goto_a3
    const/16 v3, 0x10

    if-gt v12, v3, :cond_1b0

    .line 117
    int-to-float v3, v12

    mul-float/2addr v3, v9

    float-to-int v3, v3

    const/4 v4, 0x0

    sub-int v5, v13, v3

    :try_start_ad
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/zxing/qrcode/detector/c;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v4}, Lcom/google/zxing/common/b;->d()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    add-int v6, v13, v3

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v7

    sub-int v4, v7, v5

    int-to-float v4, v4

    const/high16 v6, 0x4040

    mul-float/2addr v6, v9

    cmpg-float v4, v4, v6

    if-gez v4, :cond_e6

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v3

    throw v3
    :try_end_d0
    .catch Lcom/google/zxing/NotFoundException; {:try_start_ad .. :try_end_d0} :catch_d0

    .line 115
    :catch_d0
    move-exception v3

    shl-int/lit8 v3, v12, 0x1

    move v12, v3

    goto :goto_a3

    .line 96
    :pswitch_d5
    add-int/lit8 v3, v3, 0x1

    move/from16 v19, v3

    goto/16 :goto_52

    :pswitch_db
    add-int/lit8 v3, v3, -0x1

    move/from16 v19, v3

    goto/16 :goto_52

    :pswitch_e1
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v3

    throw v3

    .line 117
    :cond_e6
    const/4 v4, 0x0

    sub-int v6, v14, v3

    :try_start_e9
    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/zxing/qrcode/detector/c;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v4}, Lcom/google/zxing/common/b;->e()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    add-int/2addr v3, v14

    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    move-result v8

    sub-int v3, v8, v6

    int-to-float v3, v3

    const/high16 v4, 0x4040

    mul-float/2addr v4, v9

    cmpg-float v3, v3, v4

    if-gez v3, :cond_10b

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v3

    throw v3

    :cond_10b
    new-instance v3, Lcom/google/zxing/qrcode/detector/b;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/zxing/qrcode/detector/c;->a:Lcom/google/zxing/common/b;

    sub-int/2addr v7, v5

    sub-int/2addr v8, v6

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/zxing/qrcode/detector/c;->b:Lcom/google/zxing/h;

    invoke-direct/range {v3 .. v10}, Lcom/google/zxing/qrcode/detector/b;-><init>(Lcom/google/zxing/common/b;IIIIFLcom/google/zxing/h;)V

    invoke-virtual {v3}, Lcom/google/zxing/qrcode/detector/b;->a()Lcom/google/zxing/qrcode/detector/a;
    :try_end_11d
    .catch Lcom/google/zxing/NotFoundException; {:try_start_e9 .. :try_end_11d} :catch_d0

    move-result-object v3

    move-object/from16 v20, v3

    .line 129
    :goto_120
    move/from16 v0, v19

    int-to-float v3, v0

    const/high16 v4, 0x4060

    sub-float v5, v3, v4

    if-eqz v20, :cond_17f

    invoke-virtual/range {v20 .. v20}, Lcom/google/zxing/g;->a()F

    move-result v15

    invoke-virtual/range {v20 .. v20}, Lcom/google/zxing/g;->b()F

    move-result v16

    const/high16 v3, 0x4040

    sub-float v7, v5, v3

    move v8, v7

    :goto_136
    const/high16 v3, 0x4060

    const/high16 v4, 0x4060

    const/high16 v6, 0x4060

    const/high16 v9, 0x4060

    invoke-virtual/range {v21 .. v21}, Lcom/google/zxing/g;->a()F

    move-result v11

    invoke-virtual/range {v21 .. v21}, Lcom/google/zxing/g;->b()F

    move-result v12

    invoke-virtual/range {v22 .. v22}, Lcom/google/zxing/g;->a()F

    move-result v13

    invoke-virtual/range {v22 .. v22}, Lcom/google/zxing/g;->b()F

    move-result v14

    invoke-virtual/range {v23 .. v23}, Lcom/google/zxing/g;->a()F

    move-result v17

    invoke-virtual/range {v23 .. v23}, Lcom/google/zxing/g;->b()F

    move-result v18

    move v10, v5

    invoke-static/range {v3 .. v18}, Lcom/google/zxing/common/j;->a(FFFFFFFFFFFFFFFF)Lcom/google/zxing/common/j;

    move-result-object v3

    .line 132
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/zxing/qrcode/detector/c;->a:Lcom/google/zxing/common/b;

    invoke-static {}, Lcom/google/zxing/common/h;->a()Lcom/google/zxing/common/h;

    move-result-object v5

    move/from16 v0, v19

    move/from16 v1, v19

    invoke-virtual {v5, v4, v0, v1, v3}, Lcom/google/zxing/common/h;->a(Lcom/google/zxing/common/b;IILcom/google/zxing/common/j;)Lcom/google/zxing/common/b;

    move-result-object v4

    .line 135
    if-nez v20, :cond_1a0

    .line 136
    const/4 v3, 0x3

    new-array v3, v3, [Lcom/google/zxing/g;

    const/4 v5, 0x0

    aput-object v23, v3, v5

    const/4 v5, 0x1

    aput-object v21, v3, v5

    const/4 v5, 0x2

    aput-object v22, v3, v5

    .line 140
    :goto_179
    new-instance v5, Lcom/google/zxing/common/f;

    invoke-direct {v5, v4, v3}, Lcom/google/zxing/common/f;-><init>(Lcom/google/zxing/common/b;[Lcom/google/zxing/g;)V

    return-object v5

    .line 129
    :cond_17f
    invoke-virtual/range {v22 .. v22}, Lcom/google/zxing/g;->a()F

    move-result v3

    invoke-virtual/range {v21 .. v21}, Lcom/google/zxing/g;->a()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-virtual/range {v23 .. v23}, Lcom/google/zxing/g;->a()F

    move-result v4

    add-float v15, v3, v4

    invoke-virtual/range {v22 .. v22}, Lcom/google/zxing/g;->b()F

    move-result v3

    invoke-virtual/range {v21 .. v21}, Lcom/google/zxing/g;->b()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-virtual/range {v23 .. v23}, Lcom/google/zxing/g;->b()F

    move-result v4

    add-float v16, v3, v4

    move v7, v5

    move v8, v5

    goto :goto_136

    .line 138
    :cond_1a0
    const/4 v3, 0x4

    new-array v3, v3, [Lcom/google/zxing/g;

    const/4 v5, 0x0

    aput-object v23, v3, v5

    const/4 v5, 0x1

    aput-object v21, v3, v5

    const/4 v5, 0x2

    aput-object v22, v3, v5

    const/4 v5, 0x3

    aput-object v20, v3, v5

    goto :goto_179

    :cond_1b0
    move-object/from16 v20, v11

    goto/16 :goto_120

    .line 96
    :pswitch_data_1b4
    .packed-switch 0x0
        :pswitch_d5
        :pswitch_50
        :pswitch_db
        :pswitch_e1
    .end packed-switch
.end method

.method private b(IIII)F
    .registers 24
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 303
    sub-int v3, p4, p2

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    sub-int v4, p3, p1

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-le v3, v4, :cond_5e

    const/4 v3, 0x1

    move v12, v3

    .line 304
    :goto_10
    if-eqz v12, :cond_9a

    .line 313
    :goto_12
    sub-int v3, p4, p2

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v13

    .line 314
    sub-int v3, p3, p1

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v14

    .line 315
    neg-int v3, v13

    shr-int/lit8 v5, v3, 0x1

    .line 316
    move/from16 v0, p2

    move/from16 v1, p4

    if-ge v0, v1, :cond_61

    const/4 v3, 0x1

    move v11, v3

    .line 317
    :goto_29
    move/from16 v0, p1

    move/from16 v1, p3

    if-ge v0, v1, :cond_64

    const/4 v3, 0x1

    .line 320
    :goto_30
    const/4 v6, 0x0

    .line 322
    add-int v15, p4, v11

    move/from16 v8, p2

    move v10, v5

    move/from16 v5, p1

    .line 323
    :goto_38
    if-eq v8, v15, :cond_98

    .line 324
    if-eqz v12, :cond_66

    move v9, v5

    .line 325
    :goto_3d
    if-eqz v12, :cond_68

    move v7, v8

    .line 330
    :goto_40
    const/4 v4, 0x1

    if-ne v6, v4, :cond_6a

    const/4 v4, 0x1

    :goto_44
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/qrcode/detector/c;->a:Lcom/google/zxing/common/b;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v9, v7}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v7

    if-ne v4, v7, :cond_96

    .line 331
    const/4 v4, 0x2

    if-ne v6, v4, :cond_6c

    .line 332
    move/from16 v0, p2

    move/from16 v1, p1

    invoke-static {v8, v5, v0, v1}, Lcom/google/zxing/common/a/a;->a(IIII)F

    move-result v3

    .line 353
    :goto_5d
    return v3

    .line 303
    :cond_5e
    const/4 v3, 0x0

    move v12, v3

    goto :goto_10

    .line 316
    :cond_61
    const/4 v3, -0x1

    move v11, v3

    goto :goto_29

    .line 317
    :cond_64
    const/4 v3, -0x1

    goto :goto_30

    :cond_66
    move v9, v8

    .line 324
    goto :goto_3d

    :cond_68
    move v7, v5

    .line 325
    goto :goto_40

    .line 330
    :cond_6a
    const/4 v4, 0x0

    goto :goto_44

    .line 334
    :cond_6c
    add-int/lit8 v7, v6, 0x1

    .line 337
    :goto_6e
    add-int v6, v10, v14

    .line 338
    if-lez v6, :cond_93

    .line 339
    move/from16 v0, p3

    if-eq v5, v0, :cond_7f

    .line 340
    add-int v4, v5, v3

    .line 343
    sub-int v5, v6, v13

    .line 323
    :goto_7a
    add-int/2addr v8, v11

    move v6, v7

    move v10, v5

    move v5, v4

    goto :goto_38

    :cond_7f
    move v3, v7

    .line 349
    :goto_80
    const/4 v4, 0x2

    if-ne v3, v4, :cond_90

    .line 350
    add-int v3, p4, v11

    move/from16 v0, p3

    move/from16 v1, p2

    move/from16 v2, p1

    invoke-static {v3, v0, v1, v2}, Lcom/google/zxing/common/a/a;->a(IIII)F

    move-result v3

    goto :goto_5d

    .line 353
    :cond_90
    const/high16 v3, 0x7fc0

    goto :goto_5d

    :cond_93
    move v4, v5

    move v5, v6

    goto :goto_7a

    :cond_96
    move v7, v6

    goto :goto_6e

    :cond_98
    move v3, v6

    goto :goto_80

    :cond_9a
    move/from16 v17, p4

    move/from16 p4, p3

    move/from16 p3, v17

    move/from16 v18, p2

    move/from16 p2, p1

    move/from16 p1, v18

    goto/16 :goto_12
.end method


# virtual methods
.method public final a(Ljava/util/Map;)Lcom/google/zxing/common/f;
    .registers 5
    .parameter

    .prologue
    .line 76
    if-nez p1, :cond_17

    const/4 v0, 0x0

    :goto_3
    iput-object v0, p0, Lcom/google/zxing/qrcode/detector/c;->b:Lcom/google/zxing/h;

    .line 79
    new-instance v0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;

    iget-object v1, p0, Lcom/google/zxing/qrcode/detector/c;->a:Lcom/google/zxing/common/b;

    iget-object v2, p0, Lcom/google/zxing/qrcode/detector/c;->b:Lcom/google/zxing/h;

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;-><init>(Lcom/google/zxing/common/b;Lcom/google/zxing/h;)V

    .line 80
    invoke-virtual {v0, p1}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a(Ljava/util/Map;)Lcom/google/zxing/qrcode/detector/f;

    move-result-object v0

    .line 82
    invoke-direct {p0, v0}, Lcom/google/zxing/qrcode/detector/c;->a(Lcom/google/zxing/qrcode/detector/f;)Lcom/google/zxing/common/f;

    move-result-object v0

    return-object v0

    .line 76
    :cond_17
    sget-object v0, Lcom/google/zxing/DecodeHintType;->NEED_RESULT_POINT_CALLBACK:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/h;

    goto :goto_3
.end method
