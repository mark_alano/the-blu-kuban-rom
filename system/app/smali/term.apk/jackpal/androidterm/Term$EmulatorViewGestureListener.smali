.class Ljackpal/androidterm/Term$EmulatorViewGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "Term.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljackpal/androidterm/Term;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EmulatorViewGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Ljackpal/androidterm/Term;

.field private view:Ljackpal/androidterm/emulatorview/EmulatorView;


# direct methods
.method public constructor <init>(Ljackpal/androidterm/Term;Ljackpal/androidterm/emulatorview/EmulatorView;)V
    .registers 3
    .parameter
    .parameter "view"

    .prologue
    .line 212
    iput-object p1, p0, Ljackpal/androidterm/Term$EmulatorViewGestureListener;->this$0:Ljackpal/androidterm/Term;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 213
    iput-object p2, p0, Ljackpal/androidterm/Term$EmulatorViewGestureListener;->view:Ljackpal/androidterm/emulatorview/EmulatorView;

    .line 214
    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 15
    .parameter "e1"
    .parameter "e2"
    .parameter "velocityX"
    .parameter "velocityY"

    .prologue
    .line 224
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 225
    .local v0, absVelocityX:F
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 226
    .local v1, absVelocityY:F
    float-to-double v2, v0

    const-wide v4, 0x408f400000000000L

    const-wide/high16 v6, 0x4000

    float-to-double v8, v1

    mul-double/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    cmpl-double v2, v2, v4

    if-lez v2, :cond_34

    .line 228
    const/4 v2, 0x0

    cmpl-float v2, p3, v2

    if-lez v2, :cond_2a

    .line 230
    iget-object v2, p0, Ljackpal/androidterm/Term$EmulatorViewGestureListener;->this$0:Ljackpal/androidterm/Term;

    #getter for: Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;
    invoke-static {v2}, Ljackpal/androidterm/Term;->access$600(Ljackpal/androidterm/Term;)Ljackpal/androidterm/TermViewFlipper;

    move-result-object v2

    invoke-virtual {v2}, Ljackpal/androidterm/TermViewFlipper;->showPrevious()V

    .line 235
    :goto_28
    const/4 v2, 0x1

    .line 237
    :goto_29
    return v2

    .line 233
    :cond_2a
    iget-object v2, p0, Ljackpal/androidterm/Term$EmulatorViewGestureListener;->this$0:Ljackpal/androidterm/Term;

    #getter for: Ljackpal/androidterm/Term;->mViewFlipper:Ljackpal/androidterm/TermViewFlipper;
    invoke-static {v2}, Ljackpal/androidterm/Term;->access$600(Ljackpal/androidterm/Term;)Ljackpal/androidterm/TermViewFlipper;

    move-result-object v2

    invoke-virtual {v2}, Ljackpal/androidterm/TermViewFlipper;->showNext()V

    goto :goto_28

    .line 237
    :cond_34
    const/4 v2, 0x0

    goto :goto_29
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "e"

    .prologue
    .line 218
    iget-object v0, p0, Ljackpal/androidterm/Term$EmulatorViewGestureListener;->this$0:Ljackpal/androidterm/Term;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Ljackpal/androidterm/Term$EmulatorViewGestureListener;->view:Ljackpal/androidterm/emulatorview/EmulatorView;

    invoke-virtual {v3}, Ljackpal/androidterm/emulatorview/EmulatorView;->getVisibleWidth()I

    move-result v3

    iget-object v4, p0, Ljackpal/androidterm/Term$EmulatorViewGestureListener;->view:Ljackpal/androidterm/emulatorview/EmulatorView;

    invoke-virtual {v4}, Ljackpal/androidterm/emulatorview/EmulatorView;->getVisibleHeight()I

    move-result v4

    #calls: Ljackpal/androidterm/Term;->doUIToggle(IIII)V
    invoke-static {v0, v1, v2, v3, v4}, Ljackpal/androidterm/Term;->access$900(Ljackpal/androidterm/Term;IIII)V

    .line 219
    const/4 v0, 0x1

    return v0
.end method
