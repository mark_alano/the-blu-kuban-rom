.class public Ljackpal/androidterm/WindowListAdapter;
.super Landroid/widget/BaseAdapter;
.source "WindowListAdapter.java"

# interfaces
.implements Ljackpal/androidterm/emulatorview/UpdateCallback;


# instance fields
.field private mSessions:Ljackpal/androidterm/util/SessionList;


# direct methods
.method public constructor <init>(Ljackpal/androidterm/util/SessionList;)V
    .registers 2
    .parameter "sessions"

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 34
    invoke-virtual {p0, p1}, Ljackpal/androidterm/WindowListAdapter;->setSessions(Ljackpal/androidterm/util/SessionList;)V

    .line 35
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .prologue
    .line 45
    iget-object v0, p0, Ljackpal/androidterm/WindowListAdapter;->mSessions:Ljackpal/androidterm/util/SessionList;

    invoke-virtual {v0}, Ljackpal/androidterm/util/SessionList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "position"

    .prologue
    .line 49
    iget-object v0, p0, Ljackpal/androidterm/WindowListAdapter;->mSessions:Ljackpal/androidterm/util/SessionList;

    invoke-virtual {v0, p1}, Ljackpal/androidterm/util/SessionList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .parameter "position"

    .prologue
    .line 53
    int-to-long v0, p1

    return-wide v0
.end method

.method protected getSessionTitle(ILjava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "position"
    .parameter "defaultTitle"

    .prologue
    .line 57
    iget-object v1, p0, Ljackpal/androidterm/WindowListAdapter;->mSessions:Ljackpal/androidterm/util/SessionList;

    invoke-virtual {v1, p1}, Ljackpal/androidterm/util/SessionList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljackpal/androidterm/emulatorview/TermSession;

    .line 58
    .local v0, session:Ljackpal/androidterm/emulatorview/TermSession;
    if-eqz v0, :cond_14

    instance-of v1, v0, Ljackpal/androidterm/ShellTermSession;

    if-eqz v1, :cond_14

    .line 59
    check-cast v0, Ljackpal/androidterm/ShellTermSession;

    .end local v0           #session:Ljackpal/androidterm/emulatorview/TermSession;
    invoke-virtual {v0, p2}, Ljackpal/androidterm/ShellTermSession;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 61
    .end local p2
    :cond_14
    return-object p2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 15
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v10, 0x0

    .line 66
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 67
    .local v0, act:Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f030001

    invoke-virtual {v7, v8, p3, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 68
    .local v1, child:Landroid/view/View;
    const v7, 0x7f0b0003

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 70
    .local v2, close:Landroid/view/View;
    const v7, 0x7f0b0001

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 71
    .local v5, label:Landroid/widget/TextView;
    const v7, 0x7f060022

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    add-int/lit8 v9, p1, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v0, v7, v8}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 72
    .local v4, defaultTitle:Ljava/lang/String;
    invoke-virtual {p0, p1, v4}, Ljackpal/androidterm/WindowListAdapter;->getSessionTitle(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v6, p0, Ljackpal/androidterm/WindowListAdapter;->mSessions:Ljackpal/androidterm/util/SessionList;

    .line 75
    .local v6, sessions:Ljackpal/androidterm/util/SessionList;
    move v3, p1

    .line 76
    .local v3, closePosition:I
    new-instance v7, Ljackpal/androidterm/WindowListAdapter$1;

    invoke-direct {v7, p0, v6, v3}, Ljackpal/androidterm/WindowListAdapter$1;-><init>(Ljackpal/androidterm/WindowListAdapter;Ljackpal/androidterm/util/SessionList;I)V

    invoke-virtual {v2, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    return-object v1
.end method

.method public onUpdate()V
    .registers 1

    .prologue
    .line 90
    invoke-virtual {p0}, Ljackpal/androidterm/WindowListAdapter;->notifyDataSetChanged()V

    .line 91
    return-void
.end method

.method public setSessions(Ljackpal/androidterm/util/SessionList;)V
    .registers 2
    .parameter "sessions"

    .prologue
    .line 38
    iput-object p1, p0, Ljackpal/androidterm/WindowListAdapter;->mSessions:Ljackpal/androidterm/util/SessionList;

    .line 39
    if-eqz p1, :cond_7

    .line 40
    invoke-virtual {p0}, Ljackpal/androidterm/WindowListAdapter;->onUpdate()V

    .line 42
    :cond_7
    return-void
.end method
