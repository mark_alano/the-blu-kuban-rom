.class Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;
.super Ljackpal/androidterm/emulatorview/BaseTextRenderer;
.source "Bitmap4x8FontRenderer.java"


# static fields
.field private static final BYTE_SCALE:F = 0.003921569f

.field private static final kCharacterHeight:I = 0x8

.field private static final kCharacterWidth:I = 0x4


# instance fields
.field private mColorMatrix:[F

.field private mCurrentBackColor:I

.field private mCurrentForeColor:I

.field private mFont:Landroid/graphics/Bitmap;

.field private mPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Ljackpal/androidterm/emulatorview/ColorScheme;)V
    .registers 7
    .parameter "resources"
    .parameter "scheme"

    .prologue
    .line 42
    invoke-direct {p0, p2}, Ljackpal/androidterm/emulatorview/BaseTextRenderer;-><init>(Ljackpal/androidterm/emulatorview/ColorScheme;)V

    .line 43
    sget v1, Ljackpal/androidterm/emulatorview/compat/AndroidCompat;->SDK:I

    const/4 v2, 0x3

    if-gt v1, v2, :cond_24

    sget v0, Ljackpal/androidterm/emulatorview/R$drawable;->atari_small:I

    .line 45
    .local v0, fontResource:I
    :goto_a
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mFont:Landroid/graphics/Bitmap;

    .line 46
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mPaint:Landroid/graphics/Paint;

    .line 47
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mPaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 48
    return-void

    .line 43
    .end local v0           #fontResource:I
    :cond_24
    sget v0, Ljackpal/androidterm/emulatorview/R$drawable;->atari_small_nodpi:I

    goto :goto_a
.end method

.method private setColorMatrix(II)V
    .registers 12
    .parameter "foreColor"
    .parameter "backColor"

    .prologue
    .line 120
    iget v5, p0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mCurrentForeColor:I

    if-ne p1, v5, :cond_c

    iget v5, p0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mCurrentBackColor:I

    if-ne p2, v5, :cond_c

    iget-object v5, p0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mColorMatrix:[F

    if-nez v5, :cond_57

    .line 123
    :cond_c
    iput p1, p0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mCurrentForeColor:I

    .line 124
    iput p2, p0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mCurrentBackColor:I

    .line 125
    iget-object v5, p0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mColorMatrix:[F

    if-nez v5, :cond_22

    .line 126
    const/16 v5, 0x14

    new-array v5, v5, [F

    iput-object v5, p0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mColorMatrix:[F

    .line 127
    iget-object v5, p0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mColorMatrix:[F

    const/16 v6, 0x12

    const/high16 v7, 0x3f80

    aput v7, v5, v6

    .line 129
    :cond_22
    const/4 v1, 0x0

    .local v1, component:I
    :goto_23
    const/4 v5, 0x3

    if-ge v1, v5, :cond_4b

    .line 130
    rsub-int/lit8 v5, v1, 0x2

    shl-int/lit8 v4, v5, 0x3

    .line 131
    .local v4, rightShift:I
    shr-int v5, p1, v4

    and-int/lit16 v3, v5, 0xff

    .line 132
    .local v3, fore:I
    shr-int v5, p2, v4

    and-int/lit16 v0, v5, 0xff

    .line 133
    .local v0, back:I
    sub-int v2, v0, v3

    .line 134
    .local v2, delta:I
    iget-object v5, p0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mColorMatrix:[F

    mul-int/lit8 v6, v1, 0x6

    int-to-float v7, v2

    const v8, 0x3b808081

    mul-float/2addr v7, v8

    aput v7, v5, v6

    .line 135
    iget-object v5, p0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mColorMatrix:[F

    mul-int/lit8 v6, v1, 0x5

    add-int/lit8 v6, v6, 0x4

    int-to-float v7, v3

    aput v7, v5, v6

    .line 129
    add-int/lit8 v1, v1, 0x1

    goto :goto_23

    .line 137
    .end local v0           #back:I
    .end local v2           #delta:I
    .end local v3           #fore:I
    .end local v4           #rightShift:I
    :cond_4b
    iget-object v5, p0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mPaint:Landroid/graphics/Paint;

    new-instance v6, Landroid/graphics/ColorMatrixColorFilter;

    iget-object v7, p0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mColorMatrix:[F

    invoke-direct {v6, v7}, Landroid/graphics/ColorMatrixColorFilter;-><init>([F)V

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 139
    .end local v1           #component:I
    :cond_57
    return-void
.end method


# virtual methods
.method public drawTextRun(Landroid/graphics/Canvas;FFII[CIIZI)V
    .registers 37
    .parameter "canvas"
    .parameter "x"
    .parameter "y"
    .parameter "lineOffset"
    .parameter "runWidth"
    .parameter "text"
    .parameter "index"
    .parameter "count"
    .parameter "cursor"
    .parameter "textStyle"

    .prologue
    .line 65
    invoke-static/range {p10 .. p10}, Ljackpal/androidterm/emulatorview/TextStyle;->decodeForeColor(I)I

    move-result v15

    .line 66
    .local v15, foreColor:I
    invoke-static/range {p10 .. p10}, Ljackpal/androidterm/emulatorview/TextStyle;->decodeBackColor(I)I

    move-result v5

    .line 67
    .local v5, backColor:I
    invoke-static/range {p10 .. p10}, Ljackpal/androidterm/emulatorview/TextStyle;->decodeEffect(I)I

    move-result v14

    .line 69
    .local v14, effect:I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mReverseVideo:Z

    move/from16 v24, v0

    and-int/lit8 v23, v14, 0x12

    if-eqz v23, :cond_e6

    const/16 v23, 0x1

    :goto_18
    xor-int v17, v24, v23

    .line 71
    .local v17, inverse:Z
    if-eqz v17, :cond_21

    .line 72
    move/from16 v22, v15

    .line 73
    .local v22, temp:I
    move v15, v5

    .line 74
    move/from16 v5, v22

    .line 77
    .end local v22           #temp:I
    :cond_21
    and-int/lit8 v23, v14, 0x9

    if-eqz v23, :cond_ea

    const/4 v6, 0x1

    .line 78
    .local v6, bold:Z
    :goto_26
    if-eqz v6, :cond_30

    const/16 v23, 0x8

    move/from16 v0, v23

    if-ge v15, v0, :cond_30

    .line 80
    add-int/lit8 v15, v15, 0x8

    .line 83
    :cond_30
    if-eqz p9, :cond_34

    .line 84
    const/16 v5, 0x102

    .line 87
    :cond_34
    and-int/lit8 v23, v14, 0x20

    if-eqz v23, :cond_ed

    const/16 v18, 0x1

    .line 89
    .local v18, invisible:Z
    :goto_3a
    if-eqz v18, :cond_3d

    .line 90
    move v15, v5

    .line 93
    :cond_3d
    move-object/from16 v0, p0

    iget-object v0, v0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mPalette:[I

    move-object/from16 v23, v0

    aget v23, v23, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mPalette:[I

    move-object/from16 v24, v0

    aget v24, v24, v5

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->setColorMatrix(II)V

    .line 94
    move/from16 v0, p2

    float-to-int v0, v0

    move/from16 v23, v0

    mul-int/lit8 v24, p4, 0x4

    add-int v11, v23, v24

    .line 95
    .local v11, destX:I
    move/from16 v0, p3

    float-to-int v12, v0

    .line 96
    .local v12, destY:I
    new-instance v19, Landroid/graphics/Rect;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Rect;-><init>()V

    .line 97
    .local v19, srcRect:Landroid/graphics/Rect;
    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    .line 98
    .local v10, destRect:Landroid/graphics/Rect;
    add-int/lit8 v23, v12, -0x8

    move/from16 v0, v23

    iput v0, v10, Landroid/graphics/Rect;->top:I

    .line 99
    iput v12, v10, Landroid/graphics/Rect;->bottom:I

    .line 100
    move-object/from16 v0, p0

    iget-object v0, v0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mPalette:[I

    move-object/from16 v23, v0

    aget v23, v23, v5

    move-object/from16 v0, p0

    iget-object v0, v0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mPalette:[I

    move-object/from16 v24, v0

    const/16 v25, 0x101

    aget v24, v24, v25

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_f1

    const/4 v13, 0x1

    .line 101
    .local v13, drawSpaces:Z
    :goto_8d
    const/16 v16, 0x0

    .local v16, i:I
    :goto_8f
    move/from16 v0, v16

    move/from16 v1, p8

    if-ge v0, v1, :cond_f3

    .line 103
    add-int v23, v16, p7

    aget-char v7, p6, v23

    .line 104
    .local v7, c:C
    const/16 v23, 0x80

    move/from16 v0, v23

    if-ge v7, v0, :cond_e1

    const/16 v23, 0x20

    move/from16 v0, v23

    if-ne v7, v0, :cond_a7

    if-eqz v13, :cond_e1

    .line 105
    :cond_a7
    and-int/lit8 v8, v7, 0x1f

    .line 106
    .local v8, cellX:I
    shr-int/lit8 v23, v7, 0x5

    and-int/lit8 v9, v23, 0x3

    .line 107
    .local v9, cellY:I
    mul-int/lit8 v20, v8, 0x4

    .line 108
    .local v20, srcX:I
    mul-int/lit8 v21, v9, 0x8

    .line 109
    .local v21, srcY:I
    add-int/lit8 v23, v20, 0x4

    add-int/lit8 v24, v21, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 111
    iput v11, v10, Landroid/graphics/Rect;->left:I

    .line 112
    add-int/lit8 v23, v11, 0x4

    move/from16 v0, v23

    iput v0, v10, Landroid/graphics/Rect;->right:I

    .line 113
    move-object/from16 v0, p0

    iget-object v0, v0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mFont:Landroid/graphics/Bitmap;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ljackpal/androidterm/emulatorview/Bitmap4x8FontRenderer;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v19

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v10, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 115
    .end local v8           #cellX:I
    .end local v9           #cellY:I
    .end local v20           #srcX:I
    .end local v21           #srcY:I
    :cond_e1
    add-int/lit8 v11, v11, 0x4

    .line 101
    add-int/lit8 v16, v16, 0x1

    goto :goto_8f

    .line 69
    .end local v6           #bold:Z
    .end local v7           #c:C
    .end local v10           #destRect:Landroid/graphics/Rect;
    .end local v11           #destX:I
    .end local v12           #destY:I
    .end local v13           #drawSpaces:Z
    .end local v16           #i:I
    .end local v17           #inverse:Z
    .end local v18           #invisible:Z
    .end local v19           #srcRect:Landroid/graphics/Rect;
    :cond_e6
    const/16 v23, 0x0

    goto/16 :goto_18

    .line 77
    .restart local v17       #inverse:Z
    :cond_ea
    const/4 v6, 0x0

    goto/16 :goto_26

    .line 87
    .restart local v6       #bold:Z
    :cond_ed
    const/16 v18, 0x0

    goto/16 :goto_3a

    .line 100
    .restart local v10       #destRect:Landroid/graphics/Rect;
    .restart local v11       #destX:I
    .restart local v12       #destY:I
    .restart local v18       #invisible:Z
    .restart local v19       #srcRect:Landroid/graphics/Rect;
    :cond_f1
    const/4 v13, 0x0

    goto :goto_8d

    .line 117
    .restart local v13       #drawSpaces:Z
    .restart local v16       #i:I
    :cond_f3
    return-void
.end method

.method public getCharacterHeight()I
    .registers 2

    .prologue
    .line 55
    const/16 v0, 0x8

    return v0
.end method

.method public getCharacterWidth()F
    .registers 2

    .prologue
    .line 51
    const/high16 v0, 0x4080

    return v0
.end method

.method public getTopMargin()I
    .registers 2

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method
