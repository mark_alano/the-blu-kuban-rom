.class public Ljackpal/androidterm/emulatorview/ColorScheme;
.super Ljava/lang/Object;
.source "ColorScheme.java"


# instance fields
.field private backColor:I

.field private foreColor:I


# direct methods
.method public constructor <init>(II)V
    .registers 3
    .parameter "foreColor"
    .parameter "backColor"

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput p1, p0, Ljackpal/androidterm/emulatorview/ColorScheme;->foreColor:I

    .line 56
    iput p2, p0, Ljackpal/androidterm/emulatorview/ColorScheme;->backColor:I

    .line 57
    return-void
.end method

.method public constructor <init>([I)V
    .registers 4
    .parameter "scheme"

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    array-length v0, p1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_d

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 70
    :cond_d
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/ColorScheme;->foreColor:I

    .line 71
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/ColorScheme;->backColor:I

    .line 72
    return-void
.end method


# virtual methods
.method public getBackColor()I
    .registers 2

    .prologue
    .line 87
    iget v0, p0, Ljackpal/androidterm/emulatorview/ColorScheme;->backColor:I

    return v0
.end method

.method public getForeColor()I
    .registers 2

    .prologue
    .line 79
    iget v0, p0, Ljackpal/androidterm/emulatorview/ColorScheme;->foreColor:I

    return v0
.end method
