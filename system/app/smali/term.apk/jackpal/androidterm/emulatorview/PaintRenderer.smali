.class Ljackpal/androidterm/emulatorview/PaintRenderer;
.super Ljackpal/androidterm/emulatorview/BaseTextRenderer;
.source "PaintRenderer.java"


# static fields
.field private static final EXAMPLE_CHAR:[C


# instance fields
.field private mCharAscent:I

.field private mCharDescent:I

.field private mCharHeight:I

.field private mCharWidth:F

.field private mTextPaint:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 107
    const/4 v0, 0x1

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x58

    aput-char v2, v0, v1

    sput-object v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->EXAMPLE_CHAR:[C

    return-void
.end method

.method public constructor <init>(ILjackpal/androidterm/emulatorview/ColorScheme;)V
    .registers 7
    .parameter "fontSize"
    .parameter "scheme"

    .prologue
    const/4 v3, 0x1

    .line 27
    invoke-direct {p0, p2}, Ljackpal/androidterm/emulatorview/BaseTextRenderer;-><init>(Ljackpal/androidterm/emulatorview/ColorScheme;)V

    .line 28
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    .line 29
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 30
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 31
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 33
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontSpacing()F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mCharHeight:I

    .line 34
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mCharAscent:I

    .line 35
    iget v0, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mCharHeight:I

    iget v1, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mCharAscent:I

    add-int/2addr v0, v1

    iput v0, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mCharDescent:I

    .line 36
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    sget-object v1, Ljackpal/androidterm/emulatorview/PaintRenderer;->EXAMPLE_CHAR:[C

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Paint;->measureText([CII)F

    move-result v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mCharWidth:F

    .line 37
    return-void
.end method


# virtual methods
.method public drawTextRun(Landroid/graphics/Canvas;FFII[CIIZI)V
    .registers 29
    .parameter "canvas"
    .parameter "x"
    .parameter "y"
    .parameter "lineOffset"
    .parameter "runWidth"
    .parameter "text"
    .parameter "index"
    .parameter "count"
    .parameter "cursor"
    .parameter "textStyle"

    .prologue
    .line 42
    invoke-static/range {p10 .. p10}, Ljackpal/androidterm/emulatorview/TextStyle;->decodeForeColor(I)I

    move-result v13

    .line 43
    .local v13, foreColor:I
    invoke-static/range {p10 .. p10}, Ljackpal/androidterm/emulatorview/TextStyle;->decodeBackColor(I)I

    move-result v10

    .line 44
    .local v10, backColor:I
    invoke-static/range {p10 .. p10}, Ljackpal/androidterm/emulatorview/TextStyle;->decodeEffect(I)I

    move-result v12

    .line 46
    .local v12, effect:I
    move-object/from16 v0, p0

    iget-boolean v3, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mReverseVideo:Z

    and-int/lit8 v1, v12, 0x12

    if-eqz v1, :cond_c3

    const/4 v1, 0x1

    :goto_15
    xor-int v14, v3, v1

    .line 48
    .local v14, inverse:Z
    if-eqz v14, :cond_1e

    .line 49
    move/from16 v16, v13

    .line 50
    .local v16, temp:I
    move v13, v10

    .line 51
    move/from16 v10, v16

    .line 54
    .end local v16           #temp:I
    :cond_1e
    if-eqz p9, :cond_22

    .line 55
    const/16 v10, 0x102

    .line 58
    :cond_22
    move-object/from16 v0, p0

    iget-object v1, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v3, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mPalette:[I

    aget v3, v3, v10

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 60
    move/from16 v0, p4

    int-to-float v1, v0

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mCharWidth:F

    mul-float/2addr v1, v3

    add-float v2, p2, v1

    .line 61
    .local v2, left:F
    move-object/from16 v0, p0

    iget v1, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mCharAscent:I

    int-to-float v1, v1

    add-float v1, v1, p3

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mCharDescent:I

    int-to-float v3, v3

    sub-float v3, v1, v3

    move/from16 v0, p5

    int-to-float v1, v0

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mCharWidth:F

    mul-float/2addr v1, v4

    add-float v4, v2, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move/from16 v5, p3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 64
    and-int/lit8 v1, v12, 0x20

    if-eqz v1, :cond_c6

    const/4 v15, 0x1

    .line 65
    .local v15, invisible:Z
    :goto_61
    if-nez v15, :cond_c2

    .line 66
    and-int/lit8 v1, v12, 0x9

    if-eqz v1, :cond_c8

    const/4 v11, 0x1

    .line 67
    .local v11, bold:Z
    :goto_68
    and-int/lit8 v1, v12, 0x4

    if-eqz v1, :cond_ca

    const/16 v17, 0x1

    .line 68
    .local v17, underline:Z
    :goto_6e
    if-eqz v11, :cond_78

    .line 69
    move-object/from16 v0, p0

    iget-object v1, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 71
    :cond_78
    if-eqz v17, :cond_82

    .line 72
    move-object/from16 v0, p0

    iget-object v1, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setUnderlineText(Z)V

    .line 74
    :cond_82
    const/16 v1, 0x8

    if-ge v13, v1, :cond_cd

    if-eqz v11, :cond_cd

    .line 76
    move-object/from16 v0, p0

    iget-object v1, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v3, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mPalette:[I

    add-int/lit8 v4, v13, 0x8

    aget v3, v3, v4

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 80
    :goto_97
    move-object/from16 v0, p0

    iget v1, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mCharDescent:I

    int-to-float v1, v1

    sub-float v8, p3, v1

    move-object/from16 v0, p0

    iget-object v9, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    move-object/from16 v4, p6

    move/from16 v5, p7

    move/from16 v6, p8

    move v7, v2

    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Canvas;->drawText([CIIFFLandroid/graphics/Paint;)V

    .line 81
    if-eqz v11, :cond_b8

    .line 82
    move-object/from16 v0, p0

    iget-object v1, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 84
    :cond_b8
    if-eqz v17, :cond_c2

    .line 85
    move-object/from16 v0, p0

    iget-object v1, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setUnderlineText(Z)V

    .line 88
    .end local v11           #bold:Z
    .end local v17           #underline:Z
    :cond_c2
    return-void

    .line 46
    .end local v2           #left:F
    .end local v14           #inverse:Z
    .end local v15           #invisible:Z
    :cond_c3
    const/4 v1, 0x0

    goto/16 :goto_15

    .line 64
    .restart local v2       #left:F
    .restart local v14       #inverse:Z
    :cond_c6
    const/4 v15, 0x0

    goto :goto_61

    .line 66
    .restart local v15       #invisible:Z
    :cond_c8
    const/4 v11, 0x0

    goto :goto_68

    .line 67
    .restart local v11       #bold:Z
    :cond_ca
    const/16 v17, 0x0

    goto :goto_6e

    .line 78
    .restart local v17       #underline:Z
    :cond_cd
    move-object/from16 v0, p0

    iget-object v1, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v3, v0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mPalette:[I

    aget v3, v3, v13

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_97
.end method

.method public getCharacterHeight()I
    .registers 2

    .prologue
    .line 91
    iget v0, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mCharHeight:I

    return v0
.end method

.method public getCharacterWidth()F
    .registers 2

    .prologue
    .line 95
    iget v0, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mCharWidth:F

    return v0
.end method

.method public getTopMargin()I
    .registers 2

    .prologue
    .line 99
    iget v0, p0, Ljackpal/androidterm/emulatorview/PaintRenderer;->mCharDescent:I

    return v0
.end method
