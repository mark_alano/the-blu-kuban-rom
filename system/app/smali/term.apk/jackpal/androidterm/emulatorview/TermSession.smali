.class public Ljackpal/androidterm/emulatorview/TermSession;
.super Ljava/lang/Object;
.source "TermSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljackpal/androidterm/emulatorview/TermSession$FinishCallback;
    }
.end annotation


# static fields
.field private static final FINISH:I = 0x3

.field private static final NEW_INPUT:I = 0x1

.field private static final NEW_OUTPUT:I = 0x2

.field private static final TRANSCRIPT_ROWS:I = 0x2710


# instance fields
.field private mByteQueue:Ljackpal/androidterm/emulatorview/ByteQueue;

.field private mColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

.field private mDefaultUTF8Mode:Z

.field private mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

.field private mFinishCallback:Ljackpal/androidterm/emulatorview/TermSession$FinishCallback;

.field private mIsRunning:Z

.field private mMsgHandler:Landroid/os/Handler;

.field private mNotify:Ljackpal/androidterm/emulatorview/UpdateCallback;

.field private mReaderThread:Ljava/lang/Thread;

.field private mReceiveBuffer:[B

.field private mTermIn:Ljava/io/InputStream;

.field private mTermOut:Ljava/io/OutputStream;

.field private mTitle:Ljava/lang/String;

.field private mTitleChangedListener:Ljackpal/androidterm/emulatorview/UpdateCallback;

.field private mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

.field private mUTF8Encoder:Ljava/nio/charset/CharsetEncoder;

.field private mWriteByteBuffer:Ljava/nio/ByteBuffer;

.field private mWriteCharBuffer:Ljava/nio/CharBuffer;

.field private mWriteQueue:Ljackpal/androidterm/emulatorview/ByteQueue;

.field private mWriterHandler:Landroid/os/Handler;

.field private mWriterThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/16 v2, 0x1000

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    sget-object v0, Ljackpal/androidterm/emulatorview/BaseTextRenderer;->defaultColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mIsRunning:Z

    .line 107
    new-instance v0, Ljackpal/androidterm/emulatorview/TermSession$1;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/TermSession$1;-><init>(Ljackpal/androidterm/emulatorview/TermSession;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mMsgHandler:Landroid/os/Handler;

    .line 122
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/nio/CharBuffer;->allocate(I)Ljava/nio/CharBuffer;

    move-result-object v0

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mWriteCharBuffer:Ljava/nio/CharBuffer;

    .line 123
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mWriteByteBuffer:Ljava/nio/ByteBuffer;

    .line 124
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->newEncoder()Ljava/nio/charset/CharsetEncoder;

    move-result-object v0

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mUTF8Encoder:Ljava/nio/charset/CharsetEncoder;

    .line 125
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mUTF8Encoder:Ljava/nio/charset/CharsetEncoder;

    sget-object v1, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v0, v1}, Ljava/nio/charset/CharsetEncoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetEncoder;

    .line 126
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mUTF8Encoder:Ljava/nio/charset/CharsetEncoder;

    sget-object v1, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v0, v1}, Ljava/nio/charset/CharsetEncoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetEncoder;

    .line 128
    new-array v0, v2, [B

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mReceiveBuffer:[B

    .line 129
    new-instance v0, Ljackpal/androidterm/emulatorview/ByteQueue;

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/ByteQueue;-><init>(I)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mByteQueue:Ljackpal/androidterm/emulatorview/ByteQueue;

    .line 130
    new-instance v0, Ljackpal/androidterm/emulatorview/TermSession$2;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/TermSession$2;-><init>(Ljackpal/androidterm/emulatorview/TermSession;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mReaderThread:Ljava/lang/Thread;

    .line 151
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mReaderThread:Ljava/lang/Thread;

    const-string v1, "TermSession input reader"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 153
    new-instance v0, Ljackpal/androidterm/emulatorview/ByteQueue;

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/ByteQueue;-><init>(I)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mWriteQueue:Ljackpal/androidterm/emulatorview/ByteQueue;

    .line 154
    new-instance v0, Ljackpal/androidterm/emulatorview/TermSession$3;

    invoke-direct {v0, p0}, Ljackpal/androidterm/emulatorview/TermSession$3;-><init>(Ljackpal/androidterm/emulatorview/TermSession;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mWriterThread:Ljava/lang/Thread;

    .line 202
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mWriterThread:Ljava/lang/Thread;

    const-string v1, "TermSession output writer"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 203
    return-void
.end method

.method static synthetic access$000(Ljackpal/androidterm/emulatorview/TermSession;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 58
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mIsRunning:Z

    return v0
.end method

.method static synthetic access$100(Ljackpal/androidterm/emulatorview/TermSession;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 58
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TermSession;->readFromProcess()V

    return-void
.end method

.method static synthetic access$200(Ljackpal/androidterm/emulatorview/TermSession;)Ljava/io/InputStream;
    .registers 2
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTermIn:Ljava/io/InputStream;

    return-object v0
.end method

.method static synthetic access$300(Ljackpal/androidterm/emulatorview/TermSession;)Ljackpal/androidterm/emulatorview/ByteQueue;
    .registers 2
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mByteQueue:Ljackpal/androidterm/emulatorview/ByteQueue;

    return-object v0
.end method

.method static synthetic access$400(Ljackpal/androidterm/emulatorview/TermSession;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mMsgHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$502(Ljackpal/androidterm/emulatorview/TermSession;Landroid/os/Handler;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58
    iput-object p1, p0, Ljackpal/androidterm/emulatorview/TermSession;->mWriterHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$700(Ljackpal/androidterm/emulatorview/TermSession;)Ljackpal/androidterm/emulatorview/ByteQueue;
    .registers 2
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mWriteQueue:Ljackpal/androidterm/emulatorview/ByteQueue;

    return-object v0
.end method

.method static synthetic access$800(Ljackpal/androidterm/emulatorview/TermSession;)Ljava/io/OutputStream;
    .registers 2
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTermOut:Ljava/io/OutputStream;

    return-object v0
.end method

.method private notifyNewOutput()V
    .registers 3

    .prologue
    .line 292
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mWriterHandler:Landroid/os/Handler;

    .line 293
    .local v0, writerHandler:Landroid/os/Handler;
    if-nez v0, :cond_5

    .line 298
    :goto_4
    return-void

    .line 297
    :cond_5
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_4
.end method

.method private readFromProcess()V
    .registers 9

    .prologue
    const/4 v7, 0x0

    .line 444
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/TermSession;->mByteQueue:Ljackpal/androidterm/emulatorview/ByteQueue;

    invoke-virtual {v4}, Ljackpal/androidterm/emulatorview/ByteQueue;->getBytesAvailable()I

    move-result v0

    .line 445
    .local v0, bytesAvailable:I
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/TermSession;->mReceiveBuffer:[B

    array-length v4, v4

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 446
    .local v2, bytesToRead:I
    const/4 v1, 0x0

    .line 448
    .local v1, bytesRead:I
    :try_start_f
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/TermSession;->mByteQueue:Ljackpal/androidterm/emulatorview/ByteQueue;

    iget-object v5, p0, Ljackpal/androidterm/emulatorview/TermSession;->mReceiveBuffer:[B

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6, v2}, Ljackpal/androidterm/emulatorview/ByteQueue;->read([BII)I
    :try_end_17
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_17} :catch_21

    move-result v1

    .line 454
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/TermSession;->mReceiveBuffer:[B

    invoke-virtual {p0, v4, v7, v1}, Ljackpal/androidterm/emulatorview/TermSession;->processInput([BII)V

    .line 455
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/TermSession;->notifyUpdate()V

    .line 456
    :goto_20
    return-void

    .line 449
    :catch_21
    move-exception v3

    .line 450
    .local v3, e:Ljava/lang/InterruptedException;
    goto :goto_20
.end method


# virtual methods
.method protected final appendToEmulator([BII)V
    .registers 5
    .parameter "data"
    .parameter "offset"
    .parameter "count"

    .prologue
    .line 485
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    invoke-virtual {v0, p1, p2, p3}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->append([BII)V

    .line 486
    return-void
.end method

.method public finish()V
    .registers 3

    .prologue
    .line 574
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mIsRunning:Z

    .line 575
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

    if-eqz v0, :cond_c

    .line 576
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->finish()V

    .line 580
    :cond_c
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mWriterHandler:Landroid/os/Handler;

    if-eqz v0, :cond_16

    .line 581
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mWriterHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 584
    :cond_16
    :try_start_16
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTermIn:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 585
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTermOut:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_20} :catch_2c
    .catch Ljava/lang/NullPointerException; {:try_start_16 .. :try_end_20} :catch_2a

    .line 591
    :goto_20
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mFinishCallback:Ljackpal/androidterm/emulatorview/TermSession$FinishCallback;

    if-eqz v0, :cond_29

    .line 592
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mFinishCallback:Ljackpal/androidterm/emulatorview/TermSession$FinishCallback;

    invoke-interface {v0, p0}, Ljackpal/androidterm/emulatorview/TermSession$FinishCallback;->onSessionFinish(Ljackpal/androidterm/emulatorview/TermSession;)V

    .line 594
    :cond_29
    return-void

    .line 588
    :catch_2a
    move-exception v0

    goto :goto_20

    .line 586
    :catch_2c
    move-exception v0

    goto :goto_20
.end method

.method getEmulator()Ljackpal/androidterm/emulatorview/TerminalEmulator;
    .registers 2

    .prologue
    .line 348
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    return-object v0
.end method

.method public getTermIn()Ljava/io/InputStream;
    .registers 2

    .prologue
    .line 324
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTermIn:Ljava/io/InputStream;

    return-object v0
.end method

.method public getTermOut()Ljava/io/OutputStream;
    .registers 2

    .prologue
    .line 306
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTermOut:Ljava/io/OutputStream;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .registers 2

    .prologue
    .line 375
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method getTranscriptScreen()Ljackpal/androidterm/emulatorview/TranscriptScreen;
    .registers 2

    .prologue
    .line 344
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

    return-object v0
.end method

.method public getTranscriptText()Ljava/lang/String;
    .registers 2

    .prologue
    .line 437
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->getTranscriptText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUTF8Mode()Z
    .registers 2

    .prologue
    .line 531
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    if-nez v0, :cond_7

    .line 532
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mDefaultUTF8Mode:Z

    .line 534
    :goto_6
    return v0

    :cond_7
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getUTF8Mode()Z

    move-result v0

    goto :goto_6
.end method

.method public initializeEmulator(II)V
    .registers 9
    .parameter "columns"
    .parameter "rows"

    .prologue
    .line 212
    new-instance v0, Ljackpal/androidterm/emulatorview/TranscriptScreen;

    const/16 v1, 0x2710

    iget-object v2, p0, Ljackpal/androidterm/emulatorview/TermSession;->mColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    invoke-direct {v0, p1, v1, p2, v2}, Ljackpal/androidterm/emulatorview/TranscriptScreen;-><init>(IIILjackpal/androidterm/emulatorview/ColorScheme;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

    .line 213
    new-instance v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;

    iget-object v2, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

    iget-object v5, p0, Ljackpal/androidterm/emulatorview/TermSession;->mColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    move-object v1, p0

    move v3, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Ljackpal/androidterm/emulatorview/TerminalEmulator;-><init>(Ljackpal/androidterm/emulatorview/TermSession;Ljackpal/androidterm/emulatorview/Screen;IILjackpal/androidterm/emulatorview/ColorScheme;)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    .line 214
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    iget-boolean v1, p0, Ljackpal/androidterm/emulatorview/TermSession;->mDefaultUTF8Mode:Z

    invoke-virtual {v0, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setDefaultUTF8Mode(Z)V

    .line 216
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mIsRunning:Z

    .line 217
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mReaderThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 218
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mWriterThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 219
    return-void
.end method

.method public isRunning()Z
    .registers 2

    .prologue
    .line 340
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mIsRunning:Z

    return v0
.end method

.method protected notifyTitleChanged()V
    .registers 2

    .prologue
    .line 403
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTitleChangedListener:Ljackpal/androidterm/emulatorview/UpdateCallback;

    .line 404
    .local v0, listener:Ljackpal/androidterm/emulatorview/UpdateCallback;
    if-eqz v0, :cond_7

    .line 405
    invoke-interface {v0}, Ljackpal/androidterm/emulatorview/UpdateCallback;->onUpdate()V

    .line 407
    :cond_7
    return-void
.end method

.method protected notifyUpdate()V
    .registers 2

    .prologue
    .line 366
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mNotify:Ljackpal/androidterm/emulatorview/UpdateCallback;

    if-eqz v0, :cond_9

    .line 367
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mNotify:Ljackpal/androidterm/emulatorview/UpdateCallback;

    invoke-interface {v0}, Ljackpal/androidterm/emulatorview/UpdateCallback;->onUpdate()V

    .line 369
    :cond_9
    return-void
.end method

.method protected processInput([BII)V
    .registers 5
    .parameter "data"
    .parameter "offset"
    .parameter "count"

    .prologue
    .line 472
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    invoke-virtual {v0, p1, p2, p3}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->append([BII)V

    .line 473
    return-void
.end method

.method public reset()V
    .registers 2

    .prologue
    .line 554
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->reset()V

    .line 555
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/TermSession;->notifyUpdate()V

    .line 556
    return-void
.end method

.method public setColorScheme(Ljackpal/androidterm/emulatorview/ColorScheme;)V
    .registers 3
    .parameter "scheme"

    .prologue
    .line 495
    if-nez p1, :cond_4

    .line 496
    sget-object p1, Ljackpal/androidterm/emulatorview/BaseTextRenderer;->defaultColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    .line 498
    :cond_4
    iput-object p1, p0, Ljackpal/androidterm/emulatorview/TermSession;->mColorScheme:Ljackpal/androidterm/emulatorview/ColorScheme;

    .line 499
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    if-nez v0, :cond_b

    .line 504
    :goto_a
    return-void

    .line 502
    :cond_b
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    invoke-virtual {v0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setColorScheme(Ljackpal/androidterm/emulatorview/ColorScheme;)V

    .line 503
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTranscriptScreen:Ljackpal/androidterm/emulatorview/TranscriptScreen;

    invoke-virtual {v0, p1}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->setColorScheme(Ljackpal/androidterm/emulatorview/ColorScheme;)V

    goto :goto_a
.end method

.method public setDefaultUTF8Mode(Z)V
    .registers 3
    .parameter "utf8ByDefault"

    .prologue
    .line 518
    iput-boolean p1, p0, Ljackpal/androidterm/emulatorview/TermSession;->mDefaultUTF8Mode:Z

    .line 519
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    if-nez v0, :cond_7

    .line 523
    :goto_6
    return-void

    .line 522
    :cond_7
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    invoke-virtual {v0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setDefaultUTF8Mode(Z)V

    goto :goto_6
.end method

.method public setFinishCallback(Ljackpal/androidterm/emulatorview/TermSession$FinishCallback;)V
    .registers 2
    .parameter "callback"

    .prologue
    .line 565
    iput-object p1, p0, Ljackpal/androidterm/emulatorview/TermSession;->mFinishCallback:Ljackpal/androidterm/emulatorview/TermSession$FinishCallback;

    .line 566
    return-void
.end method

.method public setTermIn(Ljava/io/InputStream;)V
    .registers 2
    .parameter "termIn"

    .prologue
    .line 333
    iput-object p1, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTermIn:Ljava/io/InputStream;

    .line 334
    return-void
.end method

.method public setTermOut(Ljava/io/OutputStream;)V
    .registers 2
    .parameter "termOut"

    .prologue
    .line 315
    iput-object p1, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTermOut:Ljava/io/OutputStream;

    .line 316
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .registers 2
    .parameter "title"

    .prologue
    .line 382
    iput-object p1, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTitle:Ljava/lang/String;

    .line 383
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/TermSession;->notifyTitleChanged()V

    .line 384
    return-void
.end method

.method public setTitleChangedListener(Ljackpal/androidterm/emulatorview/UpdateCallback;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 393
    iput-object p1, p0, Ljackpal/androidterm/emulatorview/TermSession;->mTitleChangedListener:Ljackpal/androidterm/emulatorview/UpdateCallback;

    .line 394
    return-void
.end method

.method public setUTF8ModeUpdateCallback(Ljackpal/androidterm/emulatorview/UpdateCallback;)V
    .registers 3
    .parameter "utf8ModeNotify"

    .prologue
    .line 545
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    if-eqz v0, :cond_9

    .line 546
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    invoke-virtual {v0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setUTF8ModeUpdateCallback(Ljackpal/androidterm/emulatorview/UpdateCallback;)V

    .line 548
    :cond_9
    return-void
.end method

.method public setUpdateCallback(Ljackpal/androidterm/emulatorview/UpdateCallback;)V
    .registers 2
    .parameter "notify"

    .prologue
    .line 358
    iput-object p1, p0, Ljackpal/androidterm/emulatorview/TermSession;->mNotify:Ljackpal/androidterm/emulatorview/UpdateCallback;

    .line 359
    return-void
.end method

.method public updateSize(II)V
    .registers 4
    .parameter "columns"
    .parameter "rows"

    .prologue
    .line 423
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    if-nez v0, :cond_8

    .line 424
    invoke-virtual {p0, p1, p2}, Ljackpal/androidterm/emulatorview/TermSession;->initializeEmulator(II)V

    .line 428
    :goto_7
    return-void

    .line 426
    :cond_8
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mEmulator:Ljackpal/androidterm/emulatorview/TerminalEmulator;

    invoke-virtual {v0, p1, p2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->updateSize(II)V

    goto :goto_7
.end method

.method public write(I)V
    .registers 8
    .parameter "codePoint"

    .prologue
    const/4 v5, 0x0

    .line 277
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TermSession;->mWriteCharBuffer:Ljava/nio/CharBuffer;

    .line 278
    .local v1, charBuf:Ljava/nio/CharBuffer;
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mWriteByteBuffer:Ljava/nio/ByteBuffer;

    .line 279
    .local v0, byteBuf:Ljava/nio/ByteBuffer;
    iget-object v2, p0, Ljackpal/androidterm/emulatorview/TermSession;->mUTF8Encoder:Ljava/nio/charset/CharsetEncoder;

    .line 281
    .local v2, encoder:Ljava/nio/charset/CharsetEncoder;
    invoke-virtual {v1}, Ljava/nio/CharBuffer;->clear()Ljava/nio/Buffer;

    .line 282
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 283
    invoke-virtual {v1}, Ljava/nio/CharBuffer;->array()[C

    move-result-object v3

    invoke-static {p1, v3, v5}, Ljava/lang/Character;->toChars(I[CI)I

    .line 284
    invoke-virtual {v2}, Ljava/nio/charset/CharsetEncoder;->reset()Ljava/nio/charset/CharsetEncoder;

    .line 285
    const/4 v3, 0x1

    invoke-virtual {v2, v1, v0, v3}, Ljava/nio/charset/CharsetEncoder;->encode(Ljava/nio/CharBuffer;Ljava/nio/ByteBuffer;Z)Ljava/nio/charset/CoderResult;

    .line 286
    invoke-virtual {v2, v0}, Ljava/nio/charset/CharsetEncoder;->flush(Ljava/nio/ByteBuffer;)Ljava/nio/charset/CoderResult;

    .line 287
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p0, v3, v5, v4}, Ljackpal/androidterm/emulatorview/TermSession;->write([BII)V

    .line 288
    return-void
.end method

.method public write(Ljava/lang/String;)V
    .registers 5
    .parameter "data"

    .prologue
    .line 258
    :try_start_0
    const-string v1, "UTF-8"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 259
    .local v0, bytes:[B
    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p0, v0, v1, v2}, Ljackpal/androidterm/emulatorview/TermSession;->write([BII)V
    :try_end_b
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_b} :catch_c

    .line 262
    .end local v0           #bytes:[B
    :goto_b
    return-void

    .line 260
    :catch_c
    move-exception v1

    goto :goto_b
.end method

.method public write([BII)V
    .registers 5
    .parameter "data"
    .parameter "offset"
    .parameter "count"

    .prologue
    .line 239
    :try_start_0
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TermSession;->mWriteQueue:Ljackpal/androidterm/emulatorview/ByteQueue;

    invoke-virtual {v0, p1, p2, p3}, Ljackpal/androidterm/emulatorview/ByteQueue;->write([BII)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_5} :catch_9

    .line 242
    :goto_5
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TermSession;->notifyNewOutput()V

    .line 243
    return-void

    .line 240
    :catch_9
    move-exception v0

    goto :goto_5
.end method
