.class Ljackpal/androidterm/emulatorview/TerminalEmulator;
.super Ljava/lang/Object;
.source "TerminalEmulator.java"


# static fields
.field private static final CHAR_SET_ALT_SPECIAL_GRAPICS:I = 0x4

.field private static final CHAR_SET_ALT_STANDARD:I = 0x3

.field private static final CHAR_SET_ASCII:I = 0x1

.field private static final CHAR_SET_SPECIAL_GRAPHICS:I = 0x2

.field private static final CHAR_SET_UK:I = 0x0

.field private static final DEFAULT_TO_AUTOWRAP_ENABLED:Z = true

.field private static final ESC:I = 0x1

.field private static final ESC_LEFT_SQUARE_BRACKET:I = 0x5

.field private static final ESC_LEFT_SQUARE_BRACKET_QUESTION_MARK:I = 0x6

.field private static final ESC_NONE:I = 0x0

.field private static final ESC_PERCENT:I = 0x7

.field private static final ESC_POUND:I = 0x2

.field private static final ESC_RIGHT_SQUARE_BRACKET:I = 0x8

.field private static final ESC_RIGHT_SQUARE_BRACKET_ESC:I = 0x9

.field private static final ESC_SELECT_LEFT_PAREN:I = 0x3

.field private static final ESC_SELECT_RIGHT_PAREN:I = 0x4

.field private static final K_132_COLUMN_MODE_MASK:I = 0x8

.field private static final K_DECSC_DECRC_MASK:I = 0xc0

.field private static final K_ORIGIN_MODE_MASK:I = 0x40

.field private static final K_REVERSE_VIDEO_MASK:I = 0x20

.field private static final K_WRAPAROUND_MODE_MASK:I = 0x80

.field private static final MAX_ESCAPE_PARAMETERS:I = 0x10

.field private static final MAX_OSC_STRING_LENGTH:I = 0x200

.field private static final UNICODE_REPLACEMENT_CHAR:I = 0xfffd

.field private static final mSpecialGraphicsCharMap:[C


# instance fields
.field private mAboutToAutoWrap:Z

.field private mAlternateCharSet:Z

.field private mArgIndex:I

.field private mArgs:[I

.field private mAutomaticNewlineMode:Z

.field private mBackColor:I

.field private mBottomMargin:I

.field private mCharSet:[I

.field private mColumns:I

.field private mContinueSequence:Z

.field private mCursorCol:I

.field private mCursorRow:I

.field private mDecFlags:I

.field private mDefaultBackColor:I

.field private mDefaultForeColor:I

.field private mDefaultUTF8Mode:Z

.field private mEffect:I

.field private mEscapeState:I

.field private mForeColor:I

.field private mInputCharBuffer:Ljava/nio/CharBuffer;

.field private mInsertMode:Z

.field private mJustWrapped:Z

.field private mLastEmittedCharWidth:I

.field private mOSCArg:[B

.field private mOSCArgLength:I

.field private mOSCArgTokenizerIndex:I

.field private mProcessedCharCount:I

.field private mRows:I

.field private mSavedCursorCol:I

.field private mSavedCursorRow:I

.field private mSavedDecFlags:I

.field private mSavedDecFlags_DECSC_DECRC:I

.field private mSavedEffect:I

.field private mScreen:Ljackpal/androidterm/emulatorview/Screen;

.field private mScrollCounter:I

.field private mSession:Ljackpal/androidterm/emulatorview/TermSession;

.field private mTabStop:[Z

.field private mTopMargin:I

.field private mUTF8ByteBuffer:Ljava/nio/ByteBuffer;

.field private mUTF8Decoder:Ljava/nio/charset/CharsetDecoder;

.field private mUTF8EscapeUsed:Z

.field private mUTF8Mode:Z

.field private mUTF8ModeNotify:Ljackpal/androidterm/emulatorview/UpdateCallback;

.field private mUTF8ToFollow:I

.field private mUseAlternateCharSet:Z

.field private mbKeypadApplicationMode:Z


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/16 v2, 0x80

    const/16 v4, 0x68

    .line 324
    new-array v1, v2, [C

    sput-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    .line 326
    const/4 v0, 0x0

    .local v0, i:C
    :goto_9
    if-ge v0, v2, :cond_13

    .line 327
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    aput-char v0, v1, v0

    .line 326
    add-int/lit8 v1, v0, 0x1

    int-to-char v0, v1

    goto :goto_9

    .line 329
    :cond_13
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x5f

    const/16 v3, 0x20

    aput-char v3, v1, v2

    .line 330
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x62

    const/16 v3, 0x2409

    aput-char v3, v1, v2

    .line 331
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x63

    const/16 v3, 0x240c

    aput-char v3, v1, v2

    .line 332
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x64

    const/16 v3, 0x240d

    aput-char v3, v1, v2

    .line 333
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x65

    const/16 v3, 0x240a

    aput-char v3, v1, v2

    .line 334
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x2424

    aput-char v2, v1, v4

    .line 335
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x69

    const/16 v3, 0x240b

    aput-char v3, v1, v2

    .line 336
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x7d

    const/16 v3, 0xa3

    aput-char v3, v1, v2

    .line 337
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x66

    const/16 v3, 0xb0

    aput-char v3, v1, v2

    .line 338
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x60

    const/16 v3, 0x2b25

    aput-char v3, v1, v2

    .line 339
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x7e

    const/16 v3, 0x2022

    aput-char v3, v1, v2

    .line 340
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x79

    const/16 v3, 0x2264

    aput-char v3, v1, v2

    .line 341
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x7c

    const/16 v3, 0x2260

    aput-char v3, v1, v2

    .line 342
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x7a

    const/16 v3, 0x2265

    aput-char v3, v1, v2

    .line 343
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x67

    const/16 v3, 0xb1

    aput-char v3, v1, v2

    .line 344
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x7b

    const/16 v3, 0x3c0

    aput-char v3, v1, v2

    .line 345
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x2e

    const/16 v3, 0x25bc

    aput-char v3, v1, v2

    .line 346
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x2c

    const/16 v3, 0x25c0

    aput-char v3, v1, v2

    .line 347
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x2b

    const/16 v3, 0x25b6

    aput-char v3, v1, v2

    .line 348
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x2d

    const/16 v3, 0x25b2

    aput-char v3, v1, v2

    .line 349
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x23

    aput-char v2, v1, v4

    .line 350
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x61

    const/16 v3, 0x2592

    aput-char v3, v1, v2

    .line 351
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x30

    const/16 v3, 0x2588

    aput-char v3, v1, v2

    .line 352
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x71

    const/16 v3, 0x2500

    aput-char v3, v1, v2

    .line 353
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x78

    const/16 v3, 0x2502

    aput-char v3, v1, v2

    .line 354
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x6d

    const/16 v3, 0x2514

    aput-char v3, v1, v2

    .line 355
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x6a

    const/16 v3, 0x2518

    aput-char v3, v1, v2

    .line 356
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x6c

    const/16 v3, 0x250c

    aput-char v3, v1, v2

    .line 357
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x6b

    const/16 v3, 0x2510

    aput-char v3, v1, v2

    .line 358
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x77

    const/16 v3, 0x252c

    aput-char v3, v1, v2

    .line 359
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x75

    const/16 v3, 0x2524

    aput-char v3, v1, v2

    .line 360
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x74

    const/16 v3, 0x251c

    aput-char v3, v1, v2

    .line 361
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x76

    const/16 v3, 0x2534

    aput-char v3, v1, v2

    .line 362
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x6e

    const/16 v3, 0x253c

    aput-char v3, v1, v2

    .line 363
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x6f

    const/16 v3, 0x23ba

    aput-char v3, v1, v2

    .line 364
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x70

    const/16 v3, 0x23bb

    aput-char v3, v1, v2

    .line 365
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x72

    const/16 v3, 0x23bc

    aput-char v3, v1, v2

    .line 366
    sget-object v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    const/16 v2, 0x73

    const/16 v3, 0x23bd

    aput-char v3, v1, v2

    .line 367
    return-void
.end method

.method public constructor <init>(Ljackpal/androidterm/emulatorview/TermSession;Ljackpal/androidterm/emulatorview/Screen;IILjackpal/androidterm/emulatorview/ColorScheme;)V
    .registers 9
    .parameter "session"
    .parameter "screen"
    .parameter "columns"
    .parameter "rows"
    .parameter "scheme"

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    const/16 v0, 0x10

    new-array v0, v0, [I

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgs:[I

    .line 86
    const/16 v0, 0x200

    new-array v0, v0, [B

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArg:[B

    .line 271
    iput v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mLastEmittedCharWidth:I

    .line 278
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mJustWrapped:Z

    .line 314
    new-array v0, v2, [I

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCharSet:[I

    .line 372
    iput v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScrollCounter:I

    .line 378
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDefaultUTF8Mode:Z

    .line 379
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8Mode:Z

    .line 380
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8EscapeUsed:Z

    .line 381
    iput v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ToFollow:I

    .line 402
    iput-object p1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSession:Ljackpal/androidterm/emulatorview/TermSession;

    .line 403
    iput-object p2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    .line 404
    iput p4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    .line 405
    iput p3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    .line 406
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    new-array v0, v0, [Z

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTabStop:[Z

    .line 408
    invoke-virtual {p0, p5}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setColorScheme(Ljackpal/androidterm/emulatorview/ColorScheme;)V

    .line 410
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ByteBuffer:Ljava/nio/ByteBuffer;

    .line 411
    invoke-static {v2}, Ljava/nio/CharBuffer;->allocate(I)Ljava/nio/CharBuffer;

    move-result-object v0

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mInputCharBuffer:Ljava/nio/CharBuffer;

    .line 412
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    move-result-object v0

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8Decoder:Ljava/nio/charset/CharsetDecoder;

    .line 413
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8Decoder:Ljava/nio/charset/CharsetDecoder;

    sget-object v1, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v0, v1}, Ljava/nio/charset/CharsetDecoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    .line 414
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8Decoder:Ljava/nio/charset/CharsetDecoder;

    sget-object v1, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v0, v1}, Ljava/nio/charset/CharsetDecoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    .line 416
    invoke-virtual {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->reset()V

    .line 417
    return-void
.end method

.method private autoWrapEnabled()Z
    .registers 2

    .prologue
    .line 1660
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private blockClear(III)V
    .registers 5
    .parameter "sx"
    .parameter "sy"
    .parameter "w"

    .prologue
    .line 1404
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(IIII)V

    .line 1405
    return-void
.end method

.method private blockClear(IIII)V
    .registers 12
    .parameter "sx"
    .parameter "sy"
    .parameter "w"
    .parameter "h"

    .prologue
    .line 1408
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    const/16 v5, 0x20

    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getStyle()I

    move-result v6

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-interface/range {v0 .. v6}, Ljackpal/androidterm/emulatorview/Screen;->blockSet(IIIIII)V

    .line 1409
    return-void
.end method

.method private changeTitle(ILjava/lang/String;)V
    .registers 4
    .parameter "parameter"
    .parameter "title"

    .prologue
    .line 1398
    if-eqz p1, :cond_5

    const/4 v0, 0x2

    if-ne p1, v0, :cond_a

    .line 1399
    :cond_5
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSession:Ljackpal/androidterm/emulatorview/TermSession;

    invoke-virtual {v0, p2}, Ljackpal/androidterm/emulatorview/TermSession;->setTitle(Ljava/lang/String;)V

    .line 1401
    :cond_a
    return-void
.end method

.method private checkColor(I)Z
    .registers 3
    .parameter "color"

    .prologue
    .line 1337
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->isValidColor(I)Z

    move-result v0

    .line 1338
    .local v0, result:Z
    if-nez v0, :cond_6

    .line 1344
    :cond_6
    return v0
.end method

.method private collectOSCArgs(B)V
    .registers 5
    .parameter "b"

    .prologue
    .line 1548
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArgLength:I

    const/16 v1, 0x200

    if-ge v0, v1, :cond_14

    .line 1549
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArg:[B

    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArgLength:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArgLength:I

    aput-byte p1, v0, v1

    .line 1550
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->continueSequence()V

    .line 1554
    :goto_13
    return-void

    .line 1552
    :cond_14
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->unknownSequence(B)V

    goto :goto_13
.end method

.method private computeEffectiveCharSet()V
    .registers 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 798
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCharSet:[I

    iget-boolean v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mAlternateCharSet:Z

    if-eqz v1, :cond_11

    move v1, v2

    :goto_9
    aget v0, v4, v1

    .line 799
    .local v0, charSet:I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_13

    :goto_e
    iput-boolean v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUseAlternateCharSet:Z

    .line 800
    return-void

    .end local v0           #charSet:I
    :cond_11
    move v1, v3

    .line 798
    goto :goto_9

    .restart local v0       #charSet:I
    :cond_13
    move v2, v3

    .line 799
    goto :goto_e
.end method

.method private continueSequence()V
    .registers 2

    .prologue
    .line 900
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mContinueSequence:Z

    .line 901
    return-void
.end method

.method private continueSequence(I)V
    .registers 3
    .parameter "state"

    .prologue
    .line 904
    iput p1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEscapeState:I

    .line 905
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mContinueSequence:Z

    .line 906
    return-void
.end method

.method private doEsc(B)V
    .registers 9
    .parameter "b"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 956
    sparse-switch p1, :sswitch_data_b0

    .line 1045
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->unknownSequence(B)V

    .line 1048
    :goto_8
    return-void

    .line 958
    :sswitch_9
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->continueSequence(I)V

    goto :goto_8

    .line 962
    :sswitch_e
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->continueSequence(I)V

    goto :goto_8

    .line 966
    :sswitch_13
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->continueSequence(I)V

    goto :goto_8

    .line 970
    :sswitch_18
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedCursorRow:I

    .line 971
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedCursorCol:I

    .line 972
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedEffect:I

    .line 973
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    and-int/lit16 v0, v0, 0xc0

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedDecFlags_DECSC_DECRC:I

    goto :goto_8

    .line 977
    :sswitch_2b
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedCursorRow:I

    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedCursorCol:I

    invoke-direct {p0, v0, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorRowCol(II)V

    .line 978
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedEffect:I

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    .line 979
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    and-int/lit16 v0, v0, -0xc1

    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedDecFlags_DECSC_DECRC:I

    or-int/2addr v0, v1

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    goto :goto_8

    .line 984
    :sswitch_40
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doLinefeed()V

    goto :goto_8

    .line 988
    :sswitch_44
    invoke-direct {p0, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorCol(I)V

    .line 989
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doLinefeed()V

    goto :goto_8

    .line 993
    :sswitch_4b
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBottomMargin:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v1, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorRowCol(II)V

    goto :goto_8

    .line 997
    :sswitch_53
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTabStop:[Z

    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    aput-boolean v2, v0, v1

    goto :goto_8

    .line 1001
    :sswitch_5a
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    iget v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTopMargin:I

    if-gt v0, v2, :cond_7d

    .line 1002
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    iget v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTopMargin:I

    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    iget v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBottomMargin:I

    iget v5, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTopMargin:I

    add-int/lit8 v5, v5, 0x1

    sub-int/2addr v4, v5

    iget v5, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTopMargin:I

    add-int/lit8 v6, v5, 0x1

    move v5, v1

    invoke-interface/range {v0 .. v6}, Ljackpal/androidterm/emulatorview/Screen;->blockCopy(IIIIII)V

    .line 1004
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTopMargin:I

    iget v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    invoke-direct {p0, v1, v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(III)V

    goto :goto_8

    .line 1006
    :cond_7d
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    goto :goto_8

    .line 1012
    :sswitch_84
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->unimplementedSequence(B)V

    goto :goto_8

    .line 1016
    :sswitch_88
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->unimplementedSequence(B)V

    goto/16 :goto_8

    .line 1020
    :sswitch_8d
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->unimplementedSequence(B)V

    goto/16 :goto_8

    .line 1024
    :sswitch_92
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->sendDeviceAttributes()V

    goto/16 :goto_8

    .line 1028
    :sswitch_97
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->continueSequence(I)V

    goto/16 :goto_8

    .line 1032
    :sswitch_9d
    iput-boolean v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mbKeypadApplicationMode:Z

    goto/16 :goto_8

    .line 1036
    :sswitch_a1
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->startCollectingOSCArgs()V

    .line 1037
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->continueSequence(I)V

    goto/16 :goto_8

    .line 1041
    :sswitch_ab
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mbKeypadApplicationMode:Z

    goto/16 :goto_8

    .line 956
    nop

    :sswitch_data_b0
    .sparse-switch
        0x23 -> :sswitch_9
        0x28 -> :sswitch_e
        0x29 -> :sswitch_13
        0x30 -> :sswitch_88
        0x37 -> :sswitch_18
        0x38 -> :sswitch_2b
        0x3d -> :sswitch_9d
        0x3e -> :sswitch_ab
        0x44 -> :sswitch_40
        0x45 -> :sswitch_44
        0x46 -> :sswitch_4b
        0x48 -> :sswitch_53
        0x4d -> :sswitch_5a
        0x4e -> :sswitch_84
        0x50 -> :sswitch_8d
        0x5a -> :sswitch_92
        0x5b -> :sswitch_97
        0x5d -> :sswitch_a1
    .end sparse-switch
.end method

.method private doEscLSBQuest(B)V
    .registers 6
    .parameter "b"

    .prologue
    const/4 v3, 0x0

    .line 836
    invoke-direct {p0, v3}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v1

    invoke-direct {p0, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getDecFlagsMask(I)I

    move-result v0

    .line 837
    .local v0, mask:I
    sparse-switch p1, :sswitch_data_4c

    .line 855
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->parseArg(B)V

    .line 860
    :goto_f
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_1d

    .line 863
    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    iget v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    invoke-direct {p0, v3, v3, v1, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(IIII)V

    .line 864
    invoke-direct {p0, v3, v3}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorRowCol(II)V

    .line 868
    :cond_1d
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_24

    .line 870
    invoke-direct {p0, v3, v3}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorPosition(II)V

    .line 872
    :cond_24
    return-void

    .line 839
    :sswitch_25
    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    or-int/2addr v1, v0

    iput v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    goto :goto_f

    .line 843
    :sswitch_2b
    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    xor-int/lit8 v2, v0, -0x1

    and-int/2addr v1, v2

    iput v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    goto :goto_f

    .line 847
    :sswitch_33
    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    xor-int/lit8 v2, v0, -0x1

    and-int/2addr v1, v2

    iget v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedDecFlags:I

    and-int/2addr v2, v0

    or-int/2addr v1, v2

    iput v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    goto :goto_f

    .line 851
    :sswitch_3f
    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedDecFlags:I

    xor-int/lit8 v2, v0, -0x1

    and-int/2addr v1, v2

    iget v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    and-int/2addr v2, v0

    or-int/2addr v1, v2

    iput v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedDecFlags:I

    goto :goto_f

    .line 837
    nop

    :sswitch_data_4c
    .sparse-switch
        0x68 -> :sswitch_25
        0x6c -> :sswitch_2b
        0x72 -> :sswitch_33
        0x73 -> :sswitch_3f
    .end sparse-switch
.end method

.method private doEscLeftSquareBracket(B)V
    .registers 24
    .parameter "b"

    .prologue
    .line 1052
    packed-switch p1, :pswitch_data_33a

    .line 1251
    :pswitch_3
    invoke-direct/range {p0 .. p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->parseArg(B)V

    .line 1254
    :cond_6
    :goto_6
    return-void

    .line 1055
    :pswitch_7
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    sub-int v14, v2, v3

    .line 1056
    .local v14, charsAfterCursor:I
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v2

    invoke-static {v2, v14}, Ljava/lang/Math;->min(II)I

    move-result v16

    .line 1057
    .local v16, charsToInsert:I
    sub-int v5, v14, v16

    .line 1058
    .local v5, charsToMove:I
    move-object/from16 v0, p0

    iget-object v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget v7, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    add-int v7, v7, v16

    move-object/from16 v0, p0

    iget v8, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    invoke-interface/range {v2 .. v8}, Ljackpal/androidterm/emulatorview/Screen;->blockCopy(IIIIII)V

    .line 1060
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v2, v3, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(III)V

    goto :goto_6

    .line 1065
    .end local v5           #charsToMove:I
    .end local v14           #charsAfterCursor:I
    .end local v16           #charsToInsert:I
    :pswitch_48
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTopMargin:I

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorRow(I)V

    goto :goto_6

    .line 1069
    :pswitch_62
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBottomMargin:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorRow(I)V

    goto :goto_6

    .line 1073
    :pswitch_7e
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorCol(I)V

    goto/16 :goto_6

    .line 1077
    :pswitch_9b
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorCol(I)V

    goto/16 :goto_6

    .line 1081
    :pswitch_b3
    const/4 v2, 0x1

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorCol(I)V

    goto/16 :goto_6

    .line 1085
    :pswitch_d0
    invoke-direct/range {p0 .. p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setHorizontalVerticalPosition()V

    goto/16 :goto_6

    .line 1090
    :pswitch_d5
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v2

    packed-switch v2, :pswitch_data_3a6

    .line 1107
    invoke-direct/range {p0 .. p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->unknownSequence(B)V

    goto/16 :goto_6

    .line 1092
    :pswitch_e4
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    move-object/from16 v0, p0

    iget v6, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    sub-int/2addr v4, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(III)V

    .line 1093
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    move-object/from16 v0, p0

    iget v6, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    move-object/from16 v0, p0

    iget v7, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    add-int/lit8 v7, v7, 0x1

    sub-int/2addr v6, v7

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v6}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(IIII)V

    goto/16 :goto_6

    .line 1098
    :pswitch_117
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    move-object/from16 v0, p0

    iget v6, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v6}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(IIII)V

    .line 1099
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(III)V

    goto/16 :goto_6

    .line 1103
    :pswitch_138
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    move-object/from16 v0, p0

    iget v6, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v6}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(IIII)V

    goto/16 :goto_6

    .line 1113
    :pswitch_149
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v2

    packed-switch v2, :pswitch_data_3b0

    .line 1127
    invoke-direct/range {p0 .. p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->unknownSequence(B)V

    goto/16 :goto_6

    .line 1115
    :pswitch_158
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    move-object/from16 v0, p0

    iget v6, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    sub-int/2addr v4, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(III)V

    goto/16 :goto_6

    .line 1119
    :pswitch_170
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(III)V

    goto/16 :goto_6

    .line 1123
    :pswitch_182
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(III)V

    goto/16 :goto_6

    .line 1134
    :pswitch_192
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBottomMargin:I

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    sub-int v18, v2, v3

    .line 1135
    .local v18, linesAfterCursor:I
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v2

    move/from16 v0, v18

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v20

    .line 1136
    .local v20, linesToInsert:I
    sub-int v10, v18, v20

    .line 1137
    .local v10, linesToMove:I
    move-object/from16 v0, p0

    iget-object v6, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    move-object/from16 v0, p0

    iget v9, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    add-int v12, v2, v20

    invoke-interface/range {v6 .. v12}, Ljackpal/androidterm/emulatorview/Screen;->blockCopy(IIIIII)V

    .line 1139
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v2, v3, v4, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(IIII)V

    goto/16 :goto_6

    .line 1145
    .end local v10           #linesToMove:I
    .end local v18           #linesAfterCursor:I
    .end local v20           #linesToInsert:I
    :pswitch_1d4
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBottomMargin:I

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    sub-int v18, v2, v3

    .line 1146
    .restart local v18       #linesAfterCursor:I
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v2

    move/from16 v0, v18

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v19

    .line 1147
    .local v19, linesToDelete:I
    sub-int v10, v18, v19

    .line 1148
    .restart local v10       #linesToMove:I
    move-object/from16 v0, p0

    iget-object v6, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    add-int v8, v2, v19

    move-object/from16 v0, p0

    iget v9, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v12, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    invoke-interface/range {v6 .. v12}, Ljackpal/androidterm/emulatorview/Screen;->blockCopy(IIIIII)V

    .line 1150
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    add-int/2addr v3, v10

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v2, v3, v4, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(IIII)V

    goto/16 :goto_6

    .line 1156
    .end local v10           #linesToMove:I
    .end local v18           #linesAfterCursor:I
    .end local v19           #linesToDelete:I
    :pswitch_217
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    sub-int v14, v2, v3

    .line 1157
    .restart local v14       #charsAfterCursor:I
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v2

    invoke-static {v2, v14}, Ljava/lang/Math;->min(II)I

    move-result v15

    .line 1158
    .local v15, charsToDelete:I
    sub-int v5, v14, v15

    .line 1159
    .restart local v5       #charsToMove:I
    move-object/from16 v0, p0

    iget-object v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    add-int/2addr v3, v15

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget v7, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    move-object/from16 v0, p0

    iget v8, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    invoke-interface/range {v2 .. v8}, Ljackpal/androidterm/emulatorview/Screen;->blockCopy(IIIIII)V

    .line 1161
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    add-int/2addr v2, v5

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v15}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(III)V

    goto/16 :goto_6

    .line 1166
    .end local v5           #charsToMove:I
    .end local v14           #charsAfterCursor:I
    .end local v15           #charsToDelete:I
    :pswitch_257
    invoke-direct/range {p0 .. p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->unimplementedSequence(B)V

    goto/16 :goto_6

    .line 1170
    :pswitch_25c
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(III)V

    goto/16 :goto_6

    .line 1174
    :pswitch_272
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->prevTabStop(I)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorCol(I)V

    goto/16 :goto_6

    .line 1178
    :pswitch_283
    const/4 v2, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->continueSequence(I)V

    goto/16 :goto_6

    .line 1182
    :pswitch_28b
    invoke-direct/range {p0 .. p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->sendDeviceAttributes()V

    goto/16 :goto_6

    .line 1186
    :pswitch_290
    const/4 v2, 0x1

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorRow(I)V

    goto/16 :goto_6

    .line 1190
    :pswitch_2ad
    invoke-direct/range {p0 .. p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setHorizontalVerticalPosition()V

    goto/16 :goto_6

    .line 1194
    :pswitch_2b2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v2

    packed-switch v2, :pswitch_data_3ba

    :pswitch_2bc
    goto/16 :goto_6

    .line 1196
    :pswitch_2be
    move-object/from16 v0, p0

    iget-object v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTabStop:[Z

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    const/4 v4, 0x0

    aput-boolean v4, v2, v3

    goto/16 :goto_6

    .line 1200
    :pswitch_2cb
    const/16 v17, 0x0

    .local v17, i:I
    :goto_2cd
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    move/from16 v0, v17

    if-ge v0, v2, :cond_6

    .line 1201
    move-object/from16 v0, p0

    iget-object v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTabStop:[Z

    const/4 v3, 0x0

    aput-boolean v3, v2, v17

    .line 1200
    add-int/lit8 v17, v17, 0x1

    goto :goto_2cd

    .line 1212
    .end local v17           #i:I
    :pswitch_2df
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doSetMode(Z)V

    goto/16 :goto_6

    .line 1216
    :pswitch_2e7
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doSetMode(Z)V

    goto/16 :goto_6

    .line 1221
    :pswitch_2ef
    invoke-direct/range {p0 .. p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->selectGraphicRendition()V

    goto/16 :goto_6

    .line 1240
    :pswitch_2f4
    const/4 v2, 0x0

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    add-int/lit8 v4, v4, -0x2

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v21

    .line 1241
    .local v21, top:I
    add-int/lit8 v2, v21, 0x2

    move-object/from16 v0, p0

    iget v3, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg1(I)I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 1242
    .local v13, bottom:I
    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTopMargin:I

    .line 1243
    move-object/from16 v0, p0

    iput v13, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBottomMargin:I

    .line 1246
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTopMargin:I

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorRowCol(II)V

    goto/16 :goto_6

    .line 1052
    :pswitch_data_33a
    .packed-switch 0x3f
        :pswitch_283
        :pswitch_7
        :pswitch_48
        :pswitch_62
        :pswitch_7e
        :pswitch_9b
        :pswitch_3
        :pswitch_3
        :pswitch_b3
        :pswitch_d0
        :pswitch_3
        :pswitch_d5
        :pswitch_149
        :pswitch_192
        :pswitch_1d4
        :pswitch_3
        :pswitch_3
        :pswitch_217
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_257
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_25c
        :pswitch_3
        :pswitch_272
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_28b
        :pswitch_290
        :pswitch_3
        :pswitch_2ad
        :pswitch_2b2
        :pswitch_2df
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2e7
        :pswitch_2ef
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2f4
    .end packed-switch

    .line 1090
    :pswitch_data_3a6
    .packed-switch 0x0
        :pswitch_e4
        :pswitch_117
        :pswitch_138
    .end packed-switch

    .line 1113
    :pswitch_data_3b0
    .packed-switch 0x0
        :pswitch_158
        :pswitch_170
        :pswitch_182
    .end packed-switch

    .line 1194
    :pswitch_data_3ba
    .packed-switch 0x0
        :pswitch_2be
        :pswitch_2bc
        :pswitch_2bc
        :pswitch_2cb
    .end packed-switch
.end method

.method private doEscPercent(B)V
    .registers 4
    .parameter "b"

    .prologue
    const/4 v1, 0x1

    .line 821
    sparse-switch p1, :sswitch_data_12

    .line 833
    :goto_4
    return-void

    .line 823
    :sswitch_5
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setUTF8Mode(Z)V

    .line 824
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8EscapeUsed:Z

    goto :goto_4

    .line 827
    :sswitch_c
    invoke-virtual {p0, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setUTF8Mode(Z)V

    .line 828
    iput-boolean v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8EscapeUsed:Z

    goto :goto_4

    .line 821
    :sswitch_data_12
    .sparse-switch
        0x40 -> :sswitch_5
        0x47 -> :sswitch_c
    .end sparse-switch
.end method

.method private doEscPound(B)V
    .registers 9
    .parameter "b"

    .prologue
    const/4 v1, 0x0

    .line 943
    packed-switch p1, :pswitch_data_1a

    .line 950
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->unknownSequence(B)V

    .line 953
    :goto_7
    return-void

    .line 945
    :pswitch_8
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    iget v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    const/16 v5, 0x45

    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getStyle()I

    move-result v6

    move v2, v1

    invoke-interface/range {v0 .. v6}, Ljackpal/androidterm/emulatorview/Screen;->blockSet(IIIIII)V

    goto :goto_7

    .line 943
    nop

    :pswitch_data_1a
    .packed-switch 0x38
        :pswitch_8
    .end packed-switch
.end method

.method private doEscRightSquareBracket(B)V
    .registers 3
    .parameter "b"

    .prologue
    .line 1352
    sparse-switch p1, :sswitch_data_12

    .line 1360
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->collectOSCArgs(B)V

    .line 1363
    :goto_6
    return-void

    .line 1354
    :sswitch_7
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doOSC()V

    goto :goto_6

    .line 1357
    :sswitch_b
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->continueSequence(I)V

    goto :goto_6

    .line 1352
    nop

    :sswitch_data_12
    .sparse-switch
        0x7 -> :sswitch_7
        0x1b -> :sswitch_b
    .end sparse-switch
.end method

.method private doEscRightSquareBracketEsc(B)V
    .registers 3
    .parameter "b"

    .prologue
    .line 1366
    packed-switch p1, :pswitch_data_16

    .line 1374
    const/16 v0, 0x1b

    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->collectOSCArgs(B)V

    .line 1375
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->collectOSCArgs(B)V

    .line 1376
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->continueSequence(I)V

    .line 1379
    :goto_10
    return-void

    .line 1368
    :pswitch_11
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doOSC()V

    goto :goto_10

    .line 1366
    nop

    :pswitch_data_16
    .packed-switch 0x5c
        :pswitch_11
    .end packed-switch
.end method

.method private doEscSelectLeftParen(B)V
    .registers 3
    .parameter "b"

    .prologue
    .line 909
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doSelectCharSet(IB)V

    .line 910
    return-void
.end method

.method private doEscSelectRightParen(B)V
    .registers 3
    .parameter "b"

    .prologue
    .line 913
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doSelectCharSet(IB)V

    .line 914
    return-void
.end method

.method private doLinefeed()V
    .registers 3

    .prologue
    .line 891
    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    add-int/lit8 v0, v1, 0x1

    .line 892
    .local v0, newCursorRow:I
    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBottomMargin:I

    if-lt v0, v1, :cond_f

    .line 893
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->scroll()V

    .line 894
    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBottomMargin:I

    add-int/lit8 v0, v1, -0x1

    .line 896
    :cond_f
    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorRow(I)V

    .line 897
    return-void
.end method

.method private doOSC()V
    .registers 3

    .prologue
    .line 1382
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->startTokenizingOSC()V

    .line 1383
    const/16 v1, 0x3b

    invoke-direct {p0, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->nextOSCInt(I)I

    move-result v0

    .line 1384
    .local v0, ps:I
    packed-switch v0, :pswitch_data_1c

    .line 1391
    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->unknownParameter(I)V

    .line 1394
    :goto_f
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->finishSequence()V

    .line 1395
    return-void

    .line 1388
    :pswitch_13
    const/4 v1, -0x1

    invoke-direct {p0, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->nextOSCString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->changeTitle(ILjava/lang/String;)V

    goto :goto_f

    .line 1384
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_13
        :pswitch_13
        :pswitch_13
    .end packed-switch
.end method

.method private doSelectCharSet(IB)V
    .registers 5
    .parameter "charSetIndex"
    .parameter "b"

    .prologue
    .line 918
    sparse-switch p2, :sswitch_data_18

    .line 935
    invoke-direct {p0, p2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->unknownSequence(B)V

    .line 940
    :goto_6
    return-void

    .line 920
    :sswitch_7
    const/4 v0, 0x0

    .line 938
    .local v0, charSet:I
    :goto_8
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCharSet:[I

    aput v0, v1, p1

    .line 939
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->computeEffectiveCharSet()V

    goto :goto_6

    .line 923
    .end local v0           #charSet:I
    :sswitch_10
    const/4 v0, 0x1

    .line 924
    .restart local v0       #charSet:I
    goto :goto_8

    .line 926
    .end local v0           #charSet:I
    :sswitch_12
    const/4 v0, 0x2

    .line 927
    .restart local v0       #charSet:I
    goto :goto_8

    .line 929
    .end local v0           #charSet:I
    :sswitch_14
    const/4 v0, 0x3

    .line 930
    .restart local v0       #charSet:I
    goto :goto_8

    .line 932
    .end local v0           #charSet:I
    :sswitch_16
    const/4 v0, 0x4

    .line 933
    .restart local v0       #charSet:I
    goto :goto_8

    .line 918
    :sswitch_data_18
    .sparse-switch
        0x30 -> :sswitch_12
        0x31 -> :sswitch_14
        0x32 -> :sswitch_16
        0x41 -> :sswitch_7
        0x42 -> :sswitch_10
    .end sparse-switch
.end method

.method private doSetMode(Z)V
    .registers 4
    .parameter "newValue"

    .prologue
    .line 1428
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v0

    .line 1429
    .local v0, modeBit:I
    sparse-switch v0, :sswitch_data_12

    .line 1439
    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->unknownParameter(I)V

    .line 1442
    :goto_b
    return-void

    .line 1431
    :sswitch_c
    iput-boolean p1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mInsertMode:Z

    goto :goto_b

    .line 1435
    :sswitch_f
    iput-boolean p1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mAutomaticNewlineMode:Z

    goto :goto_b

    .line 1429
    :sswitch_data_12
    .sparse-switch
        0x4 -> :sswitch_c
        0x14 -> :sswitch_f
    .end sparse-switch
.end method

.method private emit(B)V
    .registers 3
    .parameter "b"

    .prologue
    .line 1722
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUseAlternateCharSet:Z

    if-eqz v0, :cond_10

    const/16 v0, 0x80

    if-ge p1, v0, :cond_10

    .line 1723
    sget-object v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSpecialGraphicsCharMap:[C

    aget-char v0, v0, p1

    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->emit(I)V

    .line 1727
    :goto_f
    return-void

    .line 1725
    :cond_10
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->emit(I)V

    goto :goto_f
.end method

.method private emit(I)V
    .registers 3
    .parameter "c"

    .prologue
    .line 1718
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getStyle()I

    move-result v0

    invoke-direct {p0, p1, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->emit(II)V

    .line 1719
    return-void
.end method

.method private emit(II)V
    .registers 13
    .parameter "c"
    .parameter "style"

    .prologue
    const/4 v4, 0x1

    const/4 v9, 0x0

    .line 1671
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->autoWrapEnabled()Z

    move-result v7

    .line 1672
    .local v7, autoWrap:Z
    invoke-static {p1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->charWidth(I)I

    move-result v8

    .line 1674
    .local v8, width:I
    if-eqz v7, :cond_34

    .line 1675
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_34

    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mAboutToAutoWrap:Z

    if-nez v0, :cond_1b

    const/4 v0, 0x2

    if-ne v8, v0, :cond_34

    .line 1676
    :cond_1b
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    invoke-interface {v0, v1}, Ljackpal/androidterm/emulatorview/Screen;->setLineWrap(I)V

    .line 1677
    iput v9, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    .line 1678
    iput-boolean v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mJustWrapped:Z

    .line 1679
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBottomMargin:I

    if-ge v0, v1, :cond_84

    .line 1680
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    .line 1687
    :cond_34
    :goto_34
    iget-boolean v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mInsertMode:Z

    if-eqz v8, :cond_88

    move v0, v4

    :goto_39
    and-int/2addr v0, v1

    if-eqz v0, :cond_52

    .line 1688
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    add-int v5, v0, v8

    .line 1689
    .local v5, destCol:I
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    if-ge v5, v0, :cond_52

    .line 1690
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    iget v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    sub-int/2addr v3, v5

    iget v6, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    invoke-interface/range {v0 .. v6}, Ljackpal/androidterm/emulatorview/Screen;->blockCopy(IIIIII)V

    .line 1695
    .end local v5           #destCol:I
    :cond_52
    if-nez v8, :cond_97

    .line 1697
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mJustWrapped:Z

    if-eqz v0, :cond_8a

    .line 1698
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    iget v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mLastEmittedCharWidth:I

    sub-int/2addr v1, v2

    iget v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v1, v2, p1, p2}, Ljackpal/androidterm/emulatorview/Screen;->set(IIII)V

    .line 1707
    :goto_66
    if-eqz v7, :cond_72

    .line 1708
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_a3

    :goto_70
    iput-boolean v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mAboutToAutoWrap:Z

    .line 1711
    :cond_72
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    add-int/2addr v0, v8

    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    .line 1712
    if-lez v8, :cond_83

    .line 1713
    iput v8, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mLastEmittedCharWidth:I

    .line 1715
    :cond_83
    return-void

    .line 1682
    :cond_84
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->scroll()V

    goto :goto_34

    :cond_88
    move v0, v9

    .line 1687
    goto :goto_39

    .line 1700
    :cond_8a
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    iget v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mLastEmittedCharWidth:I

    sub-int/2addr v1, v2

    iget v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    invoke-interface {v0, v1, v2, p1, p2}, Ljackpal/androidterm/emulatorview/Screen;->set(IIII)V

    goto :goto_66

    .line 1703
    :cond_97
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    iget v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    invoke-interface {v0, v1, v2, p1, p2}, Ljackpal/androidterm/emulatorview/Screen;->set(IIII)V

    .line 1704
    iput-boolean v9, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mJustWrapped:Z

    goto :goto_66

    :cond_a3
    move v4, v9

    .line 1708
    goto :goto_70
.end method

.method private emit([C)V
    .registers 4
    .parameter "c"

    .prologue
    const/4 v1, 0x0

    .line 1735
    aget-char v0, p1, v1

    invoke-static {v0}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1736
    aget-char v0, p1, v1

    const/4 v1, 0x1

    aget-char v1, p1, v1

    invoke-static {v0, v1}, Ljava/lang/Character;->toCodePoint(CC)I

    move-result v0

    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->emit(I)V

    .line 1740
    :goto_15
    return-void

    .line 1738
    :cond_16
    aget-char v0, p1, v1

    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->emit(I)V

    goto :goto_15
.end method

.method private emit([CIII)V
    .registers 8
    .parameter "c"
    .parameter "offset"
    .parameter "length"
    .parameter "style"

    .prologue
    .line 1748
    move v0, p2

    .local v0, i:I
    :goto_1
    if-ge v0, p3, :cond_7

    .line 1749
    aget-char v1, p1, v0

    if-nez v1, :cond_8

    .line 1759
    :cond_7
    return-void

    .line 1752
    :cond_8
    aget-char v1, p1, v0

    invoke-static {v1}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 1753
    aget-char v1, p1, v0

    add-int/lit8 v2, v0, 0x1

    aget-char v2, p1, v2

    invoke-static {v1, v2}, Ljava/lang/Character;->toCodePoint(CC)I

    move-result v1

    invoke-direct {p0, v1, p4}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->emit(II)V

    .line 1754
    add-int/lit8 v0, v0, 0x1

    .line 1748
    :goto_1f
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1756
    :cond_22
    aget-char v1, p1, v0

    invoke-direct {p0, v1, p4}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->emit(II)V

    goto :goto_1f
.end method

.method private finishSequence()V
    .registers 2

    .prologue
    .line 1656
    const/4 v0, 0x0

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEscapeState:I

    .line 1657
    return-void
.end method

.method private getArg(IIZ)I
    .registers 6
    .parameter "index"
    .parameter "defaultValue"
    .parameter "treatZeroAsDefault"

    .prologue
    .line 1536
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgs:[I

    aget v0, v1, p1

    .line 1537
    .local v0, result:I
    if-ltz v0, :cond_a

    if-nez v0, :cond_b

    if-eqz p3, :cond_b

    .line 1538
    :cond_a
    move v0, p2

    .line 1540
    :cond_b
    return v0
.end method

.method private getArg0(I)I
    .registers 4
    .parameter "defaultValue"

    .prologue
    .line 1527
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg(IIZ)I

    move-result v0

    return v0
.end method

.method private getArg1(I)I
    .registers 3
    .parameter "defaultValue"

    .prologue
    const/4 v0, 0x1

    .line 1531
    invoke-direct {p0, v0, p1, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg(IIZ)I

    move-result v0

    return v0
.end method

.method private getBackColor()I
    .registers 2

    .prologue
    .line 1416
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBackColor:I

    return v0
.end method

.method private getDecFlagsMask(I)I
    .registers 4
    .parameter "argument"

    .prologue
    const/4 v1, 0x1

    .line 875
    if-lt p1, v1, :cond_a

    const/16 v0, 0x9

    if-gt p1, v0, :cond_a

    .line 876
    shl-int v0, v1, p1

    .line 879
    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private getEffect()I
    .registers 2

    .prologue
    .line 1420
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    return v0
.end method

.method private getForeColor()I
    .registers 2

    .prologue
    .line 1412
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mForeColor:I

    return v0
.end method

.method private getStyle()I
    .registers 4

    .prologue
    .line 1424
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getForeColor()I

    move-result v0

    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getBackColor()I

    move-result v1

    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getEffect()I

    move-result v2

    invoke-static {v0, v1, v2}, Ljackpal/androidterm/emulatorview/TextStyle;->encode(III)I

    move-result v0

    return v0
.end method

.method private handleUTF8Sequence(B)Z
    .registers 11
    .parameter "b"

    .prologue
    const v8, 0xfffd

    const/16 v7, 0x80

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 734
    iget v6, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ToFollow:I

    if-nez v6, :cond_10

    and-int/lit16 v6, p1, 0x80

    if-nez v6, :cond_10

    .line 789
    :goto_f
    return v4

    .line 739
    :cond_10
    iget v6, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ToFollow:I

    if-lez v6, :cond_63

    .line 740
    and-int/lit16 v6, p1, 0xc0

    if-eq v6, v7, :cond_24

    .line 743
    iput v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ToFollow:I

    .line 744
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 745
    invoke-direct {p0, v8}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->emit(I)V

    move v4, v5

    .line 746
    goto :goto_f

    .line 749
    :cond_24
    iget-object v6, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v6, p1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 750
    iget v6, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ToFollow:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ToFollow:I

    if-nez v6, :cond_5d

    .line 752
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ByteBuffer:Ljava/nio/ByteBuffer;

    .line 753
    .local v0, byteBuf:Ljava/nio/ByteBuffer;
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mInputCharBuffer:Ljava/nio/CharBuffer;

    .line 754
    .local v1, charBuf:Ljava/nio/CharBuffer;
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8Decoder:Ljava/nio/charset/CharsetDecoder;

    .line 756
    .local v3, decoder:Ljava/nio/charset/CharsetDecoder;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 757
    invoke-virtual {v3}, Ljava/nio/charset/CharsetDecoder;->reset()Ljava/nio/charset/CharsetDecoder;

    .line 758
    invoke-virtual {v3, v0, v1, v5}, Ljava/nio/charset/CharsetDecoder;->decode(Ljava/nio/ByteBuffer;Ljava/nio/CharBuffer;Z)Ljava/nio/charset/CoderResult;

    .line 759
    invoke-virtual {v3, v1}, Ljava/nio/charset/CharsetDecoder;->flush(Ljava/nio/CharBuffer;)Ljava/nio/charset/CoderResult;

    .line 761
    invoke-virtual {v1}, Ljava/nio/CharBuffer;->array()[C

    move-result-object v2

    .line 762
    .local v2, chars:[C
    aget-char v6, v2, v4

    if-lt v6, v7, :cond_5f

    aget-char v6, v2, v4

    const/16 v7, 0x9f

    if-gt v6, v7, :cond_5f

    .line 765
    aget-char v6, v2, v4

    int-to-byte v6, v6

    invoke-direct {p0, v6, v4}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->process(BZ)V

    .line 770
    :goto_57
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 771
    invoke-virtual {v1}, Ljava/nio/CharBuffer;->clear()Ljava/nio/Buffer;

    .end local v0           #byteBuf:Ljava/nio/ByteBuffer;
    .end local v1           #charBuf:Ljava/nio/CharBuffer;
    .end local v2           #chars:[C
    .end local v3           #decoder:Ljava/nio/charset/CharsetDecoder;
    :cond_5d
    :goto_5d
    move v4, v5

    .line 789
    goto :goto_f

    .line 767
    .restart local v0       #byteBuf:Ljava/nio/ByteBuffer;
    .restart local v1       #charBuf:Ljava/nio/CharBuffer;
    .restart local v2       #chars:[C
    .restart local v3       #decoder:Ljava/nio/charset/CharsetDecoder;
    :cond_5f
    invoke-direct {p0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->emit([C)V

    goto :goto_57

    .line 774
    .end local v0           #byteBuf:Ljava/nio/ByteBuffer;
    .end local v1           #charBuf:Ljava/nio/CharBuffer;
    .end local v2           #chars:[C
    .end local v3           #decoder:Ljava/nio/charset/CharsetDecoder;
    :cond_63
    and-int/lit16 v4, p1, 0xe0

    const/16 v6, 0xc0

    if-ne v4, v6, :cond_71

    .line 775
    iput v5, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ToFollow:I

    .line 786
    :goto_6b
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4, p1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_5d

    .line 776
    :cond_71
    and-int/lit16 v4, p1, 0xf0

    const/16 v6, 0xe0

    if-ne v4, v6, :cond_7b

    .line 777
    const/4 v4, 0x2

    iput v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ToFollow:I

    goto :goto_6b

    .line 778
    :cond_7b
    and-int/lit16 v4, p1, 0xf8

    const/16 v6, 0xf0

    if-ne v4, v6, :cond_85

    .line 779
    const/4 v4, 0x3

    iput v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ToFollow:I

    goto :goto_6b

    .line 782
    :cond_85
    invoke-direct {p0, v8}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->emit(I)V

    move v4, v5

    .line 783
    goto :goto_f
.end method

.method private isValidColor(I)Z
    .registers 3
    .parameter "color"

    .prologue
    .line 1348
    if-ltz p1, :cond_8

    const/16 v0, 0x103

    if-ge p1, v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private logError(Ljava/lang/String;)V
    .registers 2
    .parameter "error"

    .prologue
    .line 1652
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->finishSequence()V

    .line 1653
    return-void
.end method

.method private logError(Ljava/lang/String;B)V
    .registers 3
    .parameter "errorType"
    .parameter "b"

    .prologue
    .line 1646
    return-void
.end method

.method private nextOSCInt(I)I
    .registers 7
    .parameter "delimiter"

    .prologue
    .line 1581
    const/4 v1, -0x1

    .line 1582
    .local v1, value:I
    :goto_1
    iget v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArgTokenizerIndex:I

    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArgLength:I

    if-ge v2, v3, :cond_13

    .line 1583
    iget-object v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArg:[B

    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArgTokenizerIndex:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArgTokenizerIndex:I

    aget-byte v0, v2, v3

    .line 1584
    .local v0, b:B
    if-ne v0, p1, :cond_14

    .line 1595
    .end local v0           #b:B
    :cond_13
    return v1

    .line 1586
    .restart local v0       #b:B
    :cond_14
    const/16 v2, 0x30

    if-lt v0, v2, :cond_25

    const/16 v2, 0x39

    if-gt v0, v2, :cond_25

    .line 1587
    if-gez v1, :cond_1f

    .line 1588
    const/4 v1, 0x0

    .line 1590
    :cond_1f
    mul-int/lit8 v2, v1, 0xa

    add-int/2addr v2, v0

    add-int/lit8 v1, v2, -0x30

    goto :goto_1

    .line 1592
    :cond_25
    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->unknownSequence(B)V

    goto :goto_1
.end method

.method private nextOSCString(I)Ljava/lang/String;
    .registers 10
    .parameter "delimiter"

    .prologue
    .line 1561
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArgTokenizerIndex:I

    .line 1562
    .local v3, start:I
    move v2, v3

    .line 1563
    .local v2, end:I
    :goto_3
    iget v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArgTokenizerIndex:I

    iget v5, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArgLength:I

    if-ge v4, v5, :cond_15

    .line 1564
    iget-object v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArg:[B

    iget v5, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArgTokenizerIndex:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArgTokenizerIndex:I

    aget-byte v0, v4, v5

    .line 1565
    .local v0, b:B
    if-ne v0, p1, :cond_1a

    .line 1570
    .end local v0           #b:B
    :cond_15
    if-ne v3, v2, :cond_1d

    .line 1571
    const-string v4, ""

    .line 1576
    :goto_19
    return-object v4

    .line 1568
    .restart local v0       #b:B
    :cond_1a
    add-int/lit8 v2, v2, 0x1

    .line 1569
    goto :goto_3

    .line 1574
    .end local v0           #b:B
    :cond_1d
    :try_start_1d
    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArg:[B

    sub-int v6, v2, v3

    const-string v7, "UTF-8"

    invoke-direct {v4, v5, v3, v6, v7}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_28
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1d .. :try_end_28} :catch_29

    goto :goto_19

    .line 1575
    :catch_29
    move-exception v1

    .line 1576
    .local v1, e:Ljava/io/UnsupportedEncodingException;
    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArg:[B

    sub-int v6, v2, v3

    invoke-direct {v4, v5, v3, v6}, Ljava/lang/String;-><init>([BII)V

    goto :goto_19
.end method

.method private nextTabStop(I)I
    .registers 4
    .parameter "cursorCol"

    .prologue
    .line 803
    add-int/lit8 v0, p1, 0x1

    .local v0, i:I
    :goto_2
    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    if-ge v0, v1, :cond_10

    .line 804
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTabStop:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_d

    .line 808
    .end local v0           #i:I
    :goto_c
    return v0

    .line 803
    .restart local v0       #i:I
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 808
    :cond_10
    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    add-int/lit8 v0, v1, -0x1

    goto :goto_c
.end method

.method private parseArg(B)V
    .registers 7
    .parameter "b"

    .prologue
    .line 1503
    const/16 v3, 0x30

    if-lt p1, v3, :cond_29

    const/16 v3, 0x39

    if-gt p1, v3, :cond_29

    .line 1504
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgIndex:I

    iget-object v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgs:[I

    array-length v4, v4

    if-ge v3, v4, :cond_23

    .line 1505
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgs:[I

    iget v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgIndex:I

    aget v0, v3, v4

    .line 1506
    .local v0, oldValue:I
    add-int/lit8 v1, p1, -0x30

    .line 1508
    .local v1, thisDigit:I
    if-ltz v0, :cond_27

    .line 1509
    mul-int/lit8 v3, v0, 0xa

    add-int v2, v3, v1

    .line 1513
    .local v2, value:I
    :goto_1d
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgs:[I

    iget v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgIndex:I

    aput v2, v3, v4

    .line 1515
    .end local v0           #oldValue:I
    .end local v1           #thisDigit:I
    .end local v2           #value:I
    :cond_23
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->continueSequence()V

    .line 1524
    :goto_26
    return-void

    .line 1511
    .restart local v0       #oldValue:I
    .restart local v1       #thisDigit:I
    :cond_27
    move v2, v1

    .restart local v2       #value:I
    goto :goto_1d

    .line 1516
    .end local v0           #oldValue:I
    .end local v1           #thisDigit:I
    .end local v2           #value:I
    :cond_29
    const/16 v3, 0x3b

    if-ne p1, v3, :cond_3e

    .line 1517
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgIndex:I

    iget-object v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgs:[I

    array-length v4, v4

    if-ge v3, v4, :cond_3a

    .line 1518
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgIndex:I

    .line 1520
    :cond_3a
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->continueSequence()V

    goto :goto_26

    .line 1522
    :cond_3e
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->unknownSequence(B)V

    goto :goto_26
.end method

.method private prevTabStop(I)I
    .registers 4
    .parameter "cursorCol"

    .prologue
    .line 812
    add-int/lit8 v0, p1, -0x1

    .local v0, i:I
    :goto_2
    if-ltz v0, :cond_e

    .line 813
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTabStop:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_b

    .line 817
    .end local v0           #i:I
    :goto_a
    return v0

    .line 812
    .restart local v0       #i:I
    :cond_b
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 817
    :cond_e
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private process(B)V
    .registers 3
    .parameter "b"

    .prologue
    .line 601
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->process(BZ)V

    .line 602
    return-void
.end method

.method private process(BZ)V
    .registers 8
    .parameter "b"
    .parameter "doUTF8"

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 606
    if-eqz p2, :cond_11

    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8Mode:Z

    if-eqz v0, :cond_11

    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->handleUTF8Sequence(B)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 731
    :cond_10
    :goto_10
    :pswitch_10
    return-void

    .line 611
    :cond_11
    and-int/lit16 v0, p1, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_2b

    and-int/lit8 v0, p1, 0x7f

    const/16 v1, 0x1f

    if-gt v0, v1, :cond_2b

    .line 614
    const/16 v0, 0x1b

    invoke-direct {p0, v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->process(BZ)V

    .line 615
    and-int/lit8 v0, p1, 0x7f

    add-int/lit8 v0, v0, 0x40

    int-to-byte v0, v0

    invoke-direct {p0, v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->process(BZ)V

    goto :goto_10

    .line 619
    :cond_2b
    packed-switch p1, :pswitch_data_b2

    .line 678
    :pswitch_2e
    iput-boolean v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mContinueSequence:Z

    .line 679
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEscapeState:I

    packed-switch v0, :pswitch_data_ee

    .line 723
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->unknownSequence(B)V

    .line 726
    :cond_38
    :goto_38
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mContinueSequence:Z

    if-nez v0, :cond_10

    .line 727
    iput v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEscapeState:I

    goto :goto_10

    .line 627
    :pswitch_3f
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEscapeState:I

    if-ne v0, v4, :cond_10

    .line 628
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doEscRightSquareBracket(B)V

    goto :goto_10

    .line 633
    :pswitch_47
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorCol(I)V

    goto :goto_10

    .line 638
    :pswitch_53
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->nextTabStop(I)I

    move-result v0

    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorCol(I)V

    goto :goto_10

    .line 642
    :pswitch_5d
    invoke-direct {p0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorCol(I)V

    goto :goto_10

    .line 648
    :pswitch_61
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doLinefeed()V

    goto :goto_10

    .line 652
    :pswitch_65
    invoke-direct {p0, v3}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setAltCharSet(Z)V

    goto :goto_10

    .line 656
    :pswitch_69
    invoke-direct {p0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setAltCharSet(Z)V

    goto :goto_10

    .line 662
    :pswitch_6d
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEscapeState:I

    if-eqz v0, :cond_10

    .line 663
    iput v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEscapeState:I

    .line 664
    const/16 v0, 0x7f

    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->emit(B)V

    goto :goto_10

    .line 670
    :pswitch_79
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEscapeState:I

    if-eq v0, v4, :cond_81

    .line 671
    invoke-direct {p0, v3}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->startEscapeSequence(I)V

    goto :goto_10

    .line 673
    :cond_81
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doEscRightSquareBracket(B)V

    goto :goto_10

    .line 681
    :pswitch_85
    const/16 v0, 0x20

    if-lt p1, v0, :cond_38

    .line 682
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->emit(B)V

    goto :goto_38

    .line 687
    :pswitch_8d
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doEsc(B)V

    goto :goto_38

    .line 691
    :pswitch_91
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doEscPound(B)V

    goto :goto_38

    .line 695
    :pswitch_95
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doEscSelectLeftParen(B)V

    goto :goto_38

    .line 699
    :pswitch_99
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doEscSelectRightParen(B)V

    goto :goto_38

    .line 703
    :pswitch_9d
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doEscLeftSquareBracket(B)V

    goto :goto_38

    .line 707
    :pswitch_a1
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doEscLSBQuest(B)V

    goto :goto_38

    .line 711
    :pswitch_a5
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doEscPercent(B)V

    goto :goto_38

    .line 715
    :pswitch_a9
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doEscRightSquareBracket(B)V

    goto :goto_38

    .line 719
    :pswitch_ad
    invoke-direct {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doEscRightSquareBracketEsc(B)V

    goto :goto_38

    .line 619
    nop

    :pswitch_data_b2
    .packed-switch 0x0
        :pswitch_10
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
        :pswitch_3f
        :pswitch_47
        :pswitch_53
        :pswitch_61
        :pswitch_61
        :pswitch_61
        :pswitch_5d
        :pswitch_65
        :pswitch_69
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
        :pswitch_6d
        :pswitch_2e
        :pswitch_6d
        :pswitch_79
    .end packed-switch

    .line 679
    :pswitch_data_ee
    .packed-switch 0x0
        :pswitch_85
        :pswitch_8d
        :pswitch_91
        :pswitch_95
        :pswitch_99
        :pswitch_9d
        :pswitch_a1
        :pswitch_a5
        :pswitch_a9
        :pswitch_ad
    .end packed-switch
.end method

.method private scroll()V
    .registers 5

    .prologue
    .line 1493
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScrollCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScrollCounter:I

    .line 1494
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTopMargin:I

    iget v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBottomMargin:I

    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getStyle()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Ljackpal/androidterm/emulatorview/Screen;->scroll(III)V

    .line 1495
    return-void
.end method

.method private selectGraphicRendition()V
    .registers 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x5

    .line 1258
    const/4 v2, 0x0

    .local v2, i:I
    :goto_4
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgIndex:I

    if-gt v2, v3, :cond_153

    .line 1259
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgs:[I

    aget v0, v3, v2

    .line 1260
    .local v0, code:I
    if-gez v0, :cond_16

    .line 1261
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgIndex:I

    if-lez v3, :cond_15

    .line 1258
    :cond_12
    :goto_12
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1264
    :cond_15
    const/4 v0, 0x0

    .line 1270
    :cond_16
    if-nez v0, :cond_23

    .line 1271
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDefaultForeColor:I

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mForeColor:I

    .line 1272
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDefaultBackColor:I

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBackColor:I

    .line 1273
    iput v6, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    goto :goto_12

    .line 1274
    :cond_23
    if-ne v0, v7, :cond_2c

    .line 1275
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    goto :goto_12

    .line 1276
    :cond_2c
    const/4 v3, 0x3

    if-ne v0, v3, :cond_36

    .line 1277
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    goto :goto_12

    .line 1278
    :cond_36
    const/4 v3, 0x4

    if-ne v0, v3, :cond_40

    .line 1279
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    goto :goto_12

    .line 1280
    :cond_40
    if-ne v0, v5, :cond_49

    .line 1281
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    goto :goto_12

    .line 1282
    :cond_49
    const/4 v3, 0x7

    if-ne v0, v3, :cond_53

    .line 1283
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    goto :goto_12

    .line 1284
    :cond_53
    const/16 v3, 0x8

    if-ne v0, v3, :cond_5e

    .line 1285
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    goto :goto_12

    .line 1286
    :cond_5e
    const/16 v3, 0xa

    if-ne v0, v3, :cond_66

    .line 1287
    invoke-direct {p0, v6}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setAltCharSet(Z)V

    goto :goto_12

    .line 1288
    :cond_66
    const/16 v3, 0xb

    if-ne v0, v3, :cond_6e

    .line 1289
    invoke-direct {p0, v7}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setAltCharSet(Z)V

    goto :goto_12

    .line 1290
    :cond_6e
    const/16 v3, 0x16

    if-ne v0, v3, :cond_79

    .line 1292
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    and-int/lit8 v3, v3, -0x2

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    goto :goto_12

    .line 1293
    :cond_79
    const/16 v3, 0x17

    if-ne v0, v3, :cond_84

    .line 1294
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    goto :goto_12

    .line 1295
    :cond_84
    const/16 v3, 0x18

    if-ne v0, v3, :cond_8f

    .line 1296
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    and-int/lit8 v3, v3, -0x5

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    goto :goto_12

    .line 1297
    :cond_8f
    const/16 v3, 0x19

    if-ne v0, v3, :cond_9b

    .line 1298
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    and-int/lit8 v3, v3, -0x9

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    goto/16 :goto_12

    .line 1299
    :cond_9b
    const/16 v3, 0x1b

    if-ne v0, v3, :cond_a7

    .line 1300
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    and-int/lit8 v3, v3, -0x11

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    goto/16 :goto_12

    .line 1301
    :cond_a7
    const/16 v3, 0x1c

    if-ne v0, v3, :cond_b3

    .line 1302
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    and-int/lit8 v3, v3, -0x21

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEffect:I

    goto/16 :goto_12

    .line 1303
    :cond_b3
    const/16 v3, 0x1e

    if-lt v0, v3, :cond_c1

    const/16 v3, 0x25

    if-gt v0, v3, :cond_c1

    .line 1304
    add-int/lit8 v3, v0, -0x1e

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mForeColor:I

    goto/16 :goto_12

    .line 1305
    :cond_c1
    const/16 v3, 0x26

    if-ne v0, v3, :cond_e5

    add-int/lit8 v3, v2, 0x2

    iget v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgIndex:I

    if-gt v3, v4, :cond_e5

    iget-object v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgs:[I

    add-int/lit8 v4, v2, 0x1

    aget v3, v3, v4

    if-ne v3, v5, :cond_e5

    .line 1306
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgs:[I

    add-int/lit8 v4, v2, 0x2

    aget v1, v3, v4

    .line 1307
    .local v1, color:I
    invoke-direct {p0, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->checkColor(I)Z

    move-result v3

    if-eqz v3, :cond_e1

    .line 1308
    iput v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mForeColor:I

    .line 1310
    :cond_e1
    add-int/lit8 v2, v2, 0x2

    .line 1311
    goto/16 :goto_12

    .end local v1           #color:I
    :cond_e5
    const/16 v3, 0x27

    if-ne v0, v3, :cond_ef

    .line 1312
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDefaultForeColor:I

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mForeColor:I

    goto/16 :goto_12

    .line 1313
    :cond_ef
    const/16 v3, 0x28

    if-lt v0, v3, :cond_fd

    const/16 v3, 0x2f

    if-gt v0, v3, :cond_fd

    .line 1314
    add-int/lit8 v3, v0, -0x28

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBackColor:I

    goto/16 :goto_12

    .line 1315
    :cond_fd
    const/16 v3, 0x30

    if-ne v0, v3, :cond_129

    add-int/lit8 v3, v2, 0x2

    iget v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgIndex:I

    if-gt v3, v4, :cond_129

    iget-object v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgs:[I

    add-int/lit8 v4, v2, 0x1

    aget v3, v3, v4

    if-ne v3, v5, :cond_129

    .line 1316
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgs:[I

    add-int/lit8 v4, v2, 0x2

    aget v3, v3, v4

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBackColor:I

    .line 1317
    iget-object v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgs:[I

    add-int/lit8 v4, v2, 0x2

    aget v1, v3, v4

    .line 1318
    .restart local v1       #color:I
    invoke-direct {p0, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->checkColor(I)Z

    move-result v3

    if-eqz v3, :cond_125

    .line 1319
    iput v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBackColor:I

    .line 1321
    :cond_125
    add-int/lit8 v2, v2, 0x2

    .line 1322
    goto/16 :goto_12

    .end local v1           #color:I
    :cond_129
    const/16 v3, 0x31

    if-ne v0, v3, :cond_133

    .line 1323
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDefaultBackColor:I

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBackColor:I

    goto/16 :goto_12

    .line 1324
    :cond_133
    const/16 v3, 0x5a

    if-lt v0, v3, :cond_143

    const/16 v3, 0x61

    if-gt v0, v3, :cond_143

    .line 1325
    add-int/lit8 v3, v0, -0x5a

    add-int/lit8 v3, v3, 0x8

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mForeColor:I

    goto/16 :goto_12

    .line 1326
    :cond_143
    const/16 v3, 0x64

    if-lt v0, v3, :cond_12

    const/16 v3, 0x6b

    if-gt v0, v3, :cond_12

    .line 1327
    add-int/lit8 v3, v0, -0x64

    add-int/lit8 v3, v3, 0x8

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBackColor:I

    goto/16 :goto_12

    .line 1334
    .end local v0           #code:I
    :cond_153
    return-void
.end method

.method private sendDeviceAttributes()V
    .registers 5

    .prologue
    .line 1469
    const/4 v1, 0x7

    new-array v0, v1, [B

    fill-array-data v0, :array_e

    .line 1488
    .local v0, attributes:[B
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSession:Ljackpal/androidterm/emulatorview/TermSession;

    const/4 v2, 0x0

    array-length v3, v0

    invoke-virtual {v1, v0, v2, v3}, Ljackpal/androidterm/emulatorview/TermSession;->write([BII)V

    .line 1489
    return-void

    .line 1469
    :array_e
    .array-data 0x1
        0x1bt
        0x5bt
        0x3ft
        0x31t
        0x3bt
        0x32t
        0x63t
    .end array-data
.end method

.method private setAltCharSet(Z)V
    .registers 2
    .parameter "alternateCharSet"

    .prologue
    .line 793
    iput-boolean p1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mAlternateCharSet:Z

    .line 794
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->computeEffectiveCharSet()V

    .line 795
    return-void
.end method

.method private setCursorCol(I)V
    .registers 3
    .parameter "col"

    .prologue
    .line 1767
    iput p1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    .line 1768
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mAboutToAutoWrap:Z

    .line 1769
    return-void
.end method

.method private setCursorPosition(II)V
    .registers 9
    .parameter "x"
    .parameter "y"

    .prologue
    .line 1452
    const/4 v1, 0x0

    .line 1453
    .local v1, effectiveTopMargin:I
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    .line 1454
    .local v0, effectiveBottomMargin:I
    iget v4, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    and-int/lit8 v4, v4, 0x40

    if-eqz v4, :cond_d

    .line 1455
    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTopMargin:I

    .line 1456
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBottomMargin:I

    .line 1458
    :cond_d
    add-int v4, v1, p2

    add-int/lit8 v5, v0, -0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1461
    .local v3, newRow:I
    const/4 v4, 0x0

    iget v5, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    add-int/lit8 v5, v5, -0x1

    invoke-static {p1, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1462
    .local v2, newCol:I
    invoke-direct {p0, v3, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorRowCol(II)V

    .line 1463
    return-void
.end method

.method private setCursorRow(I)V
    .registers 3
    .parameter "row"

    .prologue
    .line 1762
    iput p1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    .line 1763
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mAboutToAutoWrap:Z

    .line 1764
    return-void
.end method

.method private setCursorRowCol(II)V
    .registers 4
    .parameter "row"
    .parameter "col"

    .prologue
    .line 1772
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    .line 1773
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    .line 1774
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mAboutToAutoWrap:Z

    .line 1775
    return-void
.end method

.method private setDefaultTabStops()V
    .registers 4

    .prologue
    .line 566
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    if-ge v0, v1, :cond_15

    .line 567
    iget-object v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTabStop:[Z

    and-int/lit8 v1, v0, 0x7

    if-nez v1, :cond_13

    if-eqz v0, :cond_13

    const/4 v1, 0x1

    :goto_e
    aput-boolean v1, v2, v0

    .line 566
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 567
    :cond_13
    const/4 v1, 0x0

    goto :goto_e

    .line 569
    :cond_15
    return-void
.end method

.method private setHorizontalVerticalPosition()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 1448
    invoke-direct {p0, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg1(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getArg0(I)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v0, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorPosition(II)V

    .line 1449
    return-void
.end method

.method private startCollectingOSCArgs()V
    .registers 2

    .prologue
    .line 1544
    const/4 v0, 0x0

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArgLength:I

    .line 1545
    return-void
.end method

.method private startEscapeSequence(I)V
    .registers 5
    .parameter "escapeState"

    .prologue
    .line 883
    iput p1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEscapeState:I

    .line 884
    const/4 v1, 0x0

    iput v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgIndex:I

    .line 885
    const/4 v0, 0x0

    .local v0, j:I
    :goto_6
    const/16 v1, 0x10

    if-ge v0, v1, :cond_12

    .line 886
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgs:[I

    const/4 v2, -0x1

    aput v2, v1, v0

    .line 885
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 888
    :cond_12
    return-void
.end method

.method private startTokenizingOSC()V
    .registers 2

    .prologue
    .line 1557
    const/4 v0, 0x0

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mOSCArgTokenizerIndex:I

    .line 1558
    return-void
.end method

.method private unimplementedSequence(B)V
    .registers 2
    .parameter "b"

    .prologue
    .line 1602
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->finishSequence()V

    .line 1603
    return-void
.end method

.method private unknownParameter(I)V
    .registers 2
    .parameter "parameter"

    .prologue
    .line 1619
    return-void
.end method

.method private unknownSequence(B)V
    .registers 2
    .parameter "b"

    .prologue
    .line 1609
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->finishSequence()V

    .line 1610
    return-void
.end method


# virtual methods
.method public append([BII)V
    .registers 10
    .parameter "buffer"
    .parameter "base"
    .parameter "length"

    .prologue
    .line 579
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    if-ge v2, p3, :cond_41

    .line 580
    add-int v3, p2, v2

    aget-byte v0, p1, v3

    .line 590
    .local v0, b:B
    :try_start_7
    invoke-direct {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->process(B)V

    .line 591
    iget v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mProcessedCharCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mProcessedCharCount:I
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_10} :catch_13

    .line 579
    :goto_10
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 592
    :catch_13
    move-exception v1

    .line 593
    .local v1, e:Ljava/lang/Exception;
    const-string v3, "EmulatorView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception while processing character "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mProcessedCharCount:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_10

    .line 598
    .end local v0           #b:B
    .end local v1           #e:Ljava/lang/Exception;
    :cond_41
    return-void
.end method

.method public clearScrollCounter()V
    .registers 2

    .prologue
    .line 1782
    const/4 v0, 0x0

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScrollCounter:I

    .line 1783
    return-void
.end method

.method public final getCursorCol()I
    .registers 2

    .prologue
    .line 554
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    return v0
.end method

.method public final getCursorRow()I
    .registers 2

    .prologue
    .line 545
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    return v0
.end method

.method public final getKeypadApplicationMode()Z
    .registers 2

    .prologue
    .line 562
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mbKeypadApplicationMode:Z

    return v0
.end method

.method public final getReverseVideo()Z
    .registers 2

    .prologue
    .line 558
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public getScrollCounter()I
    .registers 2

    .prologue
    .line 1778
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScrollCounter:I

    return v0
.end method

.method public getSelectedText(IIII)Ljava/lang/String;
    .registers 6
    .parameter "x1"
    .parameter "y1"
    .parameter "x2"
    .parameter "y2"

    .prologue
    .line 1859
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    invoke-interface {v0, p1, p2, p3, p4}, Ljackpal/androidterm/emulatorview/Screen;->getSelectedText(IIII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUTF8Mode()Z
    .registers 2

    .prologue
    .line 1846
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8Mode:Z

    return v0
.end method

.method public reset()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1789
    iput v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    .line 1790
    iput v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    .line 1791
    iput v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mArgIndex:I

    .line 1792
    iput-boolean v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mContinueSequence:Z

    .line 1793
    iput v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mEscapeState:I

    .line 1794
    iput v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedCursorRow:I

    .line 1795
    iput v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedCursorCol:I

    .line 1796
    iput v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedEffect:I

    .line 1797
    iput v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedDecFlags_DECSC_DECRC:I

    .line 1798
    iput v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    .line 1800
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDecFlags:I

    .line 1802
    iput v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mSavedDecFlags:I

    .line 1803
    iput-boolean v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mInsertMode:Z

    .line 1804
    iput-boolean v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mAutomaticNewlineMode:Z

    .line 1805
    iput v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTopMargin:I

    .line 1806
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBottomMargin:I

    .line 1807
    iput-boolean v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mAboutToAutoWrap:Z

    .line 1808
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDefaultForeColor:I

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mForeColor:I

    .line 1809
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDefaultBackColor:I

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBackColor:I

    .line 1810
    iput-boolean v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mbKeypadApplicationMode:Z

    .line 1811
    iput-boolean v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mAlternateCharSet:Z

    .line 1812
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCharSet:[I

    aput v3, v0, v2

    .line 1813
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCharSet:[I

    const/4 v1, 0x2

    aput v1, v0, v3

    .line 1814
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->computeEffectiveCharSet()V

    .line 1816
    invoke-direct {p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setDefaultTabStops()V

    .line 1817
    iget v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    iget v1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    invoke-direct {p0, v2, v2, v0, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->blockClear(IIII)V

    .line 1819
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDefaultUTF8Mode:Z

    invoke-virtual {p0, v0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setUTF8Mode(Z)V

    .line 1820
    iput-boolean v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8EscapeUsed:Z

    .line 1821
    iput v2, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ToFollow:I

    .line 1822
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1823
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mInputCharBuffer:Ljava/nio/CharBuffer;

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->clear()Ljava/nio/Buffer;

    .line 1824
    return-void
.end method

.method public setColorScheme(Ljackpal/androidterm/emulatorview/ColorScheme;)V
    .registers 3
    .parameter "scheme"

    .prologue
    .line 1854
    const/16 v0, 0x100

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDefaultForeColor:I

    .line 1855
    const/16 v0, 0x101

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDefaultBackColor:I

    .line 1856
    return-void
.end method

.method public setDefaultUTF8Mode(Z)V
    .registers 3
    .parameter "defaultToUTF8Mode"

    .prologue
    .line 1827
    iput-boolean p1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mDefaultUTF8Mode:Z

    .line 1828
    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8EscapeUsed:Z

    if-nez v0, :cond_9

    .line 1829
    invoke-virtual {p0, p1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setUTF8Mode(Z)V

    .line 1831
    :cond_9
    return-void
.end method

.method public setUTF8Mode(Z)V
    .registers 3
    .parameter "utf8Mode"

    .prologue
    .line 1834
    if-eqz p1, :cond_13

    iget-boolean v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8Mode:Z

    if-nez v0, :cond_13

    .line 1835
    const/4 v0, 0x0

    iput v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ToFollow:I

    .line 1836
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1837
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mInputCharBuffer:Ljava/nio/CharBuffer;

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->clear()Ljava/nio/Buffer;

    .line 1839
    :cond_13
    iput-boolean p1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8Mode:Z

    .line 1840
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ModeNotify:Ljackpal/androidterm/emulatorview/UpdateCallback;

    if-eqz v0, :cond_1e

    .line 1841
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ModeNotify:Ljackpal/androidterm/emulatorview/UpdateCallback;

    invoke-interface {v0}, Ljackpal/androidterm/emulatorview/UpdateCallback;->onUpdate()V

    .line 1843
    :cond_1e
    return-void
.end method

.method public setUTF8ModeUpdateCallback(Ljackpal/androidterm/emulatorview/UpdateCallback;)V
    .registers 2
    .parameter "utf8ModeNotify"

    .prologue
    .line 1850
    iput-object p1, p0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mUTF8ModeNotify:Ljackpal/androidterm/emulatorview/UpdateCallback;

    .line 1851
    return-void
.end method

.method public updateSize(II)V
    .registers 30
    .parameter "columns"
    .parameter "rows"

    .prologue
    .line 420
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    move/from16 v0, p2

    if-ne v2, v0, :cond_11

    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    move/from16 v0, p1

    if-ne v2, v0, :cond_11

    .line 537
    :cond_10
    :goto_10
    return-void

    .line 423
    :cond_11
    if-gtz p1, :cond_2e

    .line 424
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rows:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 427
    :cond_2e
    if-gtz p2, :cond_4b

    .line 428
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rows:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 432
    :cond_4b
    const/4 v2, 0x2

    new-array v13, v2, [I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    aput v4, v13, v2

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    aput v4, v13, v2

    .line 433
    .local v13, cursor:[I
    move-object/from16 v0, p0

    iget-object v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-interface {v2, v0, v1, v13}, Ljackpal/androidterm/emulatorview/Screen;->fastResize(II[I)Z

    move-result v16

    .line 435
    .local v16, fastResize:Z
    const/4 v3, 0x0

    .line 436
    .local v3, cursorColor:Ljackpal/androidterm/emulatorview/GrowableIntArray;
    const/4 v10, 0x0

    .line 437
    .local v10, charAtCursor:Ljava/lang/String;
    const/4 v12, 0x0

    .line 438
    .local v12, colors:Ljackpal/androidterm/emulatorview/GrowableIntArray;
    const/16 v26, 0x0

    .line 439
    .local v26, transcriptText:Ljava/lang/String;
    if-nez v16, :cond_bd

    .line 444
    new-instance v3, Ljackpal/androidterm/emulatorview/GrowableIntArray;

    .end local v3           #cursorColor:Ljackpal/androidterm/emulatorview/GrowableIntArray;
    const/4 v2, 0x1

    invoke-direct {v3, v2}, Ljackpal/androidterm/emulatorview/GrowableIntArray;-><init>(I)V

    .line 445
    .restart local v3       #cursorColor:Ljackpal/androidterm/emulatorview/GrowableIntArray;
    move-object/from16 v0, p0

    iget-object v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    move-object/from16 v0, p0

    iget v5, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    move-object/from16 v0, p0

    iget v6, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    move-object/from16 v0, p0

    iget v7, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    invoke-interface/range {v2 .. v7}, Ljackpal/androidterm/emulatorview/Screen;->getSelectedText(Ljackpal/androidterm/emulatorview/GrowableIntArray;IIII)Ljava/lang/String;

    move-result-object v10

    .line 446
    move-object/from16 v0, p0

    iget-object v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    move-object/from16 v0, p0

    iget v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    move-object/from16 v0, p0

    iget v5, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    const/16 v6, 0x1b

    const/4 v7, 0x0

    invoke-interface {v2, v4, v5, v6, v7}, Ljackpal/androidterm/emulatorview/Screen;->set(IIII)V

    .line 448
    new-instance v12, Ljackpal/androidterm/emulatorview/GrowableIntArray;

    .end local v12           #colors:Ljackpal/androidterm/emulatorview/GrowableIntArray;
    const/16 v2, 0x400

    invoke-direct {v12, v2}, Ljackpal/androidterm/emulatorview/GrowableIntArray;-><init>(I)V

    .line 449
    .restart local v12       #colors:Ljackpal/androidterm/emulatorview/GrowableIntArray;
    move-object/from16 v0, p0

    iget-object v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    invoke-interface {v2, v12}, Ljackpal/androidterm/emulatorview/Screen;->getTranscriptText(Ljackpal/androidterm/emulatorview/GrowableIntArray;)Ljava/lang/String;

    move-result-object v26

    .line 451
    move-object/from16 v0, p0

    iget-object v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    invoke-direct/range {p0 .. p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->getStyle()I

    move-result v4

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-interface {v2, v0, v1, v4}, Ljackpal/androidterm/emulatorview/Screen;->resize(III)V

    .line 454
    :cond_bd
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    move/from16 v0, p2

    if-eq v2, v0, :cond_d8

    .line 455
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    .line 456
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTopMargin:I

    .line 457
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mRows:I

    move-object/from16 v0, p0

    iput v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mBottomMargin:I

    .line 459
    :cond_d8
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    move/from16 v0, p1

    if-eq v2, v0, :cond_111

    .line 460
    move-object/from16 v0, p0

    iget v0, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    move/from16 v21, v0

    .line 461
    .local v21, oldColumns:I
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    .line 462
    move-object/from16 v0, p0

    iget-object v0, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTabStop:[Z

    move-object/from16 v22, v0

    .line 463
    .local v22, oldTabStop:[Z
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mColumns:I

    new-array v2, v2, [Z

    move-object/from16 v0, p0

    iput-object v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTabStop:[Z

    .line 464
    move/from16 v0, v21

    move/from16 v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v25

    .line 465
    .local v25, toTransfer:I
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mTabStop:[Z

    const/4 v5, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v25

    invoke-static {v0, v2, v4, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 468
    .end local v21           #oldColumns:I
    .end local v22           #oldTabStop:[Z
    .end local v25           #toTransfer:I
    :cond_111
    if-eqz v16, :cond_139

    .line 470
    const/4 v2, 0x0

    aget v2, v13, v2

    if-ltz v2, :cond_12d

    const/4 v2, 0x1

    aget v2, v13, v2

    if-ltz v2, :cond_12d

    .line 471
    const/4 v2, 0x0

    aget v2, v13, v2

    move-object/from16 v0, p0

    iput v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    .line 472
    const/4 v2, 0x1

    aget v2, v13, v2

    move-object/from16 v0, p0

    iput v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    goto/16 :goto_10

    .line 475
    :cond_12d
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    .line 476
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    goto/16 :goto_10

    .line 482
    :cond_139
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    .line 483
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    .line 484
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mAboutToAutoWrap:Z

    .line 486
    const/16 v19, -0x1

    .line 487
    .local v19, newCursorRow:I
    const/16 v18, -0x1

    .line 488
    .local v18, newCursorCol:I
    const/16 v20, -0x1

    .line 489
    .local v20, newCursorTranscriptPos:I
    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v15, v2, -0x1

    .line 490
    .local v15, end:I
    :goto_154
    if-ltz v15, :cond_163

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v4, 0xa

    if-ne v2, v4, :cond_163

    .line 491
    add-int/lit8 v15, v15, -0x1

    goto :goto_154

    .line 494
    :cond_163
    const/4 v11, 0x0

    .line 495
    .local v11, colorOffset:I
    const/16 v17, 0x0

    .local v17, i:I
    :goto_166
    move/from16 v0, v17

    if-gt v0, v15, :cond_1e2

    .line 496
    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v8

    .line 497
    .local v8, c:C
    sub-int v2, v17, v11

    invoke-virtual {v12, v2}, Ljackpal/androidterm/emulatorview/GrowableIntArray;->at(I)I

    move-result v24

    .line 498
    .local v24, style:I
    invoke-static {v8}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_198

    .line 499
    add-int/lit8 v17, v17, 0x1

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v9

    .line 500
    .local v9, cLow:C
    invoke-static {v8, v9}, Ljava/lang/Character;->toCodePoint(CC)I

    move-result v2

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v2, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->emit(II)V

    .line 501
    add-int/lit8 v11, v11, 0x1

    .line 495
    .end local v9           #cLow:C
    :cond_195
    :goto_195
    add-int/lit8 v17, v17, 0x1

    goto :goto_166

    .line 502
    :cond_198
    const/16 v2, 0xa

    if-ne v8, v2, :cond_1a6

    .line 503
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->setCursorCol(I)V

    .line 504
    invoke-direct/range {p0 .. p0}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->doLinefeed()V

    goto :goto_195

    .line 505
    :cond_1a6
    const/16 v2, 0x1b

    if-ne v8, v2, :cond_1da

    .line 508
    move-object/from16 v0, p0

    iget v0, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    move/from16 v19, v0

    .line 509
    move-object/from16 v0, p0

    iget v0, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    move/from16 v18, v0

    .line 510
    move-object/from16 v0, p0

    iget-object v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    invoke-interface {v2}, Ljackpal/androidterm/emulatorview/Screen;->getActiveRows()I

    move-result v20

    .line 511
    if-eqz v10, :cond_195

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_195

    .line 513
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljackpal/androidterm/emulatorview/GrowableIntArray;->at(I)I

    move-result v14

    .line 514
    .local v14, encodedCursorColor:I
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v5, v14}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->emit([CIII)V

    goto :goto_195

    .line 517
    .end local v14           #encodedCursorColor:I
    :cond_1da
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v8, v1}, Ljackpal/androidterm/emulatorview/TerminalEmulator;->emit(II)V

    goto :goto_195

    .line 522
    .end local v8           #c:C
    .end local v24           #style:I
    :cond_1e2
    const/4 v2, -0x1

    move/from16 v0, v19

    if-eq v0, v2, :cond_10

    const/4 v2, -0x1

    move/from16 v0, v18

    if-eq v0, v2, :cond_10

    .line 523
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    .line 524
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    .line 528
    move-object/from16 v0, p0

    iget-object v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mScreen:Ljackpal/androidterm/emulatorview/Screen;

    invoke-interface {v2}, Ljackpal/androidterm/emulatorview/Screen;->getActiveRows()I

    move-result v2

    sub-int v23, v2, v20

    .line 529
    .local v23, scrollCount:I
    if-lez v23, :cond_216

    move/from16 v0, v23

    move/from16 v1, v19

    if-gt v0, v1, :cond_216

    .line 530
    move-object/from16 v0, p0

    iget v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    sub-int v2, v2, v23

    move-object/from16 v0, p0

    iput v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    goto/16 :goto_10

    .line 531
    :cond_216
    move/from16 v0, v23

    move/from16 v1, v19

    if-le v0, v1, :cond_10

    .line 533
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorRow:I

    .line 534
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Ljackpal/androidterm/emulatorview/TerminalEmulator;->mCursorCol:I

    goto/16 :goto_10
.end method
