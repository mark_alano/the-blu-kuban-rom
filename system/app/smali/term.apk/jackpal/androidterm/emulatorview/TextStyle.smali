.class public final Ljackpal/androidterm/emulatorview/TextStyle;
.super Ljava/lang/Object;
.source "TextStyle.java"


# static fields
.field static final ciBackground:I = 0x101

.field static final ciColorLength:I = 0x103

.field static final ciCursor:I = 0x102

.field static final ciForeground:I = 0x100

.field static final fxBlink:I = 0x8

.field static final fxBold:I = 0x1

.field static final fxInverse:I = 0x10

.field static final fxInvisible:I = 0x20

.field static final fxItalic:I = 0x2

.field static final fxNormal:I = 0x0

.field static final fxUnderline:I = 0x4

.field static final kNormalTextStyle:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 21
    const/16 v0, 0x100

    const/16 v1, 0x101

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Ljackpal/androidterm/emulatorview/TextStyle;->encode(III)I

    move-result v0

    sput v0, Ljackpal/androidterm/emulatorview/TextStyle;->kNormalTextStyle:I

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static decodeBackColor(I)I
    .registers 2
    .parameter "encodedColor"

    .prologue
    .line 32
    and-int/lit16 v0, p0, 0x1ff

    return v0
.end method

.method static decodeEffect(I)I
    .registers 2
    .parameter "encodedColor"

    .prologue
    .line 36
    shr-int/lit8 v0, p0, 0x12

    and-int/lit8 v0, v0, 0x3f

    return v0
.end method

.method static decodeForeColor(I)I
    .registers 2
    .parameter "encodedColor"

    .prologue
    .line 28
    shr-int/lit8 v0, p0, 0x9

    and-int/lit16 v0, v0, 0x1ff

    return v0
.end method

.method static encode(III)I
    .registers 5
    .parameter "foreColor"
    .parameter "backColor"
    .parameter "effect"

    .prologue
    .line 24
    and-int/lit8 v0, p2, 0x3f

    shl-int/lit8 v0, v0, 0x12

    and-int/lit16 v1, p0, 0x1ff

    shl-int/lit8 v1, v1, 0x9

    or-int/2addr v0, v1

    and-int/lit16 v1, p1, 0x1ff

    or-int/2addr v0, v1

    return v0
.end method
