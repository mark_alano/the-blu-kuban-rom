.class Ljackpal/androidterm/emulatorview/TranscriptScreen;
.super Ljava/lang/Object;
.source "TranscriptScreen.java"

# interfaces
.implements Ljackpal/androidterm/emulatorview/Screen;


# instance fields
.field private mColumns:I

.field private mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

.field private mScreenRows:I

.field private mTotalRows:I


# direct methods
.method public constructor <init>(IIILjackpal/androidterm/emulatorview/ColorScheme;)V
    .registers 6
    .parameter "columns"
    .parameter "totalRows"
    .parameter "screenRows"
    .parameter "scheme"

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    sget v0, Ljackpal/androidterm/emulatorview/TextStyle;->kNormalTextStyle:I

    invoke-direct {p0, p1, p2, p3, v0}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->init(IIII)V

    .line 60
    return-void
.end method

.method private init(IIII)V
    .registers 12
    .parameter "columns"
    .parameter "totalRows"
    .parameter "screenRows"
    .parameter "style"

    .prologue
    const/4 v1, 0x0

    .line 63
    iput p1, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mColumns:I

    .line 64
    iput p2, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mTotalRows:I

    .line 65
    iput p3, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mScreenRows:I

    .line 67
    new-instance v0, Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    invoke-direct {v0, p1, p2, p3, p4}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;-><init>(IIII)V

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    .line 68
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    iget v3, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mColumns:I

    iget v4, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mScreenRows:I

    const/16 v5, 0x20

    move v2, v1

    move v6, p4

    invoke-virtual/range {v0 .. v6}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->blockSet(IIIIII)V

    .line 69
    return-void
.end method

.method private internalGetTranscriptText(Ljackpal/androidterm/emulatorview/GrowableIntArray;IIII)Ljava/lang/String;
    .registers 26
    .parameter "colors"
    .parameter "selX1"
    .parameter "selY1"
    .parameter "selX2"
    .parameter "selY2"

    .prologue
    .line 302
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 303
    .local v2, builder:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v6, v0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    .line 304
    .local v6, data:Ljackpal/androidterm/emulatorview/UnicodeTranscript;
    move-object/from16 v0, p0

    iget v5, v0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mColumns:I

    .line 306
    .local v5, columns:I
    const/4 v14, 0x0

    .line 307
    .local v14, rowColorBuffer:Ljackpal/androidterm/emulatorview/StyleRow;
    invoke-virtual {v6}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getActiveTranscriptRows()I

    move-result v18

    move/from16 v0, v18

    neg-int v0, v0

    move/from16 v18, v0

    move/from16 v0, p3

    move/from16 v1, v18

    if-ge v0, v1, :cond_26

    .line 308
    invoke-virtual {v6}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getActiveTranscriptRows()I

    move-result v18

    move/from16 v0, v18

    neg-int v0, v0

    move/from16 p3, v0

    .line 310
    :cond_26
    move-object/from16 v0, p0

    iget v0, v0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mScreenRows:I

    move/from16 v18, v0

    move/from16 v0, p5

    move/from16 v1, v18

    if-lt v0, v1, :cond_3a

    .line 311
    move-object/from16 v0, p0

    iget v0, v0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mScreenRows:I

    move/from16 v18, v0

    add-int/lit8 p5, v18, -0x1

    .line 313
    :cond_3a
    move/from16 v13, p3

    .local v13, row:I
    :goto_3c
    move/from16 v0, p5

    if-gt v13, v0, :cond_150

    .line 314
    const/16 v16, 0x0

    .line 316
    .local v16, x1:I
    move/from16 v0, p3

    if-ne v13, v0, :cond_48

    .line 317
    move/from16 v16, p2

    .line 319
    :cond_48
    move/from16 v0, p5

    if-ne v13, v0, :cond_93

    .line 320
    add-int/lit8 v17, p4, 0x1

    .line 321
    .local v17, x2:I
    move/from16 v0, v17

    if-le v0, v5, :cond_54

    .line 322
    move/from16 v17, v5

    .line 327
    :cond_54
    :goto_54
    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v6, v13, v0, v1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getLine(III)[C

    move-result-object v11

    .line 328
    .local v11, line:[C
    if-eqz p1, :cond_66

    .line 329
    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v6, v13, v0, v1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getLineColor(III)Ljackpal/androidterm/emulatorview/StyleRow;

    move-result-object v14

    .line 331
    :cond_66
    if-nez v11, :cond_96

    .line 332
    invoke-virtual {v6, v13}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getLineWrap(I)Z

    move-result v18

    if-nez v18, :cond_90

    move/from16 v0, p5

    if-ge v13, v0, :cond_90

    move-object/from16 v0, p0

    iget v0, v0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mScreenRows:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    if-ge v13, v0, :cond_90

    .line 333
    const/16 v18, 0xa

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 334
    if-eqz p1, :cond_90

    .line 335
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljackpal/androidterm/emulatorview/GrowableIntArray;->append(I)V

    .line 313
    :cond_90
    :goto_90
    add-int/lit8 v13, v13, 0x1

    goto :goto_3c

    .line 325
    .end local v11           #line:[C
    .end local v17           #x2:I
    :cond_93
    move/from16 v17, v5

    .restart local v17       #x2:I
    goto :goto_54

    .line 340
    .restart local v11       #line:[C
    :cond_96
    move-object/from16 v0, p0

    iget-object v0, v0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getDefaultStyle()I

    move-result v7

    .line 341
    .local v7, defaultColor:I
    const/4 v10, -0x1

    .line 342
    .local v10, lastPrintingChar:I
    array-length v12, v11

    .line 344
    .local v12, lineLen:I
    sub-int v15, v17, v16

    .line 345
    .local v15, width:I
    const/4 v4, 0x0

    .line 346
    .local v4, column:I
    const/4 v8, 0x0

    .local v8, i:I
    :goto_a6
    if-ge v8, v12, :cond_ae

    if-ge v4, v15, :cond_ae

    .line 347
    aget-char v3, v11, v8

    .line 348
    .local v3, c:C
    if-nez v3, :cond_f1

    .line 357
    .end local v3           #c:C
    :cond_ae
    invoke-virtual {v6, v13}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getLineWrap(I)Z

    move-result v18

    if-eqz v18, :cond_c0

    const/16 v18, -0x1

    move/from16 v0, v18

    if-le v10, v0, :cond_c0

    move/from16 v0, v17

    if-ne v0, v5, :cond_c0

    .line 359
    add-int/lit8 v10, v8, -0x1

    .line 361
    :cond_c0
    const/16 v18, 0x0

    add-int/lit8 v19, v10, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v2, v11, v0, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 362
    if-eqz p1, :cond_126

    .line 363
    if-eqz v14, :cond_111

    .line 364
    const/4 v4, 0x0

    .line 365
    const/4 v9, 0x0

    .local v9, j:I
    :goto_d1
    if-gt v9, v10, :cond_126

    .line 366
    invoke-virtual {v14, v4}, Ljackpal/androidterm/emulatorview/StyleRow;->get(I)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljackpal/androidterm/emulatorview/GrowableIntArray;->append(I)V

    .line 367
    invoke-static {v11, v9}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->charWidth([CI)I

    move-result v18

    add-int v4, v4, v18

    .line 368
    aget-char v18, v11, v9

    invoke-static/range {v18 .. v18}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v18

    if-eqz v18, :cond_ee

    .line 369
    add-int/lit8 v9, v9, 0x1

    .line 365
    :cond_ee
    add-int/lit8 v9, v9, 0x1

    goto :goto_d1

    .line 350
    .end local v9           #j:I
    .restart local v3       #c:C
    :cond_f1
    const/16 v18, 0x20

    move/from16 v0, v18

    if-ne v3, v0, :cond_101

    if-eqz v14, :cond_102

    invoke-virtual {v14, v4}, Ljackpal/androidterm/emulatorview/StyleRow;->get(I)I

    move-result v18

    move/from16 v0, v18

    if-eq v0, v7, :cond_102

    .line 351
    :cond_101
    move v10, v8

    .line 353
    :cond_102
    invoke-static {v3}, Ljava/lang/Character;->isLowSurrogate(C)Z

    move-result v18

    if-nez v18, :cond_10e

    .line 354
    invoke-static {v11, v8}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->charWidth([CI)I

    move-result v18

    add-int v4, v4, v18

    .line 346
    :cond_10e
    add-int/lit8 v8, v8, 0x1

    goto :goto_a6

    .line 373
    .end local v3           #c:C
    :cond_111
    const/4 v9, 0x0

    .restart local v9       #j:I
    :goto_112
    if-gt v9, v10, :cond_126

    .line 374
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljackpal/androidterm/emulatorview/GrowableIntArray;->append(I)V

    .line 375
    aget-char v3, v11, v9

    .line 376
    .restart local v3       #c:C
    invoke-static {v3}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v18

    if-eqz v18, :cond_123

    .line 377
    add-int/lit8 v9, v9, 0x1

    .line 373
    :cond_123
    add-int/lit8 v9, v9, 0x1

    goto :goto_112

    .line 382
    .end local v3           #c:C
    .end local v9           #j:I
    :cond_126
    invoke-virtual {v6, v13}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getLineWrap(I)Z

    move-result v18

    if-nez v18, :cond_90

    move/from16 v0, p5

    if-ge v13, v0, :cond_90

    move-object/from16 v0, p0

    iget v0, v0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mScreenRows:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    if-ge v13, v0, :cond_90

    .line 383
    const/16 v18, 0xa

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 384
    if-eqz p1, :cond_90

    .line 385
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljackpal/androidterm/emulatorview/GrowableIntArray;->append(I)V

    goto/16 :goto_90

    .line 389
    .end local v4           #column:I
    .end local v7           #defaultColor:I
    .end local v8           #i:I
    .end local v10           #lastPrintingChar:I
    .end local v11           #line:[C
    .end local v12           #lineLen:I
    .end local v15           #width:I
    .end local v16           #x1:I
    .end local v17           #x2:I
    :cond_150
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    return-object v18
.end method


# virtual methods
.method public blockCopy(IIIIII)V
    .registers 14
    .parameter "sx"
    .parameter "sy"
    .parameter "w"
    .parameter "h"
    .parameter "dx"
    .parameter "dy"

    .prologue
    .line 133
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->blockCopy(IIIIII)V

    .line 134
    return-void
.end method

.method public blockSet(IIIIII)V
    .registers 14
    .parameter "sx"
    .parameter "sy"
    .parameter "w"
    .parameter "h"
    .parameter "val"
    .parameter "style"

    .prologue
    .line 150
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->blockSet(IIIIII)V

    .line 151
    return-void
.end method

.method public final drawText(ILandroid/graphics/Canvas;FFLjackpal/androidterm/emulatorview/TextRenderer;IIILjava/lang/String;)V
    .registers 55
    .parameter "row"
    .parameter "canvas"
    .parameter "x"
    .parameter "y"
    .parameter "renderer"
    .parameter "cx"
    .parameter "selx1"
    .parameter "selx2"
    .parameter "imeText"

    .prologue
    .line 171
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getLine(I)[C

    move-result-object v19

    .line 172
    .local v19, line:[C
    move-object/from16 v0, p0

    iget-object v2, v0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getLineColor(I)Ljackpal/androidterm/emulatorview/StyleRow;
    :try_end_13
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_13} :catch_40
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_13} :catch_42

    move-result-object v35

    .line 181
    .local v35, color:Ljackpal/androidterm/emulatorview/StyleRow;
    move-object/from16 v0, p0

    iget-object v2, v0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    invoke-virtual {v2}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getDefaultStyle()I

    move-result v12

    .line 183
    .local v12, defaultStyle:I
    if-nez v19, :cond_67

    .line 185
    move/from16 v0, p7

    move/from16 v1, p8

    if-eq v0, v1, :cond_44

    .line 187
    sub-int v2, p8, p7

    new-array v8, v2, [C

    .line 188
    .local v8, blank:[C
    const/16 v2, 0x20

    invoke-static {v8, v2}, Ljava/util/Arrays;->fill([CC)V

    .line 189
    sub-int v7, p8, p7

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x1

    move-object/from16 v2, p5

    move-object/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p7

    invoke-interface/range {v2 .. v12}, Ljackpal/androidterm/emulatorview/TextRenderer;->drawTextRun(Landroid/graphics/Canvas;FFII[CIIZI)V

    .line 265
    .end local v8           #blank:[C
    .end local v12           #defaultStyle:I
    .end local v19           #line:[C
    .end local v35           #color:Ljackpal/androidterm/emulatorview/StyleRow;
    :cond_3f
    :goto_3f
    return-void

    .line 173
    :catch_40
    move-exception v39

    .line 175
    .local v39, e:Ljava/lang/IllegalArgumentException;
    goto :goto_3f

    .line 176
    .end local v39           #e:Ljava/lang/IllegalArgumentException;
    :catch_42
    move-exception v39

    .line 179
    .local v39, e:Ljava/lang/NullPointerException;
    goto :goto_3f

    .line 191
    .end local v39           #e:Ljava/lang/NullPointerException;
    .restart local v12       #defaultStyle:I
    .restart local v19       #line:[C
    .restart local v35       #color:Ljackpal/androidterm/emulatorview/StyleRow;
    :cond_44
    const/4 v2, -0x1

    move/from16 v0, p6

    if-eq v0, v2, :cond_3f

    .line 193
    const/16 v18, 0x1

    const-string v2, " "

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v19

    .end local v19           #line:[C
    const/16 v20, 0x0

    const/16 v21, 0x1

    const/16 v22, 0x1

    move-object/from16 v13, p5

    move-object/from16 v14, p2

    move/from16 v15, p3

    move/from16 v16, p4

    move/from16 v17, p6

    move/from16 v23, v12

    invoke-interface/range {v13 .. v23}, Ljackpal/androidterm/emulatorview/TextRenderer;->drawTextRun(Landroid/graphics/Canvas;FFII[CIIZI)V

    goto :goto_3f

    .line 200
    .restart local v19       #line:[C
    :cond_67
    move-object/from16 v0, p0

    iget v0, v0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mColumns:I

    move/from16 v37, v0

    .line 201
    .local v37, columns:I
    const/16 v23, 0x0

    .line 202
    .local v23, lastStyle:I
    const/16 v22, 0x0

    .line 203
    .local v22, lastCursorStyle:Z
    const/16 v18, 0x0

    .line 204
    .local v18, runWidth:I
    const/16 v17, -0x1

    .line 205
    .local v17, lastRunStart:I
    const/16 v20, -0x1

    .line 206
    .local v20, lastRunStartIndex:I
    const/16 v40, 0x0

    .line 207
    .local v40, forceFlushRun:Z
    const/16 v36, 0x0

    .line 208
    .local v36, column:I
    const/16 v42, 0x0

    .line 209
    .local v42, index:I
    :cond_7d
    :goto_7d
    move/from16 v0, v36

    move/from16 v1, v37

    if-ge v0, v1, :cond_f1

    .line 210
    invoke-virtual/range {v35 .. v36}, Ljackpal/androidterm/emulatorview/StyleRow;->get(I)I

    move-result v43

    .line 211
    .local v43, style:I
    const/16 v38, 0x0

    .line 212
    .local v38, cursorStyle:Z
    const/16 v41, 0x1

    .line 214
    .local v41, incr:I
    aget-char v2, v19, v42

    invoke-static {v2}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_ea

    .line 215
    move-object/from16 v0, v19

    move/from16 v1, v42

    invoke-static {v0, v1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->charWidth([CI)I

    move-result v44

    .line 216
    .local v44, width:I
    add-int/lit8 v41, v41, 0x1

    .line 220
    :goto_9d
    move/from16 v0, p6

    move/from16 v1, v36

    if-eq v0, v1, :cond_af

    move/from16 v0, v36

    move/from16 v1, p7

    if-lt v0, v1, :cond_b1

    move/from16 v0, v36

    move/from16 v1, p8

    if-gt v0, v1, :cond_b1

    .line 222
    :cond_af
    const/16 v38, 0x1

    .line 224
    :cond_b1
    move/from16 v0, v43

    move/from16 v1, v23

    if-ne v0, v1, :cond_c1

    move/from16 v0, v38

    move/from16 v1, v22

    if-ne v0, v1, :cond_c1

    if-lez v44, :cond_dc

    if-eqz v40, :cond_dc

    .line 227
    :cond_c1
    if-ltz v17, :cond_d0

    .line 228
    sub-int v21, v42, v20

    move-object/from16 v13, p5

    move-object/from16 v14, p2

    move/from16 v15, p3

    move/from16 v16, p4

    invoke-interface/range {v13 .. v23}, Ljackpal/androidterm/emulatorview/TextRenderer;->drawTextRun(Landroid/graphics/Canvas;FFII[CIIZI)V

    .line 233
    :cond_d0
    move/from16 v23, v43

    .line 234
    move/from16 v22, v38

    .line 235
    const/16 v18, 0x0

    .line 236
    move/from16 v17, v36

    .line 237
    move/from16 v20, v42

    .line 238
    const/16 v40, 0x0

    .line 240
    :cond_dc
    add-int v18, v18, v44

    .line 241
    add-int v36, v36, v44

    .line 242
    add-int v42, v42, v41

    .line 243
    const/4 v2, 0x1

    move/from16 v0, v44

    if-le v0, v2, :cond_7d

    .line 248
    const/16 v40, 0x1

    goto :goto_7d

    .line 218
    .end local v44           #width:I
    :cond_ea
    aget-char v2, v19, v42

    invoke-static {v2}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->charWidth(I)I

    move-result v44

    .restart local v44       #width:I
    goto :goto_9d

    .line 251
    .end local v38           #cursorStyle:Z
    .end local v41           #incr:I
    .end local v43           #style:I
    .end local v44           #width:I
    :cond_f1
    if-ltz v17, :cond_100

    .line 252
    sub-int v21, v42, v20

    move-object/from16 v13, p5

    move-object/from16 v14, p2

    move/from16 v15, p3

    move/from16 v16, p4

    invoke-interface/range {v13 .. v23}, Ljackpal/androidterm/emulatorview/TextRenderer;->drawTextRun(Landroid/graphics/Canvas;FFII[CIIZI)V

    .line 258
    :cond_100
    if-ltz p6, :cond_3f

    invoke-virtual/range {p9 .. p9}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3f

    .line 259
    invoke-virtual/range {p9 .. p9}, Ljava/lang/String;->length()I

    move-result v2

    move/from16 v0, v37

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v29

    .line 260
    .local v29, imeLength:I
    invoke-virtual/range {p9 .. p9}, Ljava/lang/String;->length()I

    move-result v2

    sub-int v31, v2, v29

    .line 261
    .local v31, imeOffset:I
    sub-int v2, v37, v29

    move/from16 v0, p6

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v28

    .line 262
    .local v28, imePosition:I
    invoke-virtual/range {p9 .. p9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v30

    const/16 v33, 0x1

    const/16 v2, 0xf

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Ljackpal/androidterm/emulatorview/TextStyle;->encode(III)I

    move-result v34

    move-object/from16 v24, p5

    move-object/from16 v25, p2

    move/from16 v26, p3

    move/from16 v27, p4

    move/from16 v32, v29

    invoke-interface/range {v24 .. v34}, Ljackpal/androidterm/emulatorview/TextRenderer;->drawTextRun(Landroid/graphics/Canvas;FFII[CIIZI)V

    goto/16 :goto_3f
.end method

.method public fastResize(II[I)Z
    .registers 6
    .parameter "columns"
    .parameter "rows"
    .parameter "cursor"

    .prologue
    const/4 v0, 0x1

    .line 393
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    if-nez v1, :cond_6

    .line 402
    :goto_5
    return v0

    .line 397
    :cond_6
    iget-object v1, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    invoke-virtual {v1, p1, p2, p3}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->resize(II[I)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 398
    iput p1, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mColumns:I

    .line 399
    iput p2, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mScreenRows:I

    goto :goto_5

    .line 402
    :cond_13
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public finish()V
    .registers 2

    .prologue
    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    .line 84
    return-void
.end method

.method public getActiveRows()I
    .registers 2

    .prologue
    .line 273
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getActiveRows()I

    move-result v0

    return v0
.end method

.method public getActiveTranscriptRows()I
    .registers 2

    .prologue
    .line 282
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getActiveTranscriptRows()I

    move-result v0

    return v0
.end method

.method public getSelectedText(IIII)Ljava/lang/String;
    .registers 11
    .parameter "selX1"
    .parameter "selY1"
    .parameter "selX2"
    .parameter "selY2"

    .prologue
    .line 294
    const/4 v1, 0x0

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->internalGetTranscriptText(Ljackpal/androidterm/emulatorview/GrowableIntArray;IIII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedText(Ljackpal/androidterm/emulatorview/GrowableIntArray;IIII)Ljava/lang/String;
    .registers 7
    .parameter "colors"
    .parameter "selX1"
    .parameter "selY1"
    .parameter "selX2"
    .parameter "selY2"

    .prologue
    .line 298
    invoke-direct/range {p0 .. p5}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->internalGetTranscriptText(Ljackpal/androidterm/emulatorview/GrowableIntArray;IIII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTranscriptText()Ljava/lang/String;
    .registers 7

    .prologue
    .line 286
    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getActiveTranscriptRows()I

    move-result v0

    neg-int v3, v0

    iget v4, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mColumns:I

    iget v5, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mScreenRows:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->internalGetTranscriptText(Ljackpal/androidterm/emulatorview/GrowableIntArray;IIII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTranscriptText(Ljackpal/androidterm/emulatorview/GrowableIntArray;)Ljava/lang/String;
    .registers 8
    .parameter "colors"

    .prologue
    .line 290
    const/4 v2, 0x0

    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    invoke-virtual {v0}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->getActiveTranscriptRows()I

    move-result v0

    neg-int v3, v0

    iget v4, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mColumns:I

    iget v5, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mScreenRows:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->internalGetTranscriptText(Ljackpal/androidterm/emulatorview/GrowableIntArray;IIII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public resize(III)V
    .registers 5
    .parameter "columns"
    .parameter "rows"
    .parameter "style"

    .prologue
    .line 407
    iget v0, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mTotalRows:I

    invoke-direct {p0, p1, v0, p2, p3}, Ljackpal/androidterm/emulatorview/TranscriptScreen;->init(IIII)V

    .line 408
    return-void
.end method

.method public scroll(III)V
    .registers 5
    .parameter "topMargin"
    .parameter "bottomMargin"
    .parameter "style"

    .prologue
    .line 116
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    invoke-virtual {v0, p1, p2, p3}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->scroll(III)V

    .line 117
    return-void
.end method

.method public set(IIBI)V
    .registers 6
    .parameter "x"
    .parameter "y"
    .parameter "b"
    .parameter "style"

    .prologue
    .line 104
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->setChar(IIII)Z

    .line 105
    return-void
.end method

.method public set(IIII)V
    .registers 6
    .parameter "x"
    .parameter "y"
    .parameter "codePoint"
    .parameter "style"

    .prologue
    .line 100
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->setChar(IIII)Z

    .line 101
    return-void
.end method

.method public setColorScheme(Ljackpal/androidterm/emulatorview/ColorScheme;)V
    .registers 4
    .parameter "scheme"

    .prologue
    .line 72
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    sget v1, Ljackpal/androidterm/emulatorview/TextStyle;->kNormalTextStyle:I

    invoke-virtual {v0, v1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->setDefaultStyle(I)V

    .line 73
    return-void
.end method

.method public setLineWrap(I)V
    .registers 3
    .parameter "row"

    .prologue
    .line 87
    iget-object v0, p0, Ljackpal/androidterm/emulatorview/TranscriptScreen;->mData:Ljackpal/androidterm/emulatorview/UnicodeTranscript;

    invoke-virtual {v0, p1}, Ljackpal/androidterm/emulatorview/UnicodeTranscript;->setLineWrap(I)V

    .line 88
    return-void
.end method
